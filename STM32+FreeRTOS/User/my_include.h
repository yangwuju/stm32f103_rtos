#ifndef __MY_INCLUDE_H
#define __MY_INCLUDE_H

#include "stm32f10x.h"

/*-------库函数-------*/
#include "math.h"
#include "stdio.h"
#include "string.h"
#include <stdint.h>
#include "stdlib.h"
#include "CONST.h"
#include "bsp.h"

//#include "main.h"
//#include "myfile.h"

#include "led.h"
//#include "key.h"
//#include "buzzer.h"

/*--------看门狗------*/
//#include "iwdg.h" 
//#include "wwdg.h"

/*--------延时--------*/
#include "delay.h" 
//#include "rcc.h"
//#include "SysTick.h"
//#include "core_delay.h"

/*---------显示-------*/
//#include "lcd_ili9341.h"
//#include "lcd_xpt2046.h"
//#include "lcd_palette.h"

//#include "oled.h"
//#include "menu.h"

/*-----按键及EXTI-----*/
#include "key.h"
//#include "exti.h"

/*--------定时器------*/
//#include "time0.h"
//#include "time1.h"
//#include "time2.h"
//#include "time3.h"

//#include "tpad.h"

/*-------串口收发------*/
//#include "usmart.h"
#include "usart1.h"
//#include "usart2.h"
//#include "cmd_queue1.h"
//#include "modbus_rtu1.h"
//#include "crc.h"
//#include "hc_08.h"

/*--------存储器------*/
//#include "stmflash.h"   //内部flash
//#include "spi_flash.h"    //外部flash
//#include "fsmc_sram.h"  //扩展SRAM
//#include "at24c02.h"    //读写EEPRAM
//#include "sdio_sdcard.h"  //SD
//#include "usb_test.h"     //模拟U盘

/*-------电源时钟-----*/
//#include "pwr.h"
//#include "rtc.h"
//#include "ds1302.h"

/*------AD,DA转换-----*/
//#include "adc.h"
//#include "dac.h"
//#include "dma.h"
//#include "ds18b20.h"
//#include "dht11.h"
//#include "bh1750.h" 

/*--------电机模块------*/
//#include "motor.h"

/*--------Wifi模块------*/
//#include "nrf24l01.h"

/*--------数据处理------*/
//#include "myfile.h"
//#include "pid.h"

/*--------mp3模块------*/
//#include "mp3.h"

/*-------通信协议------*/
//#include "spi.h" //模拟SPI
//#include "stm_spi.h" //使用STM32的SPI外设
//#include "i2c.h"
//#include "can.h"

#pragma hdrstop  //表后面的头文件不在编译

#endif
