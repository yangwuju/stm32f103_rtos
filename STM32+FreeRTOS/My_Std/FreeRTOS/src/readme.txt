Each real time kernel port consists of three files that contain the core kernel
components and are common to every port, and one or more files that are 
specific to a particular microcontroller and or compiler.

+ The FreeRTOS/Source directory contains the three files that are common to 
every port - list.c, queue.c and tasks.c.  The kernel is contained within these 
three files.  croutine.c implements the optional co-routine functionality - which
is normally only used on very memory limited systems.

+ The FreeRTOS/Source/Portable directory contains the files that are specific to 
a particular microcontroller and or compiler.

+ The FreeRTOS/Source/include directory contains the real time kernel header 
files.

See the readme file in the FreeRTOS/Source/Portable directory for more 
information.

译文：
每个实时内核端口由三个包含核心内核的文件组成
组件和对每个端口都是通用的，以及一个或多个文件
特定于特定的微控制器和或编译器。
+ FreeRTOS/Source目录包含三个常用的文件
每个端口- list.c, queue.c和tasks.c。内核就包含在其中
三个文件。c实现了可选的协例程功能
通常只在内存非常有限的系统上使用。
+ FreeRTOS/Source/Portable目录包含特定于
一个particula