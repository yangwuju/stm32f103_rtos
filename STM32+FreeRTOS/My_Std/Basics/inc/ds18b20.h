#ifndef __DS18B20_H
#define __DS18B20_H 

#include "my_include.h"

//GPIO端口定义
#define DS18B20_GPIO_CLK    RCC_APB2Periph_GPIOB
#define DS18B20_GPIO_PIN    GPIO_Pin_15
#define DS18B20_GPIO_PORT   GPIOB
										   
////IO操作函数											   
#define	DS18B20_DQ_Set  GPIO_SetBits(DS18B20_GPIO_PORT,DS18B20_GPIO_PIN) 
#define	DS18B20_DQ_Clr  GPIO_ResetBits(DS18B20_GPIO_PORT,DS18B20_GPIO_PIN) 
#define	DS18B20_DQ_IN   GPIO_ReadInputDataBit(DS18B20_GPIO_PORT,DS18B20_GPIO_PIN)
   	
   	
/*-----------------函数声明-------------------*/

//static void DS18B20_Rst(void);
//static u8 DS18B20_Check(void); 	
//static u8 DS18B20_Read_Bit(void);
//static u8 DS18B20_Read_Byte(void);	
//static void DS18B20_Write_Byte(u8 dat) ;
//static void DS18B20_Start(void);

u8 DS18B20_Init(void); //初始化并检测设备是否存在
float DS18B20_Temp(void);

#endif
