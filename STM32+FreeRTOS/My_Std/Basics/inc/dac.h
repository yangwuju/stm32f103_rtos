#ifndef __DAC_H
#define __DAC_H

#include "my_include.h"

/*------------------GPIO�˿ڶ���---------------------*/
#define  DAC1_GPIO_CLK     RCC_APB2Periph_GPIOA
#define  DAC1_GPIO_PIN     GPIO_Pin_4
#define  DAC1_GPIO_PORT    GPIOA

#define  DAC2_GPIO_CLK     RCC_APB2Periph_GPIOA
#define  DAC2_GPIO_PIN     GPIO_Pin_5
#define  DAC2_GPIO_PORT    GPIOA

#define  Channel_1     1
#define  Channel_2     2

void DAC_Config(void);
void DAC_Set_Vol(u8 ch , u16 vol);

#endif 
