#ifndef __BH1750_H
#define	__BH1750_H

#include "my_include.h"


/*---------------------------- BH1750的GPIO端口定义 --------------------------*/
#define BH1750_SCL_CLK 	    RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define BH1750_SCL_PORT    	GPIOA			              /* GPIO端口 */
#define BH1750_SCL_PIN		  GPIO_Pin_5		          /* 连接到SCL时钟线的GPIO */

#define BH1750_SDA_CLK 	    RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define BH1750_SDA_PORT    	GPIOA		                /* GPIO端口 */
#define BH1750_SDA_PIN		  GPIO_Pin_6 			        /* 连接到SDA时钟线的GPIO */

/*---------------------------- BH175的输入输出高低电平定义 --------------------------*/
#define I2C_WR	0		/* 写控制bit */
#define I2C_RD	1		/* 读控制bit */

#define I2C_SCL_H      GPIO_SetBits(BH1750_SCL_PORT, BH1750_SCL_PIN)
#define I2C_SCL_L      GPIO_ResetBits(BH1750_SCL_PORT, BH1750_SCL_PIN)

#define I2C_SDA_H      GPIO_SetBits(BH1750_SDA_PORT, BH1750_SDA_PIN)
#define I2C_SDA_L      GPIO_ResetBits(BH1750_SDA_PORT, BH1750_SDA_PIN)

#define I2C_SDA_READ   GPIO_ReadInputDataBit(BH1750_SDA_PORT, BH1750_SDA_PIN)

/*---------------------------- BH1750的地址指令 --------------------------*/


#define   BH1750_SlaveAddress   0x46   //定义器件在IIC总线中的从地址,根据ADDRESS地址引脚不同修改
                             //ADDRESS引脚接地时(低电平)地址为0x46，接电源（高电平）时地址为0xB8

#define   BS1750_OFF      0x00 // 断电指令
#define   BH1750_ON   		0x01 // 通电指令
#define   BH1750_RSET     0x07 // 重置指令
#define	  BH1750_CON_H  	0x10 // 连续高分辨率模式，1lx，120ms
#define	  BH1750_CON_H2  	0x11 // 连续高分辨率模式，0.5lx，120ms
#define	  BH1750_CON_L  	0x13 // 连续低分辨率模式，4lx，16ms
#define   BH1750_ONE_H  	0x20 // 一次高分辨率模式，1lx，120ms
#define   BH1750_ONE_H2		0x21 // 一次高分辨率模式，0.5lx，120ms
#define	  BH1750_ONE_L 		0x23 // 一次低分辨率模式，4lx，16ms
 
#define  RESOLUTION  BH1750_CON_H2 // 连续高分辨率模式，0.5lx

#if RESOLUTION == BH1750_CON_H || RESOLUTION == BH1750_ONE_H
    #define SCALE_INTERVAL 1
#elif RESOLUTION == BH1750_CON_H2 || RESOLUTION == BH1750_ONE_H2
    #define SCALE_INTERVAL 0.5f
#elif	RESOLUTION == BH1750_CON_L || RESOLUTION == BH1750_ONE_L
    #define SCALE_INTERVAL 4
#endif


extern volatile uint16_t illuminance;

/*---------------------------- 定义函数声明 -------------------------*/ 
void BH1750_Init(void);
uint8_t BH1750_Check(void);
float BH1750_GetDtae(void);

#endif /* __BH1750_H */
