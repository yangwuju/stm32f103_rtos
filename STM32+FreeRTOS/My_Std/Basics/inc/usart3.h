#ifndef __USART3_H
#define __USART3_H

#include "my_include.h"


/*----------------  �������� ---------------*/
void USART3_Config(u32 baud);
void USART3_SendByte(uint8_t data);
void USART3_SendHalfWord( uint16_t data);
void USART3_SendArray( uint8_t *array, uint8_t num);
void USART3_SendString( uint8_t *str);

#endif 
