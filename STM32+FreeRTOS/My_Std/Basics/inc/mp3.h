#ifndef __MP3_H
#define __MP3_H

#include "my_include.h"

//选择串口1发送MP3指令
#define   Uart_SendData        USART1_SendData 
//MP3指令
#define   MP3_NextSong         Uart_DFPlay(0x01,0x00) //下一曲
#define   MP3_PrevSong         Uart_DFPlay(0x02,0x00) //上一曲
#define   MP3_Pointplay(num)   Uart_DFPlay(0x03,num)  //指定播放第num首[1-2999]

#define   MP3_SoundUp          Uart_DFPlay(0x04,0x00) //加声音
#define   MP3_SoundDown        Uart_DFPlay(0x05,0x00) //减声音
#define   MP3_PointSound(num)  Uart_DFPlay(0x06,num)  //指定声音[1-30]
#define   MP3_UpSound(num)     Uart_DFPlay(0x10,num)  //扩音设置

#define   MP3_Play             Uart_DFPlay(0x0D,0x00) //播放
#define   MP3_Pause            Uart_DFPlay(0x0E,0x00) //暂停
#define   MP3_Stop             Uart_DFPlay(0x16,0x00) //停止

#define   MP3_PointEQ(num)     Uart_DFPlay(0x07,num)  //指定EQ[0-5]

#define   MP3_SingleCycle(num) Uart_DFPlay(0x08,num)  //单曲循环[0-2999]指定播放
#define   MP3_AllCycle(num)    Uart_DFPlay(0x11,num)  //全部循环播放
#define   MP3_PointSet(num)    Uart_DFPlay(0x09,num)  //指定播放设置[1-5]
#define   MP3_PointFolder(num) Uart_DFPlay(0x0F,num)  //指定文件夹播放[1-10]
#define   MP3_LoudSpeaker(num) Uart_DFPlay(0x10,0x00) //扩音设置

#define   MP3_SleepMode        Uart_DFPlay(0x0A,0x00) //睡眠模式
#define   MP3_Reset            Uart_DFPlay(0x0C,0x00) //模块复位

u16 sum16Value(u8 *dat,u8 len);
void MP3_SendCMD(u8 cmd,u16 para,u8 len);
void Uart_DFPlay(u8 cmd,u16 para);

#endif
