#ifndef __PWR_H
#define __PWR_H

#include "my_include.h"

typedef enum 
{
	STANDBY = 0,  //待机模式
	SLEEP,        //睡眠模式
	STOP          //停止模式	
} PWR_Mode;

void POWER_Mode(uint8_t mode);
FlagStatus POWER_Check(void);
void SWRST(void);

#endif 
