#ifndef __TIME2_H
#define __TIME2_H

#include "my_include.h"

/***********************通用定时器TIM2参数定义***********************/
/*--------------------------------------------------------------------            
| 复用功能映像 | 没有重映像 | 部分重映像1 | 部分重映像2 | 完全重映像 |
|--------------|------------|-------------|-------------|------------|
| TIM2_CH1_ETR |    PA0     |    PA15     |     PA0     |    PA15    |
|  TIM2_CH2    |    PA1     |    PB3      |     PA1     |    PB3     |
|--------------|--------------------------|--------------------------|
|  TIM2_CH3    |           PA2            |            PB10          |
|  TIM2_CH4    |           PA3            |            PB11          |
----------------------------------------------------------------- --*/
//注：TIM2_CH1 和 TIM2_ETR 共用一个引脚，但不能同时使用

// TIM2 输出比较通道
#define         TIM2_CHx_GPIO_CLK      RCC_APB2Periph_GPIOB|RCC_APB2Periph_AFIO
#define         TIM2_CHx_PORT          GPIOB
#define         TIM2_CHx_PIN           GPIO_Pin_3

//重映像
#define         TIM2_GPIO_PinRemap     GPIO_PartialRemap1_TIM2  //部分重映像1
//#define         TIM2_GPIO_PinRemap     GPIO_FullRemap_TIM2  //完全重映像


// TIM2 输出比较通道x的比较值，可调节占空比
//#define         TIM2_SetCCRx           TIM_SetCompare1          
#define         TIM2_SetCCRx           TIM_SetCompare2 
//#define         TIM2_SetCCRx           TIM_SetCompare3 
//#define         TIM2_SetCCRx           TIM_SetCompare4 

void TIM2_Init(u16 arr,u16 psc);
void TIM2_SetCompare(u16 ccr);

#endif
