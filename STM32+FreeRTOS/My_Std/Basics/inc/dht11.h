#ifndef __DHT11_H
#define	__DHT11_H


#include "my_include.h"

/************************** DHT11 数据类型定义********************************/
typedef struct
{
	uint8_t  humi_int;		//湿度的整数部分
	uint8_t  humi_deci;	 	//湿度的小数部分
	uint8_t  temp_int;	 	//温度的整数部分
	uint8_t  temp_deci;	 	//温度的小数部分
	uint8_t  check_sum;	 	//校验和
		                 
} DHT11_Data_TypeDef;


/*---------------------------- DHT11端口定义 --------------------------*/
#define DHT11_GPIO_CLK 	    RCC_APB2Periph_GPIOE		/* GPIO端口时钟 */
#define DHT11_GPIO_PORT    	GPIOE			              /* GPIO端口 */
#define DHT11_GPIO_PIN		  GPIO_Pin_6			        /* 连接到SCL时钟线的GPIO */


#define DHT11_GPIO_Clr()    GPIO_ResetBits(DHT11_GPIO_PORT,DHT11_GPIO_PIN)
#define DHT11_GPIO_Set()    GPIO_SetBits(DHT11_GPIO_PORT,DHT11_GPIO_PIN)

#define DHT11_GPIO_READ()   GPIO_ReadInputDataBit(DHT11_GPIO_PORT,DHT11_GPIO_PIN)



extern DHT11_Data_TypeDef DHT11_Data; //定义一个（温湿度）结构体变量

/*---------------------------- 定义函数声明 -------------------------*/ 
void DHT11_Init(void);
uint8_t DHT11_Read_Data(DHT11_Data_TypeDef *DHT11_Data);


#endif /* __DHT11_H */
