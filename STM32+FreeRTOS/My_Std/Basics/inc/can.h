#ifndef __CAN_H
#define __CAN_H

#include "my_include.h"

/*------------------------ CAN定义 ----------------------------*/
/*选择CAN1*/
#define CANx            CAN1

/*设置为回环工作模式*/
#define CANx_Mode       CAN_Mode_LoopBack  

/*设置为正常工作模式*/
//#define CANx_Mode       CAN_Mode_Normal  

/*可修改CAN的ID*/
#define CANx_ID         ((uint32_t)0x1314)
#define CAN_FilterId    ((CANx_ID<<3)|CAN_ID_EXT|CAN_RTR_DATA)

/*-----------------------  GPIO定义 ---------------------------*/
#define CAN_TX_CLK 	    RCC_APB2Periph_GPIOB		/* GPIO端口时钟 */
#define CAN_TX_PORT    	GPIOB 		              /* GPIO端口 */
#define CAN_TX_PIN		  GPIO_Pin_9			        /* 选择GPIO引脚 */

#define CAN_RX_CLK 	    RCC_APB2Periph_GPIOB		/* GPIO端口时钟 */
#define CAN_RX_PORT    	GPIOB 		              /* GPIO端口 */
#define CAN_RX_PIN		  GPIO_Pin_8			        /* 选择GPIO引脚 */

//变量声明
extern uint8_t CAN_Rece_Flag;
extern CanTxMsg  CAN_Tran_Data;   //发送数据的结构体
extern CanRxMsg  CAN_Rece_Data;   //接收数据的结构体

//函数声明
void CAN_Config(void);
void CAN_SendData( uint8_t *buf, uint8_t length);


#endif /* __CAN_H */
