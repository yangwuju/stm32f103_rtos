#ifndef __PVD_H
#define __PVD_H

#include "my_include.h"

  /* 配置PVD级别(PVD检测电压的阈值为2.6V，VDD电压低于2.6V时产生PVD中断) */
	/*具体级别根据自己的实际应用要求配置*/
#define  PWR_PVDLevel_2Vx   PWR_PVDLevel_2V6


void PVD_Config(void);

#endif /* __PVD_H */

