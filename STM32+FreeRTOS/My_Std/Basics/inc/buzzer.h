#ifndef __BSP_BEEP_H
#define	__BSP_BEEP_H


#include "my_include.h"


#define BEEP_GPIO_PORT    	GPIOC			              /* GPIO端口 */
#define BEEP_GPIO_CLK 	    RCC_APB2Periph_GPIOC		/* GPIO端口时钟 */
#define BEEP_GPIO_PIN		    GPIO_Pin_15			        /* 连接到SCL时钟线的GPIO */


#define BEEP(a)	if (a)	\
					GPIO_SetBits(BEEP_GPIO_PORT,BEEP_GPIO_PIN);\
					else		\
					GPIO_ResetBits(BEEP_GPIO_PORT,BEEP_GPIO_PIN)


/* 直接操作寄存器的方法控制IO */
#define	digitalHi(p,i)		    {p->BSRR=i;}	 //输出高电平		
#define digitalLo(p,i)		    {p->BRR=i;}	 //输出低电平
#define digitalToggle(p,i)    {p->ODR ^=i;} //输出反转状态					


/* 定义控制IO的宏 */
#define BEEP_ON		     digitalHi(BEEP_GPIO_PORT,BEEP_GPIO_PIN)
#define BEEP_OFF			 digitalLo(BEEP_GPIO_PORT,BEEP_GPIO_PIN)
#define BEEP_TOGGLE		 digitalToggle(BEEP_GPIO_PORT,BEEP_GPIO_PIN)								
					
void BEEP_GPIO_Config(void);
void BUZZER_on(short times,short onms,short offms);					

#endif /* __BSP_BEEP_H */
