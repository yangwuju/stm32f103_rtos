#include "modbus_rtu1.h"
#include "cmd_queue1.h"

#define CMD_MAX_BUFFER 128 

u8 SlvAddr  =  0x01;  //站号
u8 RS4851TCPbaudRate = 0x05;   //波特率 =1:9600  =2:4800  =3:2400 =4:1200   =5:115200
      /*地址码 读REG REG个数 REG地址  CRCL,CRCH */
static u8 busbuf[64];      //MOBBUS 发送数据缓冲区
volatile u16 reg16[0x16];      //16个寄存器的数值

char cmd_buffer[CMD_MAX_BUFFER];  //串口命令接收缓冲区   
/* 新型的RS485通讯电路不用方向控制IO端口*/
void GPIO_Configuration_485Send(void)
{
//	GPIO_InitTypeDef  GPIO_InitStructure;   //定义一个GPIO_InitTypeDef类型的结构体	
//	
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);   //允许GPIOB的外设时钟
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;				 //选择要控制的GPIOB引脚
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //设置引脚模式为通用推挽输出
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //设置引脚频率为50MHz
//	GPIO_Init(GPIOB, &GPIO_InitStructure);		//调用库函数，初始化GPIOB
//	
//	GPIO_ResetBits(GPIOB, GPIO_Pin_3);          //接收数据
}

/*
*********************************************************************************************************
** 函数名称 ：Send1String
** 函数功能 ：向RS485 串口1发送一个字符串
** 入口参数 ：aa 一个字符串的首地址,length，字符串的长度
** 出口参数 ：无
*********************************************************************************************************
*/
void RS485_sendData(char *aa,u16 length)
{
	
//	RS485DIR1 = RS485DIR_SEND;
//	delay_ms(4);   //必须小于任务切换时间
	while(length)
	{
		USART2_SendByte(*aa++);
		length --;
	}
   while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
   USART_ClearFlag(USART2,USART_FLAG_TC); // 清除USART1的中断
//	RS485DIR1 = RS485DIR_RECV;;//使能485接收
}

//更新寄存器的数值，为总线查询数据准备
void updateReg16Value(void)
{
////	char i;
////	reg16[0] = 0;
////	reg16[1] = (u16)inputStatus;
////	reg16[2] = (u16)jdqStatus;
////	reg16[3] = (u16)tcpStatus;
////	reg16[4] = (~jdqStatus&0x002f);   //实际的释放状态
////	for(i=5;i<0x12;i++)
////	{
////		reg16[i] = 0; 
////	}
////	reg16[0x12] = RS4851TCPbaudRate;
////	reg16[0x13] = RS4852baudRate;
////	reg16[0x14] = RS232baudRate;
////	reg16[0x15] = SlvAddr;
}
////从reg16[0x16]更新系统变量的数值
u8 updateSysStatusFromReg16Value(u16 reg16addr, u16 reg16num,__packed u8 *datptr)
{
//		//填写具体的寄存器内容
//	if((reg16addr <= 0x02)&&(reg16addr+reg16num-1)>= 0x02)  //吸合控制变化
//		{     
//			jdqOutOpenSet = reg16[2];
//			set_jdqOpenOutput(jdqOutOpenSet);
//			needWrite = 1;
//		}

//	if((reg16addr <= 0x03)&&(reg16addr+reg16num-1)>= 0x03)  //TCP网口电源控制变化
//		{     
//			tcpSet =       reg16[3]&0x00ff;
//			needWrite = 1;
//		}
//	
//	if((reg16addr <= 0x04)&&(reg16addr+reg16num-1)>= 0x04)   //释放控制变化
//	{     
//		jdqOutCloseSet = reg16[4];
//		set_jdqCloseOutput(jdqOutCloseSet);
//		needWrite = 1;
//	}
//	//更新波特率
//	if((reg16addr <= 0x12)&&(reg16addr+reg16num-1)>= 0x12)  //需要改写第一路485或TCP波特率
//	{	
//	 if((0xbb00 == (reg16[0x12]&0xff00))&&((reg16[0x12]&0x00ff)>0)&&((reg16[0x12]&0x00ff)<9))
//	  {	 
//			 RS4851TCPbaudRate =       reg16[0x12]&0x00ff;
//		   needWrite = 1;
//    }  //波特率写出错，不存储
//		else
//		needWrite = 0;
//	}
//	if((reg16addr <= 0x13)&&(reg16addr+reg16num-1)>= 0x13)  //需要改写第二路485波特率
//	{	
//	 if((0xbb00 == (reg16[0x13]&0xff00))&&((reg16[0x13]&0x00ff)>0)&&((reg16[0x13]&0x00ff)<9))
//	  {	 
//			 RS4852baudRate =       reg16[0x13]&0x00ff;
//		   needWrite = 1;
//    }  //波特率写出错，不存储
//		else
//		needWrite = 0;
//	}
//	if((reg16addr <= 0x14)&&(reg16addr+reg16num-1)>= 0x14)  //需要改写RS232口波特率
//	{	
//	 if((0xbb00 == (reg16[0x14]&0xff00))&&((reg16[0x14]&0x00ff)>0)&&((reg16[0x14]&0x00ff)<9))
//	  {	 
//			 RS232baudRate =       reg16[0x14]&0x00ff;
//		   needWrite = 1;
//    }  //波特率写出错，不存储
//		else
//		needWrite = 0;
//	}
//	if((reg16addr <= 0x15)&&(reg16addr+reg16num-1)>= 0x15)   //需要改写从机地址
//	{	
//	  if((0xdd00 == (reg16[0x15]&0xff00))&&((reg16[0x15]&0x00ff)>0)&&((reg16[0x15]&0x00ff)<100))
//    {  
//			SlvAddr	 =  reg16[0x15]&0x00ff;
//			needWrite = 1;
//		}
//		else   //波特率写出错，不存储
//		  needWrite = 0;
//	}
//	
//	if(needWrite)
//		return 1;
//	else
		return 0;
}

/*
函数原型：void rtuSendReg16Valu(u16 reg16addr, u16 reg16num)
函数功能：从指定的寄存器reg16addr开始，发送reg16num个寄存器
输入参数：reg16addr  开始读取的寄存器地址
          reg16num   需要读取的寄存器数量
*/
void rtuSendReg16Valu(u16 reg16addr, u16 reg16num)
{
	u16 crc;
	u8  i;
	busbuf[0] = SlvAddr;
	busbuf[1] = 0x03;
	busbuf[2] = reg16num*2;    //数据字节数
	//填写具体的寄存器内容
	updateReg16Value();
  for(i=0;i<reg16num;i++)
	{
		busbuf[3+i*2] = reg16[i+reg16addr]>>8;        //寄存器高字节
		busbuf[3+i*2+1] = reg16[i+reg16addr]&0x00ff;  //寄存器低字节
	}
	//计算CRC
	crc = crc16Value(busbuf,reg16num*2+3);
	busbuf[reg16num*2+4]=(u8)(crc>>8);
	busbuf[reg16num*2+3]=(u8)(crc&0x00ff);
	RS485_sendData((char *)busbuf,reg16num*2+5);  
}

/*
函数原型：void rtuSetReg16Valu(u16 reg16addr, u16 reg16num)
函数功能：从指定的寄存器reg16addr开始，设置reg16num个寄存器的数值
输入参数：reg16addr  开始读取的寄存器地址
          reg16num   需要读取的寄存器数量
*/
void rtuSetReg16Valu_10(u16 reg16addr, u16 reg16num,u8 *datptr)
{
	u16 crc,regaddr;
	u8  i,*datp;
	u8 slvTemp = SlvAddr;
	msg1 = (PCTRL_MSG)cmd_buffer;	
  //写入寄存器
	regaddr = reg16addr; datp = datptr;
  for(i=0;i<reg16num;i++)    
	{
	  reg16[regaddr++] = PTR2U16(datp);
		datp+=2;
	}
	if((msg1->cmd_head) == slvTemp)  //广播地址不能写内容
	  i = updateSysStatusFromReg16Value(reg16addr,reg16num,datptr);  //此处更新系统变量内容
	else
		i = 0;
	
  busbuf[0] = slvTemp;
	if(0 == i)     //有设置错误
	  busbuf[1] = 0x80;
	else
	 busbuf[1] = 0x10;
	busbuf[2] = reg16addr>>8;        //寄存器地址
	busbuf[3] = reg16addr&0x00ff;    //寄存器地址
	busbuf[4] = reg16num>>8;         //寄存器数量
	busbuf[5] = reg16num&0x00ff;     //寄存器数量
		//计算CRC
	crc = crc16Value(busbuf,6);
	busbuf[7]=(u8)(crc>>8);
	busbuf[6]=(u8)(crc&0x00ff);
	RS485_sendData((char *)busbuf,8); 
}
void rtuSetReg16Valu_06(u16 reg16addr,__packed u8 *datptr)
{
	u16 crc,i;
	u8 slvTemp = SlvAddr;
	msg1 = (PCTRL_MSG)cmd_buffer;	
  //写入寄存器
	crc = PTR2U16(datptr);
	reg16[reg16addr] = (s16)crc;
	 
	//此处填写具体的需要的需要更新的系统变量内容
	if(msg1->cmd_head == slvTemp)  //广播地址不能写内容
   i = updateSysStatusFromReg16Value(reg16addr,1,(void *)datptr);//此处更新系统变量内容
	else
		i = 0;
	
 	busbuf[0] = slvTemp;   //用电脑指定的从机地址回复
	if(0 == i) //有设置错误
	  busbuf[1] = 0x86;
	else
		busbuf[1] = 0x06;
	busbuf[2] = reg16addr>>8;        //寄存器地址
	busbuf[3] = reg16addr&0x00ff;    //寄存器地址
	busbuf[4] = *datptr;      //寄存器数值高
	busbuf[5] = *(datptr+1);          //寄存器数值低
		//计算CRC
	crc = crc16Value(busbuf,6);
	busbuf[7]=(u8)(crc>>8);
	busbuf[6]=(u8)(crc&0x00ff);
	RS485_sendData((char *)busbuf,8); 
}
void rtuSendErrorResponse(void)
{
	u8 buf[6];
	u16 crc;
	msg1 = (PCTRL_MSG)cmd_buffer;
	buf[0] = SlvAddr;
	buf[1] = (msg1->cmd_type)|0x80;
	buf[2] = 0x03;      //01,02,03    
	crc = crc16Value(buf,3);
	buf[4]=(u8)(crc>>8);
	buf[3]=(u8)(crc&0x00ff);
	RS485_sendData((char *)buf,5);	
}
/* 对收到的485数据进行解析，返回对应的数值*/
void rtu_cmd_pro(void)
{
	  uint16_t    reg16_len,reg16_addr;
	  msg1 = (PCTRL_MSG)cmd_buffer;
	  reg16_len = PTR2U16(&msg1->reg16_len);	
	  reg16_addr = PTR2U16(&msg1->reg16_addr);	
		if(0x03 == msg1->cmd_type)    //读寄存器数值
		{
			rtuSendReg16Valu(reg16_addr,reg16_len);		
		}
		else if(0x06 == msg1->cmd_type)  //设置单个寄存器数值
		{
				rtuSetReg16Valu_06(reg16_addr,(void *)&msg1->reg16_len);	
		}
		else if(0x10 == msg1->cmd_type)  //设置寄存器数值
		{
			rtuSetReg16Valu_10(reg16_addr,reg16_len,&msg1->param[1]);			
		}
}
 //校验（数据解析）
u16 crc16Value(u8 *dat,u8 len)   //	u8 *dat为要进行CRC校验的信息  u8 len 为消息中的字节数
{
	int i;
  u16 crc=0xffff;
  while(len--)
  {
     crc^=*dat++;
	   for(i=0;i<8;i++)
	   {
	     if(crc&0x0001) crc=(crc>>1)^0xa001;
			 else crc=crc>>1;
	   }
   }
   return crc;
} 
//PC发送：01 03 00 01 00 06 94 08
//发数据
u8 Read_Modbus_UART2(void)
{
	u8 i=0;
	i = queue1_find_cmd(cmd_buffer,CMD_MAX_BUFFER); //从缓冲区中获取一条指令
	if(i<=0)//没有接收到指令
	    return 0;	
	{
		rtu_cmd_pro();		
		return 1;
	}
	
}
