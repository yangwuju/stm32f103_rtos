#include "dma.h"

/*-----------------------------------------------------------------
说明：DMA（直接访问寄存器）不占用CPU，有DMA1和DMA2,分别有多个通道
有：  外设Flash到内部SRAM    （ Flash <---->SRAM ） 
      外设到存储器 （ 外设数据寄存器DR<---->SRAM ）

-----------------------------------------------------------------*/

#define BUFFER_SIZE 32

/* 定义aSRC_Const_Buffer数组作为DMA传输数据源
 * const关键字将aSRC_Const_Buffer数组变量定义为常量类型
 * 表示数据存储在内部的FLASH中
 */
const u16 aSRC_Const_Buffer[BUFFER_SIZE];


/* 定义DMA传输目标存储器
 * 存储在内部的SRAM中																		
 */
uint32_t aDST_Buffer[BUFFER_SIZE];

/**
  * @brief  初始化DMA   Flash ---->SRAM
  * @param  无  
  * @retval 无
 **/																		
void DMA_Config(void)
{
	DMA_InitTypeDef DMA_InitStruct;
	
	// 开启DMA时钟
	RCC_AHBPeriphClockCmd( DMA_CLOCK, ENABLE);
	
	// 源数据地址
	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)aSRC_Const_Buffer; //flash的变量地址
	// 目标地址
	DMA_InitStruct.DMA_MemoryBaseAddr = (uint32_t)aDST_Buffer; //SRAM的变量地址
	// 方向：外设到存储器（这里的外设是内部的FLASH）
	DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralSRC;
	// 传输大小
	DMA_InitStruct.DMA_BufferSize = BUFFER_SIZE;
	// 外设（内部的FLASH）地址递增
	DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Enable;
	// 外设数据单位
	DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
	// 内存地址递增
	DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;
	// 内存数据单位
	DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
	// DMA模式，一次或者循环模式
	DMA_InitStruct.DMA_Mode = DMA_Mode_Normal;
	//DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;  
  // 优先级：高
	DMA_InitStruct.DMA_Priority = DMA_Priority_High;
	// 使能内存到内存的传输
  DMA_InitStruct.DMA_M2M = DMA_M2M_Enable;
	// 配置DMA通道	
	DMA_Init(DMA_CHANNEL, &DMA_InitStruct);
  //清除DMA数据流传输完成标志位
	DMA_ClearFlag(DMA_FLAG_TC);
	// 使能DMA
	DMA_Cmd(DMA_CHANNEL, ENABLE);	
}




/*--------------------- END OF FIAL-------------------*/
