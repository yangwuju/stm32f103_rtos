#include "led.h"

/**
  * @brief  初始化控制LED的IO
  * @param  无
  * @retval 无
**/
void LED_Init(void)
{
		/*定义一个GPIO_InitTypeDef类型的结构体*/
		GPIO_InitTypeDef GPIO_InitStructure;

		/*开启LED相关的GPIO外设时钟*/
		RCC_APB2PeriphClockCmd( LED1_GPIO_CLK | LED2_GPIO_CLK | LED3_GPIO_CLK, ENABLE);
		
		/*设置引脚模式为通用推挽输出*/
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;   
		/*设置引脚速率为50MHz */   
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 	
	
			/*选择要控制的GPIO引脚*/
		GPIO_InitStructure.GPIO_Pin = LED1_GPIO_PIN;
		/*调用库函数，初始化GPIO*/
		GPIO_Init(LED1_GPIO_PORT, &GPIO_InitStructure);	
			/* 关闭led1灯	*/
		GPIO_SetBits(LED1_GPIO_PORT, LED1_GPIO_PIN);	
		
		/*选择要控制的GPIO引脚*/
		GPIO_InitStructure.GPIO_Pin = LED2_GPIO_PIN;
		/*调用库函数，初始化GPIO*/
		GPIO_Init(LED2_GPIO_PORT, &GPIO_InitStructure);
		/* 关闭led2灯	*/
		GPIO_SetBits(LED2_GPIO_PORT, LED2_GPIO_PIN);		
		
		/*选择要控制的GPIO引脚*/
		GPIO_InitStructure.GPIO_Pin = LED3_GPIO_PIN;
		/*调用库函数，初始化GPIOF*/
		GPIO_Init(LED3_GPIO_PORT, &GPIO_InitStructure);    
    /* 关闭led3灯	*/
		GPIO_SetBits(LED3_GPIO_PORT, LED3_GPIO_PIN);
}

/*LED亮灭函数
函数原型： void LED1_on(short times,short onms,short offms)
入口参数： timse :   亮灭的次数
           onms:   单次LED灯亮的时长200-2000
           offms:  单次LED灯熄灭时长200-2000
					 onms=1 offms=0  :点亮LED1
					 onms=0 offms=1  :熄灭LED1
出口参数： 无

*/
void LED1_on(short times,short onms,short offms)
{
	u8 i;

	for(i=0;i<times;i++)      //发声频率
	{
		if(onms)
		{
			LED1_ON;
			Delay_Ms(onms);  //单次发声时间
		}
		if(offms)
		{
			LED1_OFF;
			Delay_Ms(offms);  //单次无声时间
		}			
	}
}

/*LED亮灭函数
函数原型： void LED2_on(short times,short onms,short offms)
入口参数： timse :   亮灭的次数
           onms:   单次LED灯亮的时长200-2000
           offms:  单次LED灯熄灭时长200-2000
					 onms=1 offms=0  :点亮LED1
					 onms=0 offms=1  :熄灭LED1
出口参数： 无

*/
void LED2_on(short times,short onms,short offms)
{
	u8 i;

	for(i=0;i<times;i++)      //发声频率
	{
		if(onms)
		{
			LED2_ON;
			Delay_Ms(onms);  //单次发声时间
		}
		if(offms)
		{
			LED2_OFF;
			Delay_Ms(offms);  //单次无声时间
		}	
	}
}

/*LED亮灭函数
函数原型： void LED3_on(short times,short onms,short offms)
入口参数： timse :   亮灭的次数
           onms:   单次LED灯亮的时长200-2000
           offms:  单次LED灯熄灭时长200-2000
					 onms=1 offms=0  :点亮LED1
					 onms=0 offms=1  :熄灭LED1
出口参数： 无

*/
void LED3_on(short times,short onms,short offms)
{
	u8 i;

	for(i=0;i<times;i++)      //发声频率
	{
		if(onms)
		{
			LED3_ON;
			Delay_Ms(onms);  //单次发声时间
		}
		if(offms)
		{
			LED3_OFF;
			Delay_Ms(offms);  //单次无声时间
		}	 
	}
}

/*--------------------- END OF FIAL-------------------*/
