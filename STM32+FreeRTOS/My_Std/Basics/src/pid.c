#include "pid.h"

PID_t pid;   //存取PID算法所需要的数据


/** 
 * @brief PID_Loc PID位置计算
 * @param  SetValue ----- 设置值（期望值）
 * @param  ActualValue ----- 实际值（反馈值）
 * @param  pid ----- PID数据结构体
 * @return pid_pos = Kp * e(k) + Ki * ∑e(n) + Kd * (e(k) - e(k-1))
 */
float PID_Pos(float objVal, float curVal, PID_t *pid)
{
    pid->Ek = objVal - curVal;
    pid->Es += pid->Ek;     /* 累计误差 */
    pid->result = pid->Kp * pid->Ek + pid->Ki * pid->Es + pid->Kd * (pid->Ek - pid->Ek1);  /* 位置式PID */

    pid->Ek1 = pid->Ek;
    return pid->result;
}


/** 
 * @brief PID_Inc PID增量计算
 * @param  SetValue ----- 设置值（期望值）
 * @param  ActualValue ----- 实际值（反馈值）
 * @param  pid ----- PID数据结构体
 * @return pid_inc = u(k) - u(k-1)=Kp*[e(k)-e(k-1)] + Ki*e(k) + Kd*[e(k)-2*e(k-1)+e(k-2)]
 */
float PID_Inc(float objVal, float curVal, PID_t *pid)
{
    float increment;  /* 增量 */
    pid->Ek = objVal - curVal;

    increment = (pid->Kp*(pid->Ek-pid->Ek1)) + (pid->Ki*pid->Ek) + (pid->Kd*(pid->Ek-2*pid->Ek1+pid->Ek2));  /* 位置式PID */

    pid->Ek2 = pid->Ek1;
    pid->Ek1 = pid->Ek;
    pid->result += increment;
    
    return pid->result;
}

void PID_Init(PID_t *pid)
{
    pid->Kp = 0;
    pid->Ki = 0;
    pid->Kd = 0;

    pid->Ek = 0;
    pid->Ek1 = 0;
    pid->Ek2 = 0;

    pid->result = 0;
}

void PID_SetArgs(float Kp, float Ki, float Kd, PID_t *pid)
{
    pid->Kp = Kp;
    pid->Ki = Ki;
    pid->Kd = Kd;
}

void test_pid(void)
{
    PID_t pid;
    PID_Init(&pid);
    pid.Kp = 0.2;
    pid.Ki = 0.001;
    pid.Kd = 0.0001;

    float curVal = 0;
    int i = 0;
    int max = 5000;
    while(i++ < max){
        
        curVal = PID_Inc(100,curVal,&pid);
    
        printf("%2d currentValue: %f\n",i,curVal);
    }
    
}



/*--------------------- END OF FIAL-------------------*/
