#include "i2c.h"
#include "delay.h"

//不同设备使用I2C正常通讯时用到的延时不同， 例如OLED使用I2C正常显示时的延时为0
#define  delay()      Delay_Us(3)

static void SDA_Config_out(void);
static void SDA_Config_in(void);

/*
*********************************************************************************************************
*	函 数 名: I2C_GPIO_Config
*	功能说明: I2C所用到的GPIO进行初始化
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void I2C_GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(I2C_GPIO_CLK, ENABLE);	// 打开GPIO时钟 

	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 	// 开漏输出改为推挽输出 

	GPIO_InitStructure.GPIO_Pin = I2C_SCL_PIN;
	GPIO_Init(I2C_SCL_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = I2C_SDA_PIN;
	GPIO_Init(I2C_SDA_PORT, &GPIO_InitStructure);
}

/*
*********************************************************************************************************
*	函 数 名: SDA_Config_out
*	功能说明: 将SDA对应的GPIO引脚设置为输出模式
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
static void SDA_Config_out(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
 	RCC_APB2PeriphClockCmd(I2C_GPIO_CLK, ENABLE);	// 打开GPIO时钟  
	GPIO_InitStructure.GPIO_Pin = I2C_SDA_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  	
	GPIO_Init(I2C_SDA_PORT, &GPIO_InitStructure);
}

/*
*********************************************************************************************************
*	函 数 名: SDA_Config_in
*	功能说明: 将SDA对应的GPIO引脚设置为输入模式
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
static void SDA_Config_in(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
 	RCC_APB2PeriphClockCmd(I2C_GPIO_CLK, ENABLE);	// 打开GPIO时钟 
	GPIO_InitStructure.GPIO_Pin = I2C_SDA_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;  	// 浮空输入IN_FLOATING
	GPIO_Init(I2C_SDA_PORT, &GPIO_InitStructure);
}

/*
*********************************************************************************************************
*	函 数 名: I2C_Start               
*	功能说明: CPU发起I2C总线启动信号         
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void I2C_Start(void)
{
	I2C_SCL_Set;
	SDA_Config_out();
	I2C_SDA_Set;
	delay();
	I2C_SDA_Clr;
	delay();
	I2C_SCL_Clr;
	delay();
}

/*
*********************************************************************************************************
*	函 数 名: I2C_Stop
*	功能说明: CPU发起I2C总线停止信号
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void I2C_Stop(void)
{
	I2C_SCL_Set;
	SDA_Config_out();
	I2C_SDA_Clr;
	delay();
	I2C_SDA_Set;
}

/*
*********************************************************************************************************
*	函 数 名: I2C_Ack
*	功能说明: 在第九个脉冲CPU产生一个低电平ACK信号
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void I2C_Ack(void)
{
	SDA_Config_out();
	I2C_SDA_Clr;	/* CPU驱动SDA = 0 */
	delay();
	I2C_SCL_Set;	/* CPU产生1个时钟 */
	delay();
	I2C_SCL_Clr;
	delay();
	I2C_SDA_Set; /* CPU释放SDA总线 */
}

/*
*********************************************************************************************************
*	函 数 名: I2C_NAck
*	功能说明: 在第九个脉冲CPU产生一个高电平NACK信号
*	形    参：无
*	返 回 值: 无
*********************************************************************************************************
*/
void I2C_NAck(void)
{
	SDA_Config_out();
	I2C_SDA_Set;	/* CPU驱动SDA = 1 */
	delay();
	I2C_SCL_Set;	/* CPU产生1个时钟 */
	delay();
	I2C_SCL_Clr;
	delay();	
}

/*
*********************************************************************************************************
*	函 数 名: I2C_WaitAck
*	功能说明: CPU产生一个时钟，并读取器件的ACK应答信号
*	形    参：无
*	返 回 值: 返回0表示正确应答，1表示无器件响应
*********************************************************************************************************
*/
u8 I2C_WaitAck(void) 
{
	u8 ack;

	I2C_SDA_Set;	// CPU释放SDA总线 
	SDA_Config_in();
	delay();
	
	I2C_SCL_Set;	//CPU驱动SCL = 1, 此时器件会返回ACK应答 
	delay();
	if (I2C_SDA_READ())	// CPU读取SDA口线状态
	{
		ack = 1;   //有应答时，SDA=1,应答标志ack为1
	}
	else
	{
		ack = 0;   //无应答时，SDA=0,应答标志ack为0，
	}
	I2C_SCL_Clr;
	delay();
	
	return ack;
}

/*
*********************************************************************************************************
*	函 数 名: I2C_SendByte
*	功能说明: CPU向I2C总线设备发送8bit数据
*	形    参：dat ： 等待发送的字节
*	返 回 值: 无
*********************************************************************************************************
*/
void I2C_SendByte(u8 dat)
{
	u8 i;

	SDA_Config_out();
	//先发送字节的高位bit7 
	for (i = 0; i < 8; i++)
	{		
		if (dat & 0x80)
		{
			I2C_SDA_Set;
		}
		else
		{
			I2C_SDA_Clr;
		}
		delay();
		I2C_SCL_Set;
		delay();
		I2C_SCL_Clr;
		delay();
		
		dat <<= 1;	// 左移一个bit
	}
	I2C_SDA_Set; // 释放总线
	delay();
}

/*
*********************************************************************************************************
*	函 数 名: I2C_ReadByte
*	功能说明: CPU从I2C总线设备读取8bit数据
*	形    参：无
*	返 回 值: 读到的数据（先读高位再读低位）
* 注    意：主器件不发生应答信号，但产生一个停止信号
*********************************************************************************************************
*/
u8 I2C_ReadByte(void)
{
	u8 i;
	u8 value;
	
	SDA_Config_in();
	
	//读到第1个bit为数据的bit7
  //I2C_SDA_1();
	for (i = 0; i < 8; i++)
	{ 	
		value <<= 1;
		I2C_SCL_Set;
		delay();

		if (I2C_SDA_READ())
		{
			value++;
		}
		
		I2C_SCL_Clr;
		delay();
	}
	return value;
}

/*
*********************************************************************************************************
*	函 数 名: I2C_CheckDevice
*	功能说明: 检测I2C总线设备，CPU向发送设备地址，然后读取设备应答来判断该设备是否存在
*	形    参：_Address：设备的I2C总线地址
*	返 回 值: 返回值 0 表示正确， 返回1表示未探测到
*********************************************************************************************************
*/
uint8_t I2C_CheckDevice(uint8_t _Address)
{
	uint8_t ucAck;

	I2C_GPIO_Config();		/* 配置GPIO */

	
	I2C_Start();		/* 发送启动信号 */

	/* 发送设备地址+读写控制bit（0 = w， 1 = r) bit7 先传 */
	I2C_SendByte(_Address | I2C_WR);
	ucAck = I2C_WaitAck();	/* 检测设备的ACK应答 */

	I2C_Stop();			/* 发送停止信号 */

	return ucAck;
}
