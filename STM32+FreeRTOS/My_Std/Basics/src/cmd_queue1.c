/************************************版权申明********************************************
**                             广州大彩光电科技有限公司
**                             http://www.gz-dc.com
**-----------------------------------文件信息--------------------------------------------
** 文件名称:   cmd3_queue.c
** 修改时间:   2011-05-18
** 文件说明:   用户MCU串口驱动函数库
** 技术支持：  Tel: 020-82186683  Email: hmi@gz-dc.com Web:www.gz-dc.com
**--------------------------------------------------------------------------------------*/


#include "cmd_queue1.h"

typedef struct _QUEUE
{
    qsize _head;
    qsize _tail;
    qdata _data[QUEUE_MAX_SIZE];
} QUEUE;//串口数据接收队列结构体

QUEUE que1 = {0,0,0};
static qdata cmd_state = 0;
static qsize cmd_pos = 0;
PCTRL_MSG msg1;
/********************************
*名    称： queue1_reset
*功    能    串口接收队列复位
*入口参数： 无
*出口参数： 无
*********************************/
void queue1_reset()
{
    que1._head = que1._tail = 0;
    cmd_pos = cmd_state = 0;
}

/**********************************************
*名    称： queue1_push
*功    能   存入一个数据到串口接收队列（FIFO）中
*入口参数： 无
*出口参数： 无
**********************************************/
void queue1_push(qdata _data)
{
    qsize pos = (que1._head+1)%QUEUE_MAX_SIZE;
    if(pos!=que1._tail)     //非满状态
    {
        que1._data[que1._head] = _data;
        que1._head = pos;
    }
}
/**********************************************
*名    称： queue1_pop
*功    能   从串口接收队列(FIFO)中取出一个字节数据
*入口参数： 无
*出口参数： 无
**********************************************/
static void queue_pop(qdata* _data)
{
    if(que1._tail!=que1._head)//非空状态
    {
        *_data = que1._data[que1._tail];
        que1._tail = (que1._tail+1)%QUEUE_MAX_SIZE;
    }
}
/**********************************************
*名    称： queue1_size
*功    能   获取串口接收队列(FIFO)的长度
*入口参数： 无
*出口参数： 无
**********************************************/
static qsize queue1_size()
{
    return ((que1._head+QUEUE_MAX_SIZE-que1._tail)%QUEUE_MAX_SIZE);
}

/******************************************************************
*名    称： queue1_find_cmd
*功    能   获取串口接收队列(FIFO)中一个完整的帧
            一个完整的MODBUS请求帧格式：
            读单个或多个寄存器：
            | 设备地址 | 功能码 | 起始地址 | 读取数量 | CRC低位 |CRC高位 |
            | 1 BYTE   | 1 BYTE |  2 BYTE  |   2 BYTE |   1BYTE | 1 BYTE |
例:读两路温度： 01     |   03   |  00 01   |  00 02   |   95    |   CB   |
            写单个寄存器：
										| 设备地址 | 功能码 |寄存器地址|寄存器内容| CRC低位 |CRC高位 |
										| 1 BYTE   | 1 BYTE |  2 BYTE  |   2 BYTE |   1BYTE | 1 BYTE |
例:写第二路RS485波特率： 01    |   06   |  00 13   |  BB 02   |   18    |   AF   |
            写多个寄存器：
										| 设备地址 | 功能码 | 起始地址 |寄存器数量| 字节计数|  设置内容   |CRC低位|CRC高位 |
										| 1 BYTE   | 1 BYTE |  2 BYTE  |   2 BYTE |   1BYTE |  N*2 BYTE   | 1BYTE | 1 BYTE |
例:写1,2路485波特率    ：01    |   10   |  00 12   |  00 02   |   04    | BB 02 BB 06 |  12   |   16   |
             
*入口参数： 无
*出口参数： 无
********************************************************************/
qsize queue1_find_cmd(qdata *buffer,qsize buf_len)
{
    qsize cmd_size = 0;
    qdata _data = 0;
	  static qsize  datanum = 0,dataindex=0;
	  qsize crc;
    while(queue1_size()>0)
    {
        //取一个数据
        queue_pop(&_data);

        if((cmd_pos==0)&&(SlvAddr != _data)&&(0 != _data))//帧头出错，跳过 ,改为可以接收广播地址的指令
				{ 
				   continue;
        }
				
        if(cmd_pos<buf_len)//防止缓冲区溢出
            buffer[cmd_pos++] = _data;
        
        switch (cmd_state)
            {
						case 1://判断功能代码  必须是03或06或0x10
									if((0x03==_data)||(0x06==_data)||(0x10==_data))
										cmd_state = 2;
									else   //功能码出错
									{
										cmd_pos=0;
										cmd_state=0;
									}
									break; // 
					 case 2://判断寄存器地址  必须是小于100
							    if(cmd_pos<4)
										  break;
							    if(100 > (_data+buffer[2]*256))
									   cmd_state = 3;
									else   //功能码出错
									{
										cmd_pos=0;
										cmd_state=0;
									}
									break; // 
            case 3://判断寄存器长度  必须是小于100
							    if(cmd_pos<6)
										  break;
									datanum = _data+buffer[2]*256;
									dataindex=0;
									if(0x06 == buffer[1])   //没有reg16_len 项，这个字节是寄存器内容
									{
										cmd_state = 6;
										dataindex = 0;
									}
									else if(0x03 == buffer[1])   //没有寄存器内容项
									{
										cmd_state = 6;
										dataindex = 0;
									}
									else if(0x10 == buffer[1])  //10 有reg16_len 项，后面是字节数
									{
										if(100 > datanum)
											 cmd_state = 4;
										else   //功能码出错
										{
											cmd_pos=0;
											cmd_state=0;
										}
									}			
									break; //
  				
            case 4://接受指定的个数的字节数据(寄存器数量*2)
									if(200 > _data)
									{
										if(_data<2)  //写入的数据是1个字节也要接收2个字节。=1是高字节有效；=2是两个字节都有效
											datanum = 2; 
										else
										 datanum = _data;
										cmd_state = 5;
										dataindex = 0;
									}
									else
									{
											cmd_pos=0;
											cmd_state=0;
									}
									break;
					  case 5://接受数据的字节个数(寄存器数量*2)
									if(datanum <= ++dataindex)
									{
										cmd_state = 6;
										dataindex = 0;
									}
									break;									
            case 6: //接受2字节CRC数据 
									if(++dataindex == 2)  //CRC接收完毕，开始校验
									{
											crc=crc16Value((void *)buffer,cmd_pos-2);
											if(crc==(buffer[cmd_pos-1]<<8)+buffer[cmd_pos-2])
											{
												cmd_state = 8;
											}
											else     //CRC错误，但是数据个数是对的
											  cmd_state = 7;
									}
									break;					
            default:
                cmd_state = 1;
                break; //
            }

        //得到完整的帧尾
        if(cmd_state>=7)
        {
            cmd_size = cmd_pos;
            cmd_pos = 0;
					  if(cmd_state==8)
						{
							cmd_state = 0;
              return cmd_size;
						}	
						else                //CRC错误，但是数据个数是对的
						{	
							cmd_state = 0;
							if(buffer[0] == SlvAddr)  //只有对本机发送的指令才回复错误信息
							rtuSendErrorResponse();   //发送CRC校验错误回复
							return 0;   
						}							
        }
    }

    return 0;//没有形成完整的一帧
}

