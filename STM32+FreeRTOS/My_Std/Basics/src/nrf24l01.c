#include "nrf24l01.h"


uint8_t tx_buf[TX_PLOAD_WIDTH];
uint8_t rx_buf[RX_PLOAD_WIDTH];

uint8_t TX_ADDRESS[TX_ADR_WIDTH]={0x34,0x43,0x10,0x10,0x01}; //发送地址
uint8_t RX_ADDRESS[RX_ADR_WIDTH]={0x34,0x43,0x10,0x10,0x01}; //接收地址

/**
  * @brief  SPI的 I/O配置
  * @param  无
  * @retval 无
  */
void NRF24L01_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure; 

#if    !SPI_SIMULATE	
  SPI_InitTypeDef  SPI_InitStructure;	
#endif	
	
	RCC_APB2PeriphClockCmd( NRF24L01_CS_CLK|NRF24L01_SCK_CLK|NRF24L01_MOSI_CLK|
	                NRF24L01_MISO_CLK|NRF24L01_CE_CLK|NRF24L01_IRQ_CLK, ENABLE); 
#if    SPI_SIMULATE
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 	//设置引脚模式为推挽输出  
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 
	
	GPIO_InitStructure.GPIO_Pin = NRF24L01_CS_PIN;		//CS
	GPIO_Init(NRF24L01_CS_PORT, &GPIO_InitStructure);		

	GPIO_InitStructure.GPIO_Pin = NRF24L01_SCK_PIN;		//SCK
	GPIO_Init(NRF24L01_SCK_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = NRF24L01_MOSI_PIN;	//MOSI
	GPIO_Init(NRF24L01_MOSI_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = NRF24L01_CE_PIN;		//CE
	GPIO_Init(NRF24L01_CE_PORT, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = NRF24L01_MISO_PIN;	//MISO
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 		//设置引脚模式为上拉输入
	GPIO_Init(NRF24L01_MISO_PORT, &GPIO_InitStructure);	 
	
	GPIO_InitStructure.GPIO_Pin = NRF24L01_IRQ_PIN;		//IRQ
	GPIO_Init(NRF24L01_IRQ_PORT, &GPIO_InitStructure);	
#else  //选择STM32自带的SPI
 /*使能SPI1时钟*/
  NRF_SPI_APBxClock_FUN(NRF_SPI_CLK, ENABLE);

   /*配置 SPI_NRF_SPI的 SCK,MISO,MOSI引脚，GPIOA^5,GPIOA^6,GPIOA^7 */
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; //复用功能
	
	GPIO_InitStructure.GPIO_Pin = NRF24L01_SCK_PIN;
  GPIO_Init(NRF24L01_SCK_PORT, &GPIO_InitStructure);  
	
	GPIO_InitStructure.GPIO_Pin = NRF24L01_MOSI_PIN;
  GPIO_Init(NRF24L01_MOSI_PORT, &GPIO_InitStructure); 
	
	GPIO_InitStructure.GPIO_Pin = NRF24L01_MISO_PIN;
  GPIO_Init(NRF24L01_MISO_PORT, &GPIO_InitStructure); 

  /*配置SPI_NRF_SPI的CE引脚,和SPI_NRF_SPI的 CSN 引脚*/
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	
	GPIO_InitStructure.GPIO_Pin = NRF24L01_CS_PIN;
  GPIO_Init(NRF24L01_CS_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = NRF24L01_CE_PIN;
  GPIO_Init(NRF24L01_CE_PORT, &GPIO_InitStructure);


  /*配置SPI_NRF_SPI的IRQ引脚*/
  GPIO_InitStructure.GPIO_Pin = NRF24L01_IRQ_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD ;  //上拉输入
  GPIO_Init(NRF24L01_IRQ_PORT, &GPIO_InitStructure); 
		  
  /* 这是自定义的宏，用于拉高csn引脚，NRF进入空闲状态 */
  NRF24_CS_SET; 
  
	/*----------------- SPI 模式配置 ------------------*/
	
  // 支持SPI模式0及模式3，据此选择模式0，设置CPOL=0 CPHA=0
	/*设置 SPI 的单双向模式 */
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  /*设置 SPI 的主/从机端模式 */
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	/*设置 SPI 的数据帧长度，可选 8/16 位 */
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  /*设置时钟极性 CPOL，可选高/低电平*/
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	/*设置时钟相位，可选奇/偶数边沿采样 */
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
	/*设置 NSS 引脚由 SPI 硬件控制还是软件控制*/
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	/*设置时钟分频因子， fpclk/分频数=fSCK */
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
  /*设置 MSB/LSB 先行 */
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	/*设置 CRC 校验的表达式 */
  SPI_InitStructure.SPI_CRCPolynomial = 7;
	//初始化SPI结构体
  SPI_Init( NRF_SPI_x, &SPI_InitStructure);

  /* 使能 SPI  */
  SPI_Cmd( NRF_SPI_x, ENABLE);

#endif

}

#if          SPI_SIMULATE
//写字节
static uint8_t NRF24L01_Write_Byte(u8  byte)   
{
	uint8_t i;
	delay_us(1);
	for( i=0; i<8; i++)
	{
		if(byte & 0x80)
		{
			NRF24_MOSI_SET;
		}
		else
		{
			NRF24_MOSI_CLR;
		}
		byte <<= 1;
		delay_us(1);
		NRF24_SCK_SET;
		delay_us(1);
		byte |= NRF24_MISO_READ;
		NRF24_SCK_CLR;
	}
	return (byte);
}
#else

/**
  * @brief   用于向NRF读/写一字节数据
  * @param   写入的数据
  *		@arg dat 
  * @retval  读取得的数据
  */
static uint8_t NRF24L01_Write_Byte(u8 dat)
{  	
   /* 当 SPI发送缓冲器非空时等待 */
  while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
  
   /* 通过 SPI2发送一字节数据 */
  SPI_I2S_SendData(SPI1, dat);		
 
   /* 当SPI接收缓冲器为空时等待 */
  while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);

  /* Return the byte read from the SPI bus */
  return SPI_I2S_ReceiveData(SPI1);
}

#endif

/**
  * @brief   用于向NRF特定的寄存器写入数据
  * @param   
  *		@arg reg:NRF的命令+寄存器地址
  *		@arg dat:将要向寄存器写入的数据
  * @retval  NRF的status寄存器的状态
  */
uint8_t NRF24L01_Write_Reg(u8 reg,u8 dat)
{
 	uint8_t status;
	NRF24_CE_CLR;
	/*置低CSN，使能SPI传输*/
  NRF24_CS_CLR;
				
	/*发送命令及寄存器号 */
	status = NRF24L01_Write_Byte(reg);
		 
	/*向寄存器写入数据*/
  NRF24L01_Write_Byte(dat); 
	          
	/*CSN拉高，完成*/	   
  NRF24_CS_SET;	
		
	/*返回状态寄存器的值*/
  return status;
}

/**
  * @brief   用于从NRF特定的寄存器读出数据
  * @param   
  *		@arg reg:NRF的命令+寄存器地址
  * @retval  寄存器中的数据
  */
uint8_t NRF24L01_Read_Reg(u8 reg)
{
 	uint8_t reg_val;

	NRF24_CE_CLR;
	/*置低CSN，使能SPI传输*/
 	NRF24_CS_CLR;
				
  /*发送寄存器号*/
	NRF24L01_Write_Byte(reg); 

	/*读取寄存器的值 */
	reg_val = NRF24L01_Write_Byte(NOP);
	            
  /*CSN拉高，完成*/
	NRF24_CS_SET;		
   	
	return reg_val;
}	

/**
  * @brief   用于向NRF的寄存器中写入一串数据
  * @param   
  *		@arg reg : NRF的命令+寄存器地址
  *		@arg pBuf：存储了将要写入写寄存器数据的数组，外部定义
  * 	@arg bytes: pBuf的数据长度
  * @retval  NRF的status寄存器的状态
  */
uint8_t NRF24L01_Write_Buf(uint8_t reg, uint8_t *pBuf, uint8_t bytes)
{
	 u8 status,byte_cnt;
	 NRF24_CE_CLR;
   /*置低CSN，使能SPI传输*/
	 NRF24_CS_CLR;			

	 /*发送寄存器号*/	
   status = NRF24L01_Write_Byte(reg); 
 	
   /*向缓冲区写入数据*/
	 for( byte_cnt=0; byte_cnt<bytes; byte_cnt++)
		NRF24L01_Write_Byte(*pBuf++);	//写数据到缓冲区 	 
	  	   
	 /*CSN拉高，完成*/
	 NRF24_CS_SET;			
  
  return (status);	//返回NRF24L01的状态 		
}
 
/**
  * @brief   用于向NRF的寄存器中写入一串数据
  * @param   
  *		@arg reg : NRF的命令+寄存器地址
  *		@arg pBuf：用于存储将被读出的寄存器数据的数组，外部定义
  * 	@arg bytes: pBuf的数据长度
  * @retval  NRF的status寄存器的状态
  */
uint8_t NRF24L01_Read_Buf(uint8_t reg,uint8_t *pBuf,uint8_t bytes)  
{
 	u8 status, byte_cnt;

	  NRF24_CE_CLR;
	/*置低CSN，使能SPI传输*/
	NRF24_CS_CLR;
		
	/*发送寄存器号*/		
	status = NRF24L01_Write_Byte(reg); 

 	/*读取缓冲区数据*/
	 for(byte_cnt=0;byte_cnt<bytes;byte_cnt++)		  
	   pBuf[byte_cnt] = NRF24L01_Write_Byte(NOP); //从NRF24L01读取数据  

	 /*CSN拉高，完成*/
	NRF24_CS_SET;	
		
 	return status;		//返回寄存器状态值
} 

//该函数初始化NRF24L01到RX模式
//设置RX地址,写RX数据宽度,选择RF频道,波特率和LNA HCURR
//当CE变高后,即进入RX模式,并可以接收数据了	
void NRF24L01_RX_Mode(void)    //配置为接收模式
{
	NRF24_CE_CLR;	  
  NRF24L01_Write_Buf(WRITE_REGm+RX_ADDR_P0,RX_ADDRESS,RX_ADR_WIDTH);   //写RX节点地址
	
	NRF24L01_Write_Reg(WRITE_REGm+EN_AA,0x01);    //使能通道0的自动应答    
	NRF24L01_Write_Reg(WRITE_REGm+EN_RXADDR,0x01);//使能通道0的接收地址  	 
	NRF24L01_Write_Reg(WRITE_REGm+RF_CH,16);	     //设置RF通信频率		  
	NRF24L01_Write_Reg(WRITE_REGm+RX_PW_P0,RX_PLOAD_WIDTH);//选择通道0的有效数据宽度 	    
	NRF24L01_Write_Reg(WRITE_REGm+RF_SETUP,0x0f); //设置TX发射参数,0db增益,2Mbps,低噪声增益开启   
	NRF24L01_Write_Reg(WRITE_REGm+CONFIG, 0x0f);  //配置基本工作模式的参数;PWR_UP,EN_CRC,16BIT_CRC,接收模式 
	NRF24_CE_SET; //CE为高,进入接收模式 
}	

//该函数初始化NRF24L01到TX模式
//设置TX地址,写TX数据宽度,设置RX自动应答的地址,填充TX发送数据,选择RF频道,波特率和LNA HCURR
//PWR_UP,CRC使能
//当CE变高后,即进入RX模式,并可以接收数据了		   
//CE为高大于10us,则启动发送.	 
void NRF24L01_TX_Mode(void)   //配置为发送模式
{														  
	NRF24_CE_CLR;	    
	NRF24L01_Write_Buf(WRITE_REGm+TX_ADDR,TX_ADDRESS,TX_ADR_WIDTH);//写TX节点地址 
	NRF24L01_Write_Buf(WRITE_REGm+RX_ADDR_P0,RX_ADDRESS,RX_ADR_WIDTH); //设置TX节点地址,主要为了使能ACK	  

	NRF24L01_Write_Reg(WRITE_REGm+EN_AA,0x01);     //使能通道0的自动应答    
	NRF24L01_Write_Reg(WRITE_REGm+EN_RXADDR,0x01); //使能通道0的接收地址  
	NRF24L01_Write_Reg(WRITE_REGm+SETUP_RETR,0x1f);//使能自动重发    设置自动重发间隔时间:500us + 86us;最大自动重发次数:15次
	NRF24L01_Write_Reg(WRITE_REGm+RF_CH,16);       //设置RF通道为0
	NRF24L01_Write_Reg(WRITE_REGm+RF_SETUP,0x0f);  //设置TX发射参数,0db增益,2Mbps,低噪声增益开启   
	NRF24L01_Write_Reg(WRITE_REGm+CONFIG,0x0e);    //配置基本工作模式的参数;PWR_UP,EN_CRC,16BIT_CRC,接收模式,开启所有中断
	NRF24_CE_SET;   //CE为高,10us后启动发送
}

/**
  * @brief  主要用于NRF与MCU是否正常连接
  * @param  无
  * @retval SUCCESS/ERROR 连接正常/连接失败
  */
uint8_t NRF24L01_Check(void)
{
	uint8_t buf[5]={0xC2,0xC2,0xC2,0xC2,0xC2};
	uint8_t buf1[5];
	uint8_t i; 
	 
	/*写入5个字节的地址.  */  
	NRF24L01_Write_Buf(WRITE_REGm+TX_ADDR,buf,5);

	/*读出写入的地址 */
	NRF24L01_Read_Buf(TX_ADDR,buf1,5); 
	 
	/*比较*/               
	for(i=0;i<5;i++)
	{
		if(buf1[i]!=0xC2)
		break;
	} 
	       
	if(i==5)
		return SUCCESS ;        //MCU与NRF成功连接 
	else
		return ERROR ;        //MCU与NRF不正常连接
}

//启动NRF24L01发送一次数据
//txbuf:待发送数据首地址
//返回值:发送完成状况
uint8_t NRF24L01_TxPacket(uint8_t *txbuf)    //发送一个包的数据
{
	uint8_t state;
	
	/*ce为低，进入待机模式1*/
	NRF24_CE_CLR;
	/*写数据到TX BUF 最大 32个字节*/
  NRF24L01_Write_Buf(WR_TX_PLOAD,txbuf,TX_PLOAD_WIDTH);    
 	
	/*CE为高，txbuf非空，发送数据包 */
	NRF24_CE_SET;    	
	/*等待发送完成中断(IRQ!=0) */
	while(NRF24_IRQ_READ != 0); 
	
  /*读取状态寄存器的值 */	
	state = NRF24L01_Read_Reg(STATUS);    
	printf("val=0x%x\r\n",state);
	
	/*清除TX_DS或MAX_RT中断标志*/
	NRF24L01_Write_Reg(WRITE_REGm+STATUS,state);
  /*清除TX FIFO寄存器*/
  NRF24L01_Write_Reg(FLUSH_TX,NOP);    
	if(state&MAX_TX)   //达到最大重发次数
	{
		NRF24L01_Write_Reg(FLUSH_TX,0xe1);   //清除TX FIFO寄存器  
		return MAX_TX; 
	}
	if(state&TX_OK)     //发送完成
	{
		return TX_OK;
	}
	return ERROR;    //其他原因发送失败
}

//启动NRF24L01接收一次数据
//rxbuf:待接收数据首地址
//返回值:0，接收完成；其他，错误代码
uint8_t NRF24L01_RxPacket(uint8_t *rxbuf)
{
	uint8_t state;
	/*进入接收状态*/
  NRF24_CE_SET;

	/*等待接收中断*/
	while(NRF24_IRQ_READ==0)	
	{
		/*进入待机状态*/
		NRF24_CE_CLR;
		/*读取status寄存器的值*/
		state = NRF24L01_Read_Reg(STATUS);   
		/* 清除中断标志*/
		NRF24L01_Write_Reg(WRITE_REGm+STATUS,state);   //清除TX_DS或MAX_RT中断标志
		
		/*判断是否接收到数据*/
		if(state & RX_OK)   //接收到数据
		{
			/*读取数据*/
			NRF24L01_Read_Buf(RD_RX_PLOAD,rxbuf,RX_PLOAD_WIDTH); 
			/*清除RX FIFO寄存器*/
			NRF24L01_Write_Reg(FLUSH_RX,NOP);   
			return RX_OK; 
		}	   
		return ERROR;  //没收到任何数据
  }
	return ERROR;  //没收到任何数据
}

/*--------------------- END OF FIAL-------------------*/
