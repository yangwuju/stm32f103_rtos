#include "menu.h"
#include "oledbmp.h"

//三键控制

u8 menuKey = 0; //菜单键
u8 upKey = 0;   //向上键
u8 downKey = 0; //向下键

u8 pageNum = 0; //菜单页码
u8 lineNum = 0; //行码
u8 optFlag = 0;  //选项标志位

u8 mainPage = 1;  //主页码
u8 subPage = 0;   //次页码
u8 thrPage = 0;   //三级页码


u16 interfaceNum;


void KeyControl(void)
{
	 /*-----------------menuKey键按下----------------*/
   if(menuKey == 1)
	 { 
		 if(optFlag == 0)
		 {
			 pageNum++;
		 	 switch(pageNum)
			 {
			 	 case 0: mainPage ++; break;
				 case 1:  subPage ++; break;
				 case 2:  thrPage ++; break;
				 default: break;
			 }
			 if(pageNum > maxPage)
			 {
				 pageNum = pageNum-2;
				 switch(pageNum+1)
				 {
					 case 0: mainPage --; break;
					 case 1:  subPage --; break;
					 case 2:  thrPage --; break;
				 }			 
			 }
		 }
		 else 
     {
				 (lineNum > 0) ? (optFlag=1) : (optFlag=0);
		 }
	 }
	 
	 /*-----------------upKey键按下----------------*/
   if(upKey == 1)
	 {
		 if(optFlag == 0)
		 {
			 if(pageNum < maxPage)
			 {
				 switch(pageNum)
				 {
					 case 0: mainPage ++; break;
					 case 1:  subPage ++; break;
					 case 2:  thrPage ++; break;
				 }	
			 }
			 else
			 {
					lineNum++;
			 }
		 }
		 //选项调节
		 else
		 {
		   
		 }
	 }
	 
	 /*-----------------downKey键按下----------------*/
   if(downKey == 1)
	 {
		 if(optFlag == 0)
		 {
			 if(pageNum < maxPage)
			 {
				 switch(pageNum)
				 {
					 case 0: mainPage --; break;
					 case 1:  subPage --; break;
					 case 2:  thrPage --; break;
				 }	
			 }
			 else
			 {
				 lineNum--;
			 }
		 }
		 //选项调节
		 else
		 {
		   
		 }
	 }
	 
	 interfaceNum = mainPage*100 + subPage*10 + thrPage;

	 PageDisplay();
	 
	 //按键复位
	 menuKey = 0;
	 upKey = 0;
	 downKey = 0;
}

void PageDisplay(void)
{
  switch(interfaceNum)
	{
		case 100: OLED_ShowString(17,17,"100",16,1,0); break;
		case 110: OLED_ShowString(17,17,"110",16,1,0); break;
		case 111: OLED_ShowString(17,17,"112",16,1,0); break;
		case 112: OLED_ShowString(17,17,"112",16,1,0); break;
		case 120: OLED_ShowString(17,17,"120",16,1,0); break;
		case 121: OLED_ShowString(17,17,"121",16,1,0); break;
		case 122: OLED_ShowString(17,17,"122",16,1,0); break;
		case 130: OLED_ShowString(17,17,"130",16,1,0); break;
		case 131: OLED_ShowString(17,17,"131",16,1,0); break;
		case 132: OLED_ShowString(17,17,"132",16,1,0); break;
		
		case 200: OLED_ShowString(17,17,"200",16,1,0); break;
		case 210: OLED_ShowString(17,17,"210",16,1,0); break;
		case 211: OLED_ShowString(17,17,"211",16,1,0); break;
		case 212: OLED_ShowString(17,17,"212",16,1,0); break;
		case 220: OLED_ShowString(17,17,"220",16,1,0); break;
		case 221: OLED_ShowString(17,17,"221",16,1,0); break;
		case 222: OLED_ShowString(17,17,"222",16,1,0); break;
		case 230: OLED_ShowString(17,17,"230",16,1,0); break;
		case 231: OLED_ShowString(17,17,"231",16,1,0); break;
		case 232: OLED_ShowString(17,17,"232",16,1,0); break;
		
		case 300: OLED_ShowString(17,17,"300",16,1,0); break;
		case 310: OLED_ShowString(17,17,"310",16,1,0); break;
		case 311: OLED_ShowString(17,17,"311",16,1,0); break;
		case 312: OLED_ShowString(17,17,"312",16,1,0); break;
		case 320: OLED_ShowString(17,17,"320",16,1,0); break;
		case 321: OLED_ShowString(17,17,"321",16,1,0); break;
		case 322: OLED_ShowString(17,17,"322",16,1,0); break;
		case 330: OLED_ShowString(17,17,"330",16,1,0); break;
		case 331: OLED_ShowString(17,17,"331",16,1,0); break;
		case 332: OLED_ShowString(17,17,"332",16,1,0); break;
	}
}



