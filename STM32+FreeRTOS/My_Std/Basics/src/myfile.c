#include "myfile.h"
#include "delay.h"
#include "usart1.h"
#include "stdlib.h"

float CurrentValue;

//在Min~Max之间产生一个随机数
u8 RandomNum( u8 Min, u8 Max)
{
	u8 num;
	srand(0);  //随机数初始值
	num = rand() % (Max - Min) + Min;
	return num;
}


//反转转态
u8 STATE_TOGGLE(void)
{
  static u8 state;
	
	(state==0)?(state=1):(state=0);
	
  return state;
}

/*
  设备带有短暂状态保持
  功能描述：让设备在CurrentValue超过SetValue后打开，直至小于状态保持值StateKeepValue时关闭
  CurrentValue：测得值
  SetValue：设定值
  StateKeepValue：状态保持值
*/
u8 GateValueJudge(u16 SetValue,u8 StateKeepValue)
{
	static u8 state=0;  //设备工作状态标识
	
  if(state == 0)  //设备处于关闭状态下
	{
		if( CurrentValue >= SetValue )   state = 1;	 //当前值大于设定值，让设备处于开启状态
		else state = 0; 		
	}
	else
	{
		if( CurrentValue >= SetValue - StateKeepValue ) state = 1; //在其状态保持内，不改变状态
		else state = 0;
	}
	return  state;
}


/*
  数据稳定变化
  功能描述：当前测得数据CurrentValue，变化幅度Stability
*/
float Data_StabVal(u8 Stability)
{
	static u8 state=0; //数据稳定状态标识
	static float temp=0; //记录数据
	static u8 ErrorCount=0; //数据异常计时
	
	if(state == 0) //数据不稳定
	{
		if((CurrentValue <= temp + Stability)&&(CurrentValue >= temp - Stability))
		{
			state = 1;
		}
		else 
		{
			temp = CurrentValue;
		}
	}
	else  //数据稳定
	{
		if((CurrentValue <= temp + Stability)&&(CurrentValue >= temp - Stability))
		{
			temp = CurrentValue;
		}
		else //数据异常
		{
			CurrentValue = temp; //将上一次数据作为本次数据
			ErrorCount++; //异常计数
			if(ErrorCount > 3)  //数据异常计数超过3次，说明数据发生突变为正常情况
			{
				ErrorCount = 0;
				state = 0;
			}
		}
	}
	return CurrentValue;
}

//读取times次,取最大值
//times：连续获取的次数
//返回值：times次读数里面读到的最大读数值
float Get_MaxVal(u8 times)
{
	float temp=0;
	float res=0;
	
	res=CurrentValue; //先得到一次值
	Delay_Ms(10); //数据采样间隔
	times--;  //数据采样一次
	
	while(times--)
	{
		temp=CurrentValue;//得到一次值
		if(temp>res)res=temp;
		
		Delay_Ms(10); //数据采样间隔
	}
	return res;
} 

//读取times次,取最小值
//times：连续获取的次数
//返回值：times次读数里面读到的最小读数值
float Get_MinVal(u8 times)
{
	float temp=0;
	float res=0;
	
	res=CurrentValue; //先得到一次值
	Delay_Ms(10); //数据采样间隔
	times--;  //数据采样一次
	
	while(times--) 
	{
		temp=CurrentValue; //得到一次值
		if(temp<res) res=temp;
		
		Delay_Ms(10); //数据采样间隔
	}
	return res;
}

/*
  返回数据在times次的平均值
	功能描述：获取times次数据，取其平均值
  CurrentValue：测得值
  times：次数
*/
float GetDataAverage(u8 times)
{
	u8 i;
	float sum=0;
	
	for( i=0; i<times; i++)
	{
		sum += CurrentValue;
		Delay_Ms(10); //数据获取间隔
	}
	
	return sum / times;
}

/*
  为得到的数据比较精确，可多次测量，去掉最大值和最小值，取余下平均值
  本函数读取times次数据，去掉前n个数据和后n个数据，取中间time-2n个数据的平均值
  
	功能描述：获取times次数据，取time-2n个平均值
  CurrentValue：测得值
  times：次数
  n:去掉前n个数据，同时后n个数据也会去掉，取余下的times-2n个数据平均值
  注：需保持times-2n>0;若times-2n<=0则返回测的times的平均值

  一般常取times=10，n =1或2
*/
float Data_RaminAverage(u8 times,u8 n)
{
	u16 buf[10];
	u16 temp;
	u8 i,j;
	for(i=0;i<times;i++)//连续读取times次
	{				 
		buf[i]=CurrentValue;
		Delay_Ms(10);	   //数据获取间隔 
	}
	for(i=0;i<times-1;i++)//排序
	{
		for(j=i+1;j<times;j++)
		{
			if(buf[i]>=buf[j])//升序排列
			{
				temp=buf[i];
				buf[i]=buf[j];
				buf[j]=temp;
			}
		}
	}
	temp=0;
	if(times-2*n)
	{
		for(i=n;i<times-n;i++) temp+=buf[i];//取中间的times-2*n个数据进行平均
		return temp/(times-2*n);
	}
	else
	{
		for(i=0;i<times;i++) temp+=buf[i];//取times个数据进行平均
		return temp/times;		  
	}
}

//利用回归直线方程求x与y间的关系
/*
  X=1/n ∑Xi  Y=1/n ∑Yi
  b = (∑xiyi - nXY)/（∑xi2-nX2)
  a=Y-bX
*/
float Xbuf[64]={0};
float Ybuf[64]={0}; //修改下x，y取值
void Get_XY_FUN_Line(float *Xbuf,float *Ybuf,u8 n)
{
	float x,y;
	float X=0,Y=0;
	float XX=0,XY=0;
	float a,b;
	u8 i;
	
  for(i=0;i<=n;i++)
  {
	  x = *Xbuf++;
		y = *Ybuf++;
		
		X += x;
		Y += y;
		
		XX += x*x;
		XY += x*y;
	}
	
	//求取X，Y平均值
	X /= X;
	Y /= Y; 
	
	b = (XY-n*X*Y)/(XX-n*X*X);
	a = Y-b*X;
	printf("\r\n x与y之间的关系式为：y = %fx+ %f\n",a,b);
}





