#include "usart1.h"


u8 USART1_RX_BUF[USART1_RX_LEN]; //接收缓冲,最大USART_REC_LEN个字节.

u8 rxflag1 = 0; //接收标识
u8 rxError1 = 0; //错误标识

//接收状态
//bit15，	  接收完成标志
//bit14，	  接收到0xEF
//bit13~0，	接收到的有效字节数目
u16 USART1_RX_STA=0; //接收状态标记	

 /**
  * @brief  配置嵌套向量中断控制器NVIC
  * @param  无
  * @retval 无
  */
static void NVIC_Config(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  
  /* 嵌套向量中断控制器组选择 */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
  
  /* 配置USART为中断源 */
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  /* 抢断优先级*/
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  /* 子优先级 */
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  /* 使能中断 */
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  /* 初始化配置NVIC */
  NVIC_Init(&NVIC_InitStructure);
}

 /**
  * @brief  USART GPIO 配置,工作参数配置
  * @param  无
  * @retval 无
  */
void USART1_Config(u32 baud)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	// 打开串口GPIO的时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	// 打开串口外设的时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	// 将USART Tx的GPIO配置为推挽复用模式
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

  // 将USART Rx的GPIO配置为浮空输入模式
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	if(baud < 2400)
	{
		baud = 115200;
	}
	
	// 配置串口的工作参数
	// 配置波特率
	USART_InitStructure.USART_BaudRate = baud;
	// 配置 针数据字长
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	// 配置停止位
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	// 配置校验位
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	// 配置硬件流控制
	USART_InitStructure.USART_HardwareFlowControl = 
	USART_HardwareFlowControl_None;
	// 配置工作模式，收发一起
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	// 完成串口的初始化配置
	USART_Init(USART1, &USART_InitStructure);
	
	// 串口中断优先级配置
	NVIC_Config();
	
	// 使能串口接收中断
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);	
	
	// 使能串口
	USART_Cmd(USART1, ENABLE);	    
}

/*------------------  发送一个字节 -------------------*/
void USART1_SendByte(uint8_t data)
{
  /* 发送一个字节数据到USART */
  USART_SendData(USART1, data);
	
  /* 等待发送数据寄存器为空 */
  while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
}

/*------------------  发送一组数据 -------------------*/
void  USART1_SendData(uint8_t *ch,uint8_t length)
{
  uint8_t i;
	for(i=0;i<length;i++)
	{
	  USART1_SendByte(*ch++);
	}
}

/*------------------ 发送一个16位数 ------------------*/
void USART1_SendHalfWord( uint16_t data)
{
 uint8_t temp_h, temp_l;
	
 /* 取出高八位 */
 temp_h = (data & 0xff00) >> 8 ;
 /* 取出低八位 */
 temp_l =  data & 0x00ff ;
 
 /* 发送高八位 */
 USART1_SendByte(temp_h); 

 /* 发送低八位 */
 USART1_SendByte(temp_l); 
} 

/*------------------- 发送8位的数组 ------------------*/
void USART1_SendArray( uint8_t *array, uint8_t num)
{
	uint8_t i;
	for( i=0; i<num; i++)
	{
		/* 发送一个字节数据到USART */
	  USART1_SendByte(array[i]);
	}
	/* 等待发送完成 */
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
}

/*----------------------  发送字符串 ---------------------*/
void USART1_SendString( uint8_t *str)
{
	uint8_t i=0;
  do
	{
	  USART1_SendByte( *(str + i));
		i++;
	}while(*(str + i) != '\0');
	
	/* 等待发送完成 */
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
}

/*--------------------- 缓存区收发数据 --------------------*/
//数据发送错误或格式不正确会发送错误代码 00 00 00 00 00
void USART1_SendBUF(void)
{
	u8 len;
  u8 Error[] = "Receive ERROR!"; 
	if(rxflag1 == 1)
  {
		rxflag1 = 0;
		
		if(USART1_RX_STA&0x8000)
		{
			len = USART1_RX_STA & 0x3fff;
			USART1_SendData(USART1_RX_BUF,len);
			USART1_RX_STA=0;
		}
		else if(rxError1 == 1)
		{ 		
			rxError1 = 0;
		  USART1_SendData(Error,15); //回复错误提示			 
		}		
	}
	
}

 /**
  * @brief  USART接收中断
  * @param  无
  * @retval 无
  */
//注:接收的数据格式 _ _ _ _ _ _ \r\n ,后两个数据为帧尾(换行符)，不计算在内
void USART1_IRQHandler(void)
{
  uint8_t ucTemp;
	if( USART_GetITStatus( USART1, USART_IT_RXNE) !=RESET)
	{
		USART_ClearITPendingBit(USART1, USART_IT_RXNE);
		rxflag1 = 1;
		
	  ucTemp = USART_ReceiveData(USART1);  //接收的数据赋值给ucTemp
		USART1_SendByte(ucTemp);
//    if((USART1_RX_STA&0x8000)==0) //接收未完成
//		{
//		  if(USART1_RX_STA&0x4000)
//			{
//				if(ucTemp!=0x0a)
//				{
//					USART1_RX_STA=0;//接收错误,重新开始
//					rxError1 = 0;
//				}
//				else USART1_RX_STA|=0x8000;	//接收完成了			
//			}
//			else 
//			{
//				if(ucTemp==0x0d) USART1_RX_STA|=0x4000;
//				else 
//				{
//				  USART1_RX_BUF[USART1_RX_STA&0X3FFF]=ucTemp;
//					USART1_RX_STA++;
//					if(USART1_RX_STA>(USART1_RX_LEN-1)) USART1_RX_STA=0;//接收数据错误,重新开始接收
//				}
//			}
//		}
	}
}


///重定向c库函数printf到串口，重定向后可使用printf函数
int fputc(int ch, FILE *f)
{
		/* 发送一个字节数据到串口 */
		USART_SendData(USART1, (uint8_t) ch);
		
		/* 等待发送完毕 */
		while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);		
	
		return (ch);
}

///重定向c库函数scanf到串口，重写向后可使用scanf、getchar等函数
int fgetc(FILE *f)
{
		/* 等待串口输入数据 */
		while (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET);

		return (int)USART_ReceiveData(USART1);
}

/*--------------------- END OF FIAL-------------------*/
