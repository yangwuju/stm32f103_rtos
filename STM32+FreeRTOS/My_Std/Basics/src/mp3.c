#include "mp3.h"

u8 Version = 0xFF;
static u8 buffer[64];      //发送数据缓冲区
u8 Feedback=0;   // =1反馈信息  =0不反馈

//累加和校验
u16 sum16Value(u8 *dat,u8 len)
{
	u16 sum=0xffff;
	dat++; //去掉帧头校验
	while(len--)
		sum -= *dat++; 
	return sum+1;	//累加取补码
}

//向MP3发送一帧数据
void MP3_SendCMD(u8 cmd,u16 para,u8 len)
{
	u16 sumCheck;
	
	buffer[0] = 0x7E;
	buffer[1] = Version;
	buffer[2] = len;
	buffer[3] = cmd;
	buffer[4] = Feedback;
	buffer[5] = (u8)(para>>8);
	buffer[6] = (u8)(para&0x00ff);
	sumCheck = sum16Value(buffer,len);
	buffer[7] = (u8)(sumCheck>>8);
	buffer[8] = (u8)(sumCheck&0x00ff);
	buffer[9] = 0xEF;
	Uart_SendData(buffer,len+4);
}

void Uart_DFPlay(u8 cmd,u16 para)
{
	MP3_SendCMD(cmd,para,6);
}
