#include "time2.h"

//TIM2 GPIO端口定义
static void TIM2_GPIO_Config(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
	 
  RCC_APB2PeriphClockCmd(TIM2_CHx_GPIO_CLK, ENABLE);	//使能USART1，GPIOA时钟
	
  GPIO_PinRemapConfig(TIM2_GPIO_PinRemap,ENABLE);
	
  GPIO_InitStructure.GPIO_Pin = TIM2_CHx_PIN; 
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	
  GPIO_Init(TIM2_CHx_PORT, &GPIO_InitStructure);
}

//TIM2 模式配置
static void TIM2_Mode_Config(u16 arr,u16 psc)
{
  // 开启定时器时钟,即内部时钟CK_INT=72M
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);

/*--------------------时基结构体初始化-------------------------*/
	// 配置周期，这里配置为100K
	
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	// 自动重装载寄存器的值，累计TIM_Period+1个频率后产生一个更新或者中断
	TIM_TimeBaseStructure.TIM_Period= arr;	
	// 驱动CNT计数器的时钟 = Fck_int/(psc+1)
	TIM_TimeBaseStructure.TIM_Prescaler= psc;	
	// 时钟分频因子 ，配置死区时间时需要用到
	TIM_TimeBaseStructure.TIM_ClockDivision=TIM_CKD_DIV1;		
	// 计数器计数模式，设置为向上计数
	TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up;		
	// 重复计数器的值，没用到不用管
	TIM_TimeBaseStructure.TIM_RepetitionCounter=0;	
	// 初始化定时器
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

	/*--------------------输出比较结构体初始化-------------------*/
	
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	// 配置为PWM模式1
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	// 输出使能
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	// 输出通道电平极性配置	
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	// 输出比较通道 1
	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OC1Init( TIM2, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig( TIM2, TIM_OCPreload_Enable);
	
	// 使能计数器
	TIM_Cmd(TIM2, ENABLE);
}

//TIM2 初始化
void TIM2_Init(u16 arr,u16 psc)
{
	TIM2_GPIO_Config();
	TIM2_Mode_Config(arr,psc);
}

//TIM2 输出比较通道的比较值，可调节占空比
void TIM2_SetCompare(u16 ccr)
{
	TIM2_SetCCRx(TIM2,ccr);
}


/*--------------------- END OF FIAL-------------------*/
