/************************************************************************
 * 文件名  ：motor.c
 * 描述    ：步进电机（5线四相）驱动函数配置
 *          
 * 实验平台：STM32开发板 基于STM32F103C8T6
 * 硬件连接：-------------------------------------------------------------
 *          VCC ：接VCC（5.5V）
 *          GND ：接地
 *          IN1 ：接PB6
 *          IN2 ：接PB7
 *          IN3 ：接PB8
 *          IN4 ：接PB9 
 * 说明    ：步进角度5.625*1/64  减速比1/64
 * 注意事项：																										  
*************************************************************************/

#include "motor.h"
#include "delay.h"

void Motor_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
  RCC_APB2PeriphClockCmd(MOTOR_IN1_CLK|MOTOR_IN2_CLK|MOTOR_IN3_CLK|MOTOR_IN4_CLK, ENABLE);
	
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	
	GPIO_InitStructure.GPIO_Pin = MOTOR_IN1_PIN;
	GPIO_Init(MOTOR_IN1_PORT,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = MOTOR_IN2_PIN;
	GPIO_Init(MOTOR_IN2_PORT,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = MOTOR_IN3_PIN;
	GPIO_Init(MOTOR_IN3_PORT,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = MOTOR_IN4_PIN;
	GPIO_Init(MOTOR_IN4_PORT,&GPIO_InitStructure);
	
  GPIO_ResetBits(MOTOR_IN1_PORT, MOTOR_IN1_PIN);
	GPIO_ResetBits(MOTOR_IN2_PORT, MOTOR_IN2_PIN);
	GPIO_ResetBits(MOTOR_IN3_PORT, MOTOR_IN3_PIN);
	GPIO_ResetBits(MOTOR_IN4_PORT, MOTOR_IN4_PIN);
	
}


//一个脉冲转动 5.625*1/64（步进角度）
//一圈lap = 360°/（5.625*1/64） = 4096个脉冲 
//一次产生4个脉冲，所以需要循环4096/4 =512次，才会转动一圈

//步进电机
//mode（MOTOR_CW[0] ：顺时针转动  MOTOR_CCW[1]：逆时针转动 MOTOR_STOP[2]：停止转动）
//lap(转动的圈数) 【注：在STOP时表示停转时长（s）】
void Motor_RunMode(uint8_t mode,uint8_t lap)
{
	uint16_t i,j,meter;
	for(i=0; i<512*lap; i++)
	{
		for(j=0; j<8; j++)
		{
			switch(meter)
			{
				case 0: MOTOR_IN1_Set;MOTOR_IN2_Clr;MOTOR_IN3_Clr;MOTOR_IN4_Clr; break;
				case 1: MOTOR_IN1_Set;MOTOR_IN2_Set;MOTOR_IN3_Clr;MOTOR_IN4_Clr; break;
				case 2: MOTOR_IN1_Clr;MOTOR_IN2_Set;MOTOR_IN3_Clr;MOTOR_IN4_Clr; break;
				case 3: MOTOR_IN1_Clr;MOTOR_IN2_Set;MOTOR_IN3_Set;MOTOR_IN4_Clr; break;
				case 4: MOTOR_IN1_Clr;MOTOR_IN2_Clr;MOTOR_IN3_Set;MOTOR_IN4_Clr; break;
				case 5: MOTOR_IN1_Clr;MOTOR_IN2_Clr;MOTOR_IN3_Set;MOTOR_IN4_Set; break;
				case 6: MOTOR_IN1_Clr;MOTOR_IN2_Clr;MOTOR_IN3_Clr;MOTOR_IN4_Set; break;
				case 7: MOTOR_IN1_Set;MOTOR_IN2_Clr;MOTOR_IN3_Clr;MOTOR_IN4_Set; break;	//采用半步方式，四个脉冲
				
				default:MOTOR_IN1_Clr;MOTOR_IN2_Clr;MOTOR_IN3_Clr;MOTOR_IN4_Clr; break;			
			}
			//电机顺时针转动
			if(mode == MOTOR_CW )
			{
				meter = 7-j;
				Delay_Ms(1);    //延时（速度）
			}
			//电机逆时针转动
			if(mode == MOTOR_CCW )
			{
				meter = j;
				Delay_Ms(1);
			}
      //电机停转        //延时（速度）
		  if(mode == MOTOR_STOP)
			{
				meter = 8;
				Delay_Us(244);  //停转时长： 244us*4096 = 1s
			}
	  }
 }
}

//直流电机
void Motor_Mode(uint8_t n)
{
  switch(n)
	{
		case 0 : MOTOR_IN1_Clr; MOTOR_IN2_Clr; MOTOR_IN3_Clr; MOTOR_IN4_Clr; break; //停止

    case 1 : MOTOR_IN1_Clr; MOTOR_IN2_Set; MOTOR_IN3_Clr; MOTOR_IN4_Set; break; //前进
		case 2 : MOTOR_IN1_Set; MOTOR_IN2_Clr; MOTOR_IN3_Set; MOTOR_IN4_Clr; break; //后退

		case 3 : MOTOR_IN1_Clr; MOTOR_IN2_Set; MOTOR_IN3_Set; MOTOR_IN4_Clr; break; //左转
		case 4 : MOTOR_IN1_Set; MOTOR_IN2_Clr; MOTOR_IN3_Clr; MOTOR_IN4_Set; break; //右转
  }
}
