# 〇. 入门基础

## 1. 延时

vTaskDelay(t): 阻塞延时

vTaskDelayUntil(t1,t2): 在t1时刻等待t2结束（阻塞延时t1+t2）







# 一.	静态创建任务

```
	/* 创建任务句柄 */
​	static TaskHandle_t AppTaskCreate_Handle;
​	/* AppTaskCreate任务任务堆栈 */
​	static StackType_t AppTaskCreate_Stack[128];

​	/* AppTaskCreate 任务控制块 */
​	static StaticTask_t AppTaskCreate_TCB;

​	/* 空闲任务任务堆栈 */
​	static StackType_t Idle_Task_Stack[configMINIMAL_STACK_SIZE];
​	/* 定时器任务堆栈 */
​	static StackType_t Timer_Task_Stack[configTIMER_TASK_STACK_DEPTH];

​	/* 空闲任务控制块 */
​	static StaticTask_t Idle_Task_TCB;	
​	/* 定时器任务控制块 */
​	static StaticTask_t Timer_Task_TCB;

​	/* 启动任务 */
​	static void AppTaskCreate(void)
​	{
​	  taskENTER_CRITICAL();           //进入临界区

​	  /* 创建任务 */




​	  vTaskDelete(AppTaskCreate_Handle); //删除AppTaskCreate任务
​	  
​	  taskEXIT_CRITICAL();            //退出临界区
​	}

​	int main(void)
​	{	
​		/* 开发板硬件初始化 */
​		BSP_Init();
​		/* 创建 AppTaskCreate 任务 */
​		AppTaskCreate_Handle = xTaskCreateStatic((TaskFunction_t	)AppTaskCreate,		//任务函数
​																(const char* 	)"AppTaskCreate",		//任务名称
​																(uint32_t 		)128,	//任务堆栈大小
​																(void* 		  	)NULL,				//传递给任务函数的参数
​																(UBaseType_t 	)3, 	//任务优先级
​																(StackType_t*   )AppTaskCreate_Stack,	//任务堆栈
​																(StaticTask_t*  )&AppTaskCreate_TCB);	//任务控制块   
​																
​		if(NULL != AppTaskCreate_Handle)/* 创建成功 */
​		vTaskStartScheduler();   /* 启动任务，开启调度 */
​		else 
​			return -1;
​		while(1);   /* 正常不会执行到这里 */  
​	}

​	/*获取空闲任务的任务堆栈和任务控制块内存*/
​	void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer, 
​									   StackType_t **ppxIdleTaskStackBuffer, 
​									   uint32_t *pulIdleTaskStackSize)
​	{
​		*ppxIdleTaskTCBBuffer=&Idle_Task_TCB;/* 任务控制块内存 */
​		*ppxIdleTaskStackBuffer=Idle_Task_Stack;/* 任务堆栈内存 */
​		*pulIdleTaskStackSize=configMINIMAL_STACK_SIZE;/* 任务堆栈大小 */
​	}

​	/*获取定时器任务的任务堆栈和任务控制块内存*/
​	void vApplicationGetTimerTaskMemory(StaticTask_t **ppxTimerTaskTCBBuffer, 
​										StackType_t **ppxTimerTaskStackBuffer, 
​										uint32_t *pulTimerTaskStackSize)
​	{
​		*ppxTimerTaskTCBBuffer=&Timer_Task_TCB;/* 任务控制块内存 */
​		*ppxTimerTaskStackBuffer=Timer_Task_Stack;/* 任务堆栈内存 */
​		*pulTimerTaskStackSize=configTIMER_TASK_STACK_DEPTH;/* 任务堆栈大小 */
​	}
```

# 二.	动态创建任务

​	

```
	/* 创建任务句柄 */
​	static TaskHandle_t AppTaskCreate_Handle = NULL;

​	/* 启动任务 */
​	static void AppTaskCreate(void)
​	{
​	  BaseType_t xReturn = pdPASS;/* 定义一个创建信息返回值，默认为pdPASS */
​	  
​	  taskENTER_CRITICAL();           //进入临界区
​	  
​	  /* 创建LED_Task任务 */
​	  
​	  vTaskDelete(AppTaskCreate_Handle); //删除AppTaskCreate任务
​	  
​	  taskEXIT_CRITICAL();            //退出临界区
​	}

​	int main(void)
​	{	
​	  BaseType_t xReturn = pdPASS;/* 定义一个创建信息返回值，默认为pdPASS */

​	  /* 开发板硬件初始化 */
​	  BSP_Init();

​	   /* 创建AppTaskCreate任务 */
​	  xReturn = xTaskCreate((TaskFunction_t )AppTaskCreate,  /* 任务入口函数 */
​							(const char*    )"AppTaskCreate",/* 任务名字 */
​							(uint16_t       )512,  /* 任务栈大小 */
​							(void*          )NULL,/* 任务入口函数参数 */
​							(UBaseType_t    )1, /* 任务的优先级 */
​							(TaskHandle_t*  )&AppTaskCreate_Handle);/* 任务控制块指针 */ 
​	  /* 启动任务调度 */           
​	  if(pdPASS == xReturn)
​		vTaskStartScheduler();   /* 启动任务，开启调度 */
​	  else
​		return -1;  
​	  
​	  while(1);   /* 正常不会执行到这里 */    
​	}
```

# 三. 任务管理（挂起与恢复）

```
	  printf("挂起LED任务！\n");
​	  vTaskSuspend(Task_Handle);/* 挂起LED任务 */
​	  printf("挂起LED任务成功！\n");

​	  printf("恢复LED任务！\n");
​	  vTaskResume(Task_Handle);/* 恢复LED任务！ */
​	  printf("恢复LED任务成功！\n");
```

# 四.	消息队列

```
	QueueHandle_t Test_Queue =NULL;
​	#define  QUEUE_LEN    4   /* 队列的长度，最大可包含多少个消息 */
​	#define  QUEUE_SIZE   4   /* 队列中每个消息大小（字节） */

​	//在启动任务中--------------------------------------------
​	  /* 创建Test_Queue */
​	  Test_Queue = xQueueCreate((UBaseType_t ) QUEUE_LEN,/* 消息队列的长度 */
​								(UBaseType_t ) QUEUE_SIZE);/* 消息的大小 */
​	  if(NULL != Test_Queue)
​		printf("创建Test_Queue消息队列成功!\r\n");
​	  
​	//发送消息------------------------------------------------
​	  uint32_t send_data1 = 1;
​	  printf("发送消息send_data1！\n");
​	  xReturn = xQueueSend( Test_Queue, /* 消息队列的句柄 */
​							&send_data1,/* 发送的消息内容 */
​							0 );        /* 等待时间 0 */
​	  if(pdPASS == xReturn)
​		printf("消息send_data1发送成功!\n\n");

​	//接收消息------------------------------------------------
​		uint32_t r_queue;	/* 定义一个接收消息的变量 */
​		xReturn = xQueueReceive( Test_Queue,    /* 消息队列的句柄 */
​								 &r_queue,      /* 发送的消息内容 */
​								 portMAX_DELAY); /* 等待时间一直等 */
​		if(pdTRUE == xReturn)
​		  printf("本次接收到的数据是%d\n\n",r_queue);
​		else
​		  printf("数据接收出错,错误代码0x%lx\n",xReturn);
```

# 五.	二值信号量

		/* 定义一个信号量 */
		SemaphoreHandle_t BinarySem_Handle =NULL;
	
		//创建 BinarySem------------------------------------------------------------
		BinarySem_Handle = xSemaphoreCreateBinary();	 
		if(NULL != BinarySem_Handle)
			printf("BinarySem_Handle二值信号量创建成功!\r\n");
	
		//给出二值信号量------------------------------------------------------------
		xReturn = xSemaphoreGive( BinarySem_Handle );
		if( xReturn == pdTRUE )
			printf("BinarySem_Handle二值信号量释放成功!\r\n");
		else
			rintf("BinarySem_Handle二值信号量释放失败!\r\n");
	
		//获取二值信号量 xSemaphore,没获取到则一直等待------------------------------
		xReturn = xSemaphoreTake(BinarySem_Handle,/* 二值信号量句柄 */
								  portMAX_DELAY); /* 等待时间 */
		if(pdTRUE == xReturn)
		  printf("BinarySem_Handle二值信号量获取成功!\n\n");

# 六.	计数信号量

```
	/* 定义一个信号量 */
​	SemaphoreHandle_t CountSem_Handle =NULL;

​	//创建Test_Queue---------------------------------------------------------- 
​	  CountSem_Handle = xSemaphoreCreateCounting(5,5);	 
​	  if(NULL != CountSem_Handle)
​		printf("CountSem_Handle计数信号量创建成功!\r\n");
​	//获取一个计数信号量------------------------------------------------------
​	  xReturn = xSemaphoreTake(CountSem_Handle,	/* 计数信号量句柄 */
​						 0); 	/* 等待时间：0 */
​		if ( pdTRUE == xReturn ) 
​			printf( "成功申请到停车位。\n" );
​		else
​			printf( "现在停车场已满！\n" );		
​		
​	//释放计数信号量----------------------------------------------------------
​	  xReturn = xSemaphoreGive(CountSem_Handle);                 
​			if ( pdTRUE == xReturn ) 
​				printf( "KEY2被按下，释放1个停车位。\n" );
​			else
​				printf( "KEY2被按下，但已无车位可以释放！\n" );		
```

# 七.	优先级翻转

# 八.	互斥量

```
	/* 定义一个信号量 */
​	SemaphoreHandle_t MuxSem_Handle =NULL;
​	//创建MuxSem-------------------------------------------------------------
​	MuxSem_Handle = xSemaphoreCreateMutex();	 
​	if(NULL != MuxSem_Handle)
​		printf("MuxSem_Handle互斥量创建成功!\r\n");

​	//获取互斥量 MuxSem,没获取到则一直等待-----------------------------------
​	xReturn = xSemaphoreTake(MuxSem_Handle,/* 互斥量句柄 */
​								  portMAX_DELAY); /* 等待时间 */
​								  
​	//给出互斥量-------------------------------------------------------------
​	  xReturn = xSemaphoreGive( MuxSem_Handle );
```

# 九.	事件

			static EventGroupHandle_t Event_Handle =NULL;
			//创建 Event_Handle------------------------------------------------------
		  	Event_Handle = xEventGroupCreate();	 
		  	if(NULL != Event_Handle)
			printf("Event_Handle 事件创建成功!\r\n");
	
			//等待事件触发
			/*******************************************************************
			 * 等待接收事件标志 
			 * 
			 * 如果xClearOnExit设置为pdTRUE，那么在xEventGroupWaitBits()返回之前，
			 * 如果满足等待条件（如果函数返回的原因不是超时），那么在事件组中设置
			 * 的uxBitsToWaitFor中的任何位都将被清除。 
			 * 如果xClearOnExit设置为pdFALSE，
			 * 则在调用xEventGroupWaitBits()时，不会更改事件组中设置的位。
			 *
			 * xWaitForAllBits如果xWaitForAllBits设置为pdTRUE，则当uxBitsToWaitFor中
			 * 的所有位都设置或指定的块时间到期时，xEventGroupWaitBits()才返回。 
			 * 如果xWaitForAllBits设置为pdFALSE，则当设置uxBitsToWaitFor中设置的任何
			 * 一个位置1 或指定的块时间到期时，xEventGroupWaitBits()都会返回。 
			 * 阻塞时间由xTicksToWait参数指定。          
			  *********************************************************/
			r_event = xEventGroupWaitBits(Event_Handle,  /* 事件对象句柄 */
										  KEY1_EVENT,/* 接收线程感兴趣的事件 */
										  pdTRUE,   /* 退出时清除事件位 */
										  pdTRUE,   /* 满足感兴趣的所有事件 */
										  portMAX_DELAY);/* 指定超时事件,一直等 */
								
			if((r_event & KEY1_EVENT) == KEY1_EVENT) 
			{
			  /* 如果接收完成并且正确 */
			  printf ( "KEY1与KEY2都按下\n");		
			  LED1_TOGGLE;       //LED1	反转
			}
			else
			  printf ( "事件错误！\n");	
			}
			
			// 触发一个事件1-----------------------------------------------------------------
			xEventGroupSetBits(Event_Handle,KEY1_EVENT); 

# 十.	软件定时器

		  static TimerHandle_t Swtmr1_Handle =NULL;   /* 软件定时器句柄 */
	
		  //创建软件周期定时器-------------------------------------------------------------
		  Swtmr1_Handle=xTimerCreate((const char*		)"AutoReloadTimer",
									(TickType_t			)1000,/* 定时器周期 1000(tick) */
									(UBaseType_t		)pdTRUE,//pdTRUE为周期模式，pdFALS为单次模式
									(void*				  )1,/* 为每个计时器分配一个索引的唯一ID */
									(TimerCallbackFunction_t)Swtmr1_Callback); 
		  if(Swtmr1_Handle != NULL)                          
		  {
			/***********************************************************************************
			 * xTicksToWait:如果在调用xTimerStart()时队列已满，则以tick为单位指定调用任务应保持
			 * 在Blocked(阻塞)状态以等待start命令成功发送到timer命令队列的时间。 
			 * 如果在启动调度程序之前调用xTimerStart()，则忽略xTicksToWait。在这里设置等待时间为0.
			 **********************************************************************************/
			xTimerStart(Swtmr1_Handle,0);	//开启周期定时器
		  }    
		 //软件定时器1 回调函数----------------------------------------------------------  
		 static void Swtmr1_Callback(void* parameter)
		 {		
		   TickType_t tick_num1;
		   tick_num1 = xTaskGetTickCount();	/* 获取滴答定时器的计数值 */
		   printf("滴答定时器数值=%d\n", tick_num1);
		 }

# 十一. 任务通知

	    //获取任务通知 ,没获取到则一直等待------------------------------------------------------
		//1-整型数据  
		uint32_t r_num;
		xReturn=xTaskNotifyWait(0x0,			//进入函数的时候不清除任务bit
							ULONG_MAX,	  //退出函数的时候清除所有的bit
							&r_num,		  //保存任务通知值                        
							portMAX_DELAY);	//阻塞时间
		if( pdTRUE == xReturn )
		{
		  printf("Receive2_Task 任务通知消息为 %d \n",r_num);                      
		}					
		
		//2-字符型数据					
		char *r_char;
		xReturn=xTaskNotifyWait(0x0,			//进入函数的时候不清除任务bit
							ULONG_MAX,	  //退出函数的时候清除所有的bit
							&r_char,		  //保存任务通知值                        
							portMAX_DELAY);	//阻塞时间
		if( pdTRUE == xReturn )
		{
		  printf("Receive2_Task 任务通知消息为 %s \n",r_char);                                          
		}
							
		//发布任务通知
	
		uint32_t send1 = 2;
		//1-整型数据 
		  xReturn = xTaskNotify( Receive1_Task_Handle, /*任务句柄*/
								  send1, /* 发送的数据，最大为4字节 */
								 eSetValueWithOverwrite );/*覆盖当前通知*/
		//2-字符型数据
		  char test_str1[] = "this is a mail test 1";/* 邮箱消息test1 */
		  xReturn = xTaskNotify( Receive1_Task_Handle, /*任务句柄*/
							  test_str1, /* 发送的数据，最大为4字节 */
							 eSetValueWithOverwrite );/*覆盖当前通知*/
	
		  if( xReturn == pdPASS )
		  {
			printf("Receive1_Task_Handle 任务通知消息发送成功!\r\n");
		  } 

# 十二. 内存管理

```
		    /* 获取当前内存大小 */
			uint32_t g_memsize;
			g_memsize = xPortGetFreeHeapSize();
		    //申请空间		
			uint8_t *Test_Ptr = NULL;
			Test_Ptr = pvPortMalloc(1024);
		    //释放空间
			vPortFree(Test_Ptr);	//释放内存
```

# 十三. 中断管理

	        QueueHandle_t Test_Queue =NULL;
	
	        #define  QUEUE_LEN    4   /* 队列的长度，最大可包含多少个消息 */
	        #define  QUEUE_SIZE   4   /* 队列中每个消息大小（字节） */
	
	        //创建Test_Queue-------------------------------------------------------------------
	          Test_Queue = xQueueCreate((UBaseType_t ) QUEUE_LEN,/* 消息队列的长度 */
	                                    (UBaseType_t ) QUEUE_SIZE);/* 消息的大小 */
	
	        //EXTI中断服务程序-----------------------------------------------------------------
	        void KEY1_IRQHandler(void)
	        {
	            BaseType_t pxHigherPriorityTaskWoken;
	          //确保是否产生了EXTI Line中断
	          uint32_t ulReturn;
	          /* 进入临界段，临界段可以嵌套 */
	          ulReturn = taskENTER_CRITICAL_FROM_ISR();
	
	            if(EXTI_GetITStatus(KEY1_INT_EXTI_LINE) != RESET) 
	            {
	            /* 将数据写入（发送）到队列中，等待时间为 0  */
	                xQueueSendFromISR(Test_Queue, /* 消息队列的句柄 */
	                                                    &send_data1,/* 发送的消息内容 */
	                                                    &pxHigherPriorityTaskWoken);
	
	                //如果需要的话进行一次任务切换
	                portYIELD_FROM_ISR(pxHigherPriorityTaskWoken);
	
	                //清除中断标志位
	                EXTI_ClearITPendingBit(KEY1_INT_EXTI_LINE);     
	            }  
	
	          /* 退出临界段 */
	          taskEXIT_CRITICAL_FROM_ISR( ulReturn );
	        }
	
			//队列读取（接收），等待时间为一直等待----------------------------------------------
			xReturn = xQueueReceive( Test_Queue,    /* 消息队列的句柄 */
									 &r_queue,      /* 发送的消息内容 */
									 portMAX_DELAY); /* 等待时间 一直等 */
										
			if(pdPASS == xReturn)
			{
				printf("触发中断的是 KEY%d !\n",r_queue);
			}
			else
			{
				printf("数据接收出错\n");
			}

# 十四. CPU利用率

```
static void CPU_Task(void* parameter)
{	
      uint8_t CPU_RunInfo[400];		//保存任务运行时间信息

      while (1)
      {
        memset(CPU_RunInfo,0,400);				//信息缓冲区清零


        vTaskList((char *)&CPU_RunInfo);  //获取任务运行时间信息

        printf("---------------------------------------------\r\n");
        printf("任务名      任务状态 优先级   剩余栈 任务序号\r\n");
        printf("%s", CPU_RunInfo);
        printf("---------------------------------------------\r\n");

        memset(CPU_RunInfo,0,400);				//信息缓冲区清零

        vTaskGetRunTimeStats((char *)&CPU_RunInfo);

        printf("任务名       运行计数         利用率\r\n");
        printf("%s", CPU_RunInfo);
        printf("---------------------------------------------\r\n\n");
        vTaskDelay(1000);   /* 延时500个tick */		

      }
}
```



