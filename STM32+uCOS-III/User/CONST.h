#ifndef  __CONST__H
#define  __CONST__H


/*位带别名区地址*/
#define BITBAND(addr, bitnum) ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2)) 
//计算 bit 区地址偏移 
#define MEM_ADDR(addr) (*((vu32 *)(addr) )) //强制转换为指针 
#define BIT_ADDR(addr, bitnum) MEM_ADDR( BITBAND(addr, bitnum)) 

//n:0-15 
#define PAout(n)     BIT_ADDR((u32)&(GPIOA->ODR), n)  //输出
#define PAin(n)      BIT_ADDR((u32)&(GPIOA->IDR), n)  //输入 

#define PBout(n)     BIT_ADDR((u32)&(GPIOB->ODR), n)  //输出
#define PBin(n)      BIT_ADDR((u32)&(GPIOB->IDR), n)  //输入 

#define PCout(n)     BIT_ADDR((u32)&(GPIOC->ODR), n)  //输出
#define PCin(n)      BIT_ADDR((u32)&(GPIOC->IDR), n)  //输入 

#define PDout(n)     BIT_ADDR((u32)&(GPIOD->ODR), n)  //输出
#define PDin(n)      BIT_ADDR((u32)&(GPIOD->IDR), n)  //输入 

#define PEout(n)     BIT_ADDR((u32)&(GPIOE->ODR), n)  //输出
#define PEin(n)      BIT_ADDR((u32)&(GPIOE->IDR), n)  //输入 

#define PFout(n)     BIT_ADDR((u32)&(GPIOF->ODR), n)  //输出
#define PFin(n)      BIT_ADDR((u32)&(GPIOF->IDR), n)  //输入 

#define PGout(n)     BIT_ADDR((u32)&(GPIOG->ODR), n)  //输出
#define PGin(n)      BIT_ADDR((u32)&(GPIOG->IDR), n)  //输入 



#endif
