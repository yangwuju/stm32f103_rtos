
#ifndef   OS_H
#define   OS_H

/*
************************************************************************************************************************
*                                               uC/OS-III VERSION NUMBER
************************************************************************************************************************
*/

#define  OS_VERSION  30301u                       /* Version of uC/OS-III (Vx.yy.zz mult. by 10000)                   */

/*
************************************************************************************************************************
*                                                 INCLUDE HEADER FILES
************************************************************************************************************************
*/

#ifdef __cplusplus
extern "C" {
#endif



#include <os_cfg.h>
#include <cpu.h>
#include <cpu_core.h>
#include <lib_def.h>
#include <os_type.h>
#include <os_cpu.h>


/*
************************************************************************************************************************
*                                               CRITICAL SECTION HANDLING
************************************************************************************************************************
*/


#if      OS_CFG_SCHED_LOCK_TIME_MEAS_EN > 0u && defined(CPU_CFG_INT_DIS_MEAS_EN)
#define  OS_SCHED_LOCK_TIME_MEAS_START()    OS_SchedLockTimeMeasStart()
#else
#define  OS_SCHED_LOCK_TIME_MEAS_START()
#endif


#if      OS_CFG_SCHED_LOCK_TIME_MEAS_EN > 0u && defined(CPU_CFG_INT_DIS_MEAS_EN)
#define  OS_SCHED_LOCK_TIME_MEAS_STOP()     OS_SchedLockTimeMeasStop()
#else
#define  OS_SCHED_LOCK_TIME_MEAS_STOP()
#endif

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u                             /* Deferred ISR Posts ------------------------------ */
                                                                 /* Lock the scheduler                                */
#define  OS_CRITICAL_ENTER()                                       \
         do {                                                      \
             CPU_CRITICAL_ENTER();                                 \
             OSSchedLockNestingCtr++;                              \
             if (OSSchedLockNestingCtr == 1u) {                    \
                 OS_SCHED_LOCK_TIME_MEAS_START();                  \
             }                                                     \
             CPU_CRITICAL_EXIT();                                  \
         } while (0)
                                                                 /* Lock the scheduler but re-enable interrupts       */
#define  OS_CRITICAL_ENTER_CPU_EXIT()                              \
         do {                                                      \
             OSSchedLockNestingCtr++;                              \
                                                                   \
             if (OSSchedLockNestingCtr == 1u) {                    \
                 OS_SCHED_LOCK_TIME_MEAS_START();                  \
             }                                                     \
             CPU_CRITICAL_EXIT();                                  \
         } while (0)

                                                                 /* Scheduling occurs only if an interrupt occurs     */
#define  OS_CRITICAL_EXIT()                                        \
         do {                                                      \
             CPU_CRITICAL_ENTER();                                 \
             OSSchedLockNestingCtr--;                              \
             if (OSSchedLockNestingCtr == (OS_NESTING_CTR)0) {     \
                 OS_SCHED_LOCK_TIME_MEAS_STOP();                   \
                 if (OSIntQNbrEntries > (OS_OBJ_QTY)0) {           \
                     CPU_CRITICAL_EXIT();                          \
                     OS_Sched0();                                  \
                 } else {                                          \
                     CPU_CRITICAL_EXIT();                          \
                 }                                                 \
             } else {                                              \
                 CPU_CRITICAL_EXIT();                              \
             }                                                     \
         } while (0)

#define  OS_CRITICAL_EXIT_NO_SCHED()                               \
         do {                                                      \
             CPU_CRITICAL_ENTER();                                 \
             OSSchedLockNestingCtr--;                              \
             if (OSSchedLockNestingCtr == (OS_NESTING_CTR)0) {     \
                 OS_SCHED_LOCK_TIME_MEAS_STOP();                   \
             }                                                     \
             CPU_CRITICAL_EXIT();                                  \
         } while (0)


#else                                                            /* Direct ISR Posts -------------------------------- */


#define  OS_CRITICAL_ENTER()                    CPU_CRITICAL_ENTER()

#define  OS_CRITICAL_ENTER_CPU_EXIT()

#define  OS_CRITICAL_EXIT()                     CPU_CRITICAL_EXIT()

#define  OS_CRITICAL_EXIT_NO_SCHED()            CPU_CRITICAL_EXIT()

#endif

/*
************************************************************************************************************************
*                                                     MISCELLANEOUS
************************************************************************************************************************
*/

#ifdef   OS_GLOBALS
#define  OS_EXT
#else
#define  OS_EXT  extern
#endif


#define  OS_PRIO_TBL_SIZE          ((OS_CFG_PRIO_MAX - 1u) / (DEF_INT_CPU_NBR_BITS) + 1u)

#define  OS_MSG_EN                 (((OS_CFG_TASK_Q_EN > 0u) || (OS_CFG_Q_EN > 0u)) ? 1u : 0u)

/*$PAGE*/
/*
************************************************************************************************************************
************************************************************************************************************************
*                                                   # D E F I N E S
************************************************************************************************************************
************************************************************************************************************************
*/

/*
========================================================================================================================
*                                                      TASK STATUS
========================================================================================================================
*/

#define  OS_STATE_OS_STOPPED                    (OS_STATE)(0u)
#define  OS_STATE_OS_RUNNING                    (OS_STATE)(1u)

#define  OS_STATE_NOT_RDY                    (CPU_BOOLEAN)(0u)
#define  OS_STATE_RDY                        (CPU_BOOLEAN)(1u)


                                                                /* ------------------- TASK STATES ------------------ */
#define  OS_TASK_STATE_BIT_DLY               (OS_STATE)(0x01u)  /*   /-------- SUSPENDED bit                          */
                                                                /*   |                                                */
#define  OS_TASK_STATE_BIT_PEND              (OS_STATE)(0x02u)  /*   | /-----  PEND      bit                          */
                                                                /*   | |                                              */
#define  OS_TASK_STATE_BIT_SUSPENDED         (OS_STATE)(0x04u)  /*   | | /---  Delayed/Timeout bit                    */
                                                                /*   | | |                                            */
                                                                /*   V V V                                            */

#define  OS_TASK_STATE_RDY                    (OS_STATE)(  0u)  /*   0 0 0     Ready                                  */
#define  OS_TASK_STATE_DLY                    (OS_STATE)(  1u)  /*   0 0 1     Delayed or Timeout                     */
#define  OS_TASK_STATE_PEND                   (OS_STATE)(  2u)  /*   0 1 0     Pend                                   */
#define  OS_TASK_STATE_PEND_TIMEOUT           (OS_STATE)(  3u)  /*   0 1 1     Pend + Timeout                         */
#define  OS_TASK_STATE_SUSPENDED              (OS_STATE)(  4u)  /*   1 0 0     Suspended                              */
#define  OS_TASK_STATE_DLY_SUSPENDED          (OS_STATE)(  5u)  /*   1 0 1     Suspended + Delayed or Timeout         */
#define  OS_TASK_STATE_PEND_SUSPENDED         (OS_STATE)(  6u)  /*   1 1 0     Suspended + Pend                       */
#define  OS_TASK_STATE_PEND_TIMEOUT_SUSPENDED (OS_STATE)(  7u)  /*   1 1 1     Suspended + Pend + Timeout             */
#define  OS_TASK_STATE_DEL                    (OS_STATE)(255u)

                                                                /* ----------------- PENDING ON ... ----------------- */
#define  OS_TASK_PEND_ON_NOTHING              (OS_STATE)(  0u)  /* 在没有等待                                         */
#define  OS_TASK_PEND_ON_FLAG                 (OS_STATE)(  1u)  /* 在事件标志组上挂起                                 */
#define  OS_TASK_PEND_ON_TASK_Q               (OS_STATE)(  2u)  /* 待发送到任务的消息                                 */
#define  OS_TASK_PEND_ON_MULTI                (OS_STATE)(  3u)  /* 在多个信号量和/或队列上挂起                        */
#define  OS_TASK_PEND_ON_MUTEX                (OS_STATE)(  4u)  /* 等待互斥信号量                                     */
#define  OS_TASK_PEND_ON_Q                    (OS_STATE)(  5u)  /* 在队列中等待                                       */
#define  OS_TASK_PEND_ON_SEM                  (OS_STATE)(  6u)  /* 等待信号量上                                       */
#define  OS_TASK_PEND_ON_TASK_SEM             (OS_STATE)(  7u)  /* 等待发送到任务的信号                               */

/*
------------------------------------------------------------------------------------------------------------------------
*                                                    TASK PEND STATUS
*                                      (Status codes for OS_TCBs field .PendStatus)
------------------------------------------------------------------------------------------------------------------------
*/

#define  OS_STATUS_PEND_OK                   (OS_STATUS)(  0u)  /* 待定状态OK    */
#define  OS_STATUS_PEND_ABORT                (OS_STATUS)(  1u)  /* 待定异常退出  */
#define  OS_STATUS_PEND_DEL                  (OS_STATUS)(  2u)  /* 等待对象删除  */
#define  OS_STATUS_PEND_TIMEOUT              (OS_STATUS)(  3u)  /* 等待超时      */

/*
========================================================================================================================
*                                                   OS OBJECT TYPES
*
* Note(s) : (1) OS_OBJ_TYPE_&&& #define values specifically chosen as ASCII representations of the kernel
*               object types.  Memory displays of kernel objects will display the kernel object TYPEs with
*               their chosen ASCII names.
========================================================================================================================
*/

#define  OS_OBJ_TYPE_NONE                    (OS_OBJ_TYPE)CPU_TYPE_CREATE('N', 'O', 'N', 'E')
#define  OS_OBJ_TYPE_FLAG                    (OS_OBJ_TYPE)CPU_TYPE_CREATE('F', 'L', 'A', 'G')
#define  OS_OBJ_TYPE_MEM                     (OS_OBJ_TYPE)CPU_TYPE_CREATE('M', 'E', 'M', ' ')
#define  OS_OBJ_TYPE_MUTEX                   (OS_OBJ_TYPE)CPU_TYPE_CREATE('M', 'U', 'T', 'X')
#define  OS_OBJ_TYPE_Q                       (OS_OBJ_TYPE)CPU_TYPE_CREATE('Q', 'U', 'E', 'U')
#define  OS_OBJ_TYPE_SEM                     (OS_OBJ_TYPE)CPU_TYPE_CREATE('S', 'E', 'M', 'A')
#define  OS_OBJ_TYPE_TASK_MSG                (OS_OBJ_TYPE)CPU_TYPE_CREATE('T', 'M', 'S', 'G')
#define  OS_OBJ_TYPE_TASK_RESUME             (OS_OBJ_TYPE)CPU_TYPE_CREATE('T', 'R', 'E', 'S')
#define  OS_OBJ_TYPE_TASK_SIGNAL             (OS_OBJ_TYPE)CPU_TYPE_CREATE('T', 'S', 'I', 'G')
#define  OS_OBJ_TYPE_TASK_SUSPEND            (OS_OBJ_TYPE)CPU_TYPE_CREATE('T', 'S', 'U', 'S')
#define  OS_OBJ_TYPE_TICK                    (OS_OBJ_TYPE)CPU_TYPE_CREATE('T', 'I', 'C', 'K')
#define  OS_OBJ_TYPE_TMR                     (OS_OBJ_TYPE)CPU_TYPE_CREATE('T', 'M', 'R', ' ')

/*
========================================================================================================================
*                                           Possible values for 'opt' argument
========================================================================================================================
*/

#define  OS_OPT_NONE                         (OS_OPT)(0x0000u)

/*
------------------------------------------------------------------------------------------------------------------------
*                                                    DELETE OPTIONS
------------------------------------------------------------------------------------------------------------------------
*/

#define  OS_OPT_DEL_NO_PEND                  (OS_OPT)(0x0000u)
#define  OS_OPT_DEL_ALWAYS                   (OS_OPT)(0x0001u)

/*
------------------------------------------------------------------------------------------------------------------------
*                                                     PEND OPTIONS
------------------------------------------------------------------------------------------------------------------------
*/

#define  OS_OPT_PEND_FLAG_MASK               (OS_OPT)(0x000Fu)
#define  OS_OPT_PEND_FLAG_CLR_ALL            (OS_OPT)(0x0001u)  /* 等待所有指定为CLR的位    */
#define  OS_OPT_PEND_FLAG_CLR_AND            (OS_OPT)(0x0001u)

#define  OS_OPT_PEND_FLAG_CLR_ANY            (OS_OPT)(0x0002u)  /* 等待指定为CLR的任何位    */
#define  OS_OPT_PEND_FLAG_CLR_OR             (OS_OPT)(0x0002u)

#define  OS_OPT_PEND_FLAG_SET_ALL            (OS_OPT)(0x0004u)  /* 等待所有指定的位被设置   */
#define  OS_OPT_PEND_FLAG_SET_AND            (OS_OPT)(0x0004u)

#define  OS_OPT_PEND_FLAG_SET_ANY            (OS_OPT)(0x0008u)  /* 等待指定的任何位被设置   */
#define  OS_OPT_PEND_FLAG_SET_OR             (OS_OPT)(0x0008u)

#define  OS_OPT_PEND_FLAG_CONSUME            (OS_OPT)(0x0100u)  /* 如果条件满足，则使用标志 */


#define  OS_OPT_PEND_BLOCKING                (OS_OPT)(0x0000u)
#define  OS_OPT_PEND_NON_BLOCKING            (OS_OPT)(0x8000u)

/*
------------------------------------------------------------------------------------------------------------------------
*                                                  PEND ABORT OPTIONS
------------------------------------------------------------------------------------------------------------------------
*/

#define  OS_OPT_PEND_ABORT_1                 (OS_OPT)(0x0000u)  /* 中止一个等待的任务        */
#define  OS_OPT_PEND_ABORT_ALL               (OS_OPT)(0x0100u)  /* 挂起，中止所有等待的任务  */

/*
------------------------------------------------------------------------------------------------------------------------
*                                                     POST OPTIONS
------------------------------------------------------------------------------------------------------------------------
*/


#define  OS_OPT_POST_NONE                    (OS_OPT)(0x0000u)

#define  OS_OPT_POST_FLAG_SET                (OS_OPT)(0x0000u)
#define  OS_OPT_POST_FLAG_CLR                (OS_OPT)(0x0001u)

#define  OS_OPT_POST_FIFO                    (OS_OPT)(0x0000u)  /* 默认是发布FIFO                   */
#define  OS_OPT_POST_LIFO                    (OS_OPT)(0x0010u)  /* 发送到最高优先级的任务等待       */
#define  OS_OPT_POST_1                       (OS_OPT)(0x0000u)  /* 发送消息到最高优先级的任务等待   */
#define  OS_OPT_POST_ALL                     (OS_OPT)(0x0200u)  /* 向所有等待的任务广播消息         */

#define  OS_OPT_POST_NO_SCHED                (OS_OPT)(0x8000u)  /* 如果选中了这个，不要调用调度程序 */

/*
------------------------------------------------------------------------------------------------------------------------
*                                                     TASK OPTIONS
------------------------------------------------------------------------------------------------------------------------
*/

#define  OS_OPT_TASK_NONE                    (OS_OPT)(0x0000u)  /* 未选择任何选项           */
#define  OS_OPT_TASK_STK_CHK                 (OS_OPT)(0x0001u)  /* 启用任务的堆栈检查       */
#define  OS_OPT_TASK_STK_CLR                 (OS_OPT)(0x0002u)  /* 任务创建时清除堆栈       */
#define  OS_OPT_TASK_SAVE_FP                 (OS_OPT)(0x0004u)  /* 保存任何浮点寄存器的内容 */
#define  OS_OPT_TASK_NO_TLS                  (OS_OPT)(0x0008u)  /* 指定任务不需要 TLS 支持  */

/*
------------------------------------------------------------------------------------------------------------------------
*                                                     TIME OPTIONS
------------------------------------------------------------------------------------------------------------------------
*/

#define  OS_OPT_TIME_DLY                    DEF_BIT_NONE
#define  OS_OPT_TIME_TIMEOUT                ((OS_OPT)DEF_BIT_01)
#define  OS_OPT_TIME_MATCH                  ((OS_OPT)DEF_BIT_02)
#define  OS_OPT_TIME_PERIODIC               ((OS_OPT)DEF_BIT_03)

#define  OS_OPT_TIME_HMSM_STRICT            ((OS_OPT)DEF_BIT_NONE)
#define  OS_OPT_TIME_HMSM_NON_STRICT        ((OS_OPT)DEF_BIT_04)

#define  OS_OPT_TIME_MASK                   ((OS_OPT)(OS_OPT_TIME_DLY      | \
                                                      OS_OPT_TIME_TIMEOUT  | \
                                                      OS_OPT_TIME_PERIODIC | \
                                                      OS_OPT_TIME_MATCH))

#define  OS_OPT_TIME_OPTS_MASK                       (OS_OPT_TIME_DLY            | \
                                                      OS_OPT_TIME_TIMEOUT        | \
                                                      OS_OPT_TIME_PERIODIC       | \
                                                      OS_OPT_TIME_MATCH          | \
                                                      OS_OPT_TIME_HMSM_NON_STRICT)

/*
------------------------------------------------------------------------------------------------------------------------
*                                                    TIMER OPTIONS
------------------------------------------------------------------------------------------------------------------------
*/

#define  OS_OPT_TMR_NONE                          (OS_OPT)(0u)  /* 没有选项被选中                                     */

#define  OS_OPT_TMR_ONE_SHOT                      (OS_OPT)(1u)  /* 定时器过期时不会自动重启                           */
#define  OS_OPT_TMR_PERIODIC                      (OS_OPT)(2u)  /* 定时器到期时将自动重启                             */

#define  OS_OPT_TMR_CALLBACK                      (OS_OPT)(3u)  /* OSTmrStop()选项调用'callback'带定时器参数          */
#define  OS_OPT_TMR_CALLBACK_ARG                  (OS_OPT)(4u)  /* OSTmrStop()选项调用'callback'带新参数              */

/*
------------------------------------------------------------------------------------------------------------------------
*                                                     TIMER STATES
------------------------------------------------------------------------------------------------------------------------
*/

#define  OS_TMR_STATE_UNUSED                    (OS_STATE)(0u)
#define  OS_TMR_STATE_STOPPED                   (OS_STATE)(1u)
#define  OS_TMR_STATE_RUNNING                   (OS_STATE)(2u)
#define  OS_TMR_STATE_COMPLETED                 (OS_STATE)(3u)

/*
------------------------------------------------------------------------------------------------------------------------
*                                                       PRIORITY
------------------------------------------------------------------------------------------------------------------------
*/
                                                                    /* 默认 prio初始化任务TCB                         */
#define  OS_PRIO_INIT                       (OS_PRIO)(OS_CFG_PRIO_MAX)

/*
------------------------------------------------------------------------------------------------------------------------
*                                                 TIMER TICK THRESHOLDS
------------------------------------------------------------------------------------------------------------------------
*/
                                                                    /* 阈值来初始化以前的计时时间                     */
#define  OS_TICK_TH_INIT                    (OS_TICK)(DEF_BIT       ((sizeof(OS_TICK) * DEF_OCTET_NBR_BITS) - 1u))

                                                                    /* 阈值，以检查滴答时间是否已经准备好             */
#define  OS_TICK_TH_RDY                     (OS_TICK)(DEF_BIT_FIELD(((sizeof(OS_TICK) * DEF_OCTET_NBR_BITS) / 2u), \
                                                                    ((sizeof(OS_TICK) * DEF_OCTET_NBR_BITS) / 2u)))

/*$PAGE*/
/*
************************************************************************************************************************
************************************************************************************************************************
*                                                E N U M E R A T I O N S
************************************************************************************************************************
************************************************************************************************************************
*/

/*
------------------------------------------------------------------------------------------------------------------------
*                                                      ERROR CODES
------------------------------------------------------------------------------------------------------------------------
*/

typedef  enum  os_err {
    OS_ERR_NONE                      =     0u,

    OS_ERR_A                         = 10000u,
    OS_ERR_ACCEPT_ISR                = 10001u,

    OS_ERR_B                         = 11000u,

    OS_ERR_C                         = 12000u,
    OS_ERR_CREATE_ISR                = 12001u,

    OS_ERR_D                         = 13000u,
    OS_ERR_DEL_ISR                   = 13001u,

    OS_ERR_E                         = 14000u,

    OS_ERR_F                         = 15000u,
    OS_ERR_FATAL_RETURN              = 15001u,

    OS_ERR_FLAG_GRP_DEPLETED         = 15101u,
    OS_ERR_FLAG_NOT_RDY              = 15102u,
    OS_ERR_FLAG_PEND_OPT             = 15103u,
    OS_ERR_FLUSH_ISR                 = 15104u,

    OS_ERR_G                         = 16000u,

    OS_ERR_H                         = 17000u,

    OS_ERR_I                         = 18000u,
    OS_ERR_ILLEGAL_CREATE_RUN_TIME   = 18001u,
    OS_ERR_INT_Q                     = 18002u,
    OS_ERR_INT_Q_FULL                = 18003u,
    OS_ERR_INT_Q_SIZE                = 18004u,
    OS_ERR_INT_Q_STK_INVALID         = 18005u,
    OS_ERR_INT_Q_STK_SIZE_INVALID    = 18006u,

    OS_ERR_J                         = 19000u,

    OS_ERR_K                         = 20000u,

    OS_ERR_L                         = 21000u,
    OS_ERR_LOCK_NESTING_OVF          = 21001u,

    OS_ERR_M                         = 22000u,

    OS_ERR_MEM_CREATE_ISR            = 22201u,
    OS_ERR_MEM_FULL                  = 22202u,
    OS_ERR_MEM_INVALID_P_ADDR        = 22203u,
    OS_ERR_MEM_INVALID_BLKS          = 22204u,
    OS_ERR_MEM_INVALID_PART          = 22205u,
    OS_ERR_MEM_INVALID_P_BLK         = 22206u,
    OS_ERR_MEM_INVALID_P_MEM         = 22207u,
    OS_ERR_MEM_INVALID_P_DATA        = 22208u,
    OS_ERR_MEM_INVALID_SIZE          = 22209u,
    OS_ERR_MEM_NO_FREE_BLKS          = 22210u,

    OS_ERR_MSG_POOL_EMPTY            = 22301u,
    OS_ERR_MSG_POOL_NULL_PTR         = 22302u,

    OS_ERR_MUTEX_NOT_OWNER           = 22401u,
    OS_ERR_MUTEX_OWNER               = 22402u,
    OS_ERR_MUTEX_NESTING             = 22403u,

    OS_ERR_N                         = 23000u,
    OS_ERR_NAME                      = 23001u,
    OS_ERR_NO_MORE_ID_AVAIL          = 23002u,

    OS_ERR_O                         = 24000u,
    OS_ERR_OBJ_CREATED               = 24001u,
    OS_ERR_OBJ_DEL                   = 24002u,
    OS_ERR_OBJ_PTR_NULL              = 24003u,
    OS_ERR_OBJ_TYPE                  = 24004u,

    OS_ERR_OPT_INVALID               = 24101u,

    OS_ERR_OS_NOT_RUNNING            = 24201u,
    OS_ERR_OS_RUNNING                = 24202u,

    OS_ERR_P                         = 25000u,
    OS_ERR_PEND_ABORT                = 25001u,
    OS_ERR_PEND_ABORT_ISR            = 25002u,
    OS_ERR_PEND_ABORT_NONE           = 25003u,
    OS_ERR_PEND_ABORT_SELF           = 25004u,
    OS_ERR_PEND_DEL                  = 25005u,
    OS_ERR_PEND_ISR                  = 25006u,
    OS_ERR_PEND_LOCKED               = 25007u,
    OS_ERR_PEND_WOULD_BLOCK          = 25008u,

    OS_ERR_POST_NULL_PTR             = 25101u,
    OS_ERR_POST_ISR                  = 25102u,

    OS_ERR_PRIO_EXIST                = 25201u,
    OS_ERR_PRIO                      = 25202u,
    OS_ERR_PRIO_INVALID              = 25203u,

    OS_ERR_PTR_INVALID               = 25301u,

    OS_ERR_Q                         = 26000u,
    OS_ERR_Q_FULL                    = 26001u,
    OS_ERR_Q_EMPTY                   = 26002u,
    OS_ERR_Q_MAX                     = 26003u,
    OS_ERR_Q_SIZE                    = 26004u,

    OS_ERR_R                         = 27000u,
    OS_ERR_REG_ID_INVALID            = 27001u,
    OS_ERR_ROUND_ROBIN_1             = 27002u,
    OS_ERR_ROUND_ROBIN_DISABLED      = 27003u,

    OS_ERR_S                         = 28000u,
    OS_ERR_SCHED_INVALID_TIME_SLICE  = 28001u,
    OS_ERR_SCHED_LOCK_ISR            = 28002u,
    OS_ERR_SCHED_LOCKED              = 28003u,
    OS_ERR_SCHED_NOT_LOCKED          = 28004u,
    OS_ERR_SCHED_UNLOCK_ISR          = 28005u,

    OS_ERR_SEM_OVF                   = 28101u,
    OS_ERR_SET_ISR                   = 28102u,

    OS_ERR_STAT_RESET_ISR            = 28201u,
    OS_ERR_STAT_PRIO_INVALID         = 28202u,
    OS_ERR_STAT_STK_INVALID          = 28203u,
    OS_ERR_STAT_STK_SIZE_INVALID     = 28204u,
    OS_ERR_STATE_INVALID             = 28205u,
    OS_ERR_STATUS_INVALID            = 28206u,
    OS_ERR_STK_INVALID               = 28207u,
    OS_ERR_STK_SIZE_INVALID          = 28208u,
    OS_ERR_STK_LIMIT_INVALID         = 28209u,

    OS_ERR_T                         = 29000u,
    OS_ERR_TASK_CHANGE_PRIO_ISR      = 29001u,
    OS_ERR_TASK_CREATE_ISR           = 29002u,
    OS_ERR_TASK_DEL                  = 29003u,
    OS_ERR_TASK_DEL_IDLE             = 29004u,
    OS_ERR_TASK_DEL_INVALID          = 29005u,
    OS_ERR_TASK_DEL_ISR              = 29006u,
    OS_ERR_TASK_INVALID              = 29007u,
    OS_ERR_TASK_NO_MORE_TCB          = 29008u,
    OS_ERR_TASK_NOT_DLY              = 29009u,
    OS_ERR_TASK_NOT_EXIST            = 29010u,
    OS_ERR_TASK_NOT_SUSPENDED        = 29011u,
    OS_ERR_TASK_OPT                  = 29012u,
    OS_ERR_TASK_RESUME_ISR           = 29013u,
    OS_ERR_TASK_RESUME_PRIO          = 29014u,
    OS_ERR_TASK_RESUME_SELF          = 29015u,
    OS_ERR_TASK_RUNNING              = 29016u,
    OS_ERR_TASK_STK_CHK_ISR          = 29017u,
    OS_ERR_TASK_SUSPENDED            = 29018u,
    OS_ERR_TASK_SUSPEND_IDLE         = 29019u,
    OS_ERR_TASK_SUSPEND_INT_HANDLER  = 29020u,
    OS_ERR_TASK_SUSPEND_ISR          = 29021u,
    OS_ERR_TASK_SUSPEND_PRIO         = 29022u,
    OS_ERR_TASK_WAITING              = 29023u,

    OS_ERR_TCB_INVALID               = 29101u,

    OS_ERR_TLS_ID_INVALID            = 29120u,
    OS_ERR_TLS_ISR                   = 29121u,
    OS_ERR_TLS_NO_MORE_AVAIL         = 29122u,
    OS_ERR_TLS_NOT_EN                = 29123u,
    OS_ERR_TLS_DESTRUCT_ASSIGNED     = 29124u,

    OS_ERR_TICK_PRIO_INVALID         = 29201u,
    OS_ERR_TICK_STK_INVALID          = 29202u,
    OS_ERR_TICK_STK_SIZE_INVALID     = 29203u,
    OS_ERR_TICK_WHEEL_SIZE           = 29204u,

    OS_ERR_TIME_DLY_ISR              = 29301u,
    OS_ERR_TIME_DLY_RESUME_ISR       = 29302u,
    OS_ERR_TIME_GET_ISR              = 29303u,
    OS_ERR_TIME_INVALID_HOURS        = 29304u,
    OS_ERR_TIME_INVALID_MINUTES      = 29305u,
    OS_ERR_TIME_INVALID_SECONDS      = 29306u,
    OS_ERR_TIME_INVALID_MILLISECONDS = 29307u,
    OS_ERR_TIME_NOT_DLY              = 29308u,
    OS_ERR_TIME_SET_ISR              = 29309u,
    OS_ERR_TIME_ZERO_DLY             = 29310u,

    OS_ERR_TIMEOUT                   = 29401u,

    OS_ERR_TMR_INACTIVE              = 29501u,
    OS_ERR_TMR_INVALID_DEST          = 29502u,
    OS_ERR_TMR_INVALID_DLY           = 29503u,
    OS_ERR_TMR_INVALID_PERIOD        = 29504u,
    OS_ERR_TMR_INVALID_STATE         = 29505u,
    OS_ERR_TMR_INVALID               = 29506u,
    OS_ERR_TMR_ISR                   = 29507u,
    OS_ERR_TMR_NO_CALLBACK           = 29508u,
    OS_ERR_TMR_NON_AVAIL             = 29509u,
    OS_ERR_TMR_PRIO_INVALID          = 29510u,
    OS_ERR_TMR_STK_INVALID           = 29511u,
    OS_ERR_TMR_STK_SIZE_INVALID      = 29512u,
    OS_ERR_TMR_STOPPED               = 29513u,

    OS_ERR_U                         = 30000u,

    OS_ERR_V                         = 31000u,

    OS_ERR_W                         = 32000u,

    OS_ERR_X                         = 33000u,

    OS_ERR_Y                         = 34000u,
    OS_ERR_YIELD_ISR                 = 34001u,

    OS_ERR_Z                         = 35000u
} OS_ERR;


/*$PAGE*/
/*
************************************************************************************************************************
************************************************************************************************************************
*                                                  D A T A   T Y P E S
************************************************************************************************************************
************************************************************************************************************************
*/

typedef  struct  os_flag_grp         OS_FLAG_GRP;

typedef  struct  os_mem              OS_MEM;

typedef  struct  os_msg              OS_MSG;
typedef  struct  os_msg_pool         OS_MSG_POOL;
typedef  struct  os_msg_q            OS_MSG_Q;

typedef  struct  os_mutex            OS_MUTEX;

typedef  struct  os_int_q            OS_INT_Q;

typedef  struct  os_q                OS_Q;

typedef  struct  os_sem              OS_SEM;

typedef  void                      (*OS_TASK_PTR)(void *p_arg);

typedef  struct  os_tcb              OS_TCB;

#if defined(OS_CFG_TLS_TBL_SIZE) && (OS_CFG_TLS_TBL_SIZE > 0u)
typedef  void                       *OS_TLS;

typedef  CPU_DATA                    OS_TLS_ID;

typedef  void                      (*OS_TLS_DESTRUCT_PTR)(OS_TCB    *p_tcb,
                                                          OS_TLS_ID  id,
                                                          OS_TLS     value);
#endif

typedef  struct  os_rdy_list         OS_RDY_LIST;

typedef  struct  os_tick_spoke       OS_TICK_SPOKE;

typedef  void                      (*OS_TMR_CALLBACK_PTR)(void *p_tmr, void *p_arg);
typedef  struct  os_tmr              OS_TMR;
typedef  struct  os_tmr_spoke        OS_TMR_SPOKE;


typedef  struct  os_pend_data        OS_PEND_DATA;
typedef  struct  os_pend_list        OS_PEND_LIST;
typedef  struct  os_pend_obj         OS_PEND_OBJ;

#if OS_CFG_APP_HOOKS_EN > 0u
typedef  void                      (*OS_APP_HOOK_VOID)(void);
typedef  void                      (*OS_APP_HOOK_TCB)(OS_TCB *p_tcb);
#endif

/*$PAGE*/
/*
************************************************************************************************************************
************************************************************************************************************************
*                                          D A T A   S T R U C T U R E S
************************************************************************************************************************
************************************************************************************************************************
*/

/*
------------------------------------------------------------------------------------------------------------------------
*                                                    ISR POST DATA
------------------------------------------------------------------------------------------------------------------------
*/

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
struct  os_int_q {
    OS_OBJ_TYPE          Type;                              /* Type of object placed in the circular list             */
    OS_INT_Q            *NextPtr;                           /* Pointer to next OS_INT_Q in  circular list             */
    void                *ObjPtr;                            /* Pointer to object placed in the queue                  */
    void                *MsgPtr;                            /* Pointer to message if posting to a message queue       */
    OS_MSG_SIZE          MsgSize;                           /* Message Size       if posting to a message queue       */
    OS_FLAGS             Flags;                             /* Value of flags if posting to an event flag group       */
    OS_OPT               Opt;                               /* Post Options                                           */
    CPU_TS               TS;                                /* Timestamp                                              */
};
#endif

/*
------------------------------------------------------------------------------------------------------------------------
*                                                      READY LIST
------------------------------------------------------------------------------------------------------------------------
*/

struct  os_rdy_list {
    OS_TCB              *HeadPtr;                           /* Pointer to task that will run at selected priority     */
    OS_TCB              *TailPtr;                           /* Pointer to last task          at selected priority     */
    OS_OBJ_QTY           NbrEntries;                        /* Number of entries             at selected priority     */
};


/*
------------------------------------------------------------------------------------------------------------------------
*                                                PEND DATA and PEND LIST
------------------------------------------------------------------------------------------------------------------------
*/

struct  os_pend_data {
    OS_PEND_DATA        *PrevPtr;
    OS_PEND_DATA        *NextPtr;
    OS_TCB              *TCBPtr;
    OS_PEND_OBJ         *PendObjPtr;
    OS_PEND_OBJ         *RdyObjPtr;
    void                *RdyMsgPtr;
    OS_MSG_SIZE          RdyMsgSize;
    CPU_TS               RdyTS;
};


struct  os_pend_list {
    OS_PEND_DATA        *HeadPtr;
    OS_PEND_DATA        *TailPtr;
    OS_OBJ_QTY           NbrEntries;
};


/*
------------------------------------------------------------------------------------------------------------------------
*                                                       PEND OBJ
*
* Note(s) : (1) The 'os_pend_obj' structure data type is a template/subset for specific kernel objects' data types: 
*               'os_flag_grp', 'os_mutex', 'os_q', and 'os_sem'.  Each specific kernel object data type MUST define 
*               ALL generic OS pend object parameters, synchronized in both the sequential order & data type of each 
*               parameter.
*
*               Thus, ANY modification to the sequential order or data types of OS pend object parameters MUST be 
*               appropriately synchronized between the generic OS pend object data type & ALL specific kernel objects' 
*               data types.
------------------------------------------------------------------------------------------------------------------------
*/

struct  os_pend_obj {
    OS_OBJ_TYPE          Type;
    CPU_CHAR            *NamePtr;
    OS_PEND_LIST         PendList;                          /* List of tasks pending on object                        */
#if OS_CFG_DBG_EN > 0u
    void                *DbgPrevPtr;
    void                *DbgNextPtr;
    CPU_CHAR            *DbgNamePtr;
#endif
};


/*
------------------------------------------------------------------------------------------------------------------------
*                                                     EVENT FLAGS
*
* Note(s) : See  PEND OBJ  Note #1'.
------------------------------------------------------------------------------------------------------------------------
*/


struct  os_flag_grp {                                       /* Event Flag Group                                       */
                                                            /* ------------------ GENERIC  MEMBERS ------------------ */
    OS_OBJ_TYPE          Type;                              /* Should be set to OS_OBJ_TYPE_FLAG                      */
    CPU_CHAR            *NamePtr;                           /* Pointer to Event Flag Name (NUL terminated ASCII)      */
    OS_PEND_LIST         PendList;                          /* List of tasks waiting on event flag group              */
#if OS_CFG_DBG_EN > 0u
    OS_FLAG_GRP         *DbgPrevPtr;
    OS_FLAG_GRP         *DbgNextPtr;
    CPU_CHAR            *DbgNamePtr;
#endif
                                                            /* ------------------ SPECIFIC MEMBERS ------------------ */
    OS_FLAGS             Flags;                             /* 8, 16 or 32 bit flags                                  */
    CPU_TS               TS;                                /* Timestamp of when last post occurred                   */
};

/*$PAGE*/
/*
------------------------------------------------------------------------------------------------------------------------
*                                                   MEMORY PARTITIONS
------------------------------------------------------------------------------------------------------------------------
*/


struct os_mem {                                             /* MEMORY CONTROL BLOCK                                   */
    OS_OBJ_TYPE          Type;                              /* Should be set to OS_OBJ_TYPE_MEM                       */
    void                *AddrPtr;                           /* Pointer to beginning of memory partition               */
    CPU_CHAR            *NamePtr;
    void                *FreeListPtr;                       /* Pointer to list of free memory blocks                  */
    OS_MEM_SIZE          BlkSize;                           /* Size (in bytes) of each block of memory                */
    OS_MEM_QTY           NbrMax;                            /* Total number of blocks in this partition               */
    OS_MEM_QTY           NbrFree;                           /* Number of memory blocks remaining in this partition    */
#if OS_CFG_DBG_EN > 0u
    OS_MEM              *DbgPrevPtr;
    OS_MEM              *DbgNextPtr;
#endif
};

/*$PAGE*/
/*
------------------------------------------------------------------------------------------------------------------------
*                                                       MESSAGES
------------------------------------------------------------------------------------------------------------------------
*/

struct  os_msg {                                            /* MESSAGE CONTROL BLOCK                                  */
    OS_MSG              *NextPtr;                           /* Pointer to next message                                */
    void                *MsgPtr;                            /* Actual message                                         */
    OS_MSG_SIZE          MsgSize;                           /* Size of the message (in # bytes)                       */
    CPU_TS               MsgTS;                             /* Time stamp of when message was sent                    */
};




struct  os_msg_pool {                                       /* OS_MSG POOL                                            */
    OS_MSG              *NextPtr;                           /* Pointer to next message                                */
    OS_MSG_QTY           NbrFree;                           /* Number of messages available from this pool            */
    OS_MSG_QTY           NbrUsed;                           /* Current number of messages used                        */
    OS_MSG_QTY           NbrUsedMax;                        /* Peak number of messages used                           */
};



struct  os_msg_q {                                          /* OS_MSG_Q                                               */
    OS_MSG              *InPtr;                             /* Pointer to next OS_MSG to be inserted  in   the queue  */
    OS_MSG              *OutPtr;                            /* Pointer to next OS_MSG to be extracted from the queue  */
    OS_MSG_QTY           NbrEntriesSize;                    /* Maximum allowable number of entries in the queue       */
    OS_MSG_QTY           NbrEntries;                        /* Current number of entries in the queue                 */
    OS_MSG_QTY           NbrEntriesMax;                     /* Peak number of entries in the queue                    */
};

/*$PAGE*/
/*
------------------------------------------------------------------------------------------------------------------------
*                                              MUTUAL EXCLUSION SEMAPHORES
*
* Note(s) : See  PEND OBJ  Note #1'.
------------------------------------------------------------------------------------------------------------------------
*/

struct  os_mutex {                                          /* Mutual Exclusion Semaphore                             */
                                                            /* ------------------ GENERIC  MEMBERS ------------------ */
    OS_OBJ_TYPE          Type;                              /* Should be set to OS_OBJ_TYPE_MUTEX                     */
    CPU_CHAR            *NamePtr;                           /* Pointer to Mutex Name (NUL terminated ASCII)           */
    OS_PEND_LIST         PendList;                          /* List of tasks waiting on mutex                         */
#if OS_CFG_DBG_EN > 0u
    OS_MUTEX            *DbgPrevPtr;
    OS_MUTEX            *DbgNextPtr;
    CPU_CHAR            *DbgNamePtr;
#endif
                                                            /* ------------------ SPECIFIC MEMBERS ------------------ */
    OS_TCB              *OwnerTCBPtr;
    OS_PRIO              OwnerOriginalPrio;
    OS_NESTING_CTR       OwnerNestingCtr;                   /* Mutex is available when the counter is 0               */
    CPU_TS               TS;
};

/*$PAGE*/
/*
------------------------------------------------------------------------------------------------------------------------
*                                                    MESSAGE QUEUES
*
* Note(s) : See  PEND OBJ  Note #1'.
------------------------------------------------------------------------------------------------------------------------
*/

struct  os_q {                                              /* Message Queue                                          */
                                                            /* ------------------ GENERIC  MEMBERS ------------------ */
    OS_OBJ_TYPE          Type;                              /* Should be set to OS_OBJ_TYPE_Q                         */
    CPU_CHAR            *NamePtr;                           /* Pointer to Message Queue Name (NUL terminated ASCII)   */
    OS_PEND_LIST         PendList;                          /* List of tasks waiting on message queue                 */
#if OS_CFG_DBG_EN > 0u
    OS_Q                *DbgPrevPtr;
    OS_Q                *DbgNextPtr;
    CPU_CHAR            *DbgNamePtr;
#endif
                                                            /* ------------------ SPECIFIC MEMBERS ------------------ */
    OS_MSG_Q             MsgQ;                              /* List of messages                                       */
};

/*$PAGE*/
/*
------------------------------------------------------------------------------------------------------------------------
*                                                      SEMAPHORES
*
* Note(s) : See  PEND OBJ  Note #1'.
------------------------------------------------------------------------------------------------------------------------
*/

struct  os_sem {                                            /* Semaphore                                              */
                                                            /* ------------------ GENERIC  MEMBERS ------------------ */
    OS_OBJ_TYPE          Type;                              /* Should be set to OS_OBJ_TYPE_SEM                       */
    CPU_CHAR            *NamePtr;                           /* Pointer to Semaphore Name (NUL terminated ASCII)       */
    OS_PEND_LIST         PendList;                          /* List of tasks waiting on semaphore                     */
#if OS_CFG_DBG_EN > 0u
    OS_SEM              *DbgPrevPtr;
    OS_SEM              *DbgNextPtr;
    CPU_CHAR            *DbgNamePtr;
#endif
                                                            /* ------------------ SPECIFIC MEMBERS ------------------ */
    OS_SEM_CTR           Ctr;
    CPU_TS               TS;
};

/*$PAGE*/
/*
------------------------------------------------------------------------------------------------------------------------
*                                                  TASK CONTROL BLOCK
------------------------------------------------------------------------------------------------------------------------
*/

struct os_tcb {
    CPU_STK             *StkPtr;                            /* Pointer to current top of stack                        */

    void                *ExtPtr;                            /* Pointer to user definable data for TCB extension       */

    CPU_STK             *StkLimitPtr;                       /* Pointer used to set stack 'watermark' limit            */

    OS_TCB              *NextPtr;                           /* Pointer to next     TCB in the TCB list                */
    OS_TCB              *PrevPtr;                           /* Pointer to previous TCB in the TCB list                */

    OS_TCB              *TickNextPtr;
    OS_TCB              *TickPrevPtr;

    OS_TICK_SPOKE       *TickSpokePtr;                      /* Pointer to tick spoke if task is in the tick list      */

    CPU_CHAR            *NamePtr;                           /* Pointer to task name                                   */

    CPU_STK             *StkBasePtr;                        /* Pointer to base address of stack                       */

#if defined(OS_CFG_TLS_TBL_SIZE) && (OS_CFG_TLS_TBL_SIZE > 0u)
    OS_TLS               TLS_Tbl[OS_CFG_TLS_TBL_SIZE];
#endif

    OS_TASK_PTR          TaskEntryAddr;                     /* Pointer to task entry point address                    */
    void                *TaskEntryArg;                      /* Argument passed to task when it was created            */

    OS_PEND_DATA        *PendDataTblPtr;                    /* Pointer to list containing objects pended on           */
    OS_STATE             PendOn;                            /* Indicates what task is pending on                      */
    OS_STATUS            PendStatus;                        /* Pend status                                            */

    OS_STATE             TaskState;                         /* See OS_TASK_STATE_xxx                                  */
    OS_PRIO              Prio;                              /* Task priority (0 == highest)                           */
    CPU_STK_SIZE         StkSize;                           /* Size of task stack (in number of stack elements)       */
    OS_OPT               Opt;                               /* Task options as passed by OSTaskCreate()               */

    OS_OBJ_QTY           PendDataTblEntries;                /* Size of array of objects to pend on                    */

    CPU_TS               TS;                                /* Timestamp                                              */

    OS_SEM_CTR           SemCtr;                            /* Task specific semaphore counter                        */

                                                            /* DELAY / TIMEOUT                                        */
    OS_TICK              TickCtrPrev;                       /* Previous time when task was            ready           */
    OS_TICK              TickCtrMatch;                      /* Absolute time when task is going to be ready           */
    OS_TICK              TickRemain;                        /* Number of ticks remaining for a match (updated at ...  */
                                                            /* ... run-time by OS_StatTask()                          */
    OS_TICK              TimeQuanta;
    OS_TICK              TimeQuantaCtr;

#if OS_MSG_EN > 0u
    void                *MsgPtr;                            /* Message received                                       */
    OS_MSG_SIZE          MsgSize;
#endif

#if OS_CFG_TASK_Q_EN > 0u
    OS_MSG_Q             MsgQ;                              /* Message queue associated with task                     */
#if OS_CFG_TASK_PROFILE_EN > 0u
    CPU_TS               MsgQPendTime;                      /* Time it took for signal to be received                 */
    CPU_TS               MsgQPendTimeMax;                   /* Max amount of time it took for signal to be received   */
#endif
#endif

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    OS_REG               RegTbl[OS_CFG_TASK_REG_TBL_SIZE];  /* Task specific registers                                */
#endif

#if OS_CFG_FLAG_EN > 0u
    OS_FLAGS             FlagsPend;                         /* Event flag(s) to wait on                               */
    OS_FLAGS             FlagsRdy;                          /* Event flags that made task ready to run                */
    OS_OPT               FlagsOpt;                          /* Options (See OS_OPT_FLAG_xxx)                          */
#endif

#if OS_CFG_TASK_SUSPEND_EN > 0u
    OS_NESTING_CTR       SuspendCtr;                        /* Nesting counter for OSTaskSuspend()                    */
#endif

#if OS_CFG_TASK_PROFILE_EN > 0u
    OS_CPU_USAGE         CPUUsage;                          /* CPU Usage of task (0.00-100.00%)                       */
    OS_CPU_USAGE         CPUUsageMax;                       /* CPU Usage of task (0.00-100.00%) - Peak                */
    OS_CTX_SW_CTR        CtxSwCtr;                          /* Number of time the task was switched in                */
    CPU_TS               CyclesDelta;                       /* value of OS_TS_GET() - .CyclesStart                    */
    CPU_TS               CyclesStart;                       /* Snapshot of cycle counter at start of task resumption  */
    OS_CYCLES            CyclesTotal;                       /* Total number of # of cycles the task has been running  */
    OS_CYCLES            CyclesTotalPrev;                   /* Snapshot of previous # of cycles                       */

    CPU_TS               SemPendTime;                       /* Time it took for signal to be received                 */
    CPU_TS               SemPendTimeMax;                    /* Max amount of time it took for signal to be received   */
#endif

#if OS_CFG_STAT_TASK_STK_CHK_EN > 0u
    CPU_STK_SIZE         StkUsed;                           /* Number of stack elements used from the stack           */
    CPU_STK_SIZE         StkFree;                           /* Number of stack elements free on   the stack           */
#endif

#ifdef CPU_CFG_INT_DIS_MEAS_EN
    CPU_TS               IntDisTimeMax;                     /* 最大中断禁用时间                                      */
#endif
#if OS_CFG_SCHED_LOCK_TIME_MEAS_EN > 0u
    CPU_TS               SchedLockTimeMax;                  /* 最大调度程序锁定时间                                   */
#endif

#if OS_CFG_DBG_EN > 0u
    OS_TCB              *DbgPrevPtr;
    OS_TCB              *DbgNextPtr;
    CPU_CHAR            *DbgNamePtr;
#endif
};

/*$PAGE*/
/*
------------------------------------------------------------------------------------------------------------------------
*                                                    TICK DATA TYPE
------------------------------------------------------------------------------------------------------------------------
*/

struct  os_tick_spoke {
    OS_TCB              *FirstPtr;                          /* 指针指向列表的任务在tick spoke                         */
    OS_OBJ_QTY           NbrEntries;                        /* 当前条目的数量在tick spoke                             */
    OS_OBJ_QTY           NbrEntriesMax;                     /* tick spoke中的峰值条目数                               */
};


/*
------------------------------------------------------------------------------------------------------------------------
*                                                   TIMER DATA TYPES
------------------------------------------------------------------------------------------------------------------------
*/

struct  os_tmr {
    OS_OBJ_TYPE          Type;
    CPU_CHAR            *NamePtr;                           /* 给计时器的名字                                         */
    OS_TMR_CALLBACK_PTR  CallbackPtr;                       /* 函数，在计时器过期时调用                               */
    void                *CallbackPtrArg;                    /* 参数，当计时器过期时传递给函数                         */
    OS_TMR              *NextPtr;                           /* 双链表指针                                             */
    OS_TMR              *PrevPtr;
    OS_TICK              Match;                             /* 当OSTmrTickCtr匹配该值时，定时器超时                   */
    OS_TICK              Remain;                            /* 定时器到期前的剩余时间                                 */
    OS_TICK              Dly;                               /* 在重复开始前延迟                                       */
    OS_TICK              Period;                            /* 周期重复计时器                                         */
    OS_OPT               Opt;                               /* 选项(见OS_OPT_TMR_xxx)                                 */
    OS_STATE             State;
#if OS_CFG_DBG_EN > 0u
    OS_TMR              *DbgPrevPtr;
    OS_TMR              *DbgNextPtr;
#endif
};



struct  os_tmr_spoke {
    OS_TMR              *FirstPtr;                          /* 指向链表中第一个计时器的指针                          */
    OS_OBJ_QTY           NbrEntries;
    OS_OBJ_QTY           NbrEntriesMax;
};

/*$PAGE*/
/*
************************************************************************************************************************
************************************************************************************************************************
*                                           G L O B A L   V A R I A B L E S
************************************************************************************************************************
************************************************************************************************************************
*/

#if OS_CFG_APP_HOOKS_EN > 0u
OS_EXT           OS_APP_HOOK_TCB            OS_AppTaskCreateHookPtr;    /* 应用钩子                                   */
OS_EXT           OS_APP_HOOK_TCB            OS_AppTaskDelHookPtr;
OS_EXT           OS_APP_HOOK_TCB            OS_AppTaskReturnHookPtr;

OS_EXT           OS_APP_HOOK_VOID           OS_AppIdleTaskHookPtr;
OS_EXT           OS_APP_HOOK_VOID           OS_AppStatTaskHookPtr;
OS_EXT           OS_APP_HOOK_VOID           OS_AppTaskSwHookPtr;
OS_EXT           OS_APP_HOOK_VOID           OS_AppTimeTickHookPtr;
#endif

                                                                        /* 空闲任务   -------------------------------- */
OS_EXT            OS_IDLE_CTR               OSIdleTaskCtr;
OS_EXT            OS_TCB                    OSIdleTaskTCB;

                                                                        /* MISCELLANEOUS ---------------------------- */
OS_EXT            OS_NESTING_CTR            OSIntNestingCtr;            /* 中断嵌套级                                 */
#ifdef CPU_CFG_INT_DIS_MEAS_EN
OS_EXT            CPU_TS                    OSIntDisTimeMax;            /* 总中断禁用时间                             */
#endif

OS_EXT            OS_STATE                  OSRunning;                  /* 表示内核正在运行的标志                     */


                                                                        /* ISR任务处理程序  ------------------------- */
#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
OS_EXT            OS_INT_Q                 *OSIntQInPtr;
OS_EXT            OS_INT_Q                 *OSIntQOutPtr;
OS_EXT            OS_OBJ_QTY                OSIntQNbrEntries;
OS_EXT            OS_OBJ_QTY                OSIntQNbrEntriesMax;
OS_EXT            OS_OBJ_QTY                OSIntQOvfCtr;
OS_EXT            OS_TCB                    OSIntQTaskTCB;
OS_EXT            CPU_TS                    OSIntQTaskTimeMax;
#endif

                                                                        /* FLAGS ------------------------------------ */
#if OS_CFG_FLAG_EN > 0u
#if OS_CFG_DBG_EN  > 0u
OS_EXT            OS_FLAG_GRP              *OSFlagDbgListPtr;
#endif
OS_EXT            OS_OBJ_QTY                OSFlagQty;
#endif

                                                                        /* MEMORY MANAGEMENT ------------------------ */
#if OS_CFG_MEM_EN > 0u
#if OS_CFG_DBG_EN > 0u
OS_EXT            OS_MEM                   *OSMemDbgListPtr;
#endif
OS_EXT            OS_OBJ_QTY                OSMemQty;                   /* 已创建内存分区的个数        */
#endif

                                                                        /* OS_MSG POOL ------------------------------ */
#if OS_MSG_EN > 0u
OS_EXT            OS_MSG_POOL               OSMsgPool;                  /* Pool of OS_MSG                             */
#endif

                                                                        /* 互斥对象管理    ------------------------- */
#if OS_CFG_MUTEX_EN > 0u
#if OS_CFG_DBG_EN   > 0u
OS_EXT            OS_MUTEX                 *OSMutexDbgListPtr;
#endif
OS_EXT            OS_OBJ_QTY                OSMutexQty;                 /* 创建的互斥对象的数目                       */
#endif

                                                                        /* PRIORITIES ------------------------------- */
OS_EXT            OS_PRIO                   OSPrioCur;                  /* 当前任务的优先级                           */
OS_EXT            OS_PRIO                   OSPrioHighRdy;              /* 最高优先级任务的优先级                     */
OS_EXT            OS_PRIO                   OSPrioSaved;                /* 延迟投递时保存的优先级                     */
extern            CPU_DATA                  OSPrioTbl[OS_PRIO_TBL_SIZE];

                                                                        /* QUEUES ----------------------------------- */
#if OS_CFG_Q_EN   > 0u
#if OS_CFG_DBG_EN > 0u
OS_EXT            OS_Q                     *OSQDbgListPtr;
#endif
OS_EXT            OS_OBJ_QTY                OSQQty;                     /* 已创建的消息队列数量                       */
#endif



                                                                        /* READY LIST ------------------------------- */
OS_EXT            OS_RDY_LIST               OSRdyList[OS_CFG_PRIO_MAX]; /* 准备运行的任务表                           */


#ifdef OS_SAFETY_CRITICAL_IEC61508
OS_EXT            CPU_BOOLEAN               OSSafetyCriticalStartFlag;  /* 标志，指示所有的init。完成                 */
#endif
                                                                        /* SCHEDULER -------------------------------- */
#if OS_CFG_SCHED_LOCK_TIME_MEAS_EN > 0u
OS_EXT            CPU_TS_TMR                OSSchedLockTimeBegin;       /* 调度器锁定时间测量                         */
OS_EXT            CPU_TS_TMR                OSSchedLockTimeMax;
OS_EXT            CPU_TS_TMR                OSSchedLockTimeMaxCur;
#endif

OS_EXT            OS_NESTING_CTR            OSSchedLockNestingCtr;      /* 锁定嵌套级                                 */
#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
OS_EXT            OS_TICK                   OSSchedRoundRobinDfltTimeQuanta;
OS_EXT            CPU_BOOLEAN               OSSchedRoundRobinEn;        /* 启用/禁用循环调度                          */
#endif
                                                                        /* SEMAPHORES ------------------------------- */
#if OS_CFG_SEM_EN > 0u
#if OS_CFG_DBG_EN > 0u
OS_EXT            OS_SEM                   *OSSemDbgListPtr;
#endif
OS_EXT            OS_OBJ_QTY                OSSemQty;                   /* 创建的信号量的数目                        */
#endif

                                                                        /* STATISTICS ------------------------------- */
#if OS_CFG_STAT_TASK_EN > 0u
OS_EXT            CPU_BOOLEAN               OSStatResetFlag;            /* 强制重置计算的统计信息                     */
OS_EXT            OS_CPU_USAGE              OSStatTaskCPUUsage;         /* CPU占用率(%)                               */
OS_EXT            OS_CPU_USAGE              OSStatTaskCPUUsageMax;      /* CPU占用率，单位为%(峰值)                   */
OS_EXT            OS_TICK                   OSStatTaskCtr;
OS_EXT            OS_TICK                   OSStatTaskCtrMax;
OS_EXT            OS_TICK                   OSStatTaskCtrRun;
OS_EXT            CPU_BOOLEAN               OSStatTaskRdy;
OS_EXT            OS_TCB                    OSStatTaskTCB;
OS_EXT            CPU_TS                    OSStatTaskTimeMax;
#endif

                                                                        /* TASKS ------------------------------------ */
OS_EXT            OS_CTX_SW_CTR             OSTaskCtxSwCtr;             /* 上下文切换次数                             */
#if OS_CFG_DBG_EN > 0u
OS_EXT            OS_TCB                   *OSTaskDbgListPtr;
#endif
OS_EXT            OS_OBJ_QTY                OSTaskQty;                  /* 已创建任务数                               */

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
OS_EXT            OS_REG_ID                 OSTaskRegNextAvailID;       /* 下一个可用的任务寄存器ID                   */
#endif

                                                                        /* TICK TASK -------------------------------- */
OS_EXT            OS_TICK                   OSTickCtr;                  /* 自启动或最后一次设置以来的#勾号            */
OS_EXT            OS_TCB                    OSTickTaskTCB;
OS_EXT            CPU_TS                    OSTickTaskTimeMax;


#if OS_CFG_TMR_EN > 0u                                                  /* TIMERS ----------------------------------- */
#if OS_CFG_DBG_EN > 0u
OS_EXT            OS_TMR                   *OSTmrDbgListPtr;
#endif
OS_EXT            OS_OBJ_QTY                OSTmrQty;                   /* 创建的计时器数                   */
OS_EXT            OS_TCB                    OSTmrTaskTCB;               /* 定时器任务的TCB                            */
OS_EXT            CPU_TS                    OSTmrTaskTimeMax;
OS_EXT            OS_TICK                   OSTmrTickCtr;               /* 定时器的当前时间                           */
OS_EXT            OS_CTR                    OSTmrUpdateCnt;             /* 更新计时器计数器                           */
OS_EXT            OS_CTR                    OSTmrUpdateCtr;
#endif

                                                                        /* TCBs ------------------------------------- */
OS_EXT            OS_TCB                   *OSTCBCurPtr;                /* 指向当前运行的指针           TCB           */
OS_EXT            OS_TCB                   *OSTCBHighRdyPtr;            /* 最高优先级的指针             TCB           */

/*$PAGE*/
/*
************************************************************************************************************************
************************************************************************************************************************
*                                                   E X T E R N A L S
************************************************************************************************************************
************************************************************************************************************************
*/

extern  CPU_STK     * const OSCfg_IdleTaskStkBasePtr;
extern  CPU_STK_SIZE  const OSCfg_IdleTaskStkLimit;
extern  CPU_STK_SIZE  const OSCfg_IdleTaskStkSize;
extern  CPU_INT32U    const OSCfg_IdleTaskStkSizeRAM;

extern  OS_INT_Q    * const OSCfg_IntQBasePtr;
extern  OS_OBJ_QTY    const OSCfg_IntQSize;
extern  CPU_INT32U    const OSCfg_IntQSizeRAM;
extern  CPU_STK     * const OSCfg_IntQTaskStkBasePtr;
extern  CPU_STK_SIZE  const OSCfg_IntQTaskStkLimit;
extern  CPU_STK_SIZE  const OSCfg_IntQTaskStkSize;
extern  CPU_INT32U    const OSCfg_IntQTaskStkSizeRAM;

extern  CPU_STK     * const OSCfg_ISRStkBasePtr;
extern  CPU_STK_SIZE  const OSCfg_ISRStkSize;
extern  CPU_INT32U    const OSCfg_ISRStkSizeRAM;

extern  OS_MSG_SIZE   const OSCfg_MsgPoolSize;
extern  CPU_INT32U    const OSCfg_MsgPoolSizeRAM;
extern  OS_MSG      * const OSCfg_MsgPoolBasePtr;

extern  OS_PRIO       const OSCfg_StatTaskPrio;
extern  OS_RATE_HZ    const OSCfg_StatTaskRate_Hz;
extern  CPU_STK     * const OSCfg_StatTaskStkBasePtr;
extern  CPU_STK_SIZE  const OSCfg_StatTaskStkLimit;
extern  CPU_STK_SIZE  const OSCfg_StatTaskStkSize;
extern  CPU_INT32U    const OSCfg_StatTaskStkSizeRAM;

extern  CPU_STK_SIZE  const OSCfg_StkSizeMin;

extern  OS_RATE_HZ    const OSCfg_TickRate_Hz;
extern  OS_PRIO       const OSCfg_TickTaskPrio;
extern  CPU_STK     * const OSCfg_TickTaskStkBasePtr;
extern  CPU_STK_SIZE  const OSCfg_TickTaskStkLimit;
extern  CPU_STK_SIZE  const OSCfg_TickTaskStkSize;
extern  CPU_INT32U    const OSCfg_TickTaskStkSizeRAM;
extern  OS_OBJ_QTY    const OSCfg_TickWheelSize;
extern  CPU_INT32U    const OSCfg_TickWheelSizeRAM;

extern  OS_PRIO       const OSCfg_TmrTaskPrio;
extern  OS_RATE_HZ    const OSCfg_TmrTaskRate_Hz;
extern  CPU_STK     * const OSCfg_TmrTaskStkBasePtr;
extern  CPU_STK_SIZE  const OSCfg_TmrTaskStkLimit;
extern  CPU_STK_SIZE  const OSCfg_TmrTaskStkSize;
extern  CPU_INT32U    const OSCfg_TmrTaskStkSizeRAM;
extern  OS_OBJ_QTY    const OSCfg_TmrWheelSize;
extern  CPU_INT32U    const OSCfg_TmrSizeRAM;


extern  CPU_STK        OSCfg_IdleTaskStk[];

#if (OS_CFG_ISR_POST_DEFERRED_EN > 0u)
extern  CPU_STK        OSCfg_IntQTaskStk[];
extern  OS_INT_Q       OSCfg_IntQ[];
#endif

extern  CPU_STK        OSCfg_ISRStk[];

#if (OS_MSG_EN > 0u)
extern  OS_MSG         OSCfg_MsgPool[];
#endif

#if (OS_CFG_STAT_TASK_EN > 0u)
extern  CPU_STK        OSCfg_StatTaskStk[];
#endif

extern  CPU_STK        OSCfg_TickTaskStk[];
extern  OS_TICK_SPOKE  OSCfg_TickWheel[];

#if (OS_CFG_TMR_EN > 0u)
extern  CPU_STK        OSCfg_TmrTaskStk[];
extern  OS_TMR_SPOKE   OSCfg_TmrWheel[];
#endif

/*
************************************************************************************************************************
************************************************************************************************************************
*                                        F U N C T I O N   P R O T O T Y P E S
************************************************************************************************************************
************************************************************************************************************************
*/

/* ================================================================================================================== */
/*                                                    EVENT FLAGS                                                     */
/* ================================================================================================================== */

#if OS_CFG_FLAG_EN > 0u
//创建事件标志组
void          OSFlagCreate              (OS_FLAG_GRP           *p_grp,
                                         CPU_CHAR              *p_name,
                                         OS_FLAGS               flags,
                                         OS_ERR                *p_err);

#if OS_CFG_FLAG_DEL_EN > 0u
//删除事件标志组并准备好事件标志组上挂起的所有任务。
OS_OBJ_QTY    OSFlagDel                 (OS_FLAG_GRP           *p_grp,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
#endif
//等待在事件标志组中设置位的组合
OS_FLAGS      OSFlagPend                (OS_FLAG_GRP           *p_grp,
                                         OS_FLAGS               flags,
                                         OS_TICK                timeout,
                                         OS_OPT                 opt,
                                         CPU_TS                *p_ts,
                                         OS_ERR                *p_err);

#if OS_CFG_FLAG_PEND_ABORT_EN > 0u
//中止并准备好当前等待事件标志组的任何任务
OS_OBJ_QTY    OSFlagPendAbort           (OS_FLAG_GRP           *p_grp,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
#endif
//获取使任务准备好运行的标志
OS_FLAGS      OSFlagPendGetFlagsRdy     (OS_ERR                *p_err);
//设置或清除事件标志组中的一些位。需要设置或清除的位是由“位掩码”指定。
OS_FLAGS      OSFlagPost                (OS_FLAG_GRP           *p_grp,
                                         OS_FLAGS               flags,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);

/* ------------------------------------------------ INTERNAL FUNCTIONS ---------------------------------------------- */
//OSFlagDel()调用这个函数来清除事件标志组的内容
void          OS_FlagClr                (OS_FLAG_GRP           *p_grp);
//用于将任务休眠到需要的时候设置事件标志位。
void          OS_FlagBlock              (OS_PEND_DATA          *p_pend_data,
                                         OS_FLAG_GRP           *p_grp,
                                         OS_FLAGS               flags,
                                         OS_OPT                 opt,
                                         OS_TICK                timeout);

#if OS_CFG_DBG_EN > 0u
//在事件标志调试中添加事件标志组列表。
void          OS_FlagDbgListAdd         (OS_FLAG_GRP           *p_grp);
//在事件标志调试中删除事件标志组列表。
void          OS_FlagDbgListRemove      (OS_FLAG_GRP           *p_grp);
#endif
//初始化事件标志模块
void          OS_FlagInit               (OS_ERR                *p_err);
//设置或清除事件标志组中的一些位。需要设置或清除的位是由“位掩码”指定。
OS_FLAGS      OS_FlagPost               (OS_FLAG_GRP           *p_grp,
                                         OS_FLAGS               flags,
                                         OS_OPT                 opt,
                                         CPU_TS                 ts,
                                         OS_ERR                *p_err);
//使任务准备运行，因为需要的事件已设置标志位
void          OS_FlagTaskRdy            (OS_TCB                *p_tcb,
                                         OS_FLAGS               flags_rdy,
                                         CPU_TS                 ts);
#endif

/*$PAGE*/
/* ================================================================================================================== */
/*                                          FIXED SIZE MEMORY BLOCK MANAGEMENT                                        */
/* ================================================================================================================== */

#if OS_CFG_MEM_EN > 0u
//创建一个固定大小的内存分区，由uC/OS-III管理
void          OSMemCreate               (OS_MEM                *p_mem,
                                         CPU_CHAR              *p_name,
                                         void                  *p_addr,
                                         OS_MEM_QTY             n_blks,
                                         OS_MEM_SIZE            blk_size,
                                         OS_ERR                *p_err);
//从一个分区获取一个内存块
void         *OSMemGet                  (OS_MEM                *p_mem,
                                         OS_ERR                *p_err);
//将一个内存块返回到一个分区
void          OSMemPut                  (OS_MEM                *p_mem,
                                         void                  *p_blk,
                                         OS_ERR                *p_err);

/* ------------------------------------------------ INTERNAL FUNCTIONS ---------------------------------------------- */

#if OS_CFG_DBG_EN > 0u
//OSMemCreate()调用这个函数，将内存分区添加到调试表中							 
void          OS_MemDbgListAdd          (OS_MEM                *p_mem);
#endif
//uC/OS-III调用此函数初始化内存分区管理器
void          OS_MemInit                (OS_ERR                *p_err);

#endif

/*$PAGE*/
/* ================================================================================================================== */
/*                                             MUTUAL EXCLUSION SEMAPHORES                                            */
/* ================================================================================================================== */

#if OS_CFG_MUTEX_EN > 0u
//这个函数创建一个互斥锁。
void          OSMutexCreate             (OS_MUTEX              *p_mutex,
                                         CPU_CHAR              *p_name,
                                         OS_ERR                *p_err);

#if OS_CFG_MUTEX_DEL_EN > 0u
//删除互斥锁并准备好所有在互斥锁上挂起的任务。
OS_OBJ_QTY    OSMutexDel                (OS_MUTEX              *p_mutex,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
#endif
//等待一个互斥锁。
void          OSMutexPend               (OS_MUTEX              *p_mutex,
                                         OS_TICK                timeout,
                                         OS_OPT                 opt,
                                         CPU_TS                *p_ts,
                                         OS_ERR                *p_err);

#if OS_CFG_MUTEX_PEND_ABORT_EN > 0u
//中止并准备好当前等待互斥锁的任何任务
OS_OBJ_QTY    OSMutexPendAbort          (OS_MUTEX              *p_mutex,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
#endif
//向互斥对象发出信号
void          OSMutexPost               (OS_MUTEX              *p_mutex,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);


/* ------------------------------------------------ INTERNAL FUNCTIONS ---------------------------------------------- */
//OSMutexDel()调用这个函数来清除互斥锁的内容
void          OS_MutexClr               (OS_MUTEX              *p_mutex);

#if OS_CFG_DBG_EN > 0u
//uC/OS-III调用这些函数来在调试列表中添加互斥锁。
void          OS_MutexDbgListAdd        (OS_MUTEX              *p_mutex);
//uC/OS-III调用这些函数来在调试列表中删除互斥锁
void          OS_MutexDbgListRemove     (OS_MUTEX              *p_mutex);
#endif
//OSInit()调用这个函数来初始化互斥锁管理。
void          OS_MutexInit              (OS_ERR                *p_err);
#endif

/*$PAGE*/
/* ================================================================================================================== */
/*                                                   MESSAGE QUEUES                                                   */
/* ================================================================================================================== */

#if OS_CFG_PEND_MULTI_EN > 0u
//挂起在多个对象上。挂起的对象必须是信号量或消息队列
OS_OBJ_QTY    OSPendMulti               (OS_PEND_DATA          *p_pend_data_tbl,
                                         OS_OBJ_QTY             tbl_size,
                                         OS_TICK                timeout,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);

/* ------------------------------------------------ INTERNAL FUNCTIONS ---------------------------------------------- */
//OSPendMulti()调用此函数来获取准备好的对象列表
OS_OBJ_QTY    OS_PendMultiGetRdy        (OS_PEND_DATA          *p_pend_data_tbl,
                                         OS_OBJ_QTY             tbl_size);
////该函数由OSPendMulti()调用，以验证我们对信号量或消息队列
CPU_BOOLEAN   OS_PendMultiValidate      (OS_PEND_DATA          *p_pend_data_tbl,
                                         OS_OBJ_QTY             tbl_size);
//这个函数被OSPendMulti()调用来挂起一个任务，因为多个对象中的任何一个都有未被发送到。
void          OS_PendMultiWait          (OS_PEND_DATA          *p_pend_data_tbl,
                                         OS_OBJ_QTY             tbl_size,
                                         OS_TICK                timeout);
#endif

/* ================================================================================================================== */
/*                                                   MESSAGE QUEUES                                                   */
/* ================================================================================================================== */

#if OS_CFG_Q_EN > 0u
//应用程序调用此函数来创建消息队列
void          OSQCreate                 (OS_Q                  *p_q,
                                         CPU_CHAR              *p_name,
                                         OS_MSG_QTY             max_qty,
                                         OS_ERR                *p_err);

#if OS_CFG_Q_DEL_EN > 0u
//此函数删除消息队列并准备好队列上所有挂起的任务。
OS_OBJ_QTY    OSQDel                    (OS_Q                  *p_q,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
#endif

#if OS_CFG_Q_FLUSH_EN > 0u
//刷新消息队列的内容。
OS_MSG_QTY    OSQFlush                  (OS_Q                  *p_q,
                                         OS_ERR                *p_err);
#endif
//等待消息被发送到队列
void         *OSQPend                   (OS_Q                  *p_q,
                                         OS_TICK                timeout,
                                         OS_OPT                 opt,
                                         OS_MSG_SIZE           *p_msg_size,
                                         CPU_TS                *p_ts,
                                         OS_ERR                *p_err);

#if OS_CFG_Q_PEND_ABORT_EN > 0u
//中止并准备好当前在队列中等待的任何任务。
OS_OBJ_QTY    OSQPendAbort              (OS_Q                  *p_q,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
#endif
//向队列发送一条消息。使用` opt `参数，您可以指定消息是否
//被广播到所有等待的任务和/或你是否发布消息到队列的前面(LIFO)
//或通常(FIFO)在队列的末尾。
void          OSQPost                   (OS_Q                  *p_q,
                                         void                  *p_void,
                                         OS_MSG_SIZE            msg_size,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);

/* ------------------------------------------------ INTERNAL FUNCTIONS ---------------------------------------------- */
//OSQDel()调用此函数来清除消息队列的内容
void          OS_QClr                   (OS_Q                  *p_q);

#if OS_CFG_DBG_EN > 0u
//uC/OS-III调用这些函数来在消息队列调试中添加消息队列列表
void          OS_QDbgListAdd            (OS_Q                  *p_q);
//uC/OS-III调用这些函数来在消息队列调试中删除消息队列列表
void          OS_QDbgListRemove         (OS_Q                  *p_q);
#endif
//OSInit()调用此函数来初始化消息队列管理。
void          OS_QInit                  (OS_ERR                *p_err);
//这个函数向队列发送一条消息。使用` opt `参数，您可以指定消息是否
//被广播到所有等待的任务和/或你是否发布消息到队列的前面(LIFO)
//或通常(FIFO)在队列的末尾
void          OS_QPost                  (OS_Q                  *p_q,
                                         void                  *p_void,
                                         OS_MSG_SIZE            msg_size,
                                         OS_OPT                 opt,
                                         CPU_TS                 ts,
                                         OS_ERR                *p_err);
#endif

/*$PAGE*/
/* ================================================================================================================== */
/*                                                     SEMAPHORES                                                     */
/* ================================================================================================================== */

#if OS_CFG_SEM_EN > 0u
//创建一个信号量。
void          OSSemCreate               (OS_SEM                *p_sem,
                                         CPU_CHAR              *p_name,
                                         OS_SEM_CTR             cnt,
                                         OS_ERR                *p_err);
//删除一个信号量。
OS_OBJ_QTY    OSSemDel                  (OS_SEM                *p_sem,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
//等待一个信号量。
OS_SEM_CTR    OSSemPend                 (OS_SEM                *p_sem,
                                         OS_TICK                timeout,
                                         OS_OPT                 opt,
                                         CPU_TS                *p_ts,
                                         OS_ERR                *p_err);

#if OS_CFG_SEM_PEND_ABORT_EN > 0u
//中止并准备好当前等待信号量的任何任务。应该使用这个函数
//错误中止对信号量的等待，而不是通过OSSemPost()正常发送信号
OS_OBJ_QTY    OSSemPendAbort            (OS_SEM                *p_sem,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
#endif
//向一个信号量发出信号
OS_SEM_CTR    OSSemPost                 (OS_SEM                *p_sem,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);

#if OS_CFG_SEM_SET_EN > 0u
//将信号量计数设置为作为参数指定的值。
void          OSSemSet                  (OS_SEM                *p_sem,
                                         OS_SEM_CTR             cnt,
                                         OS_ERR                *p_err);
#endif

/* ------------------------------------------------ INTERNAL FUNCTIONS ---------------------------------------------- */
//OSSemDel()调用这个函数来清除信号量的内容
void          OS_SemClr                 (OS_SEM                *p_sem);

#if OS_CFG_DBG_EN > 0u
//uC/OS-III调用这些函数来在调试列表中添加信号量
void          OS_SemDbgListAdd          (OS_SEM                *p_sem);
//uC/OS-III调用这些函数来在调试列表中删除信号量
void          OS_SemDbgListRemove       (OS_SEM                *p_sem);
#endif
//OSInit()调用这个函数来初始化信号量管理。
void          OS_SemInit                (OS_ERR                *p_err);
//向一个信号量发出信号
OS_SEM_CTR    OS_SemPost                (OS_SEM                *p_sem,
                                         OS_OPT                 opt,
                                         CPU_TS                 ts,
                                         OS_ERR                *p_err);
#endif

/*$PAGE*/
/* ================================================================================================================== */
/*                                                 TASK MANAGEMENT                                                    */
/* ================================================================================================================== */

#if OS_CFG_TASK_CHANGE_PRIO_EN > 0u
//此函数允许动态更改任务的优先级
void          OSTaskChangePrio          (OS_TCB                *p_tcb,
                                         OS_PRIO                prio_new,
                                         OS_ERR                *p_err);
#endif
//创建任务
void          OSTaskCreate              (OS_TCB                *p_tcb,
                                         CPU_CHAR              *p_name,
                                         OS_TASK_PTR            p_task,
                                         void                  *p_arg,
                                         OS_PRIO                prio,
                                         CPU_STK               *p_stk_base,
                                         CPU_STK_SIZE           stk_limit,
                                         CPU_STK_SIZE           stk_size,
                                         OS_MSG_QTY             q_size,
                                         OS_TICK                time_quanta,
                                         void                  *p_ext,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);

#if OS_CFG_TASK_DEL_EN > 0u
/* 删除任务 */
void          OSTaskDel                 (OS_TCB                *p_tcb,
                                         OS_ERR                *p_err);
#endif

#if OS_CFG_TASK_Q_EN > 0u
//此函数用于刷新任务的内部消息队列
OS_MSG_QTY    OSTaskQFlush              (OS_TCB                *p_tcb,
                                         OS_ERR                *p_err);
//这个函数导致当前任务等待消息被发送到它。
void         *OSTaskQPend               (OS_TICK                timeout,
                                         OS_OPT                 opt,
                                         OS_MSG_SIZE           *p_msg_size,
                                         CPU_TS                *p_ts,
                                         OS_ERR                *p_err);
//这个函数中止并准备好指定的任务
CPU_BOOLEAN   OSTaskQPendAbort          (OS_TCB                *p_tcb,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
//此函数向任务发送消息
void          OSTaskQPost               (OS_TCB                *p_tcb,
                                         void                  *p_void,
                                         OS_MSG_SIZE            msg_size,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);

#endif

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
//调用这个函数来获取任务寄存器的当前值
OS_REG        OSTaskRegGet              (OS_TCB                *p_tcb,
                                         OS_REG_ID              id,
                                         OS_ERR                *p_err);
//调用这个函数来获取任务寄存器ID
OS_REG_ID     OSTaskRegGetID            (OS_ERR                *p_err);
//调用这个函数来更改任务寄存器的当前值
void          OSTaskRegSet              (OS_TCB                *p_tcb,
                                         OS_REG_ID              id,
                                         OS_REG                 value,
                                         OS_ERR                *p_err);
#endif

#if OS_CFG_TASK_SUSPEND_EN > 0u
//调用这个函数来恢复先前挂起的任务
void          OSTaskResume              (OS_TCB                *p_tcb,
                                         OS_ERR                *p_err);
//调用这个函数是为了挂起一个任务。
void          OSTaskSuspend             (OS_TCB                *p_tcb,
                                         OS_ERR                *p_err);
#endif
//阻塞当前任务，直到另一个任务或ISR发送信号。
OS_SEM_CTR    OSTaskSemPend             (OS_TICK                timeout,
                                         OS_OPT                 opt,
                                         CPU_TS                *p_ts,
                                         OS_ERR                *p_err);

#if (OS_CFG_TASK_SEM_PEND_ABORT_EN > 0u)
//中止并准备好指定的任务。这个函数应该用于错误中止等待发出信号，而不是通常发出信号
CPU_BOOLEAN   OSTaskSemPendAbort        (OS_TCB                *p_tcb,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
#endif
//向等待信号的任务发出信号。
OS_SEM_CTR    OSTaskSemPost             (OS_TCB                *p_tcb,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
//调用这个函数来清除信号计数器
OS_SEM_CTR    OSTaskSemSet              (OS_TCB                *p_tcb,
                                         OS_SEM_CTR             cnt,
                                         OS_ERR                *p_err);

#if OS_CFG_STAT_TASK_STK_CHK_EN > 0u
//计算指定任务堆栈上剩余的空闲内存量。
void          OSTaskStkChk              (OS_TCB                *p_tcb,
                                         CPU_STK_SIZE          *p_free,
                                         CPU_STK_SIZE          *p_used,
                                         OS_ERR                *p_err);
#endif

#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
//更改任务的特定时间片的值。
void          OSTaskTimeQuantaSet       (OS_TCB                *p_tcb,
                                         OS_TICK                time_quanta,
                                         OS_ERR                *p_err);
#endif

/* ------------------------------------------------ INTERNAL FUNCTIONS ---------------------------------------------- */
//调用这个函数从就绪列表中删除一个任务，并将其插入到计时器tick列表中指定的超时时间不为零。
void          OS_TaskBlock              (OS_TCB                *p_tcb,
                                         OS_TICK                timeout);

#if OS_CFG_DBG_EN > 0u
//这些函数由uC/OS-III调用，用于在调试列表中添加一个OS_TCB。
void          OS_TaskDbgListAdd         (OS_TCB                *p_tcb);
//这些函数由uC/OS-III调用，用于在调试列表中删除一个OS_TCB。
void          OS_TaskDbgListRemove      (OS_TCB                *p_tcb);
#endif
//OSInit()调用这个函数来初始化任务管理。
void          OS_TaskInit               (OS_ERR                *p_err);
//OSInit()调用这个函数来初始化任务管理。
void          OS_TaskInitTCB            (OS_TCB                *p_tcb);
//OSInit()调用这个函数来初始化任务管理。
void          OS_TaskQPost              (OS_TCB                *p_tcb,
                                         void                  *p_void,
                                         OS_MSG_SIZE            msg_size,
                                         OS_OPT                 opt,
                                         CPU_TS                 ts,
                                         OS_ERR                *p_err);
//调用这个函数是为了使任务准备好运行。
void          OS_TaskRdy                (OS_TCB                *p_tcb);

#if OS_CFG_TASK_SUSPEND_EN > 0u
//调用这个函数是为了使任务准备好运行。
void          OS_TaskResume             (OS_TCB                *p_tcb,
                                         OS_ERR                *p_err);
#endif
//如果任务意外返回而没有删除自身，则调用此函数
void          OS_TaskReturn             (void);
/* 向等待信号的任务发出信号。 */
OS_SEM_CTR    OS_TaskSemPost            (OS_TCB                *p_tcb,
                                         OS_OPT                 opt,
                                         CPU_TS                 ts,
                                         OS_ERR                *p_err);

#if OS_CFG_TASK_SUSPEND_EN > 0u
/* 挂起一个任务 */
void          OS_TaskSuspend            (OS_TCB                *p_tcb,
                                         OS_ERR                *p_err);
#endif

/*$PAGE*/
/* ================================================================================================================== */
/*                                          任务本地存储(TLS)支持                                                     */
/* ================================================================================================================== */

#if defined(OS_CFG_TLS_TBL_SIZE) && (OS_CFG_TLS_TBL_SIZE > 0u)
OS_TLS_ID  OS_TLS_GetID       (OS_ERR              *p_err);

OS_TLS     OS_TLS_GetValue    (OS_TCB              *p_tcb,
                               OS_TLS_ID            id,
                               OS_ERR              *p_err);

void       OS_TLS_Init        (OS_ERR              *p_err);

void       OS_TLS_SetValue    (OS_TCB              *p_tcb,
                               OS_TLS_ID            id,
                               OS_TLS               value,
                               OS_ERR              *p_err);

void       OS_TLS_SetDestruct (OS_TLS_ID            id,
                               OS_TLS_DESTRUCT_PTR  p_destruct,
                               OS_ERR              *p_err);

void       OS_TLS_TaskCreate  (OS_TCB              *p_tcb);

void       OS_TLS_TaskDel     (OS_TCB              *p_tcb);

void       OS_TLS_TaskSw      (void);
#endif

/*$PAGE*/
/* ================================================================================================================== */
/*                                                 时间管理                                                    */
/* ================================================================================================================== */
//延迟当前正在运行的任务的执行，直到指定的数量系统滴答超时。
void          OSTimeDly                 (OS_TICK                dly,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);

#if OS_CFG_TIME_DLY_HMSM_EN > 0u
//延迟当前正在运行的任务的执行，直到某个时间过期。
void          OSTimeDlyHMSM             (CPU_INT16U             hours,
                                         CPU_INT16U             minutes,
                                         CPU_INT16U             seconds,
                                         CPU_INT32U             milli,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
#endif

#if OS_CFG_TIME_DLY_RESUME_EN > 0u
//恢复通过调用OSTimeDly()或OSTimeDlyHMSM()。注意，不能调用此函数来恢复正在等待事件的任务超时。
void          OSTimeDlyResume           (OS_TCB                *p_tcb,
                                         OS_ERR                *p_err);
#endif
//获取所跟踪的计数器的当前值时钟的数量在滴答作响
OS_TICK       OSTimeGet                 (OS_ERR                *p_err);
//设置计数器，以跟踪时钟滴答的次数。
void          OSTimeSet                 (OS_TICK                ticks,
                                         OS_ERR                *p_err);
//向uC/OS-III发出信号，表示出现“系统tick”(也称为 "时钟周期")。
void          OSTimeTick                (void);

/*$PAGE*/
/* ================================================================================================================== */
/*                                                 TIMER MANAGEMENT                                                   */
/* ================================================================================================================== */

#if OS_CFG_TMR_EN > 0u
//创建计时器
void          OSTmrCreate               (OS_TMR                *p_tmr,
                                         CPU_CHAR              *p_name,
                                         OS_TICK                dly,
                                         OS_TICK                period,
                                         OS_OPT                 opt,
                                         OS_TMR_CALLBACK_PTR    p_callback,
                                         void                  *p_callback_arg,
                                         OS_ERR                *p_err);
//删除计时器
CPU_BOOLEAN   OSTmrDel                  (OS_TMR                *p_tmr,
                                         OS_ERR                *p_err);
//获取计时器超时前的滴答数
OS_TICK       OSTmrRemainGet            (OS_TMR                *p_tmr,
                                         OS_ERR                *p_err);
//启动一个计时器。
CPU_BOOLEAN   OSTmrStart                (OS_TMR                *p_tmr,
                                         OS_ERR                *p_err);
//确定计时器处于什么状态
OS_STATE      OSTmrStateGet             (OS_TMR                *p_tmr,
                                         OS_ERR                *p_err);
//停止计时器。
CPU_BOOLEAN   OSTmrStop                 (OS_TMR                *p_tmr,
                                         OS_OPT                 opt,
                                         void                  *p_callback_arg,
                                         OS_ERR                *p_err);

/* ------------------------------------------------ 内部函数 ---------------------------------------------- */
//调用来清除所有的定时器字段
void          OS_TmrClr                 (OS_TMR                *p_tmr);

#if OS_CFG_DBG_EN > 0u
//在计时器调试表中添加计时器。 
void          OS_TmrDbgListAdd          (OS_TMR                *p_tmr);
//在计时器调试表中删除计时器。
void          OS_TmrDbgListRemove       (OS_TMR                *p_tmr);
#endif
//OSInit()调用这个函数来初始化计时器管理器模块。
void          OS_TmrInit                (OS_ERR                *p_err);
//将计时器插入计时器轮中。计时器总是被插入列表的开始。
void          OS_TmrLink                (OS_TMR                *p_tmr,
                                         OS_OPT                 opt);
//为每条辐条的条目数重置峰值检测器。
void          OS_TmrResetPeak           (void);
//将从计时器轮中移除计时器。
void          OS_TmrUnlink              (OS_TMR                *p_tmr);
//该任务由OS_TmrInit()创建。
void          OS_TmrTask                (void                  *p_arg);

#endif

/*$PAGE*/
/* ================================================================================================================== */
/*                                                    其他参数                                                   */
/* ================================================================================================================== */
//此函数用于初始化uC/OS-III的内部组件
void          OSInit                    (OS_ERR                *p_err);
//通知uC/OS-III您即将中断业务程序
void          OSIntEnter                (void);
//通知uC/OS-III已完成ISR的维护
void          OSIntExit                 (void);

#ifdef OS_SAFETY_CRITICAL_IEC61508
void          OSSafetyCriticalStart     (void);
#endif

#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
//调用此函数更改轮询调度参数
void          OSSchedRoundRobinCfg      (CPU_BOOLEAN            en,
                                         OS_TICK                dflt_time_quanta,
                                         OS_ERR                *p_err);
//当这个函数在它的时间片过期之前执行完时，调用它来放弃CPU
void          OSSchedRoundRobinYield    (OS_ERR                *p_err);

#endif
//该功能被其他uC/OS-III服务调用，用于判断是否有新的高优先级任务已经准备好要跑了
void          OSSched                   (void);
//此功能用于防止重新调度的发生。
void          OSSchedLock               (OS_ERR                *p_err);
//该功能用于重新允许重新调度
void          OSSchedUnlock             (OS_ERR                *p_err);
//用来启动多任务进程，让uC/OS-III管理你的任务创建
void          OSStart                   (OS_ERR                *p_err);

#if OS_CFG_STAT_TASK_EN > 0u
//应用程序调用此函数重置统计信息。
void          OSStatReset               (OS_ERR                *p_err);
//确定32位的CPU占用率有多高
void          OSStatTaskCPUUsageInit    (OS_ERR                *p_err);
#endif
//该函数用于返回uC/OS-III的版本号。返回值对应于uC/OS-III的版本号乘以10000
CPU_INT16U    OSVersion                 (OS_ERR                *p_err);

/* ------------------------------------------------ 内部函数 ---------------------------------------------- */
//这个任务是uC/OS-III内部的，当没有其他更高优先级的任务执行时就执行，因为他们都在等待事件发生。
void          OS_IdleTask               (void                  *p_arg);
//此函数初始化空闲任务
void          OS_IdleTaskInit           (OS_ERR                *p_err);

#if OS_CFG_STAT_TASK_EN > 0u
//计算关于多任务的一些统计数据环境。
void          OS_StatTask               (void                  *p_arg);
#endif
//OSInit()调用这个函数来初始化统计任务。
void          OS_StatTaskInit           (OS_ERR                *p_err);
//这个任务是uC/OS-III内部的，由tick中断触发。
void          OS_TickTask               (void                  *p_arg);
//OSInit()调用这个函数来创建tick任务。
void          OS_TickTaskInit           (OS_ERR                *p_err);

/*$PAGE*/
/*
************************************************************************************************************************
************************************************************************************************************************
*                                    T A R G E T   S P E C I F I C   F U N C T I O N S
************************************************************************************************************************
************************************************************************************************************************
*/
//这个函数由OSInit()在OSInit()的开头调用。
void          OSInitHook                (void);
//这个函数在创建任务时被调用。
void          OSTaskCreateHook          (OS_TCB                *p_tcb);
//删除任务时调用此函数。
void          OSTaskDelHook             (OS_TCB                *p_tcb);
//这个函数由空闲任务调用。添加这个钩子是为了让你这样做诸如停止CPU以节约能源
void          OSIdleTaskHook            (void);
//如果任务意外返回，则调用此函数。
//一项任务应该要么是一个无限循环，要么在完成时删除自身
void          OSTaskReturnHook          (OS_TCB                *p_tcb);
//这个函数被uC/OS-III的统计任务每秒钟调用一次。这允许你应用程序添加功能的统计任务。
void          OSStatTaskHook            (void);
//该函数由OS_Task_Create()或OSTaskCreateExt()调用，用于初始化栈正在创建的任务框架。
CPU_STK      *OSTaskStkInit             (OS_TASK_PTR            p_task,
                                         void                  *p_arg,
                                         CPU_STK               *p_stk_base,
                                         CPU_STK               *p_stk_limit,
                                         CPU_STK_SIZE           stk_size,
                                         OS_OPT                 opt);
//这个函数在任务切换时被调用。这允许您执行其他上下文切换期间的操作。
void          OSTaskSwHook              (void);
//这个函数在每一个tick被调用。
void          OSTimeTickHook            (void);

/*$PAGE*/
/*
************************************************************************************************************************
************************************************************************************************************************
*                   u C / O S - I I I   I N T E R N A L   F U N C T I O N   P R O T O T Y P E S
************************************************************************************************************************
************************************************************************************************************************
*/
//用于确保应用程序中未使用的调试变量不会被使用优化掉
void          OSCfg_Init                (void);

#if OS_CFG_DBG_EN > 0u
//用于确保应用程序中未使用的调试变量不会被使用优化掉
void          OS_Dbg_Init               (void);
#endif


#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
//OSInit()调用这个函数来初始化ISR队列。
void          OS_IntQTaskInit           (OS_ERR                *p_err);
//这个函数将post的内容放入中间队列，以帮助延迟中断的处理在任务级别。
void          OS_IntQPost               (OS_OBJ_TYPE            type,
                                         void                  *p_obj,
                                         void                  *p_void,
                                         OS_MSG_SIZE            msg_size,
                                         OS_FLAGS               flags,
                                         OS_OPT                 opt,
                                         CPU_TS                 ts,
                                         OS_ERR                *p_err);
//该任务由OS_IntQTaskInit()创建。
void          OS_IntQRePost             (void);
//该任务由OS_IntQTaskInit()创建。
void          OS_IntQTask               (void                  *p_arg);
#endif

/* ----------------------------------------------- 信息管理 ----------------------------------------------- */
//这个函数由OSInit()调用，用来初始化os_msg的空闲列表
void          OS_MsgPoolInit            (OS_ERR                *p_err);
//这个函数将消息队列中的所有消息返回到空闲列表。
OS_MSG_QTY    OS_MsgQFreeAll            (OS_MSG_Q              *p_msg_q);
//这个函数从消息队列中检索消息
void         *OS_MsgQGet                (OS_MSG_Q              *p_msg_q,
                                         OS_MSG_SIZE           *p_msg_size,
                                         CPU_TS                *p_ts,
                                         OS_ERR                *p_err);
//调用此函数来初始化消息队列
void          OS_MsgQInit               (OS_MSG_Q              *p_msg_q,
                                         OS_MSG_QTY             size);
//此函数将消息放置在消息队列中
void          OS_MsgQPut                (OS_MSG_Q              *p_msg_q,
                                         void                  *p_void,
                                         OS_MSG_SIZE            msg_size,
                                         OS_OPT                 opt,
                                         CPU_TS                 ts,
                                         OS_ERR                *p_err);

/* ---------------------------------------------- PEND/POST MANAGEMENT ---------------------------------------------- */
//调用此函数是为了将任务置于阻塞状态，等待事件发生
void          OS_Pend                   (OS_PEND_DATA          *p_pend_data,
                                         OS_PEND_OBJ           *p_obj,
                                         OS_STATE               pending_on,
                                         OS_TICK                timeout);
//OSxxxPendAbort()函数调用此函数来中止事件的挂起
void          OS_PendAbort              (OS_PEND_OBJ           *p_obj,
                                         OS_TCB                *p_tcb,
                                         CPU_TS                 ts);
//当任务在多个对象上挂起且其中一个对象已挂起时，将调用此函数中止。
void          OS_PendAbort1             (OS_PEND_OBJ           *p_obj,
                                         OS_TCB                *p_tcb,
                                         CPU_TS                 ts);
//调用这个函数是为了使任务准备运行，因为一个对象正在被删除
void          OS_PendObjDel             (OS_PEND_OBJ           *p_obj,
                                         OS_TCB                *p_tcb,
                                         CPU_TS                 ts);
//当一个任务挂起在多个对象上并且该对象正在被删除时，调用此函数
void          OS_PendObjDel1            (OS_PEND_OBJ           *p_obj,
                                         OS_TCB                *p_tcb,
                                         CPU_TS                 ts);
//调用这个函数是为了post到一个任务。这个函数的存在是因为它与许多OSxxxPost()服务
void          OS_Post                   (OS_PEND_OBJ           *p_obj,
                                         OS_TCB                *p_tcb,
                                         void                  *p_void,
                                         OS_MSG_SIZE            msg_size,
                                         CPU_TS                 ts);
//当一个任务挂起在多个对象上，并且对象已经被提交给该对象时，就会调用这个函数
void          OS_Post1                  (OS_PEND_OBJ           *p_obj,
                                         OS_TCB                *p_tcb,
                                         void                  *p_void,
                                         OS_MSG_SIZE            msg_size,
                                         CPU_TS                 ts);

/* ----------------------------------------------- 优先管理 ---------------------------------------------- */
//uC/OS-III调用这个函数来初始化就绪优先级列表
void          OS_PrioInit               (void);
//调用这个函数是为了在优先级表中插入一个优先级
void          OS_PrioInsert             (OS_PRIO                prio);
//调用这个函数来删除优先级表中的优先级。
void          OS_PrioRemove             (OS_PRIO                prio);
//该函数被其他uC/OS-III服务调用，用于确定优先级最高的任务等待事件。
OS_PRIO       OS_PrioGetHighest         (void);

/* ------------------------------------------------- 调度 --------------------------------------------------- */

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
//该函数被其他uC/OS-III服务调用，以调度优先级为0的任务ISR处理任务
void          OS_Sched0                 (void);
#endif

#if OS_CFG_SCHED_LOCK_TIME_MEAS_EN > 0u
//这些函数用于测量调度器锁定的峰值时间
void          OS_SchedLockTimeMeasStart (void);
void          OS_SchedLockTimeMeasStop  (void);
#endif

#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
void          OS_SchedRoundRobin        (OS_RDY_LIST           *p_rdy_list);
#endif

/* --------------------------------------------- 就绪列表管理 ---------------------------------------------- */
//OSInit()调用这个函数来初始化就绪列表。ready列表包含所有的列表准备运行的任务
void          OS_RdyListInit            (void);
//调用此函数将在就绪列表中插入一个TCB。
void          OS_RdyListInsert          (OS_TCB                *p_tcb);
//这个函数的作用是将一个OS_TCB放在一个链表的开头
void          OS_RdyListInsertHead      (OS_TCB                *p_tcb);
//这个函数的作用是将一个OS_TCB放在一个链接列表的末尾
void          OS_RdyListInsertTail      (OS_TCB                *p_tcb);
//调用这个函数是为了将当前列表的头移动到列表的尾
void          OS_RdyListMoveHeadToTail  (OS_RDY_LIST           *p_rdy_list);
//这个函数被调用，表示从“OS_RDY_LIST”中移除一个“OS_TCB”，并且知道“OS_TCB”的地址删除。
void          OS_RdyListRemove          (OS_TCB                *p_tcb);

/* ---------------------------------------------- 挂起列表管理 ---------------------------------------------- */
//这个函数的作用是对OS_PEND_DATA表中的字段进行初始化
void          OS_PendDataInit           (OS_TCB                *p_tcb,
                                         OS_PEND_DATA          *p_pend_data_tbl,
                                         OS_OBJ_QTY             tbl_size);

#if OS_CFG_DBG_EN > 0u
//函数用于添加指向对象的ASCII“名称”的指针，以便于显示它们使用内核感知工具。
void          OS_PendDbgNameAdd         (OS_PEND_OBJ           *p_obj,
                                         OS_TCB                *p_tcb);
//函数用于删除指向对象的ASCII“名称”的指针
void          OS_PendDbgNameRemove      (OS_PEND_OBJ           *p_obj,
                                         OS_TCB                *p_tcb);
#endif

OS_PEND_LIST *OS_PendListGetPtr         (OS_PEND_OBJ           *p_obj);
//这个函数的作用是初始化OS_PEND_LIST的字段
void          OS_PendListInit           (OS_PEND_LIST          *p_pend_list);
//这个函数被调用时，会在链接列表的开头添加一个OS_PEND_DATA项
void          OS_PendListInsertHead     (OS_PEND_LIST          *p_pend_list,
                                         OS_PEND_DATA          *p_pend_data);
//调用这个函数是为了将一个OS_PEND_DATA条目根据其优先级放到一个链表中
void          OS_PendListInsertPrio     (OS_PEND_LIST          *p_pend_list,
                                         OS_PEND_DATA          *p_pend_data);
//调用这个函数是为了改变一个或多个pend列表中等待的任务的位置
void          OS_PendListChangePrio     (OS_TCB                *p_tcb,
                                         OS_PRIO                prio_new);
//调用这个函数从pend列表中删除一个任务，该列表只知道要删除的任务的TCB
void          OS_PendListRemove         (OS_TCB                *p_tcb);
//调用这个函数从只知道要删除任务的TCB的等待列表中删除任务
void          OS_PendListRemove1        (OS_PEND_LIST          *p_pend_list,
                                         OS_PEND_DATA          *p_pend_data);

/* ---------------------------------------------- TICK 列表管理 ---------------------------------------------- */
//这个函数初始化tick来处理uC/OS-III的数据结构。
void          OS_TickListInit           (void);
//调用此函数是为了将任务放入等待时间到期或等待时间到期的任务列表中挂起调用的超时
void          OS_TickListInsert         (OS_TCB                *p_tcb,
                                         OS_TICK                time,
                                         OS_OPT                 opt,
                                         OS_ERR                *p_err);
//调用这个函数是为了从tick列表中删除任务
void          OS_TickListRemove         (OS_TCB                *p_tcb);
//此功能用于为每条辐条的条目数重置峰值检测器。
void          OS_TickListResetPeak      (void);
//当出现tick时调用此函数，并确定等待内核对象的超时是否已经发生过期或延迟已过期
void          OS_TickListUpdate         (void);

/*$PAGE*/
/*
************************************************************************************************************************
*                                          寻找缺失的#define常量
*
* 如果某些#define常量在编译时生成错误消息，则使用本节
* MISSING in OS_CFG.H.  这允许您快速确定错误的来源。
*
* 您不应该更改此部分，除非您想添加更多关于源代码的注释编译时错误。
************************************************************************************************************************
*/

/*
************************************************************************************************************************
*                                                     MISCELLANEOUS
************************************************************************************************************************
*/

#ifndef OS_CFG_APP_HOOKS_EN
#error  "OS_CFG.H, Missing OS_CFG_APP_HOOKS_EN: Enable (1) or Disable (0) application specific hook functions"
#endif


#ifndef OS_CFG_ARG_CHK_EN
#error  "OS_CFG.H, Missing OS_CFG_ARG_CHK_EN: Enable (1) or Disable (0) argument checking"
#endif


#ifndef OS_CFG_DBG_EN
#error  "OS_CFG.H, Missing OS_CFG_DBG_EN: Allows you to include variables for debugging or not"
#endif


#ifndef OS_CFG_CALLED_FROM_ISR_CHK_EN
#error  "OS_CFG.H, Missing OS_CFG_CALLED_FROM_ISR_CHK_EN: Enable (1) or Disable (0) checking whether in an ISR in kernel services"
#endif


#ifndef OS_CFG_OBJ_TYPE_CHK_EN
#error  "OS_CFG.H, Missing OS_CFG_OBJ_TYPE_CHK_EN: Enable (1) or Disable (0) checking for proper object types in kernel services"
#endif


#ifndef OS_CFG_PEND_MULTI_EN
#error  "OS_CFG.H, Missing OS_CFG_PEND_MULTI_EN: Enable (1) or Disable (0) multi-pend feature"
#endif


#if     OS_CFG_PRIO_MAX < 8u
#error  "OS_CFG.H,         OS_CFG_PRIO_MAX must be >= 8"
#endif


#ifndef OS_CFG_SCHED_LOCK_TIME_MEAS_EN
#error  "OS_CFG.H, Missing OS_CFG_SCHED_LOCK_TIME_MEAS_EN: Include code to measure scheduler lock time"
#else
    #if    (OS_CFG_SCHED_LOCK_TIME_MEAS_EN > 0u) && \
           (OS_CFG_TS_EN                   < 1u)
    #error  "OS_CFG.H,         OS_CFG_TS_EN must be Enabled (1) to measure scheduler lock time"
    #endif
#endif


#ifndef OS_CFG_SCHED_ROUND_ROBIN_EN
#error  "OS_CFG.H, Missing OS_CFG_SCHED_ROUND_ROBIN_EN: Include code for Round Robin Scheduling"
#endif


#ifndef OS_CFG_STK_SIZE_MIN
#error  "OS_CFG.H, Missing OS_CFG_STK_SIZE_MIN: Determines the minimum size for a task stack"
#endif

#ifndef OS_CFG_TS_EN
#error  "OS_CFG.H, Missing OS_CFG_TS_EN: Determines whether time stamping is enabled"
#else
    #if    (OS_CFG_TS_EN  >  0u) && \
           (CPU_CFG_TS_EN == DEF_DISABLED)
    #error  "CPU_CFG.H,        CPU_CFG_TS_32_EN must be Enabled (1) to use time stamp feature"
    #endif
#endif

/*
************************************************************************************************************************
*                                                     EVENT FLAGS
************************************************************************************************************************
*/

#ifndef OS_CFG_FLAG_EN
#error  "OS_CFG.H, Missing OS_CFG_FLAG_EN: Enable (1) or Disable (0) code generation for Event Flags"
#else
    #ifndef OS_CFG_FLAG_DEL_EN
    #error  "OS_CFG.H, Missing OS_CFG_FLAG_DEL_EN: Include code for OSFlagDel()"
    #endif

    #ifndef OS_CFG_FLAG_MODE_CLR_EN
    #error  "OS_CFG.H, Missing OS_CFG_FLAG_MODE_CLR_EN: Include code for Wait on Clear EVENT FLAGS"
    #endif

    #ifndef OS_CFG_FLAG_PEND_ABORT_EN
    #error  "OS_CFG.H, Missing OS_CFG_FLAG_PEND_ABORT_EN: Include code for aborting pends from another task"
    #endif
#endif

/*
************************************************************************************************************************
*                                                  MEMORY MANAGEMENT
************************************************************************************************************************
*/

#ifndef OS_CFG_MEM_EN
#error  "OS_CFG.H, Missing OS_CFG_MEM_EN: Enable (1) or Disable (0) code generation for MEMORY MANAGER"
#endif

/*
************************************************************************************************************************
*                                              MUTUAL EXCLUSION SEMAPHORES
************************************************************************************************************************
*/

#ifndef OS_CFG_MUTEX_EN
#error  "OS_CFG.H, Missing OS_CFG_MUTEX_EN: Enable (1) or Disable (0) code generation for MUTEX"
#else
    #ifndef OS_CFG_MUTEX_DEL_EN
    #error  "OS_CFG.H, Missing OS_CFG_MUTEX_DEL_EN: Include code for OSMutexDel()"
    #endif

    #ifndef OS_CFG_MUTEX_PEND_ABORT_EN
    #error  "OS_CFG.H, Missing OS_CFG_MUTEX_PEND_ABORT_EN: Include code for OSMutexPendAbort()"
    #endif
#endif

/*
************************************************************************************************************************
*                                                    MESSAGE QUEUES
************************************************************************************************************************
*/

#ifndef OS_CFG_Q_EN
#error  "OS_CFG.H, Missing OS_CFG_Q_EN: Enable (1) or Disable (0) code generation for QUEUES"
#else
    #ifndef OS_CFG_Q_DEL_EN
    #error  "OS_CFG.H, Missing OS_CFG_Q_DEL_EN: Include code for OSQDel()"
    #endif

    #ifndef OS_CFG_Q_FLUSH_EN
    #error  "OS_CFG.H, Missing OS_CFG_Q_FLUSH_EN: Include code for OSQFlush()"
    #endif

    #ifndef OS_CFG_Q_PEND_ABORT_EN
    #error  "OS_CFG.H, Missing OS_CFG_Q_PEND_ABORT_EN: Include code for OSQPendAbort()"
    #endif
#endif

/*
************************************************************************************************************************
*                                                      SEMAPHORES
************************************************************************************************************************
*/

#ifndef OS_CFG_SEM_EN
#error  "OS_CFG.H, Missing OS_CFG_SEM_EN: Enable (1) or Disable (0) code generation for SEMAPHORES"
#else
    #ifndef OS_CFG_SEM_DEL_EN
    #error  "OS_CFG.H, Missing OS_CFG_SEM_DEL_EN: Include code for OSSemDel()"
    #endif

    #ifndef OS_CFG_SEM_PEND_ABORT_EN
    #error  "OS_CFG.H, Missing OS_CFG_SEM_PEND_ABORT_EN: Include code for OSSemPendAbort()"
    #endif

    #ifndef OS_CFG_SEM_SET_EN
    #error  "OS_CFG.H, Missing OS_CFG_SEM_SET_EN: Include code for OSSemSet()"
    #endif
#endif

/*
************************************************************************************************************************
*                                                   TASK MANAGEMENT
************************************************************************************************************************
*/

#ifndef OS_CFG_STAT_TASK_EN
#error  "OS_CFG.H, Missing OS_CFG_STAT_TASK_EN: Enable (1) or Disable(0) the statistics task"
#endif

#ifndef OS_CFG_STAT_TASK_STK_CHK_EN
#error  "OS_CFG.H, Missing OS_CFG_STAT_TASK_STK_CHK_EN: Check task stacks from statistics task"
#endif

#ifndef OS_CFG_TASK_CHANGE_PRIO_EN
#error  "OS_CFG.H, Missing OS_CFG_TASK_CHANGE_PRIO_EN: Include code for OSTaskChangePrio()"
#endif

#ifndef OS_CFG_TASK_DEL_EN
#error  "OS_CFG.H, Missing OS_CFG_TASK_DEL_EN: Include code for OSTaskDel()"
#endif

#ifndef OS_CFG_TASK_Q_EN
#error  "OS_CFG.H, Missing OS_CFG_TASK_Q_EN: Include code for OSTaskQxxx()"
#endif

#ifndef OS_CFG_TASK_Q_PEND_ABORT_EN
#error  "OS_CFG.H, Missing OS_CFG_TASK_Q_PEND_ABORT_EN: Include code for OSTaskQPendAbort()"
#endif

#ifndef OS_CFG_TASK_PROFILE_EN
#error  "OS_CFG.H, Missing OS_CFG_TASK_PROFILE_EN: Include code for task profiling"
#endif

#ifndef OS_CFG_TASK_REG_TBL_SIZE
#error  "OS_CFG.H, Missing OS_CFG_TASK_REG_TBL_SIZE: Include support for task specific registers"
#endif

#ifndef OS_CFG_TASK_SEM_PEND_ABORT_EN
#error  "OS_CFG.H, Missing OS_CFG_TASK_SEM_PEND_ABORT_EN: Include code for OSTaskSemPendAbort()"
#endif

#ifndef OS_CFG_TASK_SUSPEND_EN
#error  "OS_CFG.H, Missing OS_CFG_TASK_SUSPEND_EN: Include code for OSTaskSuspend() and OSTaskResume()"
#endif

/*
************************************************************************************************************************
*                                                  TIME MANAGEMENT
************************************************************************************************************************
*/

#ifndef OS_CFG_TIME_DLY_HMSM_EN
#error  "OS_CFG.H, Missing OS_CFG_TIME_DLY_HMSM_EN: Include code for OSTimeDlyHMSM()"
#endif

#ifndef OS_CFG_TIME_DLY_RESUME_EN
#error  "OS_CFG.H, Missing OS_CFG_TIME_DLY_RESUME_EN: Include code for OSTimeDlyResume()"
#endif

/*
************************************************************************************************************************
*                                                  TIMER MANAGEMENT
************************************************************************************************************************
*/

#ifndef OS_CFG_TMR_EN
#error  "OS_CFG.H, Missing OS_CFG_TMR_EN: When (1) enables code generation for Timer Management"
#else
    #ifndef OS_CFG_TMR_DEL_EN
    #error  "OS_CFG.H, Missing OS_CFG_TMR_DEL_EN: Enables (1) or Disables (0) code for OSTmrDel()"
    #endif
#endif

/*
************************************************************************************************************************
*                                             LIBRARY CONFIGURATION ERRORS
************************************************************************************************************************
*/

                                                                /* See 'os.h  Note #1a'.                              */
#if LIB_VERSION < 126u
#error  "lib_def.h, LIB_VERSION SHOULD be >= V1.26"
#endif


                                                                /* See 'os.h  Note #1b'.                              */
#if CPU_CORE_VERSION < 125u
#error  "cpu_core.h, CPU_CORE_VERSION SHOULD be >= V1.25"
#endif


/*
************************************************************************************************************************
*                                                 uC/OS-III MODULE END
************************************************************************************************************************
*/

#ifdef __cplusplus
}
#endif
#endif
