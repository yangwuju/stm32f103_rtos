
#ifndef  __APP_CFG_H__
#define  __APP_CFG_H__


/*
*********************************************************************************************************
*                                       模块 ENABLE / DISABLE
*********************************************************************************************************
*/

#define  APP_CFG_SERIAL_EN                  DEF_DISABLED          // Modified by fire （原是 DEF_ENABLED）

/*
*********************************************************************************************************
*                                           任务优先级
*********************************************************************************************************
*/

#define  APP_TASK_START_PRIO                        2

#define  APP_TASK_LED1_PRIO                         3
#define  APP_TASK_LED2_PRIO                         3
#define  APP_TASK_LED3_PRIO                         3

/*
*********************************************************************************************************
*                                          任务堆栈大小
*********************************************************************************************************
*/

#define  APP_TASK_START_STK_SIZE                    128
#define  APP_TASK_LED1_STK_SIZE                     512
#define  APP_TASK_LED2_STK_SIZE                     512
#define  APP_TASK_LED3_STK_SIZE                     512

/*
*********************************************************************************************************
*                                         BSP 配置: RS-232
*********************************************************************************************************
*/

#define  BSP_CFG_SER_COMM_SEL             	        BSP_SER_COMM_UART_02
#define  BSP_CFG_TS_TMR_SEL                         2


/*
*********************************************************************************************************
*                                          跟踪/调试配置
*********************************************************************************************************
*/
#if 0
#define  TRACE_LEVEL_OFF                    0
#define  TRACE_LEVEL_INFO                   1
#define  TRACE_LEVEL_DEBUG                  2
#endif

#define  APP_TRACE_LEVEL                    TRACE_LEVEL_INFO
#define  APP_TRACE                          BSP_Ser_Printf

#define  APP_TRACE_INFO(x)            ((APP_TRACE_LEVEL >= TRACE_LEVEL_INFO)  ? (void)(APP_TRACE x) : (void)0)
#define  APP_TRACE_DEBUG(x)           ((APP_TRACE_LEVEL >= TRACE_LEVEL_DEBUG) ? (void)(APP_TRACE x) : (void)0)


#endif
