/*
************************************************************************************************************************
*                                                      uC/OS-III
*                                                 The Real-Time Kernel
*
*                                  (c) Copyright 2009-2010; Micrium, Inc.; Weston, FL
*                          All rights reserved.  Protected by international copyright laws.
*
*                                                  CONFIGURATION  FILE
*
* File    : OS_CFG.H
* By      : JJL
* Version : V3.01.2
*
* LICENSING TERMS:
* ---------------
*               uC/OS-III is provided in source form to registered licensees ONLY.  It is
*               illegal to distribute this source code to any third party unless you receive
*               written permission by an authorized Micrium representative.  Knowledge of
*               the source code may NOT be used to develop a similar product.
*
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*
*               You can contact us at www.micrium.com.
************************************************************************************************************************
*/

#ifndef OS_CFG_H
#define OS_CFG_H


/* --- 其他配置 --- */                                          
#define OS_CFG_APP_HOOKS_EN             1u   /* 是否使用钩子函数               Enable (1) or Disable (0) */
#define OS_CFG_ARG_CHK_EN               1u   /* 是否使用参数检查               Enable (1) or Disable (0) */
#define OS_CFG_CALLED_FROM_ISR_CHK_EN   1u   /* 是否使用中断调用检查           Enable (1) or Disable (0) */
#define OS_CFG_DBG_EN                   1u   /* 是否使用 debug                 Enable (1) or Disable (0) */
#define OS_CFG_ISR_POST_DEFERRED_EN     1u   /* 是否使用中断延迟 post 操作     Enable (1) or Disable (0) */
#define OS_CFG_OBJ_TYPE_CHK_EN          1u   /* 是否使用对象类型检查           Enable (1) or Disable (0) */
#define OS_CFG_TS_EN                    1u   /* 是否使用时间戳                 Enable (1) or Disable (0) */

#define OS_CFG_PEND_MULTI_EN            1u   /* 是否使用支持多个任务pend操作   Enable (1) or Disable (0) */

#define OS_CFG_PRIO_MAX                32u   /* 定义任务的最大优先级             (see OS_PRIO data type) */

#define OS_CFG_SCHED_LOCK_TIME_MEAS_EN  1u   /* 是否使用支持测量调度器锁定时间                           */
#define OS_CFG_SCHED_ROUND_ROBIN_EN     1u   /* 是否支持循环调度                                         */
#define OS_CFG_STK_SIZE_MIN            64u   /* 最小的任务堆栈大小                                       */


/* ---------- 事件标志位---------- */
#define OS_CFG_FLAG_EN                  1u   /* 是否使用事件标志位             Enable (1) or Disable (0) */
#define OS_CFG_FLAG_DEL_EN              1u   /* 是否包含 OSFlagDel()的代码                               */
#define OS_CFG_FLAG_MODE_CLR_EN         1u   /* 是否包含清除事件标志位的代码                             */
#define OS_CFG_FLAG_PEND_ABORT_EN       1u   /* 是否包含 OSFlagPendAbort()的代码                         */


/* --------- 内存管理 --- */
#define OS_CFG_MEM_EN                   1u   /* 是否使用内存管理               Enable (1) or Disable (0) */


/* -------- 互斥量 ----- */
#define OS_CFG_MUTEX_EN                 1u   /* 是否使用互斥量                 Enable (1) or Disable (0) */
#define OS_CFG_MUTEX_DEL_EN             1u   /* 是否包含 OSMutexDel()的代码                              */
#define OS_CFG_MUTEX_PEND_ABORT_EN      1u   /* 是否包含 OSMutexPendAbort()的代码                        */


/* ------- 消息队列--------------- */
#define OS_CFG_Q_EN                     1u   /* 是否使用信号量                 Enable (1) or Disable (0) */
#define OS_CFG_Q_DEL_EN                 1u   /* 是否包含 OSSemDel()的代码                                */
#define OS_CFG_Q_FLUSH_EN               1u   /* 是否包含 OSSemPendAbort()的代码                          */
#define OS_CFG_Q_PEND_ABORT_EN          1u   /* 是否包含 OSSemSet()的代码                                */


/* -------------- 信号量 --------- */
#define OS_CFG_SEM_EN                   1u   /* 是否使用信号量                 Enable (1) or Disable (0) */
#define OS_CFG_SEM_DEL_EN               1u   /* 是否包含 OSSemDel()的代码                                */
#define OS_CFG_SEM_PEND_ABORT_EN        1u   /* 是否包含 OSSemPendAbort()的代码                          */
#define OS_CFG_SEM_SET_EN               1u   /* 是否包含 OSSemSet()的代码                                */


/* ----------- 任务管理 -------------- */                                     
#define OS_CFG_STAT_TASK_EN             1u   /* 是否使用任务统计功能           Enable (1) or Disable(0)  */
#define OS_CFG_STAT_TASK_STK_CHK_EN     1u   /* 从统计任务中检查任务堆栈                                 */

#define OS_CFG_TASK_CHANGE_PRIO_EN      1u   /* 是否包含 OSTaskChangePrio()的代码                        */
#define OS_CFG_TASK_DEL_EN              1u   /* 是否包含 OSTaskDel()的代码                               */
#define OS_CFG_TASK_Q_EN                1u   /* 是否包含 OSTaskQXXXX()的代码                             */
#define OS_CFG_TASK_Q_PEND_ABORT_EN     1u   /* 是否包含 OSTaskQPendAbort()的代码                        */
#define OS_CFG_TASK_PROFILE_EN          1u   /* 是否在 OS_TCB 中包含变量以进行性能分析                   */
#define OS_CFG_TASK_REG_TBL_SIZE        1u   /* 任务特定寄存器的数量                                     */
#define OS_CFG_TASK_SEM_PEND_ABORT_EN   1u   /* 是否包含 OSTaskSemPendAbort()的代码                      */
#define OS_CFG_TASK_SUSPEND_EN          1u   /* 是否包含 OSTaskSuspend()和OSTaskResume()的代码           */


/* ------- 时间管理 ------- */
#define OS_CFG_TIME_DLY_HMSM_EN         1u   /* 是否包含 OSTimeDlyHMSM()的代码                           */
#define OS_CFG_TIME_DLY_RESUME_EN       1u   /* 是否包含 OSTimeDlyResume()的代码                         */


/* ---------- 定时器管理 ------- */
#define OS_CFG_TMR_EN                   1u   /* 是否使用定时器                 Enable (1) or Disable (0) */
#define OS_CFG_TMR_DEL_EN               1u   /* 是否支持 OSTmrDel()            Enable (1) or Disable (0) */

#endif
