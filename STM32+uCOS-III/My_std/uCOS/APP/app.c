/*
*************************************************************************
*                              包含的头文件
*************************************************************************
*/

#include <includes.h>


/*
*************************************************************************
*                                  变量
*************************************************************************
*/

/*
*************************************************************************
*                             任务控制块TCB
*************************************************************************
*/

static  OS_TCB   AppTaskStartTCB;

static  OS_TCB   AppTaskLed1TCB;
static  OS_TCB   AppTaskLed2TCB;
static  OS_TCB   AppTaskLed3TCB;

/*
*************************************************************************
*                               堆栈STACK
*************************************************************************
*/

static  CPU_STK  AppTaskStartStk[APP_TASK_START_STK_SIZE];

static  CPU_STK  AppTaskLed1Stk [ APP_TASK_LED1_STK_SIZE ];
static  CPU_STK  AppTaskLed2Stk [ APP_TASK_LED2_STK_SIZE ];
static  CPU_STK  AppTaskLed3Stk [ APP_TASK_LED3_STK_SIZE ];

/*
*************************************************************************
*                               函数声明
*************************************************************************
*/
static  void  AppTaskStart  (void *p_arg);

static  void  AppTaskLed1  ( void * p_arg );
static  void  AppTaskLed2  ( void * p_arg );
static  void  AppTaskLed3  ( void * p_arg );

/*
************************************************************************
*                               main 函数
************************************************************************
*/
/**
* @brief 主函数
* @param 无
* @retval 无
*/
int  main (void)
{
    OS_ERR  err;
	
    /* 初始化 uC/OS-III.*/ 
    OSInit(&err);        
	
	  /* 创建start任务 */ 
    OSTaskCreate((OS_TCB     *) &AppTaskStartTCB,          
                 (CPU_CHAR   *) "App Task Start",
                 (OS_TASK_PTR ) AppTaskStart,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_START_PRIO,
                 (CPU_STK    *) &AppTaskStartStk[0],
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE / 10,
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE,
                 (OS_MSG_QTY  ) 5u,
                 (OS_TICK     ) 0u,
                 (void       *) 0,
                 (OS_OPT      ) (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *) &err);

		/* 开始任务 */
    OSStart(&err);                                             	
		
}


/*
************************************************************************
*                                启动任务
************************************************************************
*/
/**
* @brief 任务函数
* @param p_arg ：参数是否通过OSTaskCreate()传递给AppTaskStart()
* @retval 无
*/
static  void  AppTaskStart (void *p_arg)
{
    CPU_INT32U  cpu_clk_freq;
    CPU_INT32U  cnts;
    OS_ERR      err;


    (void)p_arg;

    BSP_Init();                                                 /* BSP进行初始化函数                 */
    CPU_Init();

    cpu_clk_freq = BSP_CPU_ClkFreq();                           /* 确定SysTick参考频率               */
    cnts = cpu_clk_freq / (CPU_INT32U)OSCfg_TickRate_Hz;        /* 确定nbr SysTick增量               */
    OS_CPU_SysTickInit(cnts);                                   /* 初始化 uC/OS周期时间src (SysTick) */

    Mem_Init();                                                 /* 初始化内存管理模块                */

#if OS_CFG_STAT_TASK_EN > 0u
    OSStatTaskCPUUsageInit(&err);                               /* 在无任务运行时计算CPU容量         */
#endif

    CPU_IntDisMeasMaxCurReset();                                /* 重置当前最大中断禁用时间。        */

//    while (DEF_TRUE) 
//		{                                                           /* 任务主体，总是写成一个无限循环    */
//			LED1_TOGGLE;
//			OSTimeDly ( 1000, OS_OPT_TIME_DLY, & err );
//    }

    OSTaskCreate((OS_TCB     *)&AppTaskLed1TCB,                /* Create the Led1 task                                */
                 (CPU_CHAR   *)"App Task Led1",
                 (OS_TASK_PTR ) AppTaskLed1,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_LED1_PRIO,
                 (CPU_STK    *)&AppTaskLed1Stk[0],
                 (CPU_STK_SIZE) APP_TASK_LED1_STK_SIZE / 10,
                 (CPU_STK_SIZE) APP_TASK_LED1_STK_SIZE,
                 (OS_MSG_QTY  ) 5u,
                 (OS_TICK     ) 0u,
                 (void       *) 0,
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err);
								 
    OSTaskCreate((OS_TCB     *)&AppTaskLed2TCB,                /* Create the Led2 task                                */
                 (CPU_CHAR   *)"App Task Led2",
                 (OS_TASK_PTR ) AppTaskLed2,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_LED2_PRIO,
                 (CPU_STK    *)&AppTaskLed2Stk[0],
                 (CPU_STK_SIZE) APP_TASK_LED2_STK_SIZE / 10,
                 (CPU_STK_SIZE) APP_TASK_LED2_STK_SIZE,
                 (OS_MSG_QTY  ) 5u,
                 (OS_TICK     ) 0u,
                 (void       *) 0,
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err);

    OSTaskCreate((OS_TCB     *)&AppTaskLed3TCB,                /* Create the Led3 task                                */
                 (CPU_CHAR   *)"App Task Led3",
                 (OS_TASK_PTR ) AppTaskLed3,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_LED3_PRIO,
                 (CPU_STK    *)&AppTaskLed3Stk[0],
                 (CPU_STK_SIZE) APP_TASK_LED3_STK_SIZE / 10,
                 (CPU_STK_SIZE) APP_TASK_LED3_STK_SIZE,
                 (OS_MSG_QTY  ) 5u,
                 (OS_TICK     ) 0u,
                 (void       *) 0,
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err);
		
		/* 删除启动任务 */
		OSTaskDel ( & AppTaskStartTCB, & err );	
		
}

/*
*********************************************************************************************************
*                                          LED1 TASK
*********************************************************************************************************
*/
static  void  AppTaskLed1 ( void * p_arg )
{
		OS_ERR      err;

	 (void)p_arg;
	
    /* 任务主体，总是写成一个无限循环 */
		while (DEF_TRUE)       
		{                                         
			LED1_TOGGLE;
			OSTimeDly ( 1000, OS_OPT_TIME_DLY, & err );
			//OSTimeDlyHMSM ( 0,0,1,0, OS_OPT_TIME_DLY, & err );  //1s
		}			
}

/*
*********************************************************************************************************
*                                          LED2 TASK
*********************************************************************************************************
*/

static  void  AppTaskLed2 ( void * p_arg )
{
    OS_ERR      err;

   (void)p_arg;

    /* 任务主体，总是写成一个无限循环 */
    while (DEF_TRUE) 
		{                                          
			LED2_TOGGLE;
			OSTimeDly ( 5000, OS_OPT_TIME_DLY, & err );
    }
		
		
}

/*
*********************************************************************************************************
*                                          LED3 TASK
*********************************************************************************************************
*/

static  void  AppTaskLed3 ( void * p_arg )
{
    OS_ERR      err;


   (void)p_arg;

    /* 任务主体，总是写成一个无限循环 */
    while (DEF_TRUE) 
		{                                           
			LED3_TOGGLE;
			OSTimeDly ( 10000, OS_OPT_TIME_DLY, & err );
    }
		
		
}


/********************************END OF FILE****************************/
