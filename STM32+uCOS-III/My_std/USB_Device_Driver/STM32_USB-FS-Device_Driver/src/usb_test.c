#include "usb_test.h"
#include "hw_config.h" 
#include "usb_lib.h"
#include "usb_pwr.h" 

/*-------------   例程使用说明 ------------------*/

//本工程仅适用已插入SD卡用户，如果没有SD卡请使用名为：
//（2.STM32-F103-指南者_USB Mass Storage_flash）工程，不然在
//没有SD卡情况下使用本工程会导致模拟U盘速度非常慢。


//SD&Flash模拟U盘
void Simulate_Udisk(void)
{
	/*初始化*/
	Set_System();
  	
	/*设置USB时钟为48M*/
	Set_USBClock();
 	
	/*配置USB中断(包括SDIO中断)*/
	USB_Interrupts_Config();
 
	/*USB初始化*/
 	USB_Init();
 
 	while (bDeviceState != CONFIGURED);	 //等待配置完成
	   
	printf("\r\n  STM32 USB MASS STORAGE 实验  \r\n");
}
