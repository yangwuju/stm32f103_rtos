#include "stmflash.h"

u8 needRead,needWrite;//需要读写的参数对应的标识位，每一位对应一个参数

//存入flash的数据
int    TempIntData0=10;
int    TempIntData1=11;
int    TempIntData2=12;
int    TempIntData3=95;
int    TempIntData4=140;

float  TempFltData0=10.1;
float  TempFltData1=11.1;
float  TempFltData2=120.1;
float  TempFltData3=99.1;
float  TempFltData4=40.1;

u8 WriteFlash_FlagByte=0;	 //写标志位

u16 BIN_WRITE_BUF16[1024];//BIN文件转换为short写缓冲区
u16 BIN_READ_BUF16[1024]; //BIN文件读缓冲区



//读取指定地址的半字(16位数据)
//faddr:读地址(此地址必须为2的倍数!!)
//返回值:对应数据.
//===========================================================
u16 STMFLASH_ReadHalfWord(u32 faddr)
{
	return *(vu16*)faddr; 
}
//===========================================================

#if STM32_FLASH_WREN	//如果使能了写   
//不检查的写入
//WriteAddr:起始地址
//pBuffer:数据指针
//NumToWrite:半字(16位)数   
//===========================================================
void STMFLASH_Write_NoCheck(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite)   
{ 			 		 
	u16 i;
	for(i=0;i<NumToWrite;i++)
	{
		FLASH_ProgramHalfWord(WriteAddr,pBuffer[i]);
	    WriteAddr+=2;//地址增加2.
	}  
} 
//===========================================================

#if STM32_FLASH_SIZE<256
#define STM_SECTOR_SIZE 1024 //字节
#else 
#define STM_SECTOR_SIZE	2048
#endif		

u16 STMFLASH_BUF[STM_SECTOR_SIZE/2];//最多是2K字节


//从指定地址开始写入指定长度的数据
//WriteAddr:起始地址(此地址必须为2的倍数!!)
//pBuffer:数据指针
//NumToWrite:半字(16位)数(就是要写入的16位数据的个数.)
//===========================================================
void STMFLASH_Write(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite)	
{
	u32 secpos;	   //扇区地址
	u16 secoff;	   //扇区内偏移地址(16位字计算)
	u16 secremain; //扇区内剩余地址(16位字计算)	   
 	u16 i;    
	u32 offaddr;   //去掉0X08000000后的地址
	if(WriteAddr<STM32_FLASH_BASE||(WriteAddr>=(STM32_FLASH_BASE+1024*STM32_FLASH_SIZE)))return;//非法地址
	FLASH_Unlock();	//解锁
	offaddr=WriteAddr-STM32_FLASH_BASE;		//实际偏移地址.
	secpos=offaddr/STM_SECTOR_SIZE;			//扇区地址  0~127 for STM32F103RBT6
	secoff=(offaddr%STM_SECTOR_SIZE)/2;		//在扇区内的偏移(2个字节为基本单位.)
	secremain=STM_SECTOR_SIZE/2-secoff;		//扇区剩余空间大小   
	if(NumToWrite<=secremain)secremain=NumToWrite;//不大于该扇区范围
	while(1) 
	{	
		STMFLASH_Read(secpos*STM_SECTOR_SIZE+STM32_FLASH_BASE,STMFLASH_BUF,STM_SECTOR_SIZE/2);//读出整个扇区的内容
		
		for(i=0;i<secremain;i++)//校验数据
		{
			if(STMFLASH_BUF[secoff+i]!=0XFFFF)break;//需要擦除  	  
		}
		
		if(i<secremain)//需要擦除
		{
			FLASH_ErasePage(secpos*STM_SECTOR_SIZE+STM32_FLASH_BASE);//擦除这个扇区
			
			for(i=0;i<secremain;i++)//复制
			{
				STMFLASH_BUF[i+secoff]=pBuffer[i];	  
			}
			STMFLASH_Write_NoCheck(secpos*STM_SECTOR_SIZE+STM32_FLASH_BASE,STMFLASH_BUF,STM_SECTOR_SIZE/2);//写入整个扇区  
		}
		else
			STMFLASH_Write_NoCheck(WriteAddr,pBuffer,secremain);//写已经擦除了的,直接写入扇区剩余区间. 				   
		
		if(NumToWrite==secremain)break;//写入结束了
		else//写入未结束
		{
			secpos++;				//扇区地址增1
			secoff = 0;				//偏移位置为0 	 
		  pBuffer += secremain;  	//指针偏移
			WriteAddr += secremain;	//写地址偏移	   
		  NumToWrite -= secremain;	//字节(16位)数递减
			if(NumToWrite>(STM_SECTOR_SIZE/2))   secremain=STM_SECTOR_SIZE/2;//下一个扇区还是写不完
			else  secremain=NumToWrite;//下一个扇区可以写完了
		}	 
	}	
	
	FLASH_Lock();//上锁
}
#endif
//===========================================================

//从指定地址开始读出指定长度的数据
//ReadAddr:起始地址
//pBuffer:数据指针
//NumToWrite:半字(16位)数
//===========================================================
void STMFLASH_Read(u32 ReadAddr,u16 *pBuffer,u16 NumToRead)   	
{
	u16 i;
	for(i=0;i<NumToRead;i++)
	{
		pBuffer[i]=STMFLASH_ReadHalfWord(ReadAddr);//读取2个字节.
		ReadAddr+=2;//偏移2个字节.	
	}
}
//===========================================================

//WriteAddr:起始地址
//WriteData:要写入的数据
//===========================================================
void Test_Write(u32 WriteAddr,u16 WriteData)   	
{
	STMFLASH_Write(WriteAddr,&WriteData,1);//写入一个字 
}
//===========================================================

//读写flash
//===========================================================
void dataFlash_pro(void)
{ 
  float tempf;
	
  if(needWrite)
  {	
		//存入整型数据
		BIN_WRITE_BUF16[FNUM_WrFlash_FLAG] = 1; //写标志位置1
		
		//数据0
		if((TempIntData0>=0) &(TempIntData0<=100))
		   BIN_WRITE_BUF16[FNUM_INT0] = TempIntData0;
		else BIN_WRITE_BUF16[FNUM_INT0] = 1;
		
		//数据1
		if((TempIntData1>=0) &(TempIntData1<=100))
		   BIN_WRITE_BUF16[FNUM_INT1] = TempIntData1;
		else BIN_WRITE_BUF16[FNUM_INT1] = 1;
		
		//数据2
		if((TempIntData2>=0) &(TempIntData2<=100))
		   BIN_WRITE_BUF16[FNUM_INT2] = TempIntData2;
		else BIN_WRITE_BUF16[FNUM_INT2] = 1;
		
		//数据3
		if((TempIntData3>=0) &(TempIntData3<=100))
		   BIN_WRITE_BUF16[FNUM_INT3] = TempIntData3;
		else BIN_WRITE_BUF16[FNUM_INT3] = 1;
		
		//数据4
		if((TempIntData4>=0) &(TempIntData4<=100))
		   BIN_WRITE_BUF16[FNUM_INT4] = TempIntData4;
		else BIN_WRITE_BUF16[FNUM_INT4] = 1;
 
		
		//存入浮点数
		//数据-0
		if((TempFltData0>=0) &(TempFltData0<=1000))
			memcpy(&BIN_WRITE_BUF16[FNUM_DATA0],(const void *)&TempFltData0,4);
		else BIN_WRITE_BUF16[FNUM_DATA0] = 10;
		
		//数据-1
		if((TempFltData1>=0) &(TempFltData1<=1000))
			memcpy(&BIN_WRITE_BUF16[FNUM_DATA1],(const void *)&TempFltData1,4);
		else BIN_WRITE_BUF16[FNUM_DATA1] = 10;
		
		//数据-2
		if((TempFltData2>=0) &(TempFltData2<=1000))
			memcpy(&BIN_WRITE_BUF16[FNUM_DATA2],(const void *)&TempFltData2,4);
		else BIN_WRITE_BUF16[FNUM_DATA2] = 10;	
		
		//数据-3
		if((TempFltData3>=0) &(TempFltData3<=1000))
			memcpy(&BIN_WRITE_BUF16[FNUM_DATA3],(const void *)&TempFltData3,4);
		else BIN_WRITE_BUF16[FNUM_DATA3] = 10;
		
		//数据-4
		if((TempFltData4>=0) &(TempFltData4<=1000))
			memcpy(&BIN_WRITE_BUF16[FNUM_DATA4],(const void *)&TempFltData4,4);
		else BIN_WRITE_BUF16[FNUM_DATA4] = 10.0;
		
	  STMFLASH_Write(STM32_FLASH_DATA,BIN_WRITE_BUF16,0x20);
	  needWrite = 0;
	  needRead = 1;    //写入马上回读一下，验证写入的正确性		
	}		
  if(needRead)
  {
		STMFLASH_Read(STM32_FLASH_DATA,BIN_READ_BUF16,0x20);
		
		WriteFlash_FlagByte = BIN_READ_BUF16[FNUM_WrFlash_FLAG];
		//读取数据，若读取的写数据位未置1，表明还未写入数据，先进行写数据，再读取
		if(WriteFlash_FlagByte != 1)
		{
			needWrite = 1;
			dataFlash_pro(); //重新写入
		}
		
		//数据0
		if((BIN_READ_BUF16[FNUM_INT0]>0)&&(BIN_READ_BUF16[FNUM_INT0]<=100))
			TempIntData0 = BIN_READ_BUF16[FNUM_INT0];
		else TempIntData0 = 1;
		
		//数据1
		if((BIN_READ_BUF16[FNUM_INT1]>0)&&(BIN_READ_BUF16[FNUM_INT1]<=100))
			TempIntData1 = BIN_READ_BUF16[FNUM_INT1];
		else TempIntData1 = 1;
		
		//数据2
		if((BIN_READ_BUF16[FNUM_INT2]>0)&&(BIN_READ_BUF16[FNUM_INT2]<=100))
			TempIntData2 = BIN_READ_BUF16[FNUM_INT2];
		else TempIntData2 = 1;
		
		//数据3
		if((BIN_READ_BUF16[FNUM_INT3]>0)&&(BIN_READ_BUF16[FNUM_INT3]<=100))
			TempIntData3 = BIN_READ_BUF16[FNUM_INT3];
		else TempIntData3 = 1;
		
		//数据4
		if((BIN_READ_BUF16[FNUM_INT4]>0)&&(BIN_READ_BUF16[FNUM_INT4]<=100))
			TempIntData4 = BIN_READ_BUF16[FNUM_INT4];
		else TempIntData4 = 1;
		
		//数据-0
	  memcpy((void *)&tempf,&BIN_READ_BUF16[FNUM_DATA0],4);
		if((tempf>=0)&&(tempf<=100))
			TempFltData0 =tempf;	
		else TempFltData0 = 10;
    
		//数据-1
	  memcpy((void *)&tempf,&BIN_READ_BUF16[FNUM_DATA1],4);
		if((tempf>=0)&&(tempf<=100))
			TempFltData1 =tempf;
		else TempFltData1 = 10;
		
		//数据-2
	  memcpy((void *)&tempf,&BIN_READ_BUF16[FNUM_DATA2],4);
		if((tempf>=0)&&(tempf<=100))
		TempFltData2 =tempf;
		else TempFltData2 = 10;
		
		//数据-3
	  memcpy((void *)&tempf,&BIN_READ_BUF16[FNUM_DATA3],4);
		if((tempf>=0)&&(tempf<=100))
			TempFltData3 =tempf;
		else TempFltData3 = 10;
		
		//数据-4
	  memcpy((void *)&tempf,&BIN_READ_BUF16[FNUM_DATA4],4);
		if((tempf>=0)&&(tempf<=100))
			TempFltData4 =tempf;	
		else TempFltData4 = 10;
		
    needRead = 0;		
  }
		 
}
//===========================================================


/*--------------------- END OF FIAL-------------------*/

