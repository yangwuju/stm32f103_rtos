#include "time0.h" 

// 基本定时器TIMx,x[6,7]定时初始化函数,只有大容量含有TIM6，7
// 本函数中选择TIM5只作为定时器使用


u16  timeCnt1s=0,timeCnt100ms=0,timeCnt10ms=0;        //计数


void TIMx_IRQHandler(void)   //TIM6中断
{
	if (TIM_GetITStatus(TIMx, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源 
	{
		TIM_ClearITPendingBit(TIMx, TIM_IT_Update  );  //清除TIMx的中断待处理位:TIM 中断源 
    //添加中断处理 
		
		if(++timeCnt10ms>10)
		{
		  timeCnt10ms = 0;
		}
		if(++timeCnt100ms>100)
		{
		  timeCnt100ms = 0;
 
		}
		if(++timeCnt1s>1000)
		{
		  timeCnt1s = 0;
		}
	}
}


// 中断优先级配置
static void TIMx_NVIC_Config(void)
{
    NVIC_InitTypeDef NVIC_InitStructure; 
    // 设置中断组为0
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);		
	// 设置中断来源
    NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;	
	// 设置主优先级为 0
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;	 
	// 设置抢占优先级为3
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;	
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}


/*
 * 注意：TIM_TimeBaseInitTypeDef结构体里面有5个成员，TIM6和TIM7的寄存器里面只有
 * TIM_Prescaler和TIM_Period，所以使用TIM6和TIM7的时候只需初始化这两个成员即可，
 * 另外三个成员是通用定时器和高级定时器才有.
 *-----------------------------------------------------------------------------
 *typedef struct
 *{ TIM_Prescaler            都有
 *	TIM_CounterMode			     TIMx,x[6,7]没有，其他都有
 *  TIM_Period               都有
 *  TIM_ClockDivision        TIMx,x[6,7]没有，其他都有
 *  TIM_RepetitionCounter    TIMx,x[1,8,15,16,17]才有
 *}TIM_TimeBaseInitTypeDef; 
 *-----------------------------------------------------------------------------
 */
void TIMx_Init(u16 arr,u16 psc)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
		
	  // 开启定时器时钟,即内部时钟CK_INT=72M
    RCC_Cmd(TIMx_CLK, ENABLE);
	
	  //缺省值
	  TIM_DeInit(TIM2);   
	
	  // 自动重装载寄存器的值，累计TIM_Period+1个频率后产生一个更新或者中断
    TIM_TimeBaseStructure.TIM_Period = arr;	

	  // 时钟预分频数为
    TIM_TimeBaseStructure.TIM_Prescaler= psc;
	
	  // 时钟分频因子 ，基本定时器没有，不用管
    TIM_TimeBaseStructure.TIM_ClockDivision=TIM_CKD_DIV1;
		
	  // 计数器计数模式，基本定时器只能向上计数，没有计数模式的设置
    TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up; 
		
	  // 重复计数器的值，基本定时器没有，不用管
	  TIM_TimeBaseStructure.TIM_RepetitionCounter=0;
	
	  // 初始化定时器
    TIM_TimeBaseInit(TIMx, &TIM_TimeBaseStructure);
		
		//允许自动重装
		TIM_ARRPreloadConfig(TIMx, ENABLE);   
		
	  // 清除计数器中断标志位
    TIM_ClearFlag(TIMx, TIM_FLAG_Update);

	  // 中断优先级配置
    TIMx_NVIC_Config();

	  // 开启计数器中断
    TIM_ITConfig(TIMx,TIM_IT_Update,ENABLE);
	
	  // 使能计数器
    TIM_Cmd(TIMx, ENABLE);	
}



/*********************************************END OF FILE**********************/
