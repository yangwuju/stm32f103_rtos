/**************************************************************	 
**  文 件 名  : oled.h
**  功能描述  : OLED 4接口(IIC)
**              说明：
**              -----------------------------------------------
**              VCC  接5V或3.3v电源
**              GND  电源地              
**              SCLK 时钟线
**              SDA  数据线
**              ===============================================
**              OLED 7接口(SPI)
**              说明: 
**              -----------------------------------------------
**              VCC  接5V或3.3v电源
**              GND  电源地
**              D1   接--（SDA）
**              D0   接--（SCL）
**              CS   接-- 片选
**              RES  接-- 复位
**              DC   接-- 数据/命令     
**              -----------------------------------------------
**  注：显示字符，汉字，图片函数在
**             oledfond.c,oledHanzi.h和oledbmp.c中
***************************************************************/
#include "oled.h"
#include "oledfont.h"
#include "oledHanzi.h"
#include "oledDes.h"
//#include "oledbmp.h"

//OLED的显存
//存放格式如下.
//[0]0 1 2 3 ... 127	
//[1]0 1 2 3 ... 127	
//[2]0 1 2 3 ... 127	
//[3]0 1 2 3 ... 127	
//[4]0 1 2 3 ... 127	
//[5]0 1 2 3 ... 127	
//[6]0 1 2 3 ... 127	
//[7]0 1 2 3 ... 127 
u8  oledgram[128][8];


//选择通信协议
#if OLED_IIC  //OLED_SPI

/*-------------------I2c起始信号------------------*/  
void OLED_I2c_Start(void)
{

	OLED_SCLK_Set();
	OLED_SDIN_Set();
	OLED_SDIN_Clr();
	OLED_SCLK_Clr();
}

/*-------------------I2c停止信号------------------*/  
void OLED_I2c_Stop(void)
{
  OLED_SCLK_Set();
//	OLED_SCLK_Clr();
	OLED_SDIN_Clr();
	OLED_SDIN_Set();
	
}

/*--------------------等待应答--------------------*/  
void OLED_I2c_WaitAck(void)
{
	OLED_SCLK_Set();
	OLED_SCLK_Clr();
}

/*-------------------发送8BIT数据-----------------*/ 
void OLED_I2c_SendByte(u8 dat)
{
	u8 i;
	OLED_SCLK_Clr();
	for(i=0;i<8;i++)		
	{
		if(dat & 0x80)
		{
			OLED_SDIN_Set();
		}
		else 
		{
			OLED_SDIN_Clr();
		}
		dat <<= 1;	// 左移一个bit
		OLED_SCLK_Set();
		OLED_SCLK_Clr();
	}
}

/*--------------------发送指令--------------------*/
void OLED_SendCmd(u8 cmd)
{
	OLED_I2c_Start();
	OLED_I2c_SendByte(0x78);
	OLED_I2c_WaitAck();
	OLED_I2c_SendByte(0x00);
	OLED_I2c_WaitAck();
	OLED_I2c_SendByte(cmd);
	OLED_I2c_WaitAck();
	OLED_I2c_Stop();
}

/*--------------------发送数据--------------------*/
void OLED_SendDat(u8 dat)
{
	OLED_I2c_Start();
	OLED_I2c_SendByte(0x78);
	OLED_I2c_WaitAck();
	OLED_I2c_SendByte(0x40);
	OLED_I2c_WaitAck();
	OLED_I2c_SendByte(dat);
	OLED_I2c_WaitAck();
	OLED_I2c_Stop();
}

/*--------------------发送数据/命令--------------------*/
void OLED_WR_Byte(u8 dat,u8 cmd)
{
	if(cmd)
	{
		OLED_SendDat(dat);
	}
	else
	{
		OLED_SendCmd(dat);
	}
}


#else

#if OLED_MODE==1
/*----------------向SSD1106写入一个字节---------------*/
//dat:要写入的数据/命令
//cmd:数据/命令标志 0,表示命令;1,表示数据;
void OLED_WR_Byte(u8 dat,u8 cmd)
{
	DATAOUT(dat);	    
	if(cmd)
	  OLED_DC_Set();
	else 
	  OLED_DC_Clr();		   
	OLED_CS_Clr();
	OLED_WR_Clr();	 
	OLED_WR_Set();
	OLED_CS_Set();	  
	OLED_DC_Set();	 
} 	    	    
#else
/*----------------向SSD1106写入一个字节---------------*/
//dat:要写入的数据/命令
//cmd:数据/命令标志 0,表示命令;1,表示数据;
void OLED_WR_Byte(u8 dat,u8 cmd)
{	
	u8 i;			  
	if(cmd)
	  OLED_DC_Set();
	else 
	  OLED_DC_Clr();		  
	OLED_CS_Clr();
	for(i=0;i<8;i++)
	{			  
		OLED_SCLK_Clr();
		if(dat&0x80)
		   OLED_SDIN_Set();
		else 
		   OLED_SDIN_Clr();
		OLED_SCLK_Set();
		dat<<=1;   
	}				 		  
	OLED_CS_Set();
	OLED_DC_Set();   	  
} 

#endif

#endif

/*-------------------开启OLED显示------------------*/  
void OLED_Display_On(void)
{
	OLED_WR_Byte(0X8D,OLED_CMD);  //SET DCDC命令
	OLED_WR_Byte(0X14,OLED_CMD);  //DCDC ON
	OLED_WR_Byte(0XAF,OLED_CMD);  //DISPLAY ON
}

/*-------------------关闭OLED显示-------------------*/     
void OLED_Display_Off(void)
{
	OLED_WR_Byte(0X8D,OLED_CMD);  //SET DCDC命令
	OLED_WR_Byte(0X10,OLED_CMD);  //DCDC OFF
	OLED_WR_Byte(0XAE,OLED_CMD);  //DISPLAY OFF
}	

/*---------------------反显显示----------------------*/
//i  =0:正常显示   =1：反色显示  默认为0
void OLED_ColorTurn(u8 i)
{
  if(i==0)
	{
		OLED_WR_Byte(0XA6,OLED_CMD);   	
	}
	else if(i==1)
	{
	  OLED_WR_Byte(0XA7,OLED_CMD);   
	}
}

/*------------------屏幕旋转180°显示----------------*/
//i  =0:正常显示   =1：反转显示  默认为0
void OLED_DisplayTurn(u8 i)
{
  if(i==0)
	{
		OLED_WR_Byte(0XC8,OLED_CMD);  
		OLED_WR_Byte(0XA1,OLED_CMD);  	
	}
	else if(i==1)
	{
	  OLED_WR_Byte(0XC0,OLED_CMD);  
	  OLED_WR_Byte(0XA0,OLED_CMD);  
	}
}

/*--------------指定OLED的位置显示点--------------*/
//【每个点未更新到OLED寄存器地址中】
//
//（x,y） :坐标 x:0-127   y:0-63
// fill: 0不填充  1填充
// dir:坐标定义方向0-> Y+|        1-> ----X+
//                       |            |
//                       |___X+     Y+|
//注：由于在同一行的每一列对应的8个像素点是单独写入的
//所以在写入第二个点时，会擦除第一个点，所以两者不同步
//若多点在同一行的同一列时，会消隐其他点
//接力点就是由于这个BUG产生的
void OLED_DrawPoint1(u8 x,u8 y,u8 fill,u8 dir)
{
  u8 X,Y;
	u8 n;
	X=x;	Y=y/8;	n=y%8;
	if(dir==0)
	{
		Y=7-Y;
		n=7-n;
	}		
  OLED_WR_Byte(0xb0+Y,OLED_CMD);
	OLED_WR_Byte(((X&0xf0)>>4)|0x10,OLED_CMD);
	OLED_WR_Byte((X&0x0f)|0x01,OLED_CMD);	
	OLED_WR_Byte(fill<<n,OLED_DATA);
}

/*--------------------同向接力点1----------------*/ 
//（X1,Y1） :起点坐标
//（X2,Y2） :终点坐标
// width:相邻两列接力点之间相差width个像素点
// dir:坐标定义方向0-> Y+|        1-> ----X+
//                     |            |
//                     |___X+     Y+|
// speed:表示接力速度(10-100显示效果更好)
void OLED_RelayPoint1(u8 x1,u8 y1,u8 x2,u8 y2,u8 width,u8 dir,u8 speed)  
{  
	u8 i,j;
	for(i=y1;i<y2+1;i++)
	{ 
   	for(j=x1;j<x2+1;j++)
		{
      if(j+width<x2+1)			
		    OLED_DrawPoint1(j+width,i,1,dir);	
      else
        continue;
//      Delay_Ms(10);	//可增加显示效果		
		}
		Delay_Ms(speed);
	}
}

/*--------------------顺序接力点2----------------*/ 
//（X1,Y1） :第一组接力点起点坐标
//（X2,Y2） :第一组接力点终点坐标
// row:有row排接力点
// width:每组接力点之间的相差width个像素点
// dir:坐标定义方向0-> Y+|        1-> ----X+
//                     |            |
//                     |___X+     Y+|
// speed:表示接力速度(10-100显示效果更好)
void OLED_RelayPoint2(u8 x1,u8 y1,u8 x2,u8 y2,u8 row,u8 width,u8 dir,u8 speed)  
{  
	u8 i,d;
	d=x2-x1+1;
	d+=width; //每组接力点相差n个像素点
	for(i=0;i<row;i++)
	{ 
    OLED_RelayPoint1(x1+d*i,y1,x2+d*i,y2,1,dir,speed);
	}
}

/*----------------------更新显存到OLED--------------------*/		 
void OLED_Refresh_Gram(void)
{
	u8 i,n;		    
	for(i=0;i<8;i++)  
	{  
		OLED_WR_Byte (0xb0+i,OLED_CMD);    //设置页地址（0~7）
		OLED_WR_Byte (0x00,OLED_CMD);      //设置显示位置—列低地址
		OLED_WR_Byte (0x10,OLED_CMD);      //设置显示位置—列高地址   
		for(n=0;n<Max_Column;n++)OLED_WR_Byte(oledgram[n][i],OLED_DATA); 
	}   
}

/*----------清屏函数,清完屏,整个屏幕是黑色-----------*/  
void OLED_Clear(void)  
{  
	u8 i,n;		    
	for(i=0;i<8;i++)  
	{  
		for(n=0;n<128;n++) oledgram[n][i]=0X00;
		OLED_Refresh_Gram();//更新显示
	} //更新显示
}

/*----------将指定位置显示点数据存入寄存器---------*/
//x:0~127
//y:0~63
//mode:0,反白显示;1,正常显示
//rw:0只写入不显示   1写入并显示 
//dir:坐标定义方向0-> Y+|        1-> ----X+
//                      |            |
//                      |___X+     Y+|
void OLED_DrawPoint(u8 x,u8 y,u8 mode,u8 rw,u8 dir)
{
	u8 pos,bx,temp=0;
	if(x>127||y>63)return;//超出范围了.
	pos=y/8;
	bx=y%8;
	if(dir==0) 	{pos =7-pos;	bx =7-bx;}
	temp=1<<bx;
	if(mode)oledgram[x][pos]|=temp;
	else oledgram[x][pos]&=~temp;	
  if(rw) OLED_Refresh_Gram();//更新显示		
}

/*--------------指定一个区域进行填充-------------*/
//x1,y1,x2,y2 填充区域的对角坐标
//确保x1<=x2;y1<=y2 0<=x1<=127 0<=y1<=63	 	 
//fill:0,清空; 1,填充
//track:0,不显示点轨迹；   1，显示点轨迹 （RW_FLASH=1）
//dir:坐标定义方向0-> Y+|        1-> ----X+
//                      |            |
//                      |___X+     Y+|
void OLED_Fill(u8 x1,u8 y1,u8 x2,u8 y2,u8 fill,u8 track,u8 dir)  
{  
	vs32 X,Y,dX,dY,DX,DY,i,j=0;
  dX = x2 + x1; dY = y2 + y1;
	X = dX/2;  Y = dY/2;
	DX = X-x1+1; 	DY = Y-y1+1;
	if(dX%2 == 1)
	{
		 if(dY%2 == 1)
		 {
			 for(j=0;j<DY;j++)
			 for(i=0;i<DX;i++)
			 {
				 OLED_DrawPoint(X+1+i,Y+1+j,fill,track,dir);
				 OLED_DrawPoint(X+1+i,Y-j,fill,track,dir);
				 OLED_DrawPoint(X-i,Y+1+j,fill,track,dir);
				 OLED_DrawPoint(X-i,Y-j,fill,track,dir);			         			 
			 } 
		 }
     else
		 {	   
			 for(j=0;j<DY;j++)
			 for(i=0;i<DX;i++)
			 {
				 OLED_DrawPoint(X+1+i,Y+j,fill,track,dir);
				 OLED_DrawPoint(X+1+i,Y-j,fill,track,dir);
				 OLED_DrawPoint(X-i,Y+j,fill,track,dir);
				 OLED_DrawPoint(X-i,Y-j,fill,track,dir);		         				 
			 }			 
		 }			 
	}
	else
  {	
		 if(dY%2 == 1)
		 {
			 for(j=0;j<DY;j++)
			 for(i=0;i<DX;i++)
			 {
				 OLED_DrawPoint(X+i,Y+1+j,fill,track,dir);	
				 OLED_DrawPoint(X-i,Y+1+j,fill,track,dir);
         OLED_DrawPoint(X+i,Y-j,fill,track,dir);	
			   OLED_DrawPoint(X-i,Y-j,fill,track,dir);      			 
			 } 
		 }
     else
		 {	 
       for(j=0;j<DY;j++)			 
			 for(i=0;i<DX;i++)
			 {
				 OLED_DrawPoint(X+i,Y+j,fill,track,dir);	
         OLED_DrawPoint(X-i,Y+j,fill,track,dir);
         OLED_DrawPoint(X-i,Y-j,fill,track,dir);
				 OLED_DrawPoint(X+i,Y-j,fill,track,dir);				         				 
			 }			 
		 }
	}
	if(track==0 && RW_FLASH) OLED_Refresh_Gram();
}

/*------------只显示某一区域，其他区域是黑色-------------*/ 
//（x1,y1） :起点坐标
//（x2,y2） :终点坐标
// t:坐标定义方向0-> Y+|        1-> ----X+
//                     |            |
//                     |___X+     Y+|
void OLED_OpenWindow(u8 x1,u8 y1,u8 x2,u8 y2,u8 dir)
{
  OLED_Fill(0,0,128,y1-1,0,0,dir);//清除上边部分
	OLED_Fill(0,y2+1,128,8,0,0,dir);//清除下边部分
  OLED_Fill(0,y1,x1-1,y2,0,0,dir);//清除左部分
  OLED_Fill(x2+1,y1,128,y2,0,0,dir);//清除右边部分
}

/*--------------指定OLED的位置显示横线--------------*/
//使用 Bresenham 算法画线段
//（X1,Y1）:起点坐标  X 0-63 Y 0-128
//（X2,Y2） :终点坐标
// track:0,不显示点轨迹；   1，显示点轨迹 （RW_FLASH=1）
// dir:坐标定义方向0-> Y+|        1-> ----X+
//                     |            |
//                     |___X+     Y+|
void OLED_DrawLine(u8 x1,u8 y1,u8 x2,u8 y2,u8 track,u8 dir)
{
	u16 i;
  u16 X,Y;  //当前下x,y值
	vs16 Distance;   //距离
	vs16 ErrX = 0, ErrY = 0; //x,y误差
  vs16 dX, dY;        //x,y增量
  vs16 uX, uY;        //x,y增加值	
  dX = x2-x1;  //计算X坐标增量
	dY = y2-y1;  //计算Y坐标增量
	X = x1;	Y = y1; 
	
	if(dX > 0) uX = 1; //设置单增方向 
	else if(dX == 0) uX = 0; //垂直线
  else{	uX = -1;	dX = -dX;	}	 //设置单减方向
		
	if(dY > 0) uY = 1; //设置单增方向 
	else if(dY == 0) uY = 0; //水平线
  else{ uY = -1;	dY = -dY;	} //设置单减方向
	
  if(dX > dY) Distance = dX;  //选取基本增量坐标轴 
	else Distance = dY;
     
  for( i = 0; i <= Distance+1; i++) //画线输出
	{
		OLED_DrawPoint(X,Y,1,track,dir);   //画点
		
		ErrX += dX;		ErrY += dY;
		
		if( ErrX > Distance )
		{
		  ErrX -= Distance;
			X += uX;
		}
		
		if( ErrY > Distance )
		{
		  ErrY -= Distance;
			Y += uY;
		}
	}
	if(track==0 && RW_FLASH) OLED_Refresh_Gram();	
}
/*--------------指定OLED的位置显示矩形--------------*/
//使用 Bresenham 算法画线段
//（X1,Y1）:起点坐标  X 0-63 Y 0-128
//（X2,Y2） :终点坐标
// fill:0,不填充(空心矩形)  1,填充(实心矩形)
// track:0,不显示点轨迹；   1，显示点轨迹 （RW_FLASH=1）
// dir:坐标定义方向0-> Y+|        1-> ----X+
//                     |            |
//                     |___X+     Y+|
void OLED_DrawRectangle(u8 x1,u8 y1,u8 x2,u8 y2,u8 fill,u8 track,u8 dir)
{
   if(fill)  OLED_Fill(x1,y1,x2,y2,1,track,dir);
   else
	 {
			OLED_DrawLine(x1,y1,x2-1,y1,track,dir); 
		  OLED_DrawLine(x2,y1,x2,y2-1,track,dir);
		  OLED_DrawLine(x2,y2,x1+1,y2,track,dir);
		  OLED_DrawLine(x1,y2,x1,y1+1,track,dir);		  
	 }
}

/*--------------绘制一个圆--------------*/
//（X，Y）圆心坐标
// 半径R
// fill:0,不填充(空心圆)  1,填充(实心圆)
// track:0,不显示点轨迹；   1，显示点轨迹（RW_FLASH=1）
// dir :坐标定义方向0-> Y+|        1-> ----X+
//                      |            |
//                      |___X+     Y+|
void OLED_DrawCircle(u8 x,u8 y,u8 r,u8 fill,u8 track,u8 dir)
{
  vs16 X=0,Y=r,Err;
	Err = 3 - (r << 1);  //判断下个点位置的标志
	while(X <= Y)
	{
	  vs16  i;
		if(fill)
		{
			for(i=X;i<Y;i++)
			{
			  OLED_DrawPoint(x+X,y+i,1,track,dir);
				OLED_DrawPoint(x-X,y+i,1,track,dir);
				OLED_DrawPoint(x-i,y+X,1,track,dir);
				OLED_DrawPoint(x-i,y-X,1,track,dir);
				OLED_DrawPoint(x-X,y-i,1,track,dir);
				OLED_DrawPoint(x+X,y-i,1,track,dir);
				OLED_DrawPoint(x+i,y-X,1,track,dir);
				OLED_DrawPoint(x+i,y+X,1,track,dir);
			}
		}
		else
		{
			  OLED_DrawPoint(x+X,y+Y,1,track,dir);
				OLED_DrawPoint(x-X,y+Y,1,track,dir);
				OLED_DrawPoint(x-Y,y+X,1,track,dir);
				OLED_DrawPoint(x-Y,y-X,1,track,dir);
				OLED_DrawPoint(x-X,y-Y,1,track,dir);
				OLED_DrawPoint(x+X,y-Y,1,track,dir);
				OLED_DrawPoint(x+Y,y-X,1,track,dir);
				OLED_DrawPoint(x+Y,y+X,1,track,dir);		
		}
		X++;
		if(Err < 0)  Err += 4*X+6;
		else { Err += 10+4*(X-Y); Y--; }
	}
	if(track==0 && RW_FLASH) OLED_Refresh_Gram();
}

/*-------------------在指定位置显示一个字符--------------------*/
//x:0~127
//y:0~63
//chr:字符
//size:选择字体 12/16/20/24/28/32/36
//mode:0,反白显示;1,正常显示				 
//track:0,不显示点轨迹； 1，显示点轨迹 
//wFalsh:0,不写入显存；  1，写入显存
void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 size,u8 mode,u8 track,u8 wFalsh)
{      			    
	u8 temp,t,t1;
	u8 y0=y;
	u8 csize=(size/8+((size%8)?1:0))*(size/2);	//得到字体一个字符对应点阵集所占的字节数
	chr=chr-' ';//得到偏移后的值		 
	for(t=0;t<csize;t++)
	{  
		switch(size)
		{
			case 12: temp=F1206[chr][t];  break; //调用1206字体
			case 16: temp=F1608[chr][t];	break; //调用1608字体
			case 20: temp=F2010[chr][t];	break; //调用2010字体
			case 24: temp=F2412[chr][t];	break; //调用2412字体
			case 28: temp=F2814[chr][t];	break; //调用2814字体
			case 32: temp=F3216[chr][t];	break; //调用3216字体
			case 36: temp=F3618[chr][t];	break; //调用3618字体
				
			default:  break; //没有的字库
		}
		
		for(t1=0;t1<8;t1++)
		{
			if(temp&0x80) OLED_DrawPoint(x,y,mode,track,1);
			else OLED_DrawPoint(x,y,!mode,track,1);
			temp<<=1;
			y++;
			if((y-y0)==size){ y=y0;x++;break; }
		}  	 
	}
  if(track==0 && wFalsh) OLED_Refresh_Gram();	
}

/*--------------------显示字符串-------------------*/
//x,y:起点坐标  
//*p:字符串起始地址 
//size:选择字体 12/16/20/24/28/32/36
//mode:0,反白显示;1,正常显示				 
//track:0,不显示点轨迹；1，显示点轨迹 
void OLED_ShowString(u8 x,u8 y,char *p,u8 size,u8 mode,u8 track)
{	
    while((*p<='~')&&(*p>=' '))//判断是不是非法字符!
    {       
        if(x>(Max_Column-(size/2))){x=0;y+=size;}  //超出自动续行
        if(y>(Max_Row-size)){y=x=0;}  //超出换页
        OLED_ShowChar(x,y,*p,size,mode,0,track);	 
        x+=size/2;  p++;
    }  	
		if(track==0) OLED_Refresh_Gram();
}

/*---------------------滚动显示字符串---------------------*/
//x:0~127
//y:0~63
//num1,num2:对应汉字在数组里的编号(显示编号为num1,num1+1,...,num2的汉字)
//size:选择字体 16
//mode:0,反白显示;1,正常显示				 
//speed:滚动速度 （5-200效果最好）
//注：为显示正常，需将num2对应的中文为【空格】
void OLED_RollString(u8 x1,u8 y1,char *p,u8 size,u8 mode,u8 speed)
{
	static vs8 x,y;
	static u8 flag=1;
  //只接收到传入的x1,y1一次
	if(flag == 1) {	x = x1; y = y1;	flag = 0;}

	OLED_ShowString(x,y,p,size,mode,0);
	x--;		
	if(x<0)
	{
		x=Max_Column-1;
		y=y-size;

		if(y<0) y=Max_Row-size;				 
	}
	Delay_Ms(speed);
}

//m^n函数
u32 mypow(u8 m,u8 n)
{
	u32 result=1;	 
	while(n--)result*=m;    
	return result;
}
/*------------------------显示len个数字---------------------*/
//x,y :起点坐标	
//num:数值(0~4294967295);	
//len :数字的位数
//size:字体大小 12/16/20/24/28/32/36
//mode:模式	0,填充模式;1,叠加模式
//track:0,不显示点轨迹；   1，显示点轨迹 		  
void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size,u8 mode,u8 track)
{         	
	u8 t,temp;
	u8 enshow=0;						   
	for(t=0;t<len;t++)
	{
		temp=(num/mypow(10,len-t-1))%10;
		if(enshow==0&&t<(len-1))
		{
			if(temp==0)
			{
				OLED_ShowChar(x+(size/2)*t,y,' ',size,mode,track,1);
				continue;
			}else enshow=1; 
		 	 
		}
	 	OLED_ShowChar(x+(size/2)*t,y,temp+'0',size,mode,track,1); 
	}
}

/*----------------------显示中文----------------------*/
//x:0~127
//y:0~63
//num1,num2:对应汉字在数组里的编号(显示编号为num1,num1+1,...,num2的汉字)
//size:选择字体 12/16/24
//mode:0,反白显示;1,正常显示				 
//track:0,不显示点轨迹；   1，显示点轨迹
void OLED_ShowCHinese(u8 x,u8 y,u8 num1,u8 num2,u8 size,u8 mode,u8 track)
{
	u8 temp,t,t1,t2;
	u8 y0=y;
	u8 csize=(size/8+((size%8)?1:0))*(size/2);	//得到字体一个字符对应点阵集所占的字节数
	
  for(t2=0;t2<2*(num2-num1+1);t2++)	//中文占两个
	{
		for(t1=0;t1<csize;t1++)
		{  		
		 	switch(size)
			{
				case 12: temp=Hz1212[num1*2+t2][t1];  break; //调用1212字体
				case 16: temp=Hz1616[num1*2+t2][t1];	break; //调用1616字体
				case 20: temp=Hz2020[num1*2+t2][t1];	break; //调用2020字体
				case 24: temp=Hz2424[num1*2+t2][t1];	break; //调用2424字体
				case 28: temp=Hz2828[num1*2+t2][t1];	break; //调用2828字体
				case 32: temp=Hz3232[num1*2+t2][t1];	break; //调用3232字体
				case 36: temp=Hz3636[num1*2+t2][t1];	break; //调用3636字体
				
				default:   break; //没有的字库
			}			

			if(x>Max_Column-1){x=0;y+=size;y0=y;}  //超出自动续行
			if(y>(Max_Row-1)){x=0;y=0;y0=y;} //超出换页		
			
			for(t=0;t<8;t++)
			{				
				if(temp&0x80)OLED_DrawPoint(x,y,mode,track,1);
				else OLED_DrawPoint(x,y,!mode,track,1);
				temp<<=1; y++;
				if((y-y0)==size){ y=y0; x++; break; }
			}  	 
		}
  }
  if(track==0 && RW_FLASH) OLED_Refresh_Gram();	  
}

/*---------------------滚动显示中文---------------------*/
//x:0~127
//y:0~63
//num1,num2:对应汉字在数组里的编号(显示编号为num1,num1+1,...,num2的汉字)
//size:选择字体 16
//mode:0,反白显示;1,正常显示				 
//speed:滚动速度 （5-200效果最好）
//注：为显示正常，需将num2对应的中文为【空格】
void OLED_RollCHinese(u8 x1,u8 y1,u8 num1,u8 num2,u8 size,u8 mode,u8 speed)
{
	static vs8 x,y;
	static u8 flag=1;
	
	if(flag == 1) //只接收到传入的x1,y1一次
	{
		x = x1; 
		y = y1;
		flag = 0;
	}

	OLED_ShowCHinese(x,y,num1,num2,size,mode,0);
	x--;		
	if(x<0)
	{
		x=Max_Column-1;
		y=y-size;

		if(y<0) y=Max_Row-size;				 
	}
	Delay_Ms(speed);
	
}

/*----------------------显示设计图案----------------------*/
//x:0~127
//y:0~63
//num1,num2:对应图案在数组里的编号(显示编号为num1,num1+1,...,num2的图案)
//size:选择字体 12/16/24
//mode:0,反白显示;1,正常显示				 
//track:0,不显示点轨迹；   1，显示点轨迹
void OLED_DrawDesign(u8 x,u8 y,u8 num1,u8 num2,u8 size,u8 mode,u8 track)
{
	u8 temp,t,t1,t2;
	u8 y0=y;
	u8 csize=(size/8+((size%8)?1:0))*(size/2);	//得到字体一个字符对应点阵集所占的字节数
	
  for(t2=0;t2<2*(num2-num1+1);t2++)	
	{
		for(t1=0;t1<csize;t1++)
		{  		
		 	switch(size)
			{
				case 12: temp=Des1212[num1*2+t2][t1]; break; //调用1212字体
				case 16: temp=Des1616[num1*2+t2][t1];	break; //调用1616字体
				case 20: temp=Des2020[num1*2+t2][t1];	break; //调用2020字体
				case 24: temp=Des2424[num1*2+t2][t1];	break; //调用2424字体
				case 28: temp=Des2828[num1*2+t2][t1];	break; //调用2828字体
				case 32: temp=Des3232[num1*2+t2][t1];	break; //调用3232字体
				case 36: temp=Des3636[num1*2+t2][t1];	break; //调用3636字体
				
				default:   break; //没有的字库
			}			

			if(x>Max_Column-1){x=0;y+=size;y0=y;}  //超出自动续行
			if(y>(Max_Row-1)){x=0;y=0;y0=y;} //超出换页		
			
			for(t=0;t<8;t++)
			{				
				if(temp&0x80)OLED_DrawPoint(x,y,mode,track,1);
				else OLED_DrawPoint(x,y,!mode,track,1);
				temp<<=1; y++;
				if((y-y0)==size){ y=y0; x++; break; }
			}  	 
		}
  }
  if(track==0 && RW_FLASH) OLED_Refresh_Gram();	  
}

/*---------------------显示图片---------------------*/
//功能描述：显示BMP图片128×64起始点坐标(x,y),x的范围0～127，y为页的范围0～7
//起点坐标（x1,y1）
//终点坐标（x2,y2）
//BMP[]为bmp.h文件中的图片字模数组
void OLED_DrawBMP(u8 x1,u8 y1,u8 x2,u8 y2,u8 *bmp)
{ 	
  u8 x,y;
  
  if(y2%8==0) y=y2/8;      
  else y=y2/8+1;
	
	for(y=y1;y<y2;y++)
	{
	  OLED_WR_Byte(0xb0+y,OLED_CMD);
	  OLED_WR_Byte(((x1&0xf0)>>4)|0x10,OLED_CMD);
	  OLED_WR_Byte((x1&0x0f)|0x01,OLED_CMD);
    for(x=x1;x<x2;x++)
		{    
      OLED_WR_Byte(*bmp++,OLED_DATA);    	
		}
	}
} 

/*-----------------初始化SSD1306------------------*/					    
void OLED_Init(void)
{ 	 	 

#if OLED_IIC  //OLED_SPI	
 	GPIO_InitTypeDef  GPIO_InitStructure;
 	

	RCC_APB2PeriphClockCmd(OLED_SCLK_CLK|OLED_SDIN_CLK, ENABLE);	 //使能A端口时钟
	GPIO_InitStructure.GPIO_Pin = OLED_SCLK_PIN|OLED_SDIN_PIN;	 
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;//速度50MHz
 	GPIO_Init(OLED_SDIN_PART, &GPIO_InitStructure);	  //初始化GPIO
	GPIO_Init(OLED_SCLK_PART, &GPIO_InitStructure);	  //初始化GPIO
	
  OLED_SCLK_Set();
	OLED_SDIN_Set();
	
	Delay_Ms(300);
#else	
	
 	GPIO_InitTypeDef  GPIO_InitStructure;
 	
 	RCC_APB2PeriphClockCmd(OLED_SCLK_CLK|OLED_SDIN_CLK|OLED_RST_CLK
	                        |OLED_DC_CLK|OLED_CS_CLK, ENABLE);	 //使能端口时钟 
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;//速度50MHz
	
	GPIO_InitStructure.GPIO_Pin = OLED_SCLK_PIN;	
 	GPIO_Init(OLED_SCLK_PART, &GPIO_InitStructure);	  //初始化GPIO
	
	GPIO_InitStructure.GPIO_Pin = OLED_SDIN_PIN;	
 	GPIO_Init(OLED_SDIN_PART, &GPIO_InitStructure);	  //初始化GPIO
	
	GPIO_InitStructure.GPIO_Pin = OLED_RST_PIN;	
 	GPIO_Init(OLED_RST_PART, &GPIO_InitStructure);	  //初始化GPIO

	GPIO_InitStructure.GPIO_Pin = OLED_DC_PIN;	
 	GPIO_Init(OLED_DC_PART, &GPIO_InitStructure);	  //初始化GPIO

	GPIO_InitStructure.GPIO_Pin = OLED_CS_PIN;	
 	GPIO_Init(OLED_CS_PART, &GPIO_InitStructure);	  //初始化GPIO

	OLED_SCLK_Set();
	OLED_SDIN_Set();
  OLED_DC_Set();
	OLED_CS_Set();
	
	OLED_RST_Set();
	Delay_Ms(100);
	OLED_RST_Clr();
	Delay_Ms(200);
	OLED_RST_Set(); 

#endif

	OLED_WR_Byte(0xAE,OLED_CMD); //--关闭oled面板
	OLED_WR_Byte(0x00,OLED_CMD); //--设置低列地址
	OLED_WR_Byte(0x10,OLED_CMD); //--设置高列地址
	OLED_WR_Byte(0x40,OLED_CMD); //--内存显示起始行(0x00~0x3F)
	OLED_WR_Byte(0xB0,OLED_CMD); //--设置页地址
  OLED_WR_Byte(0x81,OLED_CMD); //--设置对比度控制寄存器
	OLED_WR_Byte(0xCF,OLED_CMD); //--设置SEG输出电流亮度
	OLED_WR_Byte(0xA1,OLED_CMD); //--设置SEG/列映射        0xa0左右反置 0xa1正常
	OLED_WR_Byte(0xC8,OLED_CMD); //--设置COM/Row扫描方向  0xc0上下反置 0xc8正常
	OLED_WR_Byte(0xA6,OLED_CMD); //--设置正常显示
	OLED_WR_Byte(0xA8,OLED_CMD); //--设置多路复用比(1：64)
	OLED_WR_Byte(0x3f,OLED_CMD); //--1/64 duty
	OLED_WR_Byte(0xD3,OLED_CMD); //--设置显示偏移移位映射RAM计数器(0x00~0x3F)
	OLED_WR_Byte(0x00,OLED_CMD); //--不抵消
	OLED_WR_Byte(0xd5,OLED_CMD); //--设置显示时钟分频比/振荡频率
	OLED_WR_Byte(0x80,OLED_CMD); //--设置分频比，设置时钟为100帧/秒
	OLED_WR_Byte(0xD9,OLED_CMD); //--设置pre-charge时期
	OLED_WR_Byte(0xF1,OLED_CMD); //--设定预充电为15个时钟，放电为1个时钟
	OLED_WR_Byte(0xDA,OLED_CMD); //--设置com引脚硬件配置
	OLED_WR_Byte(0x12,OLED_CMD);
	OLED_WR_Byte(0xDB,OLED_CMD); //--设置vcomh
	OLED_WR_Byte(0x40,OLED_CMD); //--设置VCOM取消选择级别
	OLED_WR_Byte(0x20,OLED_CMD); //--设置页面寻址模式(0x00/0x01/0x02)
	OLED_WR_Byte(0x02,OLED_CMD); //
	OLED_WR_Byte(0x8D,OLED_CMD); //--设置充电启用/禁用
	OLED_WR_Byte(0x14,OLED_CMD); //--设置(0x10)禁用
	OLED_WR_Byte(0xA4,OLED_CMD); //--禁用整个显示(0xa4/0xa5)
	OLED_WR_Byte(0xA6,OLED_CMD); //--禁用反色显示(0xa6/a7) 
	OLED_WR_Byte(0xAF,OLED_CMD); //--开启oled面板
	
	OLED_WR_Byte(0xAF,OLED_CMD); /*开显示*/ 
	OLED_Clear();	
}



/*--------------------- END OF FIAL-------------------*/
