#include "pwr.h"

//PA0用作唤醒时无需初始化 
/* 说明：

(1)待机模式：1.8V电源关闭
	 进入方式：配置PWR_CR寄存器的PDDS+SLEEPDEEP位+WFI或WFE命令
	 可通过WKUP引脚的上升沿、RTC闹钟事件、NRST引脚上的外部复位 、IWDG复位唤醒

(2)睡眠模式:内核停止,所有外设包括M3核心的外设,如NVIC,系统时钟(SysTick)等仍在运行
   进入方式：1）调用WFI命令（等待中断唤醒）进入 
             2）调用WFE命令（等待事件唤醒）进入 
	 
(3)停止模式:所有的时钟都已停止（关闭所有1.8V区域的时钟、HSI和HSE的振荡器关闭）
	 进入方式：配置PWR_CR寄存器的PDDS+LPDS位+SLEEPDEEP位+WFI或WFE命令
   可通过任一外部中断唤醒(在外部中断寄存器中设置)
*/


/**
  * @brief  配置电源模式.
  * @param  None
  * @retval None
  */
void POWER_Mode(uint8_t mode)
{
	switch( mode )
	{
		case STANDBY: 
 
				/* 使能电源管理单元的时钟,必须要使能时钟才能进入待机模式 */
				RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR , ENABLE);
				
				//printf("\r\n 即将进入待机模式，进入待机模式后可按KEY1唤醒，唤醒后会进行复位，程序从头开始执行\r\n");
				
				/*清除WU状态位*/
				PWR_ClearFlag (PWR_FLAG_WU);
				
				/* 使能WKUP引脚的唤醒功能 ，使能PA0*/
				PWR_WakeUpPinCmd (ENABLE);
				
				/* 进入待机模式 */
				PWR_EnterSTANDBYMode();
		
			break;
		
		case SLEEP:  
			
				//进入睡眠模式
				__WFI();	//WFI指令进入睡眠
			break;
		
		case STOP:
			
				/* 进入停止模式，设置电压调节器为低功耗模式，等待中断唤醒 */
		    PWR_EnterSTOPMode(PWR_Regulator_LowPower,PWR_STOPEntry_WFI);
			break;
	}
	
}


/**
  * @brief  待机状态唤醒检测.
  * @param  None
  * @retval None
  */
FlagStatus POWER_Check(void)
{
	FlagStatus state;
	//检测复位来源
	if(PWR_GetFlagStatus(PWR_FLAG_WU) == SET)
	{
		//printf("\r\n 待机唤醒复位 \r\n");
		state = SET; 
	}
	else
	{
		//printf("\r\n 非待机唤醒复位 \r\n");
		state = RESET;
	}
	return state;
}

/**
  * @brief  软件复位.
  * @param  None
  * @retval None
  */
void SWRST(void)
{
	__set_FAULTMASK(1);
	NVIC_SystemReset();
}


/*--------------------- END OF FIAL-------------------*/
