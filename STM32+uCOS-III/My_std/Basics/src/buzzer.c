/********************************************************
 * 文件名  ：buzzer.c
 * 描述    ：蜂鸣器配置
 *          
 * 实验平台：STM32开发板 基于STM32F103C8T6
 * 硬件连接：-----------------
 *          |   PC15 - BEEP   |
 *          |       
 *          |                 |
 *           ----------------- 
 * 注意事项：																										  
*********************************************************/

#include "buzzer.h"


/*---------------------  配置LED用到的I/O口 -------------------------*/
void BEEP_GPIO_Config(void)	
{
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_APB2PeriphClockCmd( BEEP_GPIO_CLK, ENABLE); // 使能PC端口时钟  
 
	GPIO_InitStructure.GPIO_Pin = BEEP_GPIO_PIN;	//选择对应的引脚
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;       
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(BEEP_GPIO_PORT, &GPIO_InitStructure);  //初始化GPIO端口

  GPIO_ResetBits(BEEP_GPIO_PORT, BEEP_GPIO_PIN );	 // 关闭蜂鸣器	
}

/*蜂鸣器发声函数
函数原型： void BUZZER_on(char times, char n10ms)
入口参数： timse :   发声的次数
           on10ms:   单次发声的时长
           offms:单次发声的间隔时长
出口参数： 无

*/
void BUZZER_on(short times,short onms,short offms)
{
	u8 i;
	BEEP_GPIO_Config();
	for(i=0;i<times;i++)      //发声频率
	{
		BEEP_ON;
		Delay_Ms(onms);  //单次发声时间
		
		BEEP_OFF;
		Delay_Ms(offms);  //单次无声时间	 
	}
}

