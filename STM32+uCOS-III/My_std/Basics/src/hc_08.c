#include "hc_08.h"
#include "usart2.h"
#include "mp3.h"

extern u8 menuKey; //菜单键
extern u8 upKey; //向上键
extern u8 downKey; //向下键

u8 Str0[] = "循环播放";
u8 Str1[] = "扩音";
u8 Str2[] = "睡眠";
u8 Str3[] = "复位";
u8 Str4[] = "下一曲";
u8 Str5[] = "上一曲";
u8 Str6[] = "音+";
u8 Str7[] = "音-";
u8 Str8[] = "音25";
u8 Str9[] = "播放暂停";
u8 Str10[] = "停止";
u8 Str11[] = "第一首";

u8 makeflag=0;

/*--------------------- 缓存区收发数据 --------------------*/
//数据发送错误或格式不正确会发送错误代码Receive ERROR!
void HC_08_RvData_Handler(void)
{
	u8 len;
	u8 SendINFO[64];

	
	
  u8 Default[] = "Send a invalid CMD!\n";
	u8 Reply[] = "OK\n";
	u8 LineFeed[] = "\n";
	
  static u8 state1,state2,state3;
	
	if(rxflag2 == 1)
  {
		rxflag2 = 0;
		
		LED_on(2,80,80);
		
		if(USART2_RX_STA&0x80)
		{
			
			len = (USART2_RX_STA>>1) & 0x1f;
			if(len==0)
			{
				strcpy((char*)SendINFO,"Sending data is NULL\n");
			  USART2_SendData(SendINFO,22);
			}
			else
			{
				strcpy((char*)SendINFO,"Send data: ");
				USART2_SendData(SendINFO,11);
				USART2_SendData(USART2_RX_BUF,len);
				USART2_SendData(LineFeed,2);
				USART2_SendData(Reply,4);
				if(strncmp((const char *)USART2_RX_BUF,(const char *)Str0,len) == 0)
				{
					makeflag++;
					if(makeflag>0)
					{
						makeflag = 0;
						(state1==0)?(state1=1):(state1=0);
						MP3_AllCycle(state1); //循环播放
					}	
				}
				else if(strncmp((const char *)USART2_RX_BUF,(const char *)Str1,len) == 0)
				{ 
					makeflag++;
					if(makeflag>0)
					{
						makeflag = 0;
						(state2==0)?(state2=1):(state2=0);
						MP3_UpSound(state2); //扩音
					}			
				}
				else if(strncmp((const char *)USART2_RX_BUF,(const char *)Str2,len) == 0)
				{
					makeflag++;
					if(makeflag>0)
					{
						makeflag = 0;
						MP3_SleepMode; //睡眠
					}			
				}
				else if(strncmp((const char *)USART2_RX_BUF,(const char *)Str3,len) == 0)
				{ 	
					makeflag++;
					if(makeflag>0)
					{
						makeflag = 0;
						MP3_Reset; //复位
					}	
				}
				else if(strncmp((const char *)USART2_RX_BUF,(const char *)Str4,len) == 0)
				{ 	
					makeflag++;
					if(makeflag>0)
					{
						makeflag = 0;
						MP3_NextSong; //下一曲
					}	
				}
				else if(strncmp((const char *)USART2_RX_BUF,(const char *)Str5,len) == 0)
				{ 	
					makeflag++;
					if(makeflag>0)
					{
						makeflag = 0;
						MP3_PrevSong; //上一曲
					}	
				}
				else if(strncmp((const char *)USART2_RX_BUF,(const char *)Str6,len) == 0)
				{ 	
					makeflag++;
					if(makeflag>0)
					{
						makeflag = 0;
						MP3_SoundUp; //音+
					}	
				}
				else if(strncmp((const char *)USART2_RX_BUF,(const char *)Str7,len) == 0)
				{ 	
					makeflag++;
					if(makeflag>0)
					{
						makeflag = 0;
						MP3_SoundDown; //音-
					}	
				}
				else if(strncmp((const char *)USART2_RX_BUF,(const char *)Str8,len) == 0)
				{ 	
					makeflag++;
					if(makeflag>0)
					{
						makeflag = 0;
						MP3_PointSound(25); //指定声音25
					}	
				}
				else if(strncmp((const char *)USART2_RX_BUF,(const char *)Str9,len) == 0)
				{ 	
					makeflag++;
					if(makeflag>0)
					{
						makeflag = 0;
						(state3==0)?(state3=1):(state3=0);
						if(state3==0)
						{
							MP3_Play; //播放
						}
						else
            {
						  MP3_Pause; //暂停
						}
					}	
				}
				else if(strncmp((const char *)USART2_RX_BUF,(const char *)Str10,len) == 0)
				{ 	
					makeflag++;
					if(makeflag>0)
					{
						makeflag = 0;
						MP3_Stop; //停止
					}	
				}
				else if(strncmp((const char *)USART2_RX_BUF,(const char *)Str11,len) == 0)
				{ 	
					makeflag++;
					if(makeflag>0)
					{
						makeflag = 0;
						MP3_Pointplay(1); //指定第一首播放
					}	
				}	
				
				else
				{
					USART2_SendData(Default,22);
				}
			}				
		}
		else //接收失败
		{ 				
		  USART2_SendERROR();
		}
    USART2_RX_STA=0;			
	}
	
}







