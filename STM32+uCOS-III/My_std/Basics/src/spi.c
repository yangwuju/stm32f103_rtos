#include "spi.h"

//模拟SPI时序

#if    SPI_SIMULATE

/**
  * @brief  初始化SPI的GPIO
  * @param  无
  * @retval 无
**/
void SPI_GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	//使能SPI的GPIO时钟
	RCC_APB2PeriphClockCmd( SPI_GPIO_CLK, ENABLE);
	
  GPIO_InitStructure.GPIO_Speed=GPIO_Speed_10MHz;	  
  GPIO_InitStructure.GPIO_Mode=GPIO_Mode_Out_PP;
	
	GPIO_InitStructure.GPIO_Pin = SPI_CS_PIN;
  GPIO_Init(SPI_CS_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = SPI_CLK_PIN;
  GPIO_Init(SPI_CLK_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = SPI_MOSI_PIN;
  GPIO_Init(SPI_MOSI_PORT, &GPIO_InitStructure);
	
	//配置SPI数据接收引脚
  GPIO_InitStructure.GPIO_Pin = SPI_MISO_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;	 //上拉输入
  GPIO_Init(SPI_MISO_PORT, &GPIO_InitStructure);
}

/**
  * @brief  模拟SPI时序发送命令
  * @param  无
  * @retval 无
**/
void SPI_SendCMD(uint8_t cmd)
{
	uint8_t i;
	SPI_CS_SET; 
  SPI_CLK_CLR;
  SPI_MOSI_CLR;	
	SPI_Delay(); //延时一段时间
	
	SPI_CS_CLR; //CS拉低，被选中
	for( i=0; i<8; i++)
	{
		if(cmd & 0x80)
		{
			SPI_MOSI_SET; //SDO引脚为高电平
		}
		else
		{
			SPI_MOSI_CLR; //SDO引脚为低电平
		}
		
		SPI_CLK_SET;
		SPI_Delay(); //延时一段时间
	
		SPI_CLK_CLR;
		SPI_Delay(); //延时一段时间
		
		cmd <<= 1;	
	}	
}

/**
  * @brief  模拟SPI时序接收数据
  * @param  无
  * @retval 无
**/
uint16_t SPI_ReadData(void)
{
	uint8_t i;
	uint16_t value=0;
	
	SPI_CLK_CLR;
  SPI_MOSI_CLR;	
	SPI_Delay(); //延时一段时间
	
  SPI_CLK_SET;
	for( i=0; i<12; i++)
	{
		value <<= 1;
		
		SPI_CLK_CLR;		
		SPI_Delay(); //延时一段时间 
		
		value |= SPI_MISO_READ; //获取数据
		
		SPI_CLK_SET;
		SPI_Delay(); //延时一段时间	
	
	}
	
	SPI_CS_SET;

	return value;
}

#else      //选择STM32自带的SPI

/**
  * @brief  SPI_FLASH初始化
  * @param  无
  * @retval 无
  */
void SPI_Config(void)
{
  SPI_InitTypeDef  SPI_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
	
	/* 使能SPI时钟 */
	SPI_APBxClock_FUN ( SPI_CLK, ENABLE );
	
	/* 使能SPI引脚相关的时钟 */
 	RCC_APB2PeriphClockCmd( SPI_CS_CLK|SPI_SCK_CLK|
								SPI_MISO_PIN|SPI_MOSI_PIN, ENABLE );
	
  /* 配置SPI的 CS引脚，普通IO即可 */
  GPIO_InitStructure.GPIO_Pin = SPI_CS_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(SPI_CS_PORT, &GPIO_InitStructure);
	
  /* 配置SPI的 SCK引脚*/
  GPIO_InitStructure.GPIO_Pin = SPI_SCK_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(SPI_SCK_PORT, &GPIO_InitStructure);

  /* 配置SPI的 MISO引脚*/
  GPIO_InitStructure.GPIO_Pin = SPI_MISO_PIN;
  GPIO_Init(SPI_MISO_PORT, &GPIO_InitStructure);

  /* 配置SPI的 MOSI引脚*/
  GPIO_InitStructure.GPIO_Pin = SPI_MOSI_PIN;
  GPIO_Init(SPI_MOSI_PORT, &GPIO_InitStructure);

  /* 停止信号 FLASH: CS引脚高电平*/
  SPI_CS_SET;

  /* SPI 模式配置 */
  // 支持SPI模式0及模式3，据此选择模式3，设置CPOL=1 CPHA=1
	/*设置 SPI 的单双向模式 */
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  /*设置 SPI 的主/从机端模式 */
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	/*设置 SPI 的数据帧长度，可选 8/16 位 */
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  /*设置时钟极性 CPOL，可选高/低电平*/
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
	/*设置时钟相位，可选奇/偶数边沿采样 */
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
	/*设置 NSS 引脚由 SPI 硬件控制还是软件控制*/
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	/*设置时钟分频因子， fpclk/分频数=fSCK */
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
  /*设置 MSB/LSB 先行 */
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	/*设置 CRC 校验的表达式 */
  SPI_InitStructure.SPI_CRCPolynomial = 7;
	//初始化SPI结构体
  SPI_Init(SPI_x , &SPI_InitStructure);

  /* 使能 SPI  */
  SPI_Cmd(SPI_x , ENABLE);
}


 /**
  * @brief  使用SPI发送一个字节的数据
  * @param  byte：要发送的数据
  * @retval 正常返回接收到的数据，超时返回ERROR
  */
uint8_t SPI_SendByte(u8 byte)
{
	static __IO uint32_t  SPITimeout = SPI_LONG_TIMEOUT;
	
  /* 等待发送缓冲区为空，TXE事件 */
  while (SPI_I2S_GetFlagStatus(SPI_x , SPI_I2S_FLAG_TXE) == RESET)
	{
    if((SPITimeout--) == 0) return ERROR;
  }

  /* 写入数据寄存器，把要写入的数据写入发送缓冲区 */
  SPI_I2S_SendData(SPI_x , byte);

	SPITimeout = SPI_FLAG_TIMEOUT;
  /* 等待接收缓冲区非空，RXNE事件 */
  while (SPI_I2S_GetFlagStatus(SPI_x , SPI_I2S_FLAG_RXNE) == RESET)
  {
    if((SPITimeout--) == 0) return ERROR;
   }

  /* 读取数据寄存器，获取接收缓冲区数据 */
  return SPI_I2S_ReceiveData(SPI_x);
}

#endif

