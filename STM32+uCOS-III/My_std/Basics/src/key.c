#include "key.h"
#include "delay.h"

uint16_t KeyPresTimeCnt = 0; //按键按下时间计数

//KeyProcessState = 0; //按键未按下状态
//KeyProcessState = 1; //按键处于消抖检测等待状态
//KeyProcessState = 2; //按键在消抖完成之前，松开按键
//KeyProcessState = 3; //按键进入按下状态
//KeyProcessState = 4; //按键处于释放状态
uint8_t KeyProcessState = 0; //按键过程状态标志

/**
  * @brief  初始化按键GPIO
  * @param  无
  * @retval 无
**/
void KEY_Init(void)	
{
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_APB2PeriphClockCmd( KEY1_GPIO_CLK|KEY2_GPIO_CLK|KEY3_GPIO_CLK|KEY4_GPIO_CLK, ENABLE); // 使能PC端口时钟  
 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; 
	GPIO_InitStructure.GPIO_Pin = KEY1_GPIO_PIN;	   //选择对应的引脚	
  GPIO_Init(KEY1_GPIO_PORT, &GPIO_InitStructure);  //初始化GPIO
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = KEY2_GPIO_PIN;     //选择对应的引脚	
	GPIO_Init(KEY2_GPIO_PORT, &GPIO_InitStructure);  //初始化GPIO
	
#if 0	    //只使用2个按键
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = KEY3_GPIO_PIN;     //选择对应的引脚	
	GPIO_Init(KEY3_GPIO_PORT, &GPIO_InitStructure);  //初始化GPIO
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = KEY4_GPIO_PIN;     //选择对应的引脚	
	GPIO_Init(KEY4_GPIO_PORT, &GPIO_InitStructure);  //初始化GPIO
#endif
	
}

/**
  * @brief  按键扫描函数【未进行硬件消抖】
  * @param  无
  * @retval 有  
**/
uint8_t Key_Scan(void)
{
	static uint8_t key_up = 1; //按键按松标志
	if(KEY_MODE)	key_up = 1;  //按键模式选择（1:支持连按  0：不支持连按）
	
  if(key_up && (KEY1 == KEY_ON||KEY2 == KEY_ON||KEY3 == KEY_ON||KEY4 == KEY_ON))
	{
	  Delay_Ms(10);  //消抖
    key_up = 0;
		if(KEY1 == KEY_ON)  return KEY1_PRES;
    else if(KEY2 == KEY_ON)  return KEY2_PRES;
		else if(KEY3 == KEY_ON)  return KEY3_PRES;
		else if(KEY4 == KEY_ON)  return KEY4_PRES;
	}		
	else if(KEY1 == KEY_OFF&&KEY2 == KEY_OFF&&KEY3 == KEY_OFF&&KEY4 == KEY_OFF)
	{
		key_up = 1;
	}
	return KEY_NPRES;
}

/**
  * @brief  按键扫描函数【硬件消抖过】
  * @param  无
  * @retval 有
**/
uint8_t KEY_Scan(GPIO_TypeDef* GPIOx,uint16_t GPIO_Pin)
{			
	/*检测是否有按键按下 */
	if(GPIO_ReadInputDataBit(GPIOx,GPIO_Pin) == KEY_ON )  
	{	 
		/*等待按键释放 */
		while(GPIO_ReadInputDataBit(GPIOx,GPIO_Pin) == KEY_ON);   
		return 	KEY_ON;	 
	}
	else
		return KEY_OFF;
}

/**
  * @brief  按键按下处理函数
  * @param  无
  * @retval 无
**/
void Key_Handle(void)
{
  u8 key;
	key = Key_Scan();
	switch(key)
	{
	  case KEY1_PRES:
		{
			LED1_TOGGLE;
		};
		break;
	  case KEY2_PRES:
		{
			LED1_TOGGLE;
		};
		break;
	  case KEY3_PRES:
		{
			LED1_TOGGLE;
		};
		break;
	  case KEY4_PRES:
		{
			LED1_TOGGLE;
		};
		break;

		default:
			break;
	}
}


/**
  * @brief  按键检测状态机
  * @param  KEY_GPIO_Read：选择按键引脚输入
  * @retval 按下状态
	*   该返回值为以下值之一：
  *     @arg KEY_PRESSED :按键按下
  *     @arg KEY_NOT_PRESSED :按键未被按下
  */
uint8_t KEY_TouchDetect(uint8_t KEY_GPIO_Read)
{ 
	static enumKEYTouchState touch_state = KEY_STATE_RELEASE;
	static uint16_t i;
	uint8_t detectResult = KEY_NOT_PRESSED;
	
	switch(touch_state)
	{
		case KEY_STATE_RELEASE:	
			  KeyProcessState = 0; //按键未按下状态
				if(KEY_GPIO_Read == KEY_ON) //第一次出现触摸信号
				{
					touch_state = KEY_STATE_WAITING;
					detectResult = KEY_NOT_PRESSED;
					
					KeyProcessState = 1; //按键处于消抖检测等待状态
				}
				else	//无触摸
				{
					touch_state = KEY_STATE_RELEASE;
					detectResult = KEY_NOT_PRESSED;
				}
				break;
				
		case KEY_STATE_WAITING:
				if(KEY_GPIO_Read == KEY_ON)
				{
					i++;
					//等待时间大于阈值则认为按键被按下
					//消抖时间 = DURIATION_TIME * 本函数被调用的时间间隔
					//如在定时器中调用，每10ms调用一次，则消抖时间为：DURIATION_TIME*10ms
					if(i > DURIATION_TIME)		
					{
						i=0;
						touch_state = KEY_STATE_PRESSED;
						detectResult = KEY_PRESSED;
						
						KeyProcessState = 3; //按键进入按下状态
					}
					else												//等待时间累加
					{
						touch_state = KEY_STATE_WAITING;
						detectResult =	 KEY_NOT_PRESSED;					
					}
				}
				else	//等待时间值未达到阈值就为无效电平，当成抖动处理					
				{
				    i = 0;
            touch_state = KEY_STATE_RELEASE; 
						detectResult = KEY_NOT_PRESSED;
					  
					  KeyProcessState = 2; //按键在消抖完成之前，松开按键
				}
		
			break;
		
		case KEY_STATE_PRESSED:	
				if(KEY_GPIO_Read == KEY_ON)		//持续按下
				{
					touch_state = KEY_STATE_PRESSED;
					detectResult = KEY_PRESSED;
					
					KeyPresTimeCnt ++; //按键按下时间计数
				}
				else	//按键释放
				{
					touch_state = KEY_STATE_RELEASE;
					detectResult = KEY_NOT_PRESSED;
					
					KeyProcessState = 4; //按键处于释放状态
				}
			break;			
		
		default:
				touch_state = KEY_STATE_RELEASE;
				detectResult = KEY_NOT_PRESSED;
		
		    KeyProcessState = 0; //按键未按下状态
				break;
	}		
	
	return detectResult;
}
/*注：按键使用状态机检测说明
      例如：在10ms定时器里调用时 
						(1)按键消抖时长为:DURIATION_TIME*10ms
            (2)按键按下时长为:KeyPresTimeCnt*10ms
            (3)函数调用：
       //按键按下
       if(KEY_TouchDetect(KEY1)){}

       //按键按下小于1s
       if((KeyProcessState = 4)&&(KeyPresTimeCnt=100)){}

       //按键按下小于x s
       if((KeyProcessState = 4)&&(KeyPresTimeCnt=x*100)){}

*/

/*--------------------- END OF FIAL-------------------*/
