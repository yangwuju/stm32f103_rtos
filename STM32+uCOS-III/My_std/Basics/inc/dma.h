#ifndef __DMA_H
#define __DMA_H

#include "my_include.h"

#define DMA_CLOCK        RCC_AHBPeriph_DMA1
#define DMA_CHANNEL      DMA1_Channel6
#define DMA_FLAG_TC      DMA1_FLAG_TC6

void DMA_Config(void);

#endif 
