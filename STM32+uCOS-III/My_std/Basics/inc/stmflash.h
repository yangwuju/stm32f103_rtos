#ifndef __STMFLASH_H
#define __STMFLASH_H

#include "my_include.h"

//宏定义
//===========================================================
//用户根据自己的需要设置
#define STM32_FLASH_SIZE 256	 		//所选STM32的FLASH容量大小(单位为K)
#define STM32_FLASH_WREN 1        //使能FLASH写入(0，不是能;1，使能)

//FLASH起始地址
#define STM32_FLASH_BASE 0x08000000 	//STM32 FLASH的起始地址

//FLASH解锁键值
#define    STM32_FLASH_DATA    0x0800f000  //STM32 参数存储FLASH的起始地址

//整型
#define    FNUM_WrFlash_FLAG   0x0001 //写flash标志位
#define	   FNUM_INT0           0x0002         
#define    FNUM_INT1           0x0003
#define    FNUM_INT2           0x0004
#define    FNUM_INT3           0x0005
#define    FNUM_INT4           0x0006
//#define    FNUM_INT5           0x0007
//#define    FNUM_INT6           0x0008
//#define    FNUM_INT7           0x0009

//浮点数
#define	   FNUM_DATA0          0x000A
#define	   FNUM_DATA1          0x000C
#define	   FNUM_DATA2          0x000E
#define	   FNUM_DATA3          0x0010
#define	   FNUM_DATA4          0x0012
//#define	   FNUM_DATA5          0x0014
//#define	   FNUM_DATA6          0x0016
//#define	   FNUM_DATA7          0x0018
//#define	   FNUM_DATA8          0x001A
//#define	   FNUM_DATA9          0x001C
//===========================================================

//变量声明
//===========================================================
extern u8 needRead,needWrite;//需要读写的参数对应的标识位，每一位对应一个参数

extern u8 WriteFlash_FlagByte; //读写FlASH标识

extern int    TempIntData0;
extern int    TempIntData1;
extern int    TempIntData2;
extern int    TempIntData3;
extern int    TempIntData4;

extern float  TempFltData0;
extern float  TempFltData1;
extern float  TempFltData2;
extern float  TempFltData3;
extern float  TempFltData4;
//===========================================================

//函数声明
//===========================================================
u16 STMFLASH_ReadHalfWord(u32 faddr);		  //读出半字
void STMFLASH_WriteLenByte(u32 WriteAddr,u32 DataToWrite,u16 Len);	//指定地址开始写入指定长度的数据
u32 STMFLASH_ReadLenByte(u32 ReadAddr,u16 Len);						//指定地址开始读取指定长度数据
void STMFLASH_Write(u32 WriteAddr,u16 *pBuffer,u16 NumToWrite);		//从指定地址开始写入指定长度的数据
void STMFLASH_Read(u32 ReadAddr,u16 *pBuffer,u16 NumToRead);   		//从指定地址开始读出指定长度的数据

//测试写入
void Test_Write(u32 WriteAddr,u16 WriteData);	
void dataFlash_pro(void);//数据读写
//===========================================================


#endif
