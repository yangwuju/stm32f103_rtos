#ifndef	__MODBUS_RTU1_H
#define	__MODBUS_RTU1_H

#include "my_include.h"

#define  RS485DIR_RECV   0
#define  RS485DIR_SEND   1 
//#define   RS485DIR1   BIT_ADDR((u32)&(GPIOB->ODR), 3)  //PB.3	  USART1 RS485方向控制



extern u8 SlvAddr;  //站号
extern u8 RS4851TCPbaudRate;   //波特率
extern volatile u16 reg16[0x16]; 
u16 crc16Value(u8 *dat,u8 len);
u8 Read_Modbus_UART1(void);
void GPIO_Configuration_485Send(void);
void RS485_sendData(char *aa,u16 length);
void rtuSendErrorResponse(void);
void updateReg16Value(void);
u8 updateSysStatusFromReg16Value(u16 reg16addr, u16 reg16num,__packed u8 *datptr);
void rtuSendReg16Valu(u16 reg16addr, u16 reg16num);

#endif

