#ifndef __PID_H
#define __PID_H

#include "my_include.h"


typedef struct 
{
    float Kp;       /* 比列系数 */
    float Ki;       /* 积分系数 */
    float Kd;       /* 微分系数 */

    float Ek;       /* 当前误差 */
    float Ek1;      /* 上一次误差 */
    float Ek2;      /* 上上一次误差 */
    float Es;       /* 累计误差 */

    float result;
} PID_t;



void PID_Init(PID_t *pid); //初始化PID参数
void PID_SetArgs(float Kp, float Ki, float Kd, PID_t *pid); //设置PID参数

float PID_Pos(float objVal, float curVal, PID_t *pid); //位置式PID
float PID_Inc(float objVal, float curVal, PID_t *pid); //增量式PID

#endif 
