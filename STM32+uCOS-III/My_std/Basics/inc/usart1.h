#ifndef __USART1_H
#define __USART1_H

#include "my_include.h"

#define  USART1_RX_LEN   64
extern u8 USART1_RX_BUF[USART1_RX_LEN]; //接收缓冲,最大USART_REC_LEN个字节.

extern u8 rxflag1; //接收标识
extern u8 rxError1; //错误标识

extern u16 USART1_RX_STA; //接收状态标记

/*----------------  函数声明 ---------------*/
void USART1_Config(u32 baud);
void USART1_SendData(uint8_t *ch,uint8_t length);
void USART1_SendByte(uint8_t data);
void USART1_SendHalfWord( uint16_t data);
void USART1_SendArray( uint8_t *array, uint8_t num);
void USART1_SendString( uint8_t *str);
	
void USART1_SendBUF(void);

#endif 
