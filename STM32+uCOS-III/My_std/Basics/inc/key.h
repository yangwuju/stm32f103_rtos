#ifndef __KEY_H
#define __KEY_H

#include "my_include.h"

/*-----------------------  按键GPIO定义 ---------------------------*/
#define KEY1_GPIO_PORT    	GPIOA			              /* GPIO端口 */
#define KEY1_GPIO_CLK 	    RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define KEY1_GPIO_PIN		    GPIO_Pin_0			        /* 连接到SCL时钟线的GPIO */

#define KEY2_GPIO_PORT    	GPIOC			              /* GPIO端口 */
#define KEY2_GPIO_CLK 	    RCC_APB2Periph_GPIOC		/* GPIO端口时钟 */
#define KEY2_GPIO_PIN		    GPIO_Pin_13			        /* 连接到SCL时钟线的GPIO */

#define KEY3_GPIO_PORT    	GPIOA			              /* GPIO端口 */
#define KEY3_GPIO_CLK 	    RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define KEY3_GPIO_PIN		    GPIO_Pin_7			        /* 连接到SCL时钟线的GPIO */

#define KEY4_GPIO_PORT    	GPIOA			              /* GPIO端口 */
#define KEY4_GPIO_CLK 	    RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define KEY4_GPIO_PIN		    GPIO_Pin_6			        /* 连接到SCL时钟线的GPIO */

/*-----------------------  按键状态定义 ---------------------------*/
#define KEY_MODE 0   //按键模式选择（1:支持连按  0：不支持连按）

#define KEY_ON   1  //按键按下
#define KEY_OFF  0  //按键松开

//未消抖按键输入定义
#define KEY1  GPIO_ReadInputDataBit( KEY1_GPIO_PORT, KEY1_GPIO_PIN)
#define KEY2  GPIO_ReadInputDataBit( KEY2_GPIO_PORT, KEY2_GPIO_PIN)
#define KEY3  GPIO_ReadInputDataBit( KEY3_GPIO_PORT, KEY3_GPIO_PIN)
#define KEY4  GPIO_ReadInputDataBit( KEY4_GPIO_PORT, KEY4_GPIO_PIN)

//消抖按键输入定义
#define KEY_NPRES  0
#define KEY1_PRES  1
#define KEY2_PRES  2
#define KEY3_PRES  3
#define KEY4_PRES  4

#define KEY1_STATE  KEY_Scan(KEY1_GPIO_PORT, KEY1_GPIO_PIN)
#define KEY2_STATE  KEY_Scan(KEY2_GPIO_PORT, KEY2_GPIO_PIN)
#define KEY3_STATE  KEY_Scan(KEY3_GPIO_PORT, KEY3_GPIO_PIN)
#define KEY4_STATE  KEY_Scan(KEY4_GPIO_PORT, KEY4_GPIO_PIN)


/******按键按下状态机相关******/
typedef enum
{
	KEY_STATE_RELEASE = 0,  //按键释放  
	KEY_STATE_WAITING,			//按键按下等待
	KEY_STATE_PRESSED,			//按键按下	
		
}enumKEYTouchState;

#define KEY_PRESSED 				1
#define KEY_NOT_PRESSED			0

//按键消抖阈值
#define DURIATION_TIME				2

extern uint16_t KeyPresTimeCnt; //按键按下时间计数
extern uint8_t KeyProcessState; //按键过程状态标志
/*-----------------------  函数声明 ---------------------------*/

void KEY_Init(void);
uint8_t Key_Scan(void);  //按键扫描【未进行硬件消抖】
uint8_t KEY_Scan(GPIO_TypeDef* GPIOx,uint16_t GPIO_Pin);  //按键扫描【硬件消抖过】
void Key_Handle(void);   //按键按下处理函数

uint8_t KEY_TouchDetect(uint8_t KEY_GPIO_Read); //按键检测状态机

#endif /* __KEY_H */
