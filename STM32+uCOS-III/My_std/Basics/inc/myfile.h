#ifndef __MYFILE_H
#define __MYFILE_H

#include "my_include.h"
u8 STATE_TOGGLE(void);

u8 GateValueJudge(u16 SetValue,u8 StateKeepValue);

float Data_StabVal(u8 Stability);

float Get_MaxVal(u8 times);
float Get_MinVal(u8 times);

float GetDataAverage(u8 times);
float Data_RaminAverage(u8 times,u8 n);

void Get_XY_FUN_Line(float *Xbuf,float *Ybuf,u8 n);

#endif
