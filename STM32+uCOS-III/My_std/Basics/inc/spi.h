#ifndef __SPI_H
#define __SPI_H

#include "my_include.h"

//选择是否模拟SPI
#define      SPI_SIMULATE      0


#if          SPI_SIMULATE
/*------------------------SPI的GPIO端口定义-----------------------*/
#define      SPI_GPIO_CLK               RCC_APB2Periph_GPIOE| RCC_APB2Periph_GPIOD

#define      SPI_CS_PIN		              GPIO_Pin_13
#define      SPI_CS_PORT		            GPIOD

#define	     SPI_CLK_PIN	              GPIO_Pin_0
#define      SPI_CLK_PORT	              GPIOE

#define	     SPI_MOSI_PIN	              GPIO_Pin_2
#define	     SPI_MOSI_PORT	            GPIOE

#define	     SPI_MISO_PIN	              GPIO_Pin_3
#define	     SPI_MISO_PORT	            GPIOE

/*-----------------------SPI的GPIO端口状态定义---------------------*/
#define      SPI_CS_SET	                GPIO_SetBits( SPI_CS_PORT, SPI_CS_PIN) 
#define      SPI_CS_CLR                 GPIO_ResetBits( SPI_CS_PORT, SPI_CS_PIN) 

#define      SPI_CLK_SET	              GPIO_SetBits( SPI_CLK_PORT, SPI_CLK_PIN) 
#define      SPI_CLK_CLR                GPIO_ResetBits( SPI_CLK_PORT, SPI_CLK_PIN)

#define      SPI_MOSI_SET	              GPIO_SetBits( SPI_MOSI_PORT, SPI_MOSI_PIN) 
#define      SPI_MOSI_CLR               GPIO_ResetBits( SPI_MOSI_PORT, SPI_MOSI_PIN)
 
#define	     SPI_MISO_READ	            GPIO_ReadInputDataBit( SPI_MISO_PORT, SPI_MISO_PIN) 

//SPI延时
#define      SPI_Delay()                Delay_Us(5)

//函数声明
void SPI_Config(void);
void SPI_SendCMD(uint8_t cmd);
uint16_t SPI_ReadData(void);              


#else                //选择STM32自带的SPI
/*------------------------选择SPI接口及时钟----------------------*/
#define      SPI_x                       SPI1
#define      SPI_APBxClock_FUN           RCC_APB2PeriphClockCmd
#define      SPI_CLK                     RCC_APB2Periph_SPI1

/*------------------------SPI的GPIO端口定义-----------------------*/
//CS(NSS)引脚 片选选普通GPIO即可
#define      SPI_CS_CLK                  RCC_APB2Periph_GPIOC    
#define      SPI_CS_PORT                 GPIOC
#define      SPI_CS_PIN                  GPIO_Pin_0

//SCK引脚
#define      SPI_SCK_CLK                 RCC_APB2Periph_GPIOA   
#define      SPI_SCK_PORT                GPIOA   
#define      SPI_SCK_PIN                 GPIO_Pin_5

//MISO引脚
#define      SPI_MISO_CLK                RCC_APB2Periph_GPIOA    
#define      SPI_MISO_PORT               GPIOA 
#define      SPI_MISO_PIN                GPIO_Pin_6

//MOSI引脚
#define      SPI_MOSI_CLK                RCC_APB2Periph_GPIOA    
#define      SPI_MOSI_PORT               GPIOA 
#define      SPI_MOSI_PIN                GPIO_Pin_7

/*-----------------------SPI的GPIO端口状态定义-------------------*/
#define      SPI_CS_SET                  GPIO_SetBits(SPI_CS_PORT, SPI_CS_PIN)             
#define      SPI_CS_CLR                  GPIO_ResetBits(SPI_CS_PORT, SPI_CS_PIN)             

/*等待超时时间*/
#define SPI_FLAG_TIMEOUT         ((uint32_t)0x1000)
#define SPI_LONG_TIMEOUT         ((uint32_t)(10 * SPI_FLAG_TIMEOUT))

//函数声明
void SPI_Config(void);
uint8_t SPI_SendByte(u8 byte);

#endif


#endif /* __SPI_H */
