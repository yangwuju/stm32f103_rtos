#ifndef __TIME3_H
#define __TIME3_H

#include "my_include.h"

/********************通用定时器TIM3参数定义*******************/
/*-------------------------------------------------------------            
| 复用功能映像 |  没有重映像  |   部分重映像  |   完全重映像  |
|--------------|--------------|---------------|---------------|
|  TIM1_CH1    |    PA6       |      PB4      |      PC6      |
|  TIM1_CH2    |    PA7       |      PB5      |      PC7      |
|--------------|--------------|---------------|---------------|
|  TIM1_CH3    |            PB0               |      PC8      |
|  TIM1_CH4    |            PB1               |      PC9      |
--------------------------------------------------------------*/

// TIM3 输出比较通道
#define            TIM3_CHx_GPIO_CLK      RCC_APB2Periph_GPIOB
#define            TIM3_CHx_PORT          GPIOB
#define            TIM3_CHx_PIN           GPIO_Pin_4

// TIM3 重映像
#define            TIM3_GPIO_PinRemap     GPIO_PartialRemap_TIM3 //部分重映像
//#define            TIM3_GPIO_PinRemap     GPIO_FullRemap_TIM3  //完全重映像

// TIM3 捕获通道
#define            TIM3_CHANNEL_x         TIM_Channel_1  //选择捕获通道1
#define            TIM3_GetCapture        TIM_GetCapture1 //捕获比较寄存器的值

// TIM3 中断源（捕获通道）
#define            TIM3_IT_CCx            TIM_IT_CC1   //通道1中断源

// 测量的起始边沿
#define            TIM3_STRAT_ICPolarity  TIM_ICPolarity_Rising   //上升沿
// 测量的结束边沿
#define            TIM3_END_ICPolarity    TIM_ICPolarity_Falling  //下降沿



// 定时器输入捕获用户自定义变量结构体声明
typedef struct
{   
	uint8_t   Capture_FinishFlag;   // 捕获结束标志位
	uint8_t   Capture_StartFlag;    // 捕获开始标志位
	uint16_t  Capture_CcrValue;     // 捕获寄存器的值
	uint16_t  Capture_Period;       // 自动重装载寄存器更新标志 
}TIM_ICUserValueTypeDef;

/**************************函数声明********************************/

void TIM3_Init(u16 arr,u16 psc);
float TIM3_GetCaptureValue(u16 arr,u16 psc);



#endif 
