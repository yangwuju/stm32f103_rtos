#ifndef __OLED_H
#define __OLED_H

#include "my_include.h"

//选择通信协议
#define OLED_IIC      1
#define OLED_SPI      0


//OLED模式设置
//0:4线串行模式
//1:并行8080模式
#define OLED_MODE     0    
#define SIZE          16     
#define XLevelL		    0x00
#define XLevelH		    0x10
#define Max_Column	  128
#define Max_Row		    64
#define	Brightness	  0xFF 
#define X_WIDTH 	    128
#define Y_WIDTH 	    64	    

#define RW_FLASH      1 //是否写入Flash

#define OLED_CMD           0	//写命令
#define OLED_DATA          1	//写数据

/*------------------OLED的GPIO定义------------------*/
#if OLED_IIC //OLED_SPI

#define OLED_SCLK_CLK     RCC_APB2Periph_GPIOB   //SCL
#define OLED_SCLK_PART    GPIOB	
#define OLED_SCLK_PIN     GPIO_Pin_9

#define OLED_SDIN_CLK     RCC_APB2Periph_GPIOB   //SDA
#define OLED_SDIN_PART    GPIOB	
#define OLED_SDIN_PIN     GPIO_Pin_8

/*------------------OLED端口状态定义------------------*/ 					   
#define OLED_SCLK_Clr() GPIO_ResetBits(OLED_SCLK_PART,OLED_SCLK_PIN)//SCL
#define OLED_SCLK_Set() GPIO_SetBits(OLED_SCLK_PART,OLED_SCLK_PIN)

#define OLED_SDIN_Clr() GPIO_ResetBits(OLED_SDIN_PART,OLED_SDIN_PIN)//SDA
#define OLED_SDIN_Set() GPIO_SetBits(OLED_SDIN_PART,OLED_SDIN_PIN)


#else

#define OLED_SCLK_CLK     RCC_APB2Periph_GPIOB   //DO
#define OLED_SCLK_PART    GPIOB	
#define OLED_SCLK_PIN     GPIO_Pin_9

#define OLED_SDIN_CLK     RCC_APB2Periph_GPIOB   //DI
#define OLED_SDIN_PART    GPIOB	
#define OLED_SDIN_PIN     GPIO_Pin_8

#define OLED_RST_CLK      RCC_APB2Periph_GPIOB    //RES
#define OLED_RST_PART     GPIOB	
#define OLED_RST_PIN      GPIO_Pin_7

#define OLED_DC_CLK       RCC_APB2Periph_GPIOB    //DC
#define OLED_DC_PART      GPIOB	
#define OLED_DC_PIN       GPIO_Pin_6

#define OLED_CS_CLK       RCC_APB2Periph_GPIOB    //CS
#define OLED_CS_PART      GPIOB	
#define OLED_CS_PIN       GPIO_Pin_5

/*------------------OLED端口状态定义------------------*/ 					   

#define OLED_SCLK_Clr()    GPIO_ResetBits(OLED_SCLK_PART,OLED_SCLK_PIN)//CLK
#define OLED_SCLK_Set()    GPIO_SetBits(OLED_SCLK_PART,OLED_SCLK_PIN)

#define OLED_SDIN_Clr()    GPIO_ResetBits(OLED_SDIN_PART,OLED_SDIN_PIN)//DIN
#define OLED_SDIN_Set()    GPIO_SetBits(OLED_SDIN_PART,OLED_SDIN_PIN)

#define OLED_RST_Clr()     GPIO_ResetBits(OLED_RST_PART,OLED_RST_PIN)//RES
#define OLED_RST_Set()     GPIO_SetBits(OLED_RST_PART,OLED_RST_PIN)

#define OLED_DC_Clr()      GPIO_ResetBits(OLED_DC_PART,OLED_DC_PIN)//DC
#define OLED_DC_Set()      GPIO_SetBits(OLED_DC_PART,OLED_DC_PIN)
 		     
#define OLED_CS_Clr()      GPIO_ResetBits(OLED_CS_PART,OLED_CS_PIN)//CS
#define OLED_CS_Set()      GPIO_SetBits(OLED_CS_PART,OLED_CS_PIN)

#endif

/*---------------- 函数声明 --------------------*/
void OLED_I2c_Start(void);
void OLED_I2c_Stop(void);
void OLED_I2c_WaitAck(void);
void OLED_I2c_SendByte(u8 dat);

void OLED_SendCmd(u8 cmd);
void OLED_SendDat(u8 dat);	
void OLED_WR_Byte(u8 dat,u8 cmd);	 

void OLED_Display_On(void);
void OLED_Display_Off(void);
void OLED_ColorTurn(u8 i);
void OLED_DisplayTurn(u8 i);
void OLED_Clear(void);

void OLED_DrawPoint1(u8 x,u8 y,u8 fill,u8 dir);
void OLED_RelayPoint1(u8 x1,u8 y1,u8 x2,u8 y2,u8 width,u8 dir,u8 speed);
void OLED_RelayPoint2(u8 x1,u8 y1,u8 x2,u8 y2,u8 row,u8 width,u8 dir,u8 speed);

void OLED_DrawPoint(u8 x,u8 y,u8 fill,u8 rw,u8 dir);
void OLED_Fill(u8 x1,u8 y1,u8 x2,u8 y2,u8 fill,u8 track,u8 dir);
void OLED_OpenWindow(u8 x1,u8 y1,u8 x2,u8 y2,u8 dir);
void OLED_DrawLine(u8 x1,u8 y1,u8 x2,u8 y2,u8 track,u8 dir);
void OLED_DrawRectangle(u8 x1,u8 y1,u8 x2,u8 y2,u8 fill,u8 track,u8 dir);
void OLED_DrawCircle(u8 x,u8 y,u8 r,u8 fill,u8 track,u8 dir);

void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 size,u8 mode,u8 track,u8 wFalsh);
void OLED_ShowString(u8 x,u8 y,char *p,u8 size,u8 mode,u8 track);
void OLED_RollString(u8 x1,u8 y1,char *p,u8 size,u8 mode,u8 speed);

void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size,u8 mode,u8 track);
void OLED_ShowCHinese(u8 x,u8 y,u8 num1,u8 num2,u8 size,u8 mode,u8 track);
void OLED_RollCHinese(u8 x1,u8 y1,u8 num1,u8 num2,u8 size,u8 mode,u8 speed);

void OLED_DrawDesign(u8 x,u8 y,u8 num1,u8 num2,u8 size,u8 mode,u8 track);
void OLED_DrawBMP(u8 x1, u8 y1,u8 x2, u8 y2,u8 *bmp);

void OLED_Init(void);
void OLED_Refresh_Gram(void);

#endif 
