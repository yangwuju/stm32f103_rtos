#include "usmart.h"
#include "usmart_str.h" 

////////////////////////////用户配置区///////////////////////////////////////////////
//这下面要包含所用到的函数所申明的头文件(用户自己添加) 
#include "delay.h"		
												 
 
//函数名列表初始化(用户自己添加)
//用户直接在这里输入要执行的函数名及其查找串
struct _m_usmart_nametab usmart_nametab[]=
{
#if USMART_USE_WRFUNS==1 	//如果使能了读写操作
	(void*)read_addr,"u32 read_addr(u32 addr)",
	(void*)write_addr,"void write_addr(u32 addr,u32 val)",	 
#endif
	(void*)Delay_Us,"void Delay_Us(u32 nus)",
	(void*)Delay_Ms,"void Delay_Ms(u16 nms)",	
//	(void*)LCD_ILI9341_Clear,"void LCD_ILI9341_Clear( uint16_t usX, uint16_t usY, uint16_t usWidth, uint16_t usHeight )",
//	(void*)LCD_ILI9341_SetPointColor,"void LCD_ILI9341_SetPointColor ( uint16_t usX, uint16_t usY, uint16_t usColor)",
//	(void*)LCD_ILI9341_DrawLine,"void LCD_ILI9341_DrawLine( uint16_t usX1, uint16_t usY1, uint16_t usX2, uint16_t usY2 )",
//	(void*)LCD_ILI9341_DrawRectangle,"void LCD_ILI9341_DrawRectangle( uint16_t usX_Start, uint16_t usY_Start, uint16_t usWidth, uint16_t usHeight,uint8_t ucFilled )",
//	(void*)LCD_ILI9341_DrawCircle,"void LCD_ILI9341_DrawCircle( uint16_t usX_Center, uint16_t usY_Center, uint16_t usRadius, uint8_t ucFilled )",
//	(void*)LCD_ILI9341_DispStringLine_EN,"void LCD_ILI9341_DispStringLine_EN(uint16_t usX, uint16_t line, char * pStr, uint16_t DrawModel )",
//	(void*)LCD_ILI9341_DispChar_CH,"void LCD_ILI9341_DispChar_CH( uint16_t usX, uint16_t usY, uint16_t usChar, uint16_t DrawModel )",
//	(void*)LED1_on,"void LED1_on(short times,short onms,short offms)",
//	(void*)LED2_on,"void LED2_on(short times,short onms,short offms)",				  	    
//	(void*)LCD_ILI9341_GetPointPixel,"uint16_t LCD_ILI9341_GetPointPixel( uint16_t usX, uint16_t usY )",
};						  
///////////////////////////////////END///////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//函数控制管理器初始化
//得到各个受控函数的名字
//得到函数总数量
struct _m_usmart_dev usmart_dev=
{
	usmart_nametab,
	usmart_init,
	usmart_cmd_rec,
	usmart_exe,
	usmart_scan,
	sizeof(usmart_nametab)/sizeof(struct _m_usmart_nametab),//函数数量
	0,	  //参数数量
	0,	 	//函数ID
	1,		//参数显示类型,0,10进制;1,16进制
	0,		//参数类型.bitx:,0,数字;1,字符串	    
	0,	  //每个参数的长度暂存表,需要MAX_PARM个0初始化
	0,		//函数的参数,需要PARM_LEN个0初始化
};   



















