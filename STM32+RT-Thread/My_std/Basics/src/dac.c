#include "dac.h"

/**
  * @brief  使能DAC的时钟，初始化GPIO
  * @param  无
  * @retval 无
  */
void DAC_Config(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
	DAC_InitTypeDef  DAC_InitStructure;

  /* 使能GPIOA时钟 */
  RCC_APB2PeriphClockCmd(DAC1_GPIO_CLK | DAC2_GPIO_CLK, ENABLE);	
	
	/* 使能DAC时钟 */	
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
	
  /* DAC的GPIO配置，模拟输入 */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN; //为防止干扰，DAC所有引脚需配置为模拟输入
	GPIO_InitStructure.GPIO_Pin =  DAC1_GPIO_PIN;
  GPIO_Init(DAC1_GPIO_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin =  DAC2_GPIO_PIN;
  GPIO_Init(DAC2_GPIO_PORT, &GPIO_InitStructure);
	

  /* 配置DAC 通道1 */
  DAC_InitStructure.DAC_Trigger = DAC_Trigger_T2_TRGO;						//使用TIM2作为触发源
  DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;	//不使用波形发生器
	DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bit0; //屏蔽、幅值设置
  DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Disable;	//不使用DAC输出缓冲
  DAC_Init(DAC_Channel_1, &DAC_InitStructure);

  /* 配置DAC 通道2 */
  DAC_Init(DAC_Channel_2, &DAC_InitStructure);

  /* 使能通道1 由PA4输出 */
  DAC_Cmd(DAC_Channel_1, ENABLE);
  /* 使能通道2 由PA5输出 */
  DAC_Cmd(DAC_Channel_2, ENABLE);
	
  DAC_SetChannel1Data(DAC_Align_12b_R, 0);//12位右对齐数据格式设置DAC值
	DAC_SetChannel2Data(DAC_Align_12b_R, 0);//12位右对齐数据格式设置DAC值
}


/**
  * @brief  设定DAC输出电压值
  * @param  有
     @para：ch选择DAC输出通道Channel_1 或Channel_2
     @para：vol设定范围0-3300即代表 0-3.3V
  * @retval 无
  */
void DAC_Set_Vol(u8 ch , u16 vol)
{
	float temp=(float)vol;
	temp/=1000;
	temp=temp*4096/3.3;
	if(ch==Channel_1)
	  DAC_SetChannel1Data(DAC_Align_12b_R,temp);
  else if(ch==Channel_2)
		DAC_SetChannel2Data(DAC_Align_12b_R,temp);
}



/*--------------------- END OF FIAL-------------------*/
