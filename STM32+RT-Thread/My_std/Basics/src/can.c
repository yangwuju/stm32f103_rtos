#include "can.h"

//CAN通信

uint8_t CAN_Rece_Flag = 0; //CAN接收数据标志

CanTxMsg  CAN_Tran_Data;   //发送数据的结构体
CanRxMsg  CAN_Rece_Data;   //接收数据的结构体

//CAN接收中断
void USB_LP_CAN1_RX0_IRQHandler(void)
{
	CAN_Rece_Flag = 1;
	CAN_Receive( CANx, CAN_FIFO0, &CAN_Rece_Data);
}

/**
  * @brief  初始化CAN的GPIO
  * @param  无
  * @retval 无
**/
static void CAN_GPIO_Config(void)
{
		/*定义一个GPIO_InitTypeDef类型的结构体*/
		GPIO_InitTypeDef GPIO_InitStructure;

		/*开启CAN1的时钟*/
	  RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
	  /*开启CAN1的GPIO时钟*/
		RCC_APB2PeriphClockCmd( CAN_TX_CLK|CAN_RX_CLK|RCC_APB2Periph_AFIO, ENABLE);
		
	  //重映射GPIO引脚
	  GPIO_PinRemapConfig( GPIO_Remap1_CAN1, ENABLE);
	
		/*设置TX引脚模式为复用推挽输出*/
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;   
		/*设置引脚速率为50MHz */   
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 	
	
			/*选择要控制的GPIO引脚*/
		GPIO_InitStructure.GPIO_Pin = CAN_TX_PIN;
		/*调用库函数，初始化GPIO*/
		GPIO_Init(CAN_TX_PORT, &GPIO_InitStructure);	
 
    /*设置RX引脚模式为上拉输入*/
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
		/*选择要控制的GPIO引脚*/
		GPIO_InitStructure.GPIO_Pin = CAN_RX_PIN;
		/*调用库函数，初始化GPIO*/
		GPIO_Init(CAN_RX_PORT, &GPIO_InitStructure);
 
}


/**
  * @brief  配置CAN的工作模式
  * @param  无
  * @retval 无
**/
static void CAN_Mode_Config(void)
{
	CAN_InitTypeDef CAN_InitStructure;
	
	/*CAN单元初始化*/
	/*配置为1MBps（为CAN的最高速率）*/
	CAN_InitStructure.CAN_Prescaler = 4;
	/*配置CAN正常模式或回环模式*/
	CAN_InitStructure.CAN_Mode = CANx_Mode;
	/*重新同步跳跃宽度 2个时间单元*/
	CAN_InitStructure.CAN_SJW = CAN_SJW_2tq;
	/*时间段1 占用了5个时间单元*/
	CAN_InitStructure.CAN_BS1 = CAN_BS1_5tq;
	/*时间段2 占用了3个时间单元*/
	CAN_InitStructure.CAN_BS2 = CAN_BS2_3tq;
	/*关闭时间触发通信模式使能*/
	CAN_InitStructure.CAN_TTCM = DISABLE;
	/*自动离线管理*/
	CAN_InitStructure.CAN_ABOM = ENABLE;
	/*使用自动唤醒模式*/
	CAN_InitStructure.CAN_AWUM = ENABLE;
	/*发送错误是否重传*/
	CAN_InitStructure.CAN_NART = ENABLE;
	/*接收FIFO 锁定模式*/
	CAN_InitStructure.CAN_RFLM = ENABLE;
	/*使能：按照存入邮箱的先后顺序发送数据，失能：按照ID优先级发送数据*/
	CAN_InitStructure.CAN_TXFP = DISABLE;

  /*初始化CAN结构体*/
	CAN_Init( CANx, &CAN_InitStructure );
}


/**
  * @brief  CAN的过滤器
  * @param  无
  * @retval 无
**/
static void CAN_Filer_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	CAN_FilterInitTypeDef CAN_FilterInitStructure;
	
	/*---------------------配置CAN的过滤器----------------------------*/
	CAN_FilterInitStructure.CAN_FilterIdHigh = (CAN_FilterId&0xFFFF0000) >> 16;
	CAN_FilterInitStructure.CAN_FilterIdLow = CAN_FilterId&0x0000FFFF;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0xFFFF;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0xFFFF;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_Filter_FIFO0;
	CAN_FilterInitStructure.CAN_FilterNumber = 0;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	
	CAN_FilterInit( &CAN_FilterInitStructure );
	
	/*------------------------配置CAN的中断NVIC-----------------------*/
  
  /* 嵌套向量中断控制器组选择 */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
  
  /* 配置CAN为中断源 */
  NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
  /* 抢断优先级*/
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  /* 子优先级 */
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  /* 使能中断 */
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  /* 初始化配置NVIC */
  NVIC_Init(&NVIC_InitStructure);

	/*使能CAN接收中断*/
	CAN_ITConfig( CANx, CAN_IT_FMP0, ENABLE);
	
}

/**
  * @brief  CAN的初始化
  * @param  无
  * @retval 无
**/
void CAN_Config(void)
{
	CAN_GPIO_Config();
	CAN_Mode_Config();
	CAN_Filer_Config();
}

//CAN发送数据
void CAN_SendData( uint8_t *buf, uint8_t length)
{
	uint8_t i;
	
	if(length>8) length = 8;
	
	CAN_Tran_Data.StdId = 0;
	CAN_Tran_Data.ExtId = CANx_ID;
	CAN_Tran_Data.IDE = CAN_Id_Extended;
	CAN_Tran_Data.RTR = CAN_RTR_DATA;
	CAN_Tran_Data.DLC = length;
	
	//将buf中的数据传入CAN的发送数据的数组中
	for( i=0; i<length; i++)
	{
		CAN_Tran_Data.Data[i] = buf[i];
	}
	
  CAN_Transmit(CANx, &CAN_Tran_Data);
}

