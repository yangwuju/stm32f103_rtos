#include "crc.h"

__IO uint32_t CRCValue = 0;		 // 用于存放产生的CRC校验值

/*
 * 函数名：CRC_Config
 * 描述  ：使能CRC时钟
 * 输入  ：无
 * 输出  ：无
 * 调用  : 外部调用
 */
void CRC_Config(void)
{
	/* Enable CRC clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);
}


uint32_t  CRC_Value(u8 *DataBuffer,u8 len)
{
	u8 i;
	
	CRC_Config();
	
	for(i=0; i<len; i++ ) 
	{
		//对该寄存器进行写操作时，作为输入寄存器，可以输入要进行CRC计算的新数据。
		//对该寄存器进行读操作时，返回上一次CRC计算的结果。
		//每一次写入数据寄存器，其计算结果是前一次CRC计算结果和新计算结果的组合(对整个32位字
		//进行CRC计算，而不是逐字节地计算)。
		CRCValue = CRC_CalcBlockCRC((uint32_t *)DataBuffer, len);
	}
    return 	CRCValue;
}


 //校验（数据解析）
u16 crc16Value(u8 *dat,u8 len)   //	u8 *dat为要进行CRC校验的信息  u8 len 为消息中的字节数

{
	int i;
  u16 crc=0xffff;
  while(len--)
  {
     crc^=*dat++;
	   for(i=0;i<8;i++)
	   {
	     if(crc&0x0001) crc=(crc>>1)^0xa001;
			 else crc=crc>>1;
	   }
   }
   return crc;
} 

u16 CRC16(puchMsg, usDataLen)
{
//	u8 *puchMsg ; //要进行CRC校验的信息
//	u16 usDataLen ; /* 消息中的字节数 */ 
		unsigned char uchCRCHi = 0xFF ; /* ?CRC高字节初始化*/ 
		unsigned char uchCRCLo = 0xFF ; /* ?CRC低字节初始化*/
		unsigned uIndex ; /* CRC循环中的索引 */ 
		while (usDataLen--) /* 传输消息缓冲区 */ 
		{ 
			uIndex = uchCRCHi ^ puchMsg++ ; /* 计算CRC */ 
			uchCRCHi = uchCRCLo ^ auchCRCHi[uIndex] ; 
			uchCRCLo = auchCRCLo[uIndex] ; 
		} 
		return (uchCRCHi << 8 | uchCRCLo) ; 
}