#include "exti.h"

/**
 * @brief  配置 IO为EXTI中断口，并设置中断优先级
 * @param  无
 * @retval 无
 */
void EXIT_INT1_IRQHandler(void)
{
  if(EXTI_GetITStatus(EXIT_INT1_LINE ) != RESET)
	{
	  //添加EXTI中断处理程序
		LED1_TOGGLE;
	}
  EXTI_ClearITPendingBit(EXIT_INT1_LINE);
}


/**
 * @brief  配置 IO为EXTI中断口，并设置中断优先级
 * @param  无
 * @retval 无
 */
void EXIT_INT2_IRQHandler(void)
{
  if(EXTI_GetITStatus(EXIT_INT2_LINE ) != RESET)
	{
	  //添加EXTI中断处理程序
		LED2_TOGGLE;
	}
  EXTI_ClearITPendingBit(EXIT_INT2_LINE);
}

/**
 * @brief  配置嵌套向量中断控制器NVIC
 * @param  无
 * @retval 无
 */
static void EXIT_NVIC_Config(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  
  /* 配置NVIC为优先级组1 */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
  
  /* 配置中断源：按键1 */
  NVIC_InitStructure.NVIC_IRQChannel = EXIT_INT1_IRQ;
  /* 配置抢占优先级 */
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  /* 配置子优先级 */
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  /* 使能中断通道 */
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
  /* 配置中断源：按键2，其他使用上面相关配置 */  
  NVIC_InitStructure.NVIC_IRQChannel = EXIT_INT2_IRQ;
  NVIC_Init(&NVIC_InitStructure);
}

/**
 * @brief  配置 IO为EXTI中断口，并设置中断优先级
 * @param  无
 * @retval 无
 */
void EXTI_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure; 
	EXTI_InitTypeDef EXTI_InitStructure;

	/*开启GPIO口的时钟*/
	RCC_APB2PeriphClockCmd(EXIT_INT1_GPIO_CLK|EXIT_INT2_GPIO_CLK,ENABLE);
												
	/* 配置 NVIC 中断*/
	EXIT_NVIC_Config();
	
/*--------------------------EXIT_INT1配置-----------------------------*/
	/* 选择按键用到的GPIO */	
  GPIO_InitStructure.GPIO_Pin = EXIT_INT1_GPIO_PIN;
  /* 配置为浮空输入 */	
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(EXIT_INT1_GPIO_PORT, &GPIO_InitStructure);

	/* 选择EXTI的信号源 */
  GPIO_EXTILineConfig(EXIT_INT1_GPIO_PORTSOURCE, EXIT_INT1_GPIO_PINSOURCE);
	
  EXTI_InitStructure.EXTI_Line = EXIT_INT1_LINE;
	/* EXTI为中断模式 */
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	/* 上升沿中断 */
  EXTI_InitStructure.EXTI_Trigger = EXIT_INT1_Trigger;
  /* 使能中断 */	
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
	
  /*--------------------------EXIT_INT2配置-----------------------------*/
	/* 选择按键用到的GPIO */	
  GPIO_InitStructure.GPIO_Pin = EXIT_INT2_GPIO_PIN;
  /* 配置为浮空输入 */	
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(EXIT_INT2_GPIO_PORT, &GPIO_InitStructure);

	/* 选择EXTI的信号源 */
  GPIO_EXTILineConfig(EXIT_INT2_GPIO_PORTSOURCE, EXIT_INT2_GPIO_PINSOURCE);
	
	EXTI_InitStructure.EXTI_Line = EXIT_INT2_LINE;	
	/* EXTI为中断模式 */
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	/* 下降沿中断 */
  EXTI_InitStructure.EXTI_Trigger = EXIT_INT2_Trigger;
  /* 使能中断 */	
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
}

/*--------------------- END OF FIAL-------------------*/
