#include "rcc.h"

/*
 * 初始化MCO引脚PA8
 * 在F1系列中MCO引脚只有一个，即PA8，在F4系列中，MCO引脚会有两个
 * 可在MCO引脚连接示波器，验证我们的系统时钟配置是否正确
 */
// 设置MCO引脚输出时钟，用示波器即可在PA8测量到输出的时钟信号，
// 我们可以把PLLCLK/2作为MCO引脚的时钟来检测系统时钟是否配置准确
// MCO引脚输出可以是HSE,HSI,PLLCLK/2,SYSCLK	
// RCC_MCOConfig(RCC_MCO_HSE);	             	        
// RCC_MCOConfig(RCC_MCO_HSI);	                   
// RCC_MCOConfig(RCC_MCO_PLLCLK_Div2);    	
// RCC_MCOConfig(RCC_MCO_SYSCLK);	
void MCO_GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	// 开启GPIOA的时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	// 选择GPIO8引脚
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	
	//设置为复用功能推挽输出
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	
	//设置IO的翻转速率为50M
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	
	// 初始化GPIOA8
  GPIO_Init(GPIOA, &GPIO_InitStructure);
}


/* 1.HSE作为时钟来源，经过PLL倍频作为系统时钟，这是通常的做法
 *   使用时最大8MHZ * 16 = 128MHZ，超频慎用
 *
 * 2.HSI作为时钟来源，经过PLL倍频作为系统时钟，这是在HSE故障的时候才使用的方法
 *   HSI会因为温度等原因会有漂移，不稳定，一般不会用HSI作为时钟来源，除非是迫不得已的情况
 *   如果HSI要作为PLL时钟的来源的话，必须二分频之后才可以，即HSI/2，而PLL倍频因子最大只能是16
 *   所以当使用HSI的时候，SYSCLK最大只能是4M*16=64M
 *
 * 3.设置 系统时钟:SYSCLK, AHB总线时钟:HCLK, APB2总线时钟:PCLK2, APB1总线时钟:PCLK1
 *   PCLK2 = HCLK = SYSCLK
 *   PCLK1 = HCLK/2,最高只能是36M
 *
 * 注：可修改条件编译，选择HES或HSI作为时钟来源
 *
 * 参数说明：RCC_PLLMul_x是PLL的倍频因子，在调用的时候可以是：RCC_PLLMul_x , x:[2,3,...16] 
 *           配置PLL时钟等于x*4MHZ[HSI] 或 x*8MHZ[HSE]
 */

void RCC_Configuration(uint32_t RCC_PLLMul_x)   
{
	__IO uint32_t StartUpStatus = 0, RCC_PLLSource_DivX = 0;
	// 把RCC外设初始化成复位状态，这句是必须的
	RCC_DeInit(); 

#if 1        
	//使能HSI，开启内部晶振，选用的是4M
	RCC_HSICmd(ENABLE);
  // 等待 HSI 就绪
	StartUpStatus = RCC_GetFlagStatus(RCC_FLAG_HSIRDY);
	RCC_PLLSource_DivX = RCC_PLLSource_HSI_Div2;
#else
	//使能HSE，开启外部晶振，常选用的是8M
  RCC_HSEConfig(RCC_HSE_ON);
  // 等待 HSE 启动稳定
  StartUpStatus = RCC_WaitForHSEStartUp();
	RCC_PLLSource_DivX = RCC_PLLSource_HSE_Div1;
#endif
	
	// 只有 HSI就绪之后则继续往下执行
  if (StartUpStatus != RESET)
  {
		FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);    //开启FLASH预读缓存功能，加速FLASH的
	  // SYSCLK周期与闪存访问时间的比例设置，这里统一设置成2
		// 设置成2的时候，SYSCLK低于48M也可以工作，如果设置成0或者1的时候，
		// 如果配置的SYSCLK超出了范围的话，则会进入硬件错误，程序就死了
		// 0：0 < SYSCLK <= 24M
		// 1：24< SYSCLK <= 48M
		// 2：48< SYSCLK <= 72M
		FLASH_SetLatency(FLASH_Latency_2);        //FLASH操作的延时
		
		RCC_HCLKConfig(RCC_SYSCLK_Div1);          //配置AHB(HCLK)时钟等于SYSCLK
		RCC_PCLK2Config(RCC_HCLK_Div1);           //配置APB2(PCLK2)钟等于AHB时钟
		RCC_PCLK1Config(RCC_HCLK_Div2);           //配置APB1(PCLK1)钟等于 AHB1/2 时钟
		
		//配置PLL时钟等于x*4MHZ[HSI] 或 x*8MHZ[HSE]
		RCC_PLLConfig(RCC_PLLSource_DivX, RCC_PLLMul_x); 
		//使能PLL时钟
		RCC_PLLCmd(ENABLE);                                       

		while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY)==RESET)       //等待PLL时钟就绪
		{}
		RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);          //配置系统时钟SYSCLK等于PLL时钟
		while(RCC_GetSYSCLKSource() !=0x08)                 //检查PLL时钟是否作为系统时钟源
		{}
	}
  else
  { // 如果HSI开启失败，那么程序就会来到这里，用户可在这里添加出错的代码处理
		// 当HSE开启失败或者故障的时候，单片机会自动把HSI设置为系统时钟，
		// HSI是内部的高速时钟，8MHZ
    while (1)
    {
    }
  }
}



/*--------------------- END OF FIAL-------------------*/
