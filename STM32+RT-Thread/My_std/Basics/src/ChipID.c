//获取CPU的ID函数，每个芯片都有唯一的 96_bit unique ID         

#include "ChipID.h"
#include "usart1.h"

uint32_t ChipUniqueID[3];
__IO u16 ChipVolume;

/*
 * 函数名：Get_ChipID
 * 描述  ：获取芯片ID
 * 输入  ：无
 * 输出  ：无
 */
void Get_ChipID(void)
{
	ChipUniqueID[0] = *(__IO u32 *)(0X1FFFF7F0); // 高字节
	ChipUniqueID[1] = *(__IO u32 *)(0X1FFFF7EC); // 
	ChipUniqueID[2] = *(__IO u32 *)(0X1FFFF7E8); // 低字节
	
	printf("\r\n芯片的唯一ID为: 0x%08X-%08X-%08X\r\n",
	ChipUniqueID[0],ChipUniqueID[1],ChipUniqueID[2]);	
}


/*
 * 函数名：Get_ChipID
 * 描述  ：获取芯片容量
 * 输入  ：无
 * 输出  ：无
 */
void Get_ChipVolume(void)
{
  ChipVolume = *(__IO u16 *)(0X1FFFF7E0);
	printf("\r\n芯片flash的容量为: %dK \r\n", ChipVolume);
}


