#include "ds18b20.h"
#include "delay.h"	

//设置DS18B20IO引脚为输入模式
static void DS18B20_InputMode(void)
{
 	GPIO_InitTypeDef  GPIO_InitStructure;
 	
 	RCC_APB2PeriphClockCmd(DS18B20_GPIO_CLK, ENABLE);	  
	
 	GPIO_InitStructure.GPIO_Pin = DS18B20_GPIO_PIN;
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 		  
// 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(DS18B20_GPIO_PORT, &GPIO_InitStructure);
}

//设置DS18B20IO引脚为输出模式
static void DS18B20_OutMode(void)
{
 	GPIO_InitTypeDef  GPIO_InitStructure;
 	
 	RCC_APB2PeriphClockCmd(DS18B20_GPIO_CLK, ENABLE);	
	
 	GPIO_InitStructure.GPIO_Pin = DS18B20_GPIO_PIN;	
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		  
 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(DS18B20_GPIO_PORT, &GPIO_InitStructure);
  DS18B20_DQ_Set;
}

//复位DS18B20
static void DS18B20_Rst(void)	   
{                 
	DS18B20_OutMode(); //SET OUTPUT
	DS18B20_DQ_Clr;    //拉低DQ
	Delay_Us(750);     //拉低750us
	DS18B20_DQ_Set;   //DQ=1 
	Delay_Us(15);     //15us
}

//等待DS18B20的回应
//返回1:未检测到DS18B20的存在
//返回0:存在
static u8 DS18B20_Check(void) 	   
{   
	u8 retry=0;
	DS18B20_InputMode();//SET INPUT	 
  while (DS18B20_DQ_IN&&retry<200)
	{
		retry++;
		Delay_Us(1);
	}	 
	if(retry>=200)return 1;
	else retry=0;
	//检测超时处理
  while(!DS18B20_DQ_IN&&retry<240)
	{
		retry++;
		Delay_Us(1);
	}
	if(retry>=240) return 1;	    
	return 0;
}

//从DS18B20读取一个位
//返回值：1/0
static u8 DS18B20_Read_Bit(void) 
{
   u8 data=0;
	 DS18B20_OutMode();//SET OUTPUT
   DS18B20_DQ_Clr;
	 Delay_Us(2);
   DS18B20_DQ_Set;
	 DS18B20_InputMode();//SET INPUT
	 Delay_Us(12);
 
   data |= DS18B20_DQ_IN;
   Delay_Us(50);           
   return data;
}
//从DS18B20读取一个字节
//返回值：读到的数据
static u8 DS18B20_Read_Byte(void)  
{        
  u8 i,j,dat;
  dat=0;
	for (i=1;i<=8;i++) 
	{
		j=DS18B20_Read_Bit();
		dat=(j<<7)|(dat>>1);
  }						    
  return dat;
}

//写一个字节到DS18B20
//dat：要写入的字节
static void DS18B20_Write_Byte(u8 dat)     
{             
  u8 j;
  u8 testb;
	DS18B20_OutMode();//SET PA0 OUTPUT;
  for (j=1;j<=8;j++) 
	{
		testb=dat&0x01;
		dat=dat>>1;
		if (testb) 
		{
			DS18B20_DQ_Clr;// Write 1
			Delay_Us(2);                            
			DS18B20_DQ_Set;
			Delay_Us(60);             
		}
		else 
		{
			DS18B20_DQ_Clr;// Write 0
			Delay_Us(60);             
			DS18B20_DQ_Set;
			Delay_Us(2);                          
		}
  }
}

//开始温度转换
static void DS18B20_Start(void)// ds1820 start convert
{   						               
    DS18B20_Rst();	   
  	DS18B20_Check();	 
    DS18B20_Write_Byte(0xcc);// skip rom
    DS18B20_Write_Byte(0x44);// convert
} 

//初始化DS18B20的IO口 DQ 同时检测DS的存在
//返回1:不存在
//返回0:存在    	 
u8 DS18B20_Init(void)
{
 	GPIO_InitTypeDef  GPIO_InitStructure;
 	
 	RCC_APB2PeriphClockCmd(DS18B20_GPIO_CLK, ENABLE);	 //使能PORT口时钟 
	
 	GPIO_InitStructure.GPIO_Pin = DS18B20_GPIO_PIN;				//PORT 推挽输出
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		  
 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(DS18B20_GPIO_PORT, &GPIO_InitStructure);

 	GPIO_SetBits(DS18B20_GPIO_PORT,DS18B20_GPIO_PIN);    //输出1

	DS18B20_Rst();

	return DS18B20_Check();
}  
//从ds18b20得到温度值
//精度：0.1C
//返回值：温度值 （-550~1250） 
float DS18B20_Temp(void)
{
	u8 temp;
	u8 TL,TH;
	short tem;
	float value;
	DS18B20_Start ();                    // ds1820 start convert
	DS18B20_Rst();
	DS18B20_Check();	 
	DS18B20_Write_Byte(0xcc);// skip rom
	DS18B20_Write_Byte(0xbe);// convert	    
	TL=DS18B20_Read_Byte(); // LSB   
	TH=DS18B20_Read_Byte(); // MSB  
				
	if(TH>7)
	{
		TH=~TH;
		TL=~TL; 
		temp=0;//温度为负  
	}
	else temp=1;//温度为正	  	  
	tem=TH; //获得高八位
	tem<<=8;    
	tem+=TL;//获得底八位
	value=(float)tem*0.0625;//转换     
	if(temp)return value; //返回温度值
	else return -value;    
} 
 
/*--------------------- END OF FIAL-------------------*/
