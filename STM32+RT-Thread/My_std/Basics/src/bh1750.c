/************************************************************************
 * 文件名  ：bh1750.c
 * 描述    ：BH1750光照传感器传感器配置
 *          
 * 实验平台：STM32开发板 基于STM32F103C8T6
 * 硬件连接：-------------------------------------------------------------
 *          VCC ：接VCC（供电 3－5.5VDC）
 *          GND ：接地
 *          SCL ：接PA5
 *          SDA ：接PA6
 *          ADDR：接PA7 （可直接接地）
 * 说明    ：输入光范围1-65535lx
 * 注意事项：																										  
*************************************************************************/

#include "bh1750.h" 
#include "delay.h"

volatile uint16_t illuminance; //光照强度

uint8_t buf[2];

/*--------------------- BH1750的GPIO端口配置 -------------------------*/
void BH1750_I2C_GPIO_Config(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(BH1750_SCL_CLK|BH1750_SDA_CLK,ENABLE);
	
	
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  //推挽模式     
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_InitStructure.GPIO_Pin = BH1750_SCL_PIN;	//选择对应的引脚
  GPIO_Init( BH1750_SCL_PORT, &GPIO_InitStructure);  //初始化PC端口
	
	GPIO_InitStructure.GPIO_Pin = BH1750_SDA_PIN;	//选择对应的引脚
  GPIO_Init( BH1750_SDA_PORT, &GPIO_InitStructure);  //初始化端口 
	
  GPIO_SetBits(BH1750_SCL_PORT,BH1750_SCL_PIN);
	GPIO_SetBits(BH1750_SDA_PORT,BH1750_SDA_PIN);	
	
}

/*--------------------- 设置BH1750的SDA端口为输入模式 -------------------------*/
void SDA_Mode_Input(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(BH1750_SDA_CLK,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = BH1750_SDA_PIN;	//选择对应的引脚
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;  //上拉输入模式     
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init( BH1750_SDA_PORT, &GPIO_InitStructure);  //初始化端口
}

/*--------------------- 设置BH1750的SDA端口为输出模式 -------------------------*/
void SDA_Mode_Ouput(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(BH1750_SDA_CLK,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = BH1750_SDA_PIN;	//选择对应的引脚
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  //输出模式   
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init( BH1750_SDA_PORT, &GPIO_InitStructure);  //初始化端口
}

/*--------------------- 起始信号 -------------------------*/
void BH1750_I2C_Start(void)
{
	SDA_Mode_Ouput();           //SDA线输出
	I2C_SDA_H;                  //拉高数据线
	I2C_SCL_H;                  //拉高时钟线
	Delay_Us(5);                //延时
	I2C_SDA_L;                  //产生下降沿
	Delay_Us(5);                //延时
	I2C_SCL_L;                  //拉低时钟线
}

/*--------------------- 停止信号 -------------------------*/
void BH1750_I2C_Stop(void)
{
	SDA_Mode_Ouput();          //SDA线输出
	I2C_SCL_H;                 //拉高时钟线
	I2C_SDA_L;                 //拉低数据线
	Delay_Us(5);               //延时
	I2C_SDA_H;                 //产生上升沿
	Delay_Us(5);               //延时
}

/*--------------------- BH1750发送应答信号 -------------------------*/
//ack（0：ACK  1：NACK）
void BH1750_I2C_ACK(uint8_t ack)
{
	SDA_Mode_Ouput();          //SDA线输出	
	I2C_SCL_L;                 //拉低时钟线
	if(ack)
	{
	  I2C_SDA_H;               //NACK
	}
	else
	{
	  I2C_SDA_L;               //ACK
	}
	
	Delay_Us(5);               //延时5us
	I2C_SCL_H;                 //拉高时钟线等待响应
	Delay_Us(5);               //延时5us
	I2C_SCL_L;                 //拉低时钟线

}

/*--------------------- BH1750接收应答信号 -------------------------*/
//返回1：应答失败
//返回0：应答成功
uint8_t BH1750_I2C_RecvACK(void)
{
	uint8_t con=0;
	SDA_Mode_Input();           //SDA上拉输入模式
	I2C_SDA_H;
	Delay_Us(5);                //延时
	I2C_SCL_H;                  //拉高时钟线
	Delay_Us(5);                //延时
	while(I2C_SDA_READ)
	{
	  con++;
		Delay_Us(1);
		if(con >= 250)
		{
		  BH1750_I2C_Stop();  //应答失败，产生停止信号
			return 1; 
		}		
	}
	//应答成功
	I2C_SCL_L;                  //拉低时钟线
	
	return 0;  
}

/*--------------------- 向IIC总线发送一个字节数据 -------------------------*/
void BH1750_SendByte(uint8_t dat)
{
	uint8_t i;
  SDA_Mode_Ouput();      //SDA线输出 
	
	I2C_SCL_L;            //拉低时钟线,准备发送数据
  Delay_Us(5);            //延时
	for (i=0; i<8; i++)         //8位计数器
	{	
		if(0X80&dat)
		{
				I2C_SDA_H;
		}
		else
		{
				I2C_SDA_L;     
		}
		dat <<= 1;              //左移一个bit 		
		I2C_SCL_H;              //拉高时钟线
		Delay_Us(5);            //延时		
		I2C_SCL_L;	
		Delay_Us(5);            //延时					
	}          		
}

/*--------------------- 从IIC总线接收一个字节数据 -------------------------*/
//ack（0：ACK  1：NACK）
uint8_t BH1750_ReadByte(uint8_t ack)
{
	uint8_t i;
	uint8_t dat = 0;	 
	SDA_Mode_Input();           //SDA上拉输入模式 
	for (i=0; i<8; i++)         //8位计数器
	{
		I2C_SCL_L;                //拉低时钟线，准备发送数据
		Delay_Us(5);
		
		dat <<= 1;
		I2C_SCL_H;                //拉高时钟线,使得数据有效
		Delay_Us(5);
		
		dat |= I2C_SDA_READ;      //读数据   	
		Delay_Us(5);              //延时
		
	}
	I2C_SCL_L;   
  BH1750_I2C_ACK(ack);
	
	return dat;
}

/*--------------------- BH1750写信号 -------------------------*/
void BH1750_Write(uint8_t SlaveAddress,uint8_t REG_Address)
{
	BH1750_I2C_Start(); 
	
	BH1750_SendByte(SlaveAddress+I2C_WR);   //发送设备地址+写信号
	while(BH1750_I2C_RecvACK());
		
	BH1750_SendByte(REG_Address);    //内部寄存器地址
	while(BH1750_I2C_RecvACK());
	
	BH1750_I2C_Stop();                    //停止信号
}

/*--------------------- 连续读出BH1750内部数据 -------------------------*/
void BH1750_Read(uint8_t SlaveAddress)
{   
	BH1750_I2C_Start();                      //起始信号
	BH1750_SendByte(SlaveAddress + I2C_RD);  //发送设备地址+读信号
	while(BH1750_I2C_RecvACK());       //接收应答，准备接收数据
	
	buf[0] = BH1750_ReadByte(0);			//BUF[0]存储0x32地址中的数据
	
	buf[1] = BH1750_ReadByte(1);

	BH1750_I2C_Stop();                    //停止信号
  Delay_Ms(5);
}

/*--------------------- BH1750初始化 -------------------------*/
void BH1750_Init(void)
{
  BH1750_I2C_GPIO_Config();
}

/*--------------------- 检测BH1750设备 -------------------------*/
uint8_t BH1750_Check(void)
{
	uint8_t con;
	BH1750_I2C_Start(); 
	
	BH1750_SendByte(BH1750_SlaveAddress+I2C_WR);   //发送设备地址+写信号
	while(BH1750_I2C_RecvACK())
	{
		con++;
		if(con>225)
		{
			return ERROR;                //检测失败
		}
	}
	BH1750_SendByte(BH1750_RSET);
	BH1750_I2C_RecvACK();
	BH1750_I2C_Stop();               //停止信号
	return SUCCESS;                  //检测成功
}

/*--------------------- 获得BH1750数据 -------------------------*/
float BH1750_GetDtae(void)
{
  int data=0;
	float temp;
	BH1750_Write(BH1750_SlaveAddress, BH1750_ON); // 通电
  BH1750_Write(BH1750_SlaveAddress, RESOLUTION); // 设置分辨率
	Delay_Ms(180);
	BH1750_Read(BH1750_SlaveAddress);
	
	data = buf[0];
	data = (data << 8) + buf[1];
	temp = (float)data/ 1.2f * SCALE_INTERVAL;	

	return temp;
}




