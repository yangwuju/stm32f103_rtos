#include "usart2.h"
//此函数为蓝牙接收

u8 USART2_RX_BUF[USART2_RX_LEN]; //接收缓冲,最大USART_REC_LEN个字节.

u8 rxflag2 = 0; //接收标识
u8 rxError2 = 0; //接收错误标识

//7bit : 接收一帧完成标识
//6bit : 接收到结束符0x7d 0x20-> } space
//5~1bit :接收数据长度
//0 bit  :接收到开始符0x7b-> { 开始接收数据
u8 USART2_RX_STA; //接收状态标记


 /**
  * @brief  配置嵌套向量中断控制器NVIC
  * @param  无
  * @retval 无
  */
static void NVIC_Config(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  
  /* 嵌套向量中断控制器组选择 */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
  
  /* 配置USART为中断源 */
  NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
  /* 抢断优先级*/
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  /* 子优先级 */
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  /* 使能中断 */
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  /* 初始化配置NVIC */
  NVIC_Init(&NVIC_InitStructure);
}

 /**
  * @brief  USART GPIO 配置,工作参数配置
  * @param  无
  * @retval 无
  */
void USART2_Config(u32 baud)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	// 打开串口GPIO的时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	// 打开串口外设的时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	// 将USART Tx的GPIO配置为推挽复用模式
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

  // 将USART Rx的GPIO配置为浮空输入模式
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	// 配置串口的工作参数
	// 配置波特率
	USART_InitStructure.USART_BaudRate = baud;
	// 配置 针数据字长
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	// 配置停止位
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	// 配置校验位
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	// 配置硬件流控制
	USART_InitStructure.USART_HardwareFlowControl = 
	USART_HardwareFlowControl_None;
	// 配置工作模式，收发一起
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	// 完成串口的初始化配置
	USART_Init(USART2, &USART_InitStructure);
	
	// 串口中断优先级配置
	NVIC_Config();
	
	// 使能串口接收中断
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);	
	
	// 使能串口
	USART_Cmd(USART2, ENABLE);	    
}


/*------------------  发送一个字节 -------------------*/
void USART2_SendByte(uint8_t data)
{
 /* 发送一个字节数据到USART */
 USART_SendData(USART2, data);
	
 /* 等待发送数据寄存器为空 */
 while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
}

/*------------------  发送一组数据 -------------------*/
void  USART2_SendData(uint8_t *ch,uint8_t length)
{
  uint8_t i;
	for(i=0;i<length;i++)
	{
	  USART2_SendByte(*ch++);
	}
}

/*------------------ 发送一个16位数 ------------------*/
void USART2_SendHalfWord( uint16_t data)
{
 uint8_t temp_h, temp_l;
	
 /* 取出高八位 */
 temp_h = (data & 0xff00) >> 8 ;
 /* 取出低八位 */
 temp_l =  data & 0x00ff ;
 
 /* 发送高八位 */
 USART2_SendByte(temp_h); 

 /* 发送低八位 */
 USART2_SendByte(temp_l); 
} 

/*------------------- 发送8位的数组 ------------------*/
void USART2_SendArray( uint8_t *array, uint8_t num)
{
	uint8_t i;
	for( i=0; i<num; i++)
	{
		/* 发送一个字节数据到USART */
	  USART2_SendByte(array[i]);
	}
	/* 等待发送完成 */
	while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
}

/*-------------------  发送字符串 --------------------*/
void USART2_SendString( uint8_t *str)
{
	uint8_t i=0;
  do
	{
	  USART2_SendByte( *(str + i));
		i++;
	}while(*(str + i) != '\0');
	
	/* 等待发送完成 */
	while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
}

void USART2_SendERROR(void)
{
	 u8 ErrorINFO[64];	
	 static u8 ErrCnt=0; //异常计数
	 u8 DataFormat[] = "Please send data in the format: {data}+space\n";
		
	 if(rxError2 == 1)
	 {
		 strcpy((char*)ErrorINFO,"Send Error:Head error!\n");
		 USART2_SendData(ErrorINFO,23);				 
	 }
	 else if(rxError2 == 2)
	 {
		 strcpy((char*)ErrorINFO,"Send Error:Not send an end character!\n");
		 USART2_SendData(ErrorINFO,39);				 
	 }
	 else if(rxError2 == 3)
	 {
		 strcpy((char*)ErrorINFO,"Send Error:Tail error!\n");
		 USART2_SendData(ErrorINFO,24);				 
	 }

	 else if(rxError2 == 4)
	 {
		 strcpy((char*)ErrorINFO,"Send Error:Beyond maximum length!\n");
		 USART2_SendData(ErrorINFO,34);				 
	 }
	 else
   {
	   ErrCnt = 0;
	 }
	 
	 ErrCnt++;
	 if(ErrCnt>=3)
	 {
		 ErrCnt = 0;
		 USART2_SendData(DataFormat,46);
	 }
	 
	 rxError2 = 0;	
}

 /**
  * @brief  USART接收中断
  * @param  无
  * @retval 无
  */
//注:接收的数据格式7B _ _ _ _ _ _ 7D 20 ,第一个数据和后两个数据，不计算在内
void USART2_IRQHandler(void)
{
  uint8_t ucTemp;
	uint8_t len;
	
	if( USART_GetITStatus( USART2, USART_IT_RXNE) !=RESET)
	{
		USART_ClearITPendingBit(USART2, USART_IT_RXNE);
	  ucTemp = USART_ReceiveData(USART2);  //手动输入数据赋值给ucTemp
		
		rxflag2 = 1;
		
		if((USART2_RX_STA & 0x80)==0)
		{
		  if(USART2_RX_STA & 0x40)
			{
				if(ucTemp != 0x20) 
				{
//					USART2_RX_STA=0;//接收帧尾错误,重新开始
					rxError2 = 3; //错误标识3 未接收到0x20
				}
				else
				{
					USART2_RX_STA |= 0x80;	//接收完成了
					rxError2 = 0; 
				}
			}
			else
      {
				if(USART2_RX_STA & 0x01)  //接收数据
				{					
					if(ucTemp == 0x7D) USART2_RX_STA|=0x40;
					else 
					{
						len = USART2_RX_STA;
						len = (len>>1)&0X1F;
				    USART2_RX_BUF[len] = ucTemp;
					  USART2_RX_STA += 2;
						
						rxError2 = 2; //错误标识2 未接收到0x7D
						
						//接收数据超过缓冲区长度,重新开始接收	
//					  if(len > USART2_RX_MAX_LEN) 
//						{
////							USART2_RX_STA=0;
//							rxError2 = 4; //错误标识4 					
//						}
						
					}
				}
				else
				{
					if(ucTemp != 0x7B) 
					{
						USART2_RX_STA = 0;//未接收到开始符0x7B
						rxError2 = 1; //错误标识1
					}
					else USART2_RX_STA |= 0x01;	//接收0x7B,开始接收数据				   
				}					
			}
		}	
	}
}


/////重定向c库函数printf到串口，重定向后可使用printf函数
//int fputc(int ch, FILE *f)
//{
//		/* 发送一个字节数据到串口 */
//		USART_SendData(USART2, (uint8_t) ch);
//		
//		/* 等待发送完毕 */
//		while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);		
//	
//		return (ch);
//}

/////重定向c库函数scanf到串口，重写向后可使用scanf、getchar等函数
//int fgetc(FILE *f)
//{
//		/* 等待串口输入数据 */
//		while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE) == RESET);

//		return (int)USART_ReceiveData(USART2);
//}


/*--------------------- END OF FIAL-------------------*/
