/************************************************************************
 * 文件名  ：dht11.c
 * 描述    ：DHT11温湿度传感器配置
 *          
 * 实验平台：MINI STM32开发板 基于STM32F103C8T6
 * 硬件连接：-------------------------------------------------------------
 *          VCC ：接VCC（供电 3－5.5VDC）
 *          DATA：接PA1（串行数据，单总线）
 *          NC  ：空引脚，悬空
 *          GND ：接地
 * 说明    ：温度测量范围0-50℃（±2℃）
 *           湿度测量范围20-95RH（±5RH）
 * 注意事项：																										  
*************************************************************************/

#include "dht11.h"
#include "delay.h"

DHT11_Data_TypeDef   DHT11_Data; //定义一个（温湿度）结构体变量

/*---------------------  设置DHT11的DATA口为输入 -------------------------*/
void DHT11_Mode_Input(void)	
{
  GPIO_InitTypeDef GPIO_InitStructure;
	
  GPIO_InitStructure.GPIO_Pin = DHT11_GPIO_PIN;	//选择对应的引脚
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;  //浮空输入模式     
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init( DHT11_GPIO_PORT, &GPIO_InitStructure);  //初始化PC端口
}


/*---------------------  设置DHT11的DATA口为输出 -------------------------*/
void DHT11_Mode_Output(void)	
{
  GPIO_InitTypeDef GPIO_InitStructure; 
	
  GPIO_InitStructure.GPIO_Pin = DHT11_GPIO_PIN;	//选择对应的引脚
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  //推挽输出模式     
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init( DHT11_GPIO_PORT, &GPIO_InitStructure);  //初始化PC端口
}


/*--------------------------- 复位DHT11 -----------------------------*/
void DHT11_Rst(void)
{
  DHT11_Mode_Output();  //设置为输出模式
  DHT11_GPIO_Clr();  //主机拉低
	Delay_Ms(18);  //拉低至少18ms
	
	DHT11_GPIO_Set();
	Delay_Us(30); //总线拉高 主机延时30us
	DHT11_Mode_Input();
}


/*--------------------- 读取DHT11的一个位 -------------------------*/
uint8_t DHT11_Read_Bit(void)
{	
	while(DHT11_GPIO_READ() == 0);  //每bit以50us低电平标置开始，轮询直到从机发出 的50us低电平结束
  Delay_Us(40); //延时需要大于数据0持续的时间即可	
	
  if(DHT11_GPIO_READ() == 1) // 40us后仍为高电平表示数据“1” 
	{
	   while(DHT11_GPIO_READ() == 1); //等待数据1的高电平结束
		 return 1;
	}	
	else
		return 0;
}


/*--------------------- 读取DHT11的一个字节 -------------------------*/
uint8_t DHT11_Read_Byte(void)
{
	u8 i,data=0;
	
	for( i=0; i<8; i++)
	{
		data <<= 1;
		data |= DHT11_Read_Bit();
	}
	return data;
}


/*---------------------- 读取DHT11的数据,并校验 --------------------*/
//humi_h，humi_l:湿度的整数和小数部分(测量范围：20%~90%)
//temp_h，temp_l:湿度的整数和小数部分(测量范围：0~50·C)

//数据正确返回SUCCESS，失败返回ERROR
uint8_t DHT11_Read_Data(DHT11_Data_TypeDef *DHT11_Data)
{
	uint8_t i,Data_Buff[5];
	
	DHT11_Rst();
	if(DHT11_GPIO_READ() == 0)
	{
		/*轮询直到从机发出 的80us 低电平 响应信号结束*/ 
		while(DHT11_GPIO_READ() == 0);
		/*轮询直到从机发出的 80us 高电平 标置信号结束*/
		while(DHT11_GPIO_READ() == 1);
		
		for( i=0; i<5; i++) //读取40位数据，送入缓存
		{
			Data_Buff[i] = DHT11_Read_Byte();
		}
		
		/*开始接收缓存数据*/   
		DHT11_Data->humi_int = Data_Buff[0];

		DHT11_Data->humi_deci = Data_Buff[1];

		DHT11_Data->temp_int = Data_Buff[2];

		DHT11_Data->temp_deci = Data_Buff[3];

		DHT11_Data->check_sum = Data_Buff[4];
		
		/*读取结束，引脚改为输出模式*/
		DHT11_Mode_Output();
		/*主机拉高*/
		DHT11_GPIO_Set();
		
		/*检查读取的数据是否正确*/
		if(DHT11_Data->check_sum == DHT11_Data->humi_int + DHT11_Data->humi_deci + DHT11_Data->temp_int+ DHT11_Data->temp_deci)
		{
			return SUCCESS;
		}
	  else	
			return ERROR;
	}
  else 
		return ERROR;
}


/*--------------------- 初始化DHT11 -------------------------*/
void DHT11_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure; 
	RCC_APB2PeriphClockCmd( DHT11_GPIO_CLK, ENABLE); // 使能PC端口时钟 	
	
  GPIO_InitStructure.GPIO_Pin = DHT11_GPIO_PIN;	//选择对应的引脚
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  //推挽输出模式     
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init( DHT11_GPIO_PORT, &GPIO_InitStructure);  //初始化PC端口

	GPIO_SetBits ( DHT11_GPIO_PORT, DHT11_GPIO_PIN ); 
}


/*数据格式:8bit湿度整数数据+8bit湿度小数数据
          +8bi温度整数数据+8bit温度小数数据
          +8bit校验
*/
