#include "ds1302.h"

#define ds1302_sec_add			0x80		//秒数据地址
#define ds1302_min_add			0x82		//分数据地址
#define ds1302_hr_add			  0x84		//时数据地址
#define ds1302_date_add			0x86		//日数据地址
#define ds1302_month_add		0x88		//月数据地址
#define ds1302_day_add			0x8a		//星期数据地址
#define ds1302_year_add			0x8c		//年数据地址
#define ds1302_control_add		0x8e		//控制数据地址
#define ds1302_charger_add		0x90 					 
#define ds1302_clkburst_add		0xbe

u8 time_buf1[8] = {20,15,9,13,9,10,22,7};//20年月日时分秒周
u8 time_buf[8] ;//空年月日时分秒周

//配置DS1302的IO引脚为输入模式
void DS1302_Mode_Input(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;   //定义一个GPIO_InitTypeDef类型的结构体	
	
	RCC_APB2PeriphClockCmd(DS1302_IO_CLK,ENABLE);   //允许GPIOA的外设时钟
	GPIO_InitStructure.GPIO_Pin = DS1302_IO_PIN;				 //选择要控制的GPIOA引脚
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN; 		 //设置引脚模式为通用上拉输入
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //设置引脚频率为50MHz
	GPIO_Init(DS1302_IO_PORT, &GPIO_InitStructure);		//调用库函数，初始化GPIOA
}

//配置DS1302的IO引脚为输出模式
void DS1302_Mode_Out(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;   //定义一个GPIO_InitTypeDef类型的结构体	
	
	RCC_APB2PeriphClockCmd(DS1302_IO_CLK,ENABLE);   //允许GPIOA的外设时钟
	GPIO_InitStructure.GPIO_Pin = DS1302_IO_PIN;				 //选择要控制的GPIOA引脚
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD; 		 //设置引脚模式为通用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //设置引脚频率为50MHz
	GPIO_Init(DS1302_IO_PORT, &GPIO_InitStructure);		//调用库函数，初始化GPIOA
} 

/*向DS1302写入一字节数据*/
void DS1302_Write_Byte(u8 addr, u8 d) 
{
	u8 i;
	DS1302_Mode_Out();
	CE_SET;					/*启动DS1302总线*/
	
	/*写入目标地址：addr*/
	addr = addr & 0xFE;/*最低位置零*/	
	for (i = 0; i < 8; i ++) 
	{    
		if (addr & 0x01) 
		{
			IO_SET;
		}
		else 
		{
			IO_CLR;
		}
		SCK_SET;
		SCK_CLR;
		addr = addr >> 1;
	}
/*写入数据：d*/
	for (i = 0; i < 8; i ++) 
	{    
		if (d & 0x01) 
		{
			IO_SET;
		}
		else 
		{
			IO_CLR;
		}
		SCK_SET;
		SCK_CLR;
		d = d >> 1;
	}
	CE_CLR;					/*停止DS1302总线*/
}

/*从DS1302读出一字节数据*/
u8 DS1302_Read_Byte(u8 addr) 
{
	u8 i;
	u8 temp;
	//DS1302_Mode_Input();
	CE_SET;					/*启动DS1302总线*/

	/*写入目标地址：addr*/
	addr |= 0x01;/*最低位置高*/
	for (i = 0; i < 8; i ++) 
	{     
		if (addr & 0x01) 
		{
			IO_SET;
		}
		else 
		{
			IO_CLR;
		}
		SCK_SET;
		SCK_CLR;
		addr >>=  1;
	}
	
	/*输出数据：temp*/
	for (i = 0; i < 8; i++) 
	{
		temp = temp >> 1;	
		if(IO_READ) 
		{
			temp |= 0x80;
		}
		else 
		{
			temp &= 0x7F;
		}
		SCK_SET;
		SCK_CLR;
	}
	
	CE_CLR;					/*停止DS1302总线*/
	return temp;
}

/*向DS302写入时钟数据*/
void DS1302_Write_Time(void) 
{  
    u8 i,tmp;
		for(i=0;i<8;i++)
		{           
			tmp=time_buf1[i]/10;    //BCD处理
			time_buf[i]=time_buf1[i]%10;
			time_buf[i]=time_buf[i]+tmp*16;
		}
	DS1302_Write_Byte(ds1302_control_add,0x00);			//关闭写保护 
	DS1302_Write_Byte(ds1302_sec_add,0x80);				//暂停 
	DS1302_Write_Byte(ds1302_charger_add,0xa9);			//涓流充电 
	DS1302_Write_Byte(ds1302_year_add,time_buf[1]);		//年 
	DS1302_Write_Byte(ds1302_month_add,time_buf[2]);	//月 
	DS1302_Write_Byte(ds1302_date_add,time_buf[3]);		//日 
	DS1302_Write_Byte(ds1302_day_add,time_buf[7]);		//周 
	DS1302_Write_Byte(ds1302_hr_add,time_buf[4]);		//时 
	DS1302_Write_Byte(ds1302_min_add,time_buf[5]);		//分
	DS1302_Write_Byte(ds1302_sec_add,time_buf[6]);		//秒
	DS1302_Write_Byte(ds1302_day_add,time_buf[7]);		//周 
	DS1302_Write_Byte(0xc6,0x27);
	DS1302_Write_Byte(ds1302_control_add,0x80);			//打开写保护 
}

/*从DS302读出时钟数据*/
void DS1302_Read_Time(void) 
{ 
  u8 i,tmp;
	DS1302_Mode_Out();
	time_buf[1]=DS1302_Read_Byte(ds1302_year_add);		//年 
	time_buf[2]=DS1302_Read_Byte(ds1302_month_add);		//月 
	time_buf[3]=DS1302_Read_Byte(ds1302_date_add);		//日 
	time_buf[4]=DS1302_Read_Byte(ds1302_hr_add);		//时 
	time_buf[5]=DS1302_Read_Byte(ds1302_min_add);		//分 
	time_buf[6]=(DS1302_Read_Byte(ds1302_sec_add))&0x7F;//秒 
	time_buf[7]=DS1302_Read_Byte(ds1302_day_add);		//周 
	for(i=1;i<8;i++)
	{          
		tmp=time_buf[i]/16;     //BCD处理
		time_buf1[i]=time_buf[i]%16;
		time_buf1[i]=time_buf1[i]+tmp*10;	
	}	
}

/*DS302初始化函数*/
void DS1302_Init(void) 
{	
 	GPIO_InitTypeDef  GPIO_InitStructure;   //定义一个GPIO_InitTypeDef类型的结构体	
	
	RCC_APB2PeriphClockCmd(DS1302_CE_CLK|DS1302_SCK_CLK,ENABLE);   //允许GPIOA的外设时钟
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //设置引脚模式为通用推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //设置引脚频率为50MHz
	
	GPIO_InitStructure.GPIO_Pin = DS1302_SCK_PIN;		//选择要控制的GPIOA引脚
	GPIO_Init(DS1302_SCK_PORT, &GPIO_InitStructure);		//初始化GPIO

	GPIO_InitStructure.GPIO_Pin = DS1302_CE_PIN;		//选择要控制的GPIOA引脚	
	GPIO_Init(DS1302_CE_PORT, &GPIO_InitStructure);		//初始化GPIO
	
	CE_CLR;			  /*RST脚置低*/
	SCK_CLR;			/*SCK脚置低*/
}
