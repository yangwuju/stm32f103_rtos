#ifndef __CMD_QUEUE1_H
#define __CMD_QUEUE1_H

#include "my_include.h"

//缓冲区大小，在资源容许的情况下，尽量设置大一些，防止溢出
#define QUEUE_MAX_SIZE 800

#define PTR2U16(PTR) ((((uint8_t *)(PTR))[0]<<8)|((uint8_t *)(PTR))[1])  //从缓冲区取16位数据
#define PTR2U32(PTR) ((((uint8_t *)(PTR))[0]<<24)|(((uint8_t *)(PTR))[1]<<16)|(((uint8_t *)(PTR))[2]<<8)|((uint8_t *)(PTR))[3])  //从缓冲区取32位数据

typedef char qdata;
typedef unsigned short qsize;

#pragma pack(push)
#pragma pack(1)	//按字节对齐

typedef struct
{
	uint8_t    cmd_head;    //帧头    设备地址  
	uint8_t    cmd_type;    //命令类型(UPDATE_CONTROL)   功能代码(03:读单个或多个寄存器；)
	                        //06:写单个寄存器；0x10:写多个寄存器 
  uint16_t   reg16_addr;  //寄存器起始地址(03,10)或寄存器地址(06)
	uint16_t   reg16_len;   //寄存器数量(03,10),对于06功能指令这个项不用 
  uint8_t    param[256];  //可变长度内容，最多256个字节	
  uint8_t    crc16[2];    //CRC16校验的2个字节
} CTRL_MSG,*PCTRL_MSG;

#pragma pack(pop)

extern PCTRL_MSG msg1;

/****************************************************************************
* 名    称： queue_reset()
* 功    能： 复位指令接收缓冲区，清空数据
* 入口参数： 无
* 出口参数： 无
****************************************************************************/
extern void queue1_reset(void);

/****************************************************************************
* 名    称： queue_push()
* 功    能： 添加一个数据到指令缓冲区
* 入口参数： _data-指令数据
* 出口参数： 无
****************************************************************************/
extern void queue1_push(qdata _data);

/****************************************************************************
* 名    称： queue_find_cmd
* 功    能： 从指令缓冲区中查找一条完整的指令
* 入口参数： poffset-指令的开始位置，psize-指令的字节大小
* 出口参数： 无
****************************************************************************/
extern qsize queue1_find_cmd(qdata *buffer,qsize buf_len);

#endif
