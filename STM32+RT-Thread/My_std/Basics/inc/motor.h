#ifndef __MOTOR_H
#define __MOTOR_H

#include "my_include.h"
#include "delay.h"

#define MOTOR_IN1_CLK   RCC_APB2Periph_GPIOB
#define MOTOR_IN1_PORT  GPIOB
#define MOTOR_IN1_PIN   GPIO_Pin_6

#define MOTOR_IN2_CLK   RCC_APB2Periph_GPIOB
#define MOTOR_IN2_PORT  GPIOB
#define MOTOR_IN2_PIN   GPIO_Pin_7

#define MOTOR_IN3_CLK   RCC_APB2Periph_GPIOB
#define MOTOR_IN3_PORT  GPIOB
#define MOTOR_IN3_PIN   GPIO_Pin_8

#define MOTOR_IN4_CLK   RCC_APB2Periph_GPIOB
#define MOTOR_IN4_PORT  GPIOB
#define MOTOR_IN4_PIN   GPIO_Pin_9



#define MOTOR_IN1_Set   GPIO_SetBits(MOTOR_IN1_PORT, MOTOR_IN1_PIN)
#define MOTOR_IN1_Clr   GPIO_ResetBits(MOTOR_IN1_PORT, MOTOR_IN1_PIN)

#define MOTOR_IN2_Set   GPIO_SetBits(MOTOR_IN2_PORT, MOTOR_IN2_PIN)
#define MOTOR_IN2_Clr   GPIO_ResetBits(MOTOR_IN2_PORT, MOTOR_IN2_PIN)

#define MOTOR_IN3_Set   GPIO_SetBits(MOTOR_IN3_PORT, MOTOR_IN3_PIN)
#define MOTOR_IN3_Clr   GPIO_ResetBits(MOTOR_IN3_PORT, MOTOR_IN3_PIN)

#define MOTOR_IN4_Set   GPIO_SetBits(MOTOR_IN4_PORT, MOTOR_IN4_PIN)
#define MOTOR_IN4_Clr   GPIO_ResetBits(MOTOR_IN4_PORT, MOTOR_IN4_PIN)

#define MOTOR_CW    0
#define MOTOR_CCW   1
#define MOTOR_STOP  2

void Motor_Init(void);
void Motor_RunMode(uint8_t mode,uint8_t lap);
void Motor_Mode(uint8_t n);

#endif /* __MOTOR_H */
