#ifndef      __LCD_ILI9341_H
#define	     __LCD_ILI9341_H

#include "my_include.h"
#include "lcdfont.h"

 

/***************************************************************************************
2^26 =0X0400 0000 = 64MB,每个 BANK 有4*64MB = 256MB
64MB:FSMC_Bank1_NORSRAM1:0X6000 0000 ~ 0X63FF FFFF
64MB:FSMC_Bank1_NORSRAM2:0X6400 0000 ~ 0X67FF FFFF
64MB:FSMC_Bank1_NORSRAM3:0X6800 0000 ~ 0X6BFF FFFF
64MB:FSMC_Bank1_NORSRAM4:0X6C00 0000 ~ 0X6FFF FFFF

选择BANK1-BORSRAM1 连接 TFT，地址范围为0X6000 0000 ~ 0X63FF FFFF
FSMC_A16 接LCD的DC(寄存器/数据选择)脚
寄存器基地址 = 0X60000000
RAM基地址 = 0X60020000 = 0X60000000+2^16*2 = 0X60000000 + 0X20000 = 0X60020000
当选择不同的地址线时，地址要重新计算  
****************************************************************************************/

/******************************* ILI9341 显示屏的 FSMC 参数定义 ***************************/
//FSMC_Bank1_NORSRAM用于LCD命令操作的地址(A16引脚)
#define      FSMC_Addr_ILI9341_CMD         ( uint32_t ) (0x60000000 & (~(1<<(16+1)))) //0x60000000  

//FSMC_Bank1_NORSRAM用于LCD数据操作的地址(A16引脚)      
#define      FSMC_Addr_ILI9341_DATA        ( uint32_t ) (0x60000000 | (1<<(16+1)) )  //0x60020000

//由片选引脚决定的NOR/SRAM块
#define      FSMC_Bank1_NORSRAMx           FSMC_Bank1_NORSRAM1



/******************************* ILI9341 显示屏8080通讯引脚定义 ***************************/
/******控制信号线******/
//片选，选择NOR/SRAM块
#define      ILI9341_CS_CLK                RCC_APB2Periph_GPIOD   
#define      ILI9341_CS_PORT               GPIOD
#define      ILI9341_CS_PIN                GPIO_Pin_7

//DC引脚，使用FSMC的地址信号控制，本引脚决定了访问LCD时使用的地址
//PD11为FSMC_A16
#define      ILI9341_DC_CLK                RCC_APB2Periph_GPIOD   
#define      ILI9341_DC_PORT               GPIOD
#define      ILI9341_DC_PIN                GPIO_Pin_11

//写使能
#define      ILI9341_WR_CLK                RCC_APB2Periph_GPIOD   
#define      ILI9341_WR_PORT               GPIOD
#define      ILI9341_WR_PIN                GPIO_Pin_5

//读使能
#define      ILI9341_RD_CLK                RCC_APB2Periph_GPIOD   
#define      ILI9341_RD_PORT               GPIOD
#define      ILI9341_RD_PIN                GPIO_Pin_4

//复位引脚
#define      ILI9341_RST_CLK               RCC_APB2Periph_GPIOE
#define      ILI9341_RST_PORT              GPIOE
#define      ILI9341_RST_PIN               GPIO_Pin_1

//背光引脚
#define      ILI9341_BK_CLK                RCC_APB2Periph_GPIOD    
#define      ILI9341_BK_PORT               GPIOD
#define      ILI9341_BK_PIN                GPIO_Pin_12

/********数据信号线***************/
#define      ILI9341_D0_CLK                RCC_APB2Periph_GPIOD   
#define      ILI9341_D0_PORT               GPIOD
#define      ILI9341_D0_PIN                GPIO_Pin_14

#define      ILI9341_D1_CLK                RCC_APB2Periph_GPIOD   
#define      ILI9341_D1_PORT               GPIOD
#define      ILI9341_D1_PIN                GPIO_Pin_15

#define      ILI9341_D2_CLK                RCC_APB2Periph_GPIOD   
#define      ILI9341_D2_PORT               GPIOD
#define      ILI9341_D2_PIN                GPIO_Pin_0

#define      ILI9341_D3_CLK                RCC_APB2Periph_GPIOD  
#define      ILI9341_D3_PORT               GPIOD
#define      ILI9341_D3_PIN                GPIO_Pin_1

#define      ILI9341_D4_CLK                RCC_APB2Periph_GPIOE   
#define      ILI9341_D4_PORT               GPIOE
#define      ILI9341_D4_PIN                GPIO_Pin_7

#define      ILI9341_D5_CLK                RCC_APB2Periph_GPIOE   
#define      ILI9341_D5_PORT               GPIOE
#define      ILI9341_D5_PIN                GPIO_Pin_8

#define      ILI9341_D6_CLK                RCC_APB2Periph_GPIOE   
#define      ILI9341_D6_PORT               GPIOE
#define      ILI9341_D6_PIN                GPIO_Pin_9

#define      ILI9341_D7_CLK                RCC_APB2Periph_GPIOE  
#define      ILI9341_D7_PORT               GPIOE
#define      ILI9341_D7_PIN                GPIO_Pin_10

#define      ILI9341_D8_CLK                RCC_APB2Periph_GPIOE   
#define      ILI9341_D8_PORT               GPIOE
#define      ILI9341_D8_PIN                GPIO_Pin_11

#define      ILI9341_D9_CLK                RCC_APB2Periph_GPIOE   
#define      ILI9341_D9_PORT               GPIOE
#define      ILI9341_D9_PIN                GPIO_Pin_12

#define      ILI9341_D10_CLK               RCC_APB2Periph_GPIOE   
#define      ILI9341_D10_PORT              GPIOE
#define      ILI9341_D10_PIN               GPIO_Pin_13

#define      ILI9341_D11_CLK               RCC_APB2Periph_GPIOE   
#define      ILI9341_D11_PORT              GPIOE
#define      ILI9341_D11_PIN               GPIO_Pin_14

#define      ILI9341_D12_CLK               RCC_APB2Periph_GPIOE   
#define      ILI9341_D12_PORT              GPIOE
#define      ILI9341_D12_PIN               GPIO_Pin_15

#define      ILI9341_D13_CLK               RCC_APB2Periph_GPIOD   
#define      ILI9341_D13_PORT              GPIOD
#define      ILI9341_D13_PIN               GPIO_Pin_8

#define      ILI9341_D14_CLK               RCC_APB2Periph_GPIOD   
#define      ILI9341_D14_PORT              GPIOD
#define      ILI9341_D14_PIN               GPIO_Pin_9

#define      ILI9341_D15_CLK               RCC_APB2Periph_GPIOD   
#define      ILI9341_D15_PORT              GPIOD
#define      ILI9341_D15_PIN               GPIO_Pin_10

/*************************************** 调试预用 ******************************************/
#define      DEBUG_DELAY()     //空的宏，可自主添加             

/***************************** ILI934 显示区域的起始坐标和总行列数 ***************************/
#define      LCD_ILI9341_DispWindow_X_Star		    0     //起始点的X坐标
#define      LCD_ILI9341_DispWindow_Y_Star		    0     //起始点的Y坐标

#define 		 LCD_ILI9341_LESS_PIXEL	  				  	240			//液晶屏较短方向的像素宽度
#define 		 LCD_ILI9341_MORE_PIXEL	 						  320			//液晶屏较长方向的像素宽度

//根据液晶扫描方向而变化的XY像素宽度
//调用ILI9341_GramScan函数设置方向时会自动更改
extern uint16_t LCD_X_LENGTH,LCD_Y_LENGTH; 

//液晶屏扫描模式
//参数可选值为0-7
extern uint8_t  LCD_SCAN_MODE;

/******************************* 定义 ILI934 显示屏常用颜色 ********************************/
#define      BACKGROUND		                 BLACK     //默认背景颜色

#define      WHITE		 		                 0xFFFF	   //白色
#define      BLACK                         0x0000	   //黑色 
#define      GREY                          0xF7DE	   //灰色 
#define      BLUE                          0x001F	   //蓝色 
#define      BLUE2                         0x051F	   //浅蓝色 
#define      RED                           0xF800	   //红色 
#define      MAGENTA                       0xF81F	   //红紫色，洋红色 
#define      GREEN                         0x07E0	   //绿色 
#define      CYAN                          0x7FFF	   //蓝绿色，青色 
#define      YELLOW                        0xFFE0	   //黄色 
#define      BRED                          0xF81F    //蓝红
#define      GRED                          0xFFE0    //绿红
#define      GBLUE                         0x07FF    //绿蓝

#define      RGB565(R,G,B)                 (((0x1F&R)<<11)+((0x3F&G)<<5)+0x1F&B) //任意颜色

/******************************* 定义 ILI934 常用命令 ********************************/
#define      CMD_SetCoordinateX		 		     0x2A	     //设置X坐标
#define      CMD_SetCoordinateY		 		     0x2B	     //设置Y坐标
#define      CMD_SetPixel		 		           0x2C	     //填充像素


/********************************** 声明 ILI934 函数 ***************************************/
void      LCD_ILI9341_Init( void );  //ILI9341初始化函数 【注：要放在串口初始化之前】
void      LCD_ILI9341_Rst( void );   //ILI9341 软件复位
void      LCD_ILI9341_BackLed_Control( FunctionalState enumState );  //ILI9341背光LED控制

void      LCD_ILI9341_GramScan( uint8_t ucOtion );  //设置ILI9341的GRAM的扫描方向
void      LCD_ILI9341_OpenWindow( uint16_t usX, uint16_t usY, uint16_t usWidth, uint16_t usHeight );  //在（x,y）处开辟一个窗口

void      LCD_ILI9341_Color_Fill ( uint16_t usX1, uint16_t usY1, uint16_t usX2, uint16_t usY2, uint16_t usColor); //使用色块进行填充
void      LCD_ILI9341_SetPointPixel( uint16_t usX, uint16_t usY );  //对（x,y）以某种颜色进行填充
void      LCD_ILI9341_SetPointColor ( uint16_t usX, uint16_t usY, uint16_t usColor); //对（x,y）以usColor颜色进行填充
uint16_t  LCD_ILI9341_GetPointPixel( uint16_t usX, uint16_t usY );  //获取（x,y）的像素数据
	

//显示图形
void      LCD_ILI9341_DrawLine( uint16_t usX1, uint16_t usY1, uint16_t usX2, uint16_t usY2 );  //以起到为（x1,y1），终点为（x2,y2）.使用 Bresenham 算法画线段 
void      LCD_ILI9341_DrawParallelogram ( int16_t usX1, int16_t usY1, int16_t usX2, int16_t usY2, int16_t usX3, int16_t usY3); //绘制一个平行四边形
void      LCD_ILI9341_DrawRectangle( uint16_t usX_Start, uint16_t usY_Start, uint16_t usWidth, uint16_t usHeight,uint8_t ucFilled );  //在（x,y）出上画一个矩形
void      LCD_ILI9341_DrawCircle( uint16_t usX_Center, uint16_t usY_Center, uint16_t usRadius, uint8_t ucFilled );  //在（x,y）处使用 Bresenham 算法画出半径为R的圆

//显示英文
void      LCD_ILI9341_DispChar_EN( uint16_t usX, uint16_t usY, const char cChar, uint16_t DrawModel );   //在（x,y）处显示一个英文字符
void      LCD_ILI9341_DispStringLine_EN(uint16_t usX, uint16_t line, char * pStr, uint16_t DrawModel );  //在 某一行 的（x,*）显示英文字符串
void      LCD_ILI9341_DispString_EN_XDir( uint16_t usX, uint16_t usY, char * pStr, uint16_t DrawModel ); //在（x,y）处显示英文字符串（沿X轴方向）
void 			LCD_ILI9341_DispString_EN_YDir( uint16_t usX, uint16_t usY, char * pStr, uint16_t DrawModel ); //在（x,y）处显示英文字符串 (沿Y轴方向)

//显示中文
void      LCD_ILI9341_DispChar_CH( uint16_t usX, uint16_t usY, uint16_t usChar, uint16_t DrawModel ); //显示一个中文汉字
void      LCD_ILI9341_DispString_CH( uint16_t usX, uint16_t usY, char * pStr, uint16_t DrawModel );   //显示一串中文

//显示中英文
void      LCD_ILI9341_DispStringLine_EN_CH( uint16_t usX, uint16_t line, char * pStr, uint16_t DrawModel );  //在 某一行 的（x,*）显示一串中英文
void      LCD_ILI9341_DispString_EN_CH_XDir(	uint16_t usX, uint16_t usY, char * pStr, uint16_t DrawModel ); //在（x,y）处显示英文字符串（沿X轴方向）
void 			LCD_ILI9341_DispString_EN_CH_YDir( uint16_t usX, uint16_t usY, char * pStr, uint16_t DrawModel );  //在（x,y）处显示英文字符串（沿Y轴方向）

//显示中英文（字体大小可控制）[横向显示]
void      LCD_ILI9341_DisplayStringEx_XDir(uint16_t x, //字符显示位置x
																 uint16_t y, 				   //字符显示位置y
																 uint16_t Font_width,	 //要显示的字体宽度，英文字符在此基础上/2。注意为偶数
																 uint16_t Font_Height, //要显示的字体高度，注意为偶数
																 uint8_t *ptr,				 //显示的字符内容
																 uint16_t DrawModel);  //是否反色显示 0正常显示 1反色显示

//显示中英文（字体大小可控制）[纵向显示]
void      LCD_ILI9341_DisplayStringEx_YDir(uint16_t x, //字符显示位置x
																 uint16_t y, 				   //字符显示位置y
																 uint16_t Font_width,	 //要显示的字体宽度，英文字符在此基础上/2。注意为偶数
																 uint16_t Font_Height, //要显示的字体高度，注意为偶数
																 uint8_t *ptr,				 //显示的字符内容
																 uint16_t DrawModel);  //是否反色显示 0正常显示 1反色显示

//显示图片 [色素点取向：从左到右，从上到下，高位在前] 竖屏显示（240x320）横屏（320x240）
void      LCD_ILI9341_DownPicture ( uint16_t usX1,     //显示图片的起始x位置
	                               uint16_t usY1,        //显示图片的起始y位置
                                 uint16_t usX2,        //显示图片的终点x位置
                                 uint16_t usY2,        //显示图片的终点y位置
                                 uint16_t usWidth,     //显示图片的宽度
                                 uint16_t usHeight,    //显示图片的高度
                                 uint8_t *usColor);    //显示图片的像素点颜色内容

void LCD_ILI9341_DownBMP ( uint16_t usX,       //显示位图的起始x位置
													 uint16_t usY,       //显示位图的起始y位置
                           uint8_t num1,       //从dat中第num1个
                           uint8_t num2,       //到dat中第num2个显示
													 uint8_t size,       //BMP显示的尺寸size * size
													 uint8_t *dat,       //BMP数组
                           uint8_t DrawModel); //是否反色显示 0正常显示 1反色显示


//void 			LCD_SetFont(sFONT *fonts);  //设置英文字体类型
//sFONT 		*LCD_GetFont(void);         //获取当前字体类型 【由于出现未定义sFONT莫名报错，将这两个函数放到lcdfont.h中】

void      LCD_ILI9341_ClearLine( uint16_t Line, uint16_t usX1 ,uint16_t usX2 );  //清除某行从usX1到usX2的文字
void      LCD_ILI9341_Clear( uint16_t usX, uint16_t usY, uint16_t usWidth, uint16_t usHeight ); //对（x,y）处的某一窗口进行清屏

void 			LCD_SetBackColor(uint16_t Color);                        //设置LCD的背景颜色,RGB565
void 			LCD_SetTextColor(uint16_t Color);                        //设置LCD的前景颜色,RGB565
void 			LCD_SetColors(uint16_t TextColor, uint16_t BackColor);   //设置LCD的前景及背景颜色,RGB565
void 			LCD_GetColors(uint16_t *TextColor, uint16_t *BackColor); //获取LCD的前景(字体)及背景颜色,RGB565


#endif    /* __LCD_ILI9341_H */


