#ifndef __TIME0_H
#define __TIME0_H


#include "my_include.h"


/*--------------------GPIO端口定义---------------------*/

#define RCC_Cmd          RCC_APB1PeriphClockCmd
#define TIMx_CLK         RCC_APB1Periph_TIM4

#define TIMx             TIM5
#define TIMx_IRQn        TIM5_IRQn
#define TIMx_IRQHandler  TIM5_IRQHandler

extern u16  timeCnt1s,timeCnt100ms,timeCnt10ms;        //计数

/*---------------------函数声明------------------------*/
void TIMx_Init(u16 arr,u16 psc);


#endif
