#ifndef __MENU_H
#define __MENU_H

#include "my_include.h"
  
#define  maxPage  2 //最大页数



extern u8 menuKey; //菜单键
extern u8 upKey; //向上键
extern u8 downKey; //向下键

extern u8 mainPage;  //主页码
extern u8 subPage;   //次页码
extern u8 thrPage;   //三级页码

extern u8 pageNum; //菜单页码
extern u8 lineNum; //行码
extern u8 optFlag;  //选项码

  
void PageDisplay(void);

  

#endif /* __MENU_H */
