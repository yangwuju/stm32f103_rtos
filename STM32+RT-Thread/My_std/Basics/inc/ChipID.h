#ifndef __CHIPID_H
#define	__CHIPID_H

#include "my_include.h"

extern uint32_t ChipUniqueID[3];
extern __IO u16 ChipVolume;

void Get_ChipID(void);
void Get_ChipVolume(void);

#endif /* __CHIPID_H */
