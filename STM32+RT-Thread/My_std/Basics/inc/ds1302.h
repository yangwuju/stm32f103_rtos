#ifndef __DS1302_H
#define __DS1302_H

#include "my_include.h"

/*-----------------------  GPIO定义 ---------------------------*/
#define DS1302_IO_PORT    	GPIOA		                /* GPIO端口 */
#define DS1302_IO_CLK 	    RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define DS1302_IO_PIN		    GPIO_Pin_1			        /* 选择GPIO引脚 */

#define DS1302_CE_PORT    	GPIOA		                /* GPIO端口 */
#define DS1302_CE_CLK 	    RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define DS1302_CE_PIN		    GPIO_Pin_0			        /* 选择GPIO引脚 */

#define DS1302_SCK_PORT    	GPIOA		                /* GPIO端口 */
#define DS1302_SCK_CLK 	    RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define DS1302_SCK_PIN		  GPIO_Pin_4			        /* 选择GPIO引脚 */


#define SCK_SET  GPIO_SetBits(GPIOA, GPIO_Pin_0);
#define SCK_CLR  GPIO_ResetBits(GPIOA, GPIO_Pin_0);

#define IO_SET   GPIO_SetBits(GPIOA, GPIO_Pin_1);
#define IO_CLR   GPIO_ResetBits(GPIOA, GPIO_Pin_1);

#define CE_SET   GPIO_SetBits(GPIOA, GPIO_Pin_4);
#define CE_CLR   GPIO_ResetBits(GPIOA, GPIO_Pin_4);

#define IO_READ  GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_1)

void DS1302_Mode_Input(void);
void DS1302_Mode_Out(void);
void DS1302_Write_Byte(u8 addr, u8 d); 
u8 DS1302_Read_Byte(u8 addr);
void DS1302_Write_Time(void);
void DS1302_Read_Time(void); 
void DS1302_Init(void); 

#endif /* __DS1302_H */
