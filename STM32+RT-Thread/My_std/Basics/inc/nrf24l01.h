#ifndef __NRF24L01_H
#define __NRF24L01_H

#include "my_include.h"

//选择是否模拟SPI
#define SPI_SIMULATE      0


#define TX_ADR_WIDTH    5   // 5字节的地址宽度
#define RX_ADR_WIDTH    5

#define TX_PLOAD_WIDTH  32  // 32字节的用户数据宽度
#define RX_PLOAD_WIDTH  32 

#define MAX_TX    0x10      //达到最大自动重发次数中断
#define TX_OK     0x20      //发送完成产生中断
#define RX_OK     0x40      //接收到数据中断

extern uint8_t tx_buf[TX_PLOAD_WIDTH];
extern uint8_t rx_buf[RX_PLOAD_WIDTH];

extern uint8_t TX_ADDRESS[TX_ADR_WIDTH]; //发送地址
extern uint8_t RX_ADDRESS[RX_ADR_WIDTH]; //接收地址

#define READ_REGm       0x00  // 读配置寄存器，低五位为寄存器地址
#define WRITE_REGm      0x20  // 写配置寄存器，低五位为寄存器地址
#define RD_RX_PLOAD     0x61  // 读 RX 有效数据，1~32字节
#define WR_TX_PLOAD     0xA0  // 读 RX 有效数据，1~32字节
#define FLUSH_TX        0xE1  // 清除TX FIFO寄存器，发射模式下用
#define FLUSH_RX        0xE2  // 清除RX FIFO寄存器，接收模式下用
#define REUSE_TX_PL     0xE3  // 重新使用上一包数据，CE为高，数据被不断发送
#define NOP             0xFF  // 空操作，可用来读状态寄存器

#define CONFIG          0x00  // 配置寄存器地址
#define EN_AA           0x01  // 使能自动应答功能
#define EN_RXADDR       0x02  // 接收地址允许
#define SETUP_AW        0x03  //  接收/发送地址宽度为5个字节
#define SETUP_RETR      0x04  // 建立自动重发
#define RF_CH           0x05  // RF 通道
#define RF_SETUP        0x06  // RF 寄存器
#define STATUS          0x07  // 状态寄存器
#define OBSERVE_TX      0x08  // 发送检测寄存器
#define CD              0x09  // 载波检测寄存器
#define RX_ADDR_P0      0x0A  // 数据通道0接收地址
#define RX_ADDR_P1      0x0B  // 数据通道1接收地址
#define RX_ADDR_P2      0x0C  // 数据通道2接收地址
#define RX_ADDR_P3      0x0D  // 数据通道3接收地址
#define RX_ADDR_P4      0x0E  // 数据通道4接收地址
#define RX_ADDR_P5      0x0F  // 数据通道5接收地址
#define TX_ADDR         0x10  // 发送地址寄存器
#define RX_PW_P0        0x11  // 接收数据通道0有效数据宽度（1~32字节）
#define RX_PW_P1        0x12  // 接收数据通道1有效数据宽度（1~32字节）
#define RX_PW_P2        0x13  // 接收数据通道2有效数据宽度（1~32字节）
#define RX_PW_P3        0x14  // 接收数据通道3有效数据宽度（1~32字节)
#define RX_PW_P4        0x15  // 接收数据通道4有效数据宽度（1~32字节)
#define RX_PW_P5        0x16  // 接收数据通道5有效数据宽度（1~32字节)
#define FIFO_STATUS     0x17  // FIFO 状态寄存器   

#if !SPI_SIMULATE       //选择STM32自带的SPI
/*------------------------选择SPI接口及时钟----------------------*/

#define NRF_SPI_x               SPI1
#define NRF_SPI_APBxClock_FUN   RCC_APB2PeriphClockCmd
#define NRF_SPI_CLK             RCC_APB2Periph_SPI1

#endif
/*-----------------------  GPIO定义 ---------------------------*/
//CS
#define NRF24L01_CS_CLK 	    RCC_APB2Periph_GPIOC		/* GPIO端口时钟 */
#define NRF24L01_CS_PORT    	GPIOC 		              /* GPIO端口 */
#define NRF24L01_CS_PIN		    GPIO_Pin_6			        /* 选择GPIO引脚 */

//SCK
#define NRF24L01_SCK_CLK 	    RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define NRF24L01_SCK_PORT    	GPIOA 		              /* GPIO端口 */
#define NRF24L01_SCK_PIN		  GPIO_Pin_5			        /* 选择GPIO引脚 */

//MOSI
#define NRF24L01_MOSI_CLK 	  RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define NRF24L01_MOSI_PORT    GPIOA 		              /* GPIO端口 */
#define NRF24L01_MOSI_PIN		  GPIO_Pin_7			        /* 选择GPIO引脚 */

//MISO
#define NRF24L01_MISO_CLK 	  RCC_APB2Periph_GPIOA		/* GPIO端口时钟 */
#define NRF24L01_MISO_PORT    GPIOA 		              /* GPIO端口 */
#define NRF24L01_MISO_PIN		  GPIO_Pin_6			        /* 选择GPIO引脚 */

//CE
#define NRF24L01_CE_CLK 	    RCC_APB2Periph_GPIOC		/* GPIO端口时钟 */
#define NRF24L01_CE_PORT    	GPIOC 		              /* GPIO端口 */
#define NRF24L01_CE_PIN		    GPIO_Pin_5			        /* 选择GPIO引脚 */

//IRQ
#define NRF24L01_IRQ_CLK 	    RCC_APB2Periph_GPIOC		/* GPIO端口时钟 */
#define NRF24L01_IRQ_PORT    	GPIOC 		              /* GPIO端口 */
#define NRF24L01_IRQ_PIN		  GPIO_Pin_4			        /* 选择GPIO引脚 */

/*---------------------- GPIO端口状态定义 -----------------------*/
#define NRF24_CS_SET     GPIO_SetBits(NRF24L01_CS_PORT, NRF24L01_CS_PIN)
#define NRF24_CS_CLR     GPIO_ResetBits(NRF24L01_CS_PORT, NRF24L01_CS_PIN)

#define NRF24_SCK_SET    GPIO_SetBits(NRF24L01_SCK_PORT, NRF24L01_SCK_PIN)
#define NRF24_SCK_CLR    GPIO_ResetBits(NRF24L01_SCK_PORT, NRF24L01_SCK_PIN)

#define NRF24_MOSI_SET   GPIO_SetBits(NRF24L01_MOSI_PORT, NRF24L01_MOSI_PIN)
#define NRF24_MOSI_CLR   GPIO_ResetBits(NRF24L01_MOSI_PORT, NRF24L01_MOSI_PIN)

#define NRF24_MISO_READ  GPIO_ReadInputDataBit(NRF24L01_MISO_PORT, NRF24L01_MISO_PIN)

#define NRF24_CE_SET     GPIO_SetBits(NRF24L01_CE_PORT, NRF24L01_CE_PIN)
#define NRF24_CE_CLR     GPIO_ResetBits(NRF24L01_CE_PORT, NRF24L01_CE_PIN)

#define NRF24_IRQ_READ   GPIO_ReadInputDataBit(NRF24L01_IRQ_PORT, NRF24L01_IRQ_PIN)
  
 
/*-------------------------- 函数声明 ---------------------------*/

void    NRF24L01_Init(void); //初始化GPIO
uint8_t NRF24L01_Check(void);//用于检测NRF与MCU是否正常连接

void    NRF24L01_RX_Mode(void);//配置为接收模式
void    NRF24L01_TX_Mode(void);//配置为发送模式

uint8_t NRF24L01_Write_Reg(uint8_t reg, uint8_t value);//写寄存器
uint8_t NRF24L01_Read_Reg(uint8_t reg);			//读寄存器 

uint8_t NRF24L01_Write_Buf(uint8_t reg, uint8_t *pBuf, uint8_t bytes);//写数据区
uint8_t NRF24L01_Read_Buf(uint8_t reg, uint8_t *pBuf, uint8_t bytes);//读数据区		  

uint8_t NRF24L01_TxPacket(uint8_t *txbuf);//发送一个包的数据
uint8_t NRF24L01_RxPacket(uint8_t *rxbuf);//接收一个包的数据

#endif  /* __NRF24L01_H */
