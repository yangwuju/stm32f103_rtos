#ifndef __ADC_H
#define __ADC_H

#include "my_include.h"

//如果采集芯片内部温度，修改宏置0
#define    ADC_ExtenInput     0    


// 注意：用作ADC采集的IO必须没有复用，否则采集电压会有影响
/*---------------ADC1输入通道（引脚）配置-------------------------*/
#define    ADC_x                         ADC1
#define    ADC_APBxClock_FUN             RCC_APB2PeriphClockCmd
#define    ADC_CLK                       RCC_APB2Periph_ADC1

// DMA1时钟 通道1
//注：选择DMA1时，ADC1对应DMA1通道1  （ADC2无法使用DMA）
//    选择DMA2时，ADC3对应DMA2通道5
#define    ADC_DMA_CLK                   RCC_AHBPeriph_DMA1
#define    ADC_DMA_CHANNEL               DMA1_Channel1

#if        ADC_ExtenInput  
//通过GPIO端口对外进行ADC采集
#define    ADC_GPIO_CLK                  RCC_APB2Periph_GPIOB  
#define    ADC_GPIO_PORT                 GPIOB
#define    ADC_GPIO_PIN                  GPIO_Pin_1   // PB0-通道8 独立IO

#define    ADC_CHANNEL                   ADC_Channel_9

#else 

//采集芯片内部温度
#define    ADC_CHANNEL                   ADC_Channel_16
//对于12位的ADC，3.3V的ADC值为0xfff,温度为25度时对应的电压值为1.43V即0x6EE
#define V25  0x6EE	 
//斜率 每摄氏度4.3mV 对应每摄氏度0x05
#define AVG_SLOPE 0x05 

//芯片内部温度：(V25-ADC_ConvertedValue)/AVG_SLOPE+25

#endif     

/* ADC1转换的电压值通过MDA方式传到sram*/
extern __IO u16 ADC_ConvertedValue;

/*---------------------------函数声明------------------------------*/
void ADC_Config(void);
float Get_ADCValue(void);
float Get_ADC_Average(u8 times);
	
#endif 
