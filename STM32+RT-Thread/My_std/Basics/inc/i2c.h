#ifndef __I2C_H
#define __I2C_H

#include "my_include.h"

#define I2C_WR	0		/* 写控制bit */
#define I2C_RD	1		/* 读控制bit */


// 定义I2C总线连接的GPIO端口, 用户只需要修改下面4行代码即可任意改变SCL和SDA的引脚

#define I2C_GPIO_CLK 	 RCC_APB2Periph_GPIOB // GPIO端口时钟 

#define I2C_SCL_PORT	 GPIOB		            // GPIO端口 
#define I2C_SCL_PIN		 GPIO_Pin_6			      // 连接到SCL时钟线的GPIO 

#define I2C_SDA_PORT	 GPIOB		            // GPIO端口 
#define I2C_SDA_PIN		 GPIO_Pin_7			      // 连接到SDA数据线的GPIO 


// 定义读写SCL和SDA的宏，已增加代码的可移植性和可阅读性 
#define I2C_SCL_Set  GPIO_SetBits(I2C_SCL_PORT, I2C_SCL_PIN)		// SCL = 1 
#define I2C_SCL_Clr  GPIO_ResetBits(I2C_SCL_PORT, I2C_SCL_PIN)	// SCL = 0 

#define I2C_SDA_Set  GPIO_SetBits(I2C_SDA_PORT, I2C_SDA_PIN)		// SDA = 1 
#define I2C_SDA_Clr  GPIO_ResetBits(I2C_SDA_PORT, I2C_SDA_PIN)	// SDA = 0 

#define I2C_SDA_READ()  GPIO_ReadInputDataBit(I2C_SDA_PORT, I2C_SDA_PIN)	// 读SDA口线状态 


//函数声明
void I2C_GPIO_Config(void);

void I2C_Start(void); 
void I2C_Stop(void);

void I2C_SendByte(u8 dat);
u8 I2C_ReadByte(void);

u8 I2C_WaitAck(void);
void I2C_Ack(void);
void I2C_NAck(void);

uint8_t I2C_CheckDevice(uint8_t _Address);

#endif  /* __I2C_H */
