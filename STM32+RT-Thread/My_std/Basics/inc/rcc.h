#ifndef __RCC_H
#define __RCC_H

#include "my_include.h"

void MCO_GPIO_Config(void);
void RCC_Configuration(uint32_t RCC_PLLMul_x);

#endif 
