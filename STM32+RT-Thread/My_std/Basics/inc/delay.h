#ifndef __DELAY_H
#define __DELAY_H

#include "my_include.h"

#define  DELAY_FUNCTION  0  //开启初始化延时,以避免在main.c中每次都调用Delay_Init()

void Delay_Init(void);

void Delay_Us(u32 nus);
void Delay_Ms(u16 nms);

#define delay_us(nus)  Delay_Us(nus)
#define delay_ms(nms)  Delay_Ms(nms)

#endif
