#ifndef __TIME1_H
#define __TIME1_H

#include "my_include.h"

/************高级定时器TIM参数定义，只限TIM1和TIM8************/
/*-------------------------------------------------------------            
| 复用功能映像 |  没有重映像  |   部分重映像  |   完全重映像  |
|--------------|--------------|---------------|---------------|
|  TIM1_ETR    |            PA12              |      PE7      |
|  TIM1_CH1    |            PA8               |      PE9      |
|  TIM1_CH2    |            PA9               |      PE11     |
|  TIM1_CH3    |            PA10              |      PE13     |
|  TIM1_CH4    |            PA11              |      PE14     |
|--------------|--------------|---------------|---------------|
|  TIM1_BKIN   |       PB12   |      PA6      |      PE15     |
|  TIM1_CH1N   |       PB13   |      PA7      |      PE8      |
|  TIM1_CH2N   |       PB14   |      PB0      |      PE10     |
|  TIM1_CH3N   |       PB15   |      PB1      |      PE12     |
--------------------------------------------------------------*/
// TIM1 中断源
//#define            TIM1_IRQ               TIM1_UP_IRQn
//#define            TIM1_IRQHandler        TIM1_UP_IRQHandler

// TIM1 输出比较通道
#define            TIM1_CHx_GPIO_CLK      RCC_APB2Periph_GPIOA
#define            TIM1_CHx_PORT          GPIOA
#define            TIM1_CHx_PIN           GPIO_Pin_8

// TIM1 输出比较通道的互补通道
#define            TIM1_CHxN_GPIO_CLK      RCC_APB2Periph_GPIOB
#define            TIM1_CHxN_PORT          GPIOB
#define            TIM1_CHxN_PIN           GPIO_Pin_13

// TIM1 输出比较通道的刹车通道
#define            TIM1_BKxN_GPIO_CLK      RCC_APB2Periph_GPIOB
#define            TIM1_BKxN_PORT          GPIOB
#define            TIM1_BKxN_PIN           GPIO_Pin_12

/**************************函数声明********************************/

void TIM1_Init(u16 arr,u16 psc);




#endif 
