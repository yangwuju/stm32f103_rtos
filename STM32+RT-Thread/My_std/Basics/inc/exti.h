#ifndef __EXTI_H
#define __EXTI_H

#include "my_include.h"

/*-----------------------  GPIO定义 ---------------------------*/
#define EXIT_INT1_GPIO_PORT         GPIOA
#define EXIT_INT1_GPIO_CLK          (RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO)
#define EXIT_INT1_GPIO_PIN          GPIO_Pin_0

#define EXIT_INT1_GPIO_PORTSOURCE   GPIO_PortSourceGPIOA
#define EXIT_INT1_GPIO_PINSOURCE    GPIO_PinSource0
#define EXIT_INT1_LINE              EXTI_Line0
#define EXIT_INT1_IRQ               EXTI0_IRQn

//选择中断边沿触发信号
#define EXIT_INT1_Trigger           EXTI_Trigger_Rising //下降沿触发中断
                                    
//中断服务函数名定义
#define EXIT_INT1_IRQHandler        EXTI0_IRQHandler


/*-------------------------- 宏定义 -----------------------------*/
#define EXIT_INT2_GPIO_PORT         GPIOC
#define EXIT_INT2_GPIO_CLK          (RCC_APB2Periph_GPIOC|RCC_APB2Periph_AFIO)
#define EXIT_INT2_GPIO_PIN          GPIO_Pin_13

#define EXIT_INT2_GPIO_PORTSOURCE   GPIO_PortSourceGPIOC
#define EXIT_INT2_GPIO_PINSOURCE    GPIO_PinSource13
#define EXIT_INT2_LINE              EXTI_Line13
#define EXIT_INT2_IRQ               EXTI15_10_IRQn

//选择中断边沿触发信号
#define EXIT_INT2_Trigger           EXTI_Trigger_Rising //下降沿触发中断
 
//中断服务函数名定义
#define EXIT_INT2_IRQHandler        EXTI15_10_IRQHandler


/*-----------------------  函数声明 ---------------------------*/
void EXTI_Config(void);


#endif 
