#ifndef __USART2_H
#define __USART2_H

#include "my_include.h"

#define  USART2_RX_MAX_LEN  17  //蓝牙接收最大长度为17

#define  USART2_RX_LEN   64
extern u8 USART2_RX_BUF[USART2_RX_LEN]; //接收缓冲,最大USART_REC_LEN个字节.

extern u8 USART2_RX_STA;
extern u8 rxflag2; //接收标识
extern u8 rxError2; //接收错误标识

/*----------------  函数声明 ---------------*/
void USART2_Config(u32 baud);
void USART2_SendByte(uint8_t data);
void USART2_SendData(uint8_t *ch,uint8_t length);
void USART2_SendHalfWord( uint16_t data);
void USART2_SendArray( uint8_t *array, uint8_t num);
void USART2_SendString( uint8_t *str);

void USART2_SendERROR(void);

#endif 
