#ifndef __IWDG_H
#define	__IWDG_H

#include "my_include.h"

#define Psc4    IWDG_Prescaler_4
#define Psc8    IWDG_Prescaler_8
#define Psc16   IWDG_Prescaler_16
#define Psc32   IWDG_Prescaler_32
#define Psc64   IWDG_Prescaler_64
#define Psc128  IWDG_Prescaler_128
#define Psc256  IWDG_Prescaler_256

void IWDG_Feed(void);
void IWDG_Init(uint8_t prv ,uint16_t rlv);

#endif /* __IWDG_H */
