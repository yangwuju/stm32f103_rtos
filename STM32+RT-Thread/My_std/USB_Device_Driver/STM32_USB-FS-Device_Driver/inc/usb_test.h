#ifndef __USB_TEST_H
#define __USB_TEST_H

#include "my_include.h"

//SD未插入时，应将一下Sim_Udisk->0
#define   Sim_Udisk    1  //SD模拟U盘 该宏在usb_test.h中定义

void Simulate_Udisk(void);

#endif /* __USB_TSET_H */
